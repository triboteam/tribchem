from importlib_metadata import entry_points
from tribchem.utils.metadata import get_tribchem_metadata, read
from setuptools import setup


long_description = read('README.md')
metadata = get_tribchem_metadata()

setup(
    name='tribchem',
    py_modules=[],
    version=metadata['version'],
    description='tribchem contains different packages to perform computational physics and data analysis'
                'it offers different complex workflow for tribology and tribochemistry, based on Atomate and FireWorks.',
    entry_points={
        "console_scripts": 
            ['tribchem = tribchem.entrypoints.main:main']
                  },
    long_description=open('README.md').read(),
    url='https://gitlab.com/triboteam/tribchem',
    author=metadata['author'],
    author_email=metadata['email'],
    maintainer=metadata['maintainer'],
    maintainer_email=metadata['email'],
    license=metadata['license'],
    #install_requires=['atomate@git+https://github.com/hackingmaterials/atomate.git',
    #                  'mpinterfaces@git+https://github.com/henniggroup/MPInterfaces.git'],
    classifiers=["Programming Language :: Python :: 3",
                 "Programming Language :: Python :: 3.6",
                 "Programming Language :: Python :: 3.7",
                 'Development Status :: 3 - Alpha',
                 'Intended Audience :: Science/Research',
                 'Intended Audience :: System Administrators',
                 'Operating System :: OS Independent',
                 'Topic :: Other/Nonlisted Topic',
                 'Topic :: Scientific/Engineering'],
)
