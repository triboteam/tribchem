# -*- coding: utf-8 -*-


"""Package tribchem entry point."""


from .entrypoints.main import main

if __name__ == '__main__':
    main()
