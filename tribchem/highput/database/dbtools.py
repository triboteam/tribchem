#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 15:26:19 2021

This module contains functions to handle dictionaries. They are used to read 
fields that are extracted from a database query and to write dictionaries, 
consistently with the update notation of MongoDB, to be stored in a database.
The module also contains tools to convert images and store them to a database.

The module contains the following functions:

    - read_one_entry
    - read_multiple_entry
    - write_one_entry
    - write_multiple_entry
    - image_bytes_converter
    - create_filter
    - prepare_data_to_update

    Author: Gabriele Losi (glosi000)
    Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna


    - convert_dict_to_mongodb
    
    Author: Gabriele Losi (glosi000)
    Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna
    Credits: Code based on StackOverflow, Creative Commons licence 3.0
    https://creativecommons.org/licenses/by-sa/3.0/
    https://stackoverflow.com/questions/29267519/mongodb-update-dictionary-in-document


    - convert_bytes_to_image
    - convert_image_to_bytes

    Author: Gabriele Losi (glosi000)
    Credits: Code based on the original work of Michael Wolloch, TriboFlow package, Wien University
    Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Prof. M.C. Righi, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 15th, 2021'

import io
import os
from PIL import Image
from pathlib import Path, PurePosixPath

import numpy as np
import pandas as pd

from tribchem.highput.utils.errors import (
    ReadParamsError,
    WriteParamsError
)
from pymatgen.core.surface import Structure, Slab
from tribchem.highput.database.navigator import Navigator

# ============================================================================
# Read dictionary from entry
# ============================================================================

def read_one_entry(input_dict, entry):
    """
    Extract subdictionaries or values from an input dictionary by providing a
    list containing the key to be read one after the other.

    Parameters
    ----------
    input_dict : dict
        Input dictionary to be read.

    entry : str or list
        Contains the key(s) that should be read in sequence from the dictionary.
        The keys should be "innested" within the dictionary or an error raises.
        
        Example:
            1.  entry = 'key'
                function returns input_dict[key]

            2.  entry = ['key1', 'key2', 'key3']
                function returns input_dict['key1']['key2']['key3']

    Returns
    -------
    dict
        Subdictionary containing the desired information.

    """

    # If entry is None, then there is no data to be read
    if entry is None:
        info = None

    # Simply read a dictionary key
    if isinstance(entry, str):
        info = input_dict.get(entry, None)
    
    # You can have multiple nested keys
    elif isinstance(entry, list):
        info = input_dict.copy()
        for n in entry:
            if info is not None:
                info = info.get(n, None)  # Read one key after the other

    else:
        ReadParamsError('Error in reading input_dict, entry is wrong.')

    return info

def read_multiple_entry(input_dict, entry):
    """
    Extract multiple values from an input dictionary by providing a list of
    lists where each element contains the key(s) to be read.

    Parameters
    ----------
    input_dict : dict
        Input dictionary to be read.

    entry : str or list or list of lists
        Contains the key(s) that should be read in sequence from the dictionary.
        The keys should be "innested" within the dictionary or an error raises.
        If a list of lists is passed the dictionary is read multiple times.
        
        Example:
            1.  entry = 'key' or ['key1', 'key2']
                function returns:
                    input_dict[key] or input_dict['key1']['key2']

            2.  entry = [['key1', 'key2'], ['key3']]
                function returns:
                    [ 
                        input_dict['key1']['key2'],
                        input_dict['key3']
                    ]

    Returns
    -------
    dict or list of dicts
        Subdictionary containing the desired information.

    """

    # Extract many info at the same time 
    if isinstance(entry, list):
        if all([isinstance(x, list) for x in entry]):
            info = []
            for n in entry:
                info.append(read_one_entry(input_dict, n))

        else:
            info = read_one_entry(input_dict, entry)
    
    else:
        info = read_one_entry(input_dict, entry)
    
    return info


# ============================================================================
# Prepare dictionary with data stored at entry
# ============================================================================

def write_one_entry(data, entry=None, to_mongodb=True):
    """
    Prepare an object by creating a dictionary with nested keys as provided by 
    entry. Useful to create a dictionary to update a Database fields.

    Parameters
    ----------
    data : any python object
        Python object containing the data to be the value of the dictionary.

    entry : str or list or None, optional
        Contains the key(s) to be used to create the dictionary. If a list is
        passed, then the elements of the list will constitute the keys for each
        inner subdictionary that is created. At innermost level is placed data.
        If nothing is passed, data is used as it is. The default is None.

        Example:
            1.  entry = 'key'
                function returns:
                    {'key': data}

            2.  entry = ['key1', 'key2', 'key3']
                function returns:
                    { 'key1':
                        { 'key2':
                            {
                                'key3': data
                            }
                        }
                    }

    to_mongodb : bool, optional
        Decide whether to return a dictionary which is conformal to the queries
        of MongoDB, in order to really update the fields and not override other
        data which could be already in the field.

    Returns
    -------
    dict
        Dictionary containing the data.

    """
    
    # If entry is None, then data is already fine as dictionary
    if entry is None:
        d = data

    # Simply write a dictionary with a single key
    elif isinstance(entry, str):
        d = {entry: data}

    # You can have multiple nested keys
    elif isinstance(entry, list):
        d = {entry[-1]: data}   
        for key in entry[-2::-1]:
            d = {key: d}
    
    else:
        WriteParamsError('Error in writing data, entry is wrong.')

    # Convert the dictionary to suit MongoDB query
    if to_mongodb:
        d = convert_dict_to_mongodb(d)
    
    return d

def write_multiple_entry(data, entry=None, to_mongodb=True):
    """
    Prepare multiple dictionaries containing the passed data. It is a wrapper of
    `write_one_dict` and works with a list of data as entry.

    Parameters
    ----------
    data : list of any python object
        List of Python object containing the value of the dictionaries.

    entry : str or list of lists or None, optional
        Contains the keys to be used to create the dictionaries. If a list of
        lists is passed, then multiple dictionaries are created and returned as
        a list. In that case: `len(data) == len(entry)`, otherwise data will be
        considered as a list object and placed as a single dictionary element.
        If nothing is passed, data is used as it is. The default is None.

        Example:
            1.  entry = 'key'
                function returns:
                    {'key': data}

            2.  entry = [['key1', 'key2'], ['key3']]
                function returns:
                [
                    { 'key1':
                        { 
                            'key2': data[0]
                        }
                    },

                    {
                        'key3': data[1]
                    }
                ]
    
    to_mongodb : bool, optional
        Decide whether to return a dictionary which is conformal to the queries
        of MongoDB, in order to really update the fields and not override other
        data which could be already in the field.

    Returns
    -------
    dict or list of dicts
        Dictionaries containing the data.

    """
    
    # Extract many info at the same time 
    if isinstance(entry, list):

        bool_1 = all([isinstance(n, list) for n in entry])  # All n are lists
        bool_2 = isinstance(data, (list, np.ndarray))  # Data is list-like
        bool_3 = (len(data) == len(entry)) if bool_2 else False  # Same length

        if bool_1 and bool_2 and bool_3:
            d = []
            for i, n in enumerate(entry):
                d.append(write_one_entry(data[i], n, to_mongodb))

        else:
            d = write_one_entry(data, entry, to_mongodb)

    else:
        d = write_one_entry(data, entry, to_mongodb)
    
    return d


# ============================================================================
# Prepare data dictionary to be stored in a Database
# ============================================================================

def create_filter(**kwargs):
    """
    Convert a custom ensemble of key-values pair to a dictionary. Useful to 
    create a filter to query the database without writing a dictionary by hand.

    """    
    return kwargs

def convert_dict_to_mongodb(input_dict):
    """
    Convert a dictionary to be suitable to the update_one method of pymongo.
    This is necessary in order to avoid updating a dictionary by substituting
    the existing one with the new one.

    Parameters
    ----------
    input_dict : dict
        Dictionary to be converted to the MongoDB style to update 

    Returns
    -------
    output_dict : dict
        Output dictionary which is now compliant to MongoDB. In this way you
        can update entries containing dictionaries or nested dictionaries
        without overwriting them.
    
    Examples
    --------
    Normally when you update nested dictionaries you substitute the first one
    with the second, without having a "proper" updating, as you might intend.
    
    >>> dict1 = {'key': {'data_1': 5 }}
    >>> dict2 = {'key': {'data_2': 2 }}
    >>> dict2.update(dict1)
    >>> dict2
    {'key': {'data_1': 5}}
    
    If your intention was to have {'key': {'data_1': 5, 'data_2': 2}} the
    operation failed. The same thing occur when updating a MongoDB entry.
    To really update an existing (nested) dictionary and not overriding it run:

    >>> convert_dict_to_mongodb(dict1)
    {'key.data_1': 5}

    """
    
    output_dict = {}
    
    for key, val in input_dict.items():

        if not isinstance(val, dict):
            output_dict[key] = val
            continue

        for sub_key, sub_val in val.items():
            new_key = '{}.{}'.format(key, sub_key)
            output_dict[new_key] = sub_val
            if not isinstance(sub_val, dict):
                continue

            output_dict.update(convert_dict_to_mongodb(output_dict))
            if new_key in output_dict:
                del output_dict[new_key]

    return output_dict

def get_one_info_from_dict(input_dict, entry):
    """
    Extract subdictionaries or values from an input dictionary by providing a
    list containing the key to be read one after the other.

    Parameters
    ----------
    input_dict : dict
        Input dictionary to be read.

    entry : str or list
        Contains the key(s) that should be read in sequence from the dictionary.
        The keys should be nested within the dictionary or an error raises.
        
        Example:
            1.  entry = 'key'
                function returns input_dict[key]

            2.  entry = ['key1', 'key2', 'key3']
                function returns input_dict['key1']['key2']['key3']

    Returns
    -------
    dict
        Subdictionary containing the desired information.

    """

    # Simply read a dictionary key
    if isinstance(entry, str):
        info = input_dict[entry]
    
    # You can have multiple nested keys
    elif isinstance(entry, list):
        info = input_dict.copy()
        for n in entry:
            info = info[n]  # Read one key after the other

    else:
        ReadParamsError('Error in reading input_dict, entry is wrong.')

    return info

def get_multiple_info_from_dict(input_dict, entry):
    """
    Extract multiple values from an input dictionary by providing a list of
    lists where each element contains the key(s) to be read.

    Parameters
    ----------
    input_dict : dict
        Input dictionary to be read.

    entry : str or list or list of lists
        Contains the key(s) that should be read in sequence from the dictionary.
        The keys should be "innested" within the dictionary or an error raises.
        If a list of lists is passed the dictionary is read multiple times.
        
        Example:
            1.  entry = 'key' or ['key1', 'key2']
                function returns:
                    input_dict[key] or input_dict['key1']['key2']

            2.  entry = [['key1', 'key2'], ['key3']]
                function returns:
                    [ 
                        input_dict['key1']['key2'],
                        input_dict['key3']
                    ]

    Returns
    -------
    dict or list of dicts
        Subdictionary containing the desired information.
    """

    # Extract many info at the same time 
    if isinstance(entry, list):
        if all([isinstance(x, list) for x in entry]):
            info = []
            for n in entry:
                info.append(get_one_info_from_dict(input_dict, n))

        else:
            info = get_one_info_from_dict(input_dict, entry)
    
    else:
        info = get_one_info_from_dict(input_dict, entry)
    
    return info


# ============================================================================
# Retrieve structure and VASP output from DB
# ============================================================================

def retrieve_from_db(mp_id, collection, db_file=None, database=None, 
                     miller=None, entry=None, is_slab=False, pymatgen_obj=False):
    """
    Retrieve data from a selected database and collection. By specifing an entry
    you can extract specific data, even from nested data in the field dictionary.
    If pymatgen_obj is True, it is assumed that the object present in `entry` is
    a pymatgen structure, thus having a `from_dict` method.

    Parameters
    ----------
    mp_id : str
        MP ID from the Materials Project, identify a material.

    collection : str
        Collection in the database to parse through.

    db_file : str or None, optional
        Location of the database. If it is None, it will be searched for a
        'localhost' on the hosting machine. The default is None.

    database : str or None, optional
        Database toquery. The default is None.

    miller : list, optional
        Miller index identifying the orientation of the slab. If it is not 
        None, it can be used as an option as filter (fltr) together with `mp_id`.
        The default is None.

    entry : str or list or None, optional
        Key or list of keys to be used to extract a piece of information from 
        the `vasp_calc` dictionary. The default is None.

    is_slab : bool, optional
        Recognize the type of structure to convert a Structure or Slab
        dictionary back to a pymatgen object, by applying the `.from_dict`
        method. Meaningful only when `pymatgen_obj=True`. The default is False.

    pymatgen_obj : bool, optional
        Decide to return a pymatgen object or a dictionary. The default is 
        True.

    Returns
    -------
    structure : any python object
        Data extracted from the field of the database. It can be a pymatgen
        structure.

    """

    # Call the navigator for retrieving structure
    nav = Navigator(db_file=db_file, high_level=database)
    
    # Define the filter (fltr) to be used
    fltr = {'mpid': mp_id}
    if miller is not None:
        fltr.update({'miller': miller})
    
    # Extract data from the database
    field = nav.find_data(collection=collection, fltr=fltr)
    
    structure = None
    if field is not None and entry is not None:
        try:
            structure = get_one_info_from_dict(field, entry)
        except:
            structure = None

        if pymatgen_obj and structure is not None:
            func = Slab if is_slab else Structure
            structure = func.from_dict(structure)

    return field, structure

def save_calctags(tag, collection, formula=None, mpid=None, miller=None, 
                  name=None, db_file=None, database='tribchem'):
    """
    Store in a csv file the tags of a calculation which was succesfully done by
    vasp. Useful to retrieve later the tags in order to have a complete access
    to the results data stored in the low level datababase.

    """
    
    project_folder = os.path.dirname(__file__)
    # Check the folder containing calculation tags, if not present create it
    folder_object = PurePosixPath(project_folder)
    folder = str(folder_object.parent.parent.parent) + '/results/'
    path = Path(folder)
    if not path.is_dir():
        print("WARNING: There is no folder for calculation tags.")
        print("Creating a new mp_structures folder in " + folder)
        folder = PurePosixPath(folder)
        os.mkdir(folder)
        path = Path(folder)
        if not path.is_dir():
            raise RuntimeError('The creation of struct path has failed!')
            
    # Create the path to the csv file
    path = str(path)
    csv_file = path + '/calc_tags.csv'
    columns = ['tag', 'formula', 'mpid', 'miller', 'name',
               'db_file', 'database', 'collection']

    # Create the new row for the Dataframe 
    df_new = pd.DataFrame([[tag, formula, mpid, miller, name, db_file, 
                           database, collection]], columns=columns)
    
    # Check if the csv table does exist, if not create it, else append df_new
    if not os.path.exists(csv_file):
        df_new.to_csv(csv_file, index=False)
    else:
        df = pd.read_csv(csv_file)
        df = df.append(df_new)
        df.to_csv(csv_file, index=False)

# ============================================================================
# Converter from images to bytes and viceversa
# ============================================================================

def convert_bytes_to_image(bytes_object):
    """
    Convert an image saved as bytes for storing in MongoDB to an PIL image.

    Parameters
    ----------
    bytes_object : bytes
        Image converted to bytes for storage in a MongoDB database.

    Returns
    -------
    pil_img : PIL.PngImagePlugin.PngImageFile
        Image in python image library format ready to be viewed or saved.

    """
    
    pil_img = Image.open(io.BytesIO(bytes_object))
    return pil_img

def convert_image_to_bytes(path_to_fig):
    """
    Convert an image to bytes for storage in MongoDB database.

    Parameters
    ----------
    path_to_fig : Str
        Path to the input figure.

    Returns
    -------
    bytes
        image encoded in bytes ready for storage in MongoDB.

    """

    im = Image.open(path_to_fig)
    image_bytes = io.BytesIO()
    im.save(image_bytes, format='png')
    
    return image_bytes.getvalue()

def image_bytes_converter(data, to_image=True):
    """
    Convert an image to a bytes-object or a bytes-object to a PIL image. 
    Useful for storing data in MongoDB database.

    Parameters
    ----------
    data : bytes or str
        bytes-object or a path to an image.
        
    to_image : bool, optional
        Decide whether to convert to a PIL image or bytes. The default is True.

    Returns
    -------
    data_conv : PIL.PngImagePlugin.PngImageFile or bytes
        Converted data type

    """
    
    # We have bytes and convert to image
    if to_image:
        data_conv = convert_bytes_to_image(data)
      
    # We have (a path to an) image and convert to bytes
    else:
        data_conv = convert_image_to_bytes(data)
            
    return data_conv
