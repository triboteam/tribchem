#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 10:36:11 2021

In this class are contained several functions which manage the output files 
produced by VASP.

Author: Omar Chehaimi (@omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Prof. M.C. Righi, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'April 29th, 2021'

import os
from distutils.dir_util import copy_tree, remove_tree
from pathlib import PurePosixPath, Path
from datetime import datetime

from tribchem.highput.database.navigator import Navigator
from tribchem.core.logging import LoggingBase

currentdir = os.path.dirname(__file__)

# Logging
configurations = LoggingBase.get_config()
logging_path = LoggingBase.logging_path()
log = LoggingBase(
    name='filetools',
    console_level=configurations['logging']['file_tools_logging_level'],
    path=logging_path+'/filetools.log',
    file_level=configurations['logging']['file_tools_logging_level'])

class FileTools:
    """
    In this class are contained several functions which manage the output files 
    produced by VASP. FireWorks saves the output files in folders which are
    automatically created during the execution. This behavior makes hard to 
    retrive and read the output files. For this reason the following class is 
    written.
    
    Methods
    -------
    move_files(calc_tag, loc) (static method)
        Move all the files related to a particular VASP calculation by using the
        information saved in the 'tasks' table.
    
    check_folder(fol_path) (static method)
        Check if a folder exists given its path. If not a new folder will be 
        created.
    
    fol_name(mid) (static method)
        Generate the name of the folder by using the notation: 
            mid_day_month_year_hour_minute_second.

    """
    
    @staticmethod
    def move_files(nav, main_folder, task_label, custom_name=None):
        """
        Move all the files related to a particular VASP calculation by using the
        information saved in the 'tasks' table and remove the source folders.
        
        Parameters
        ----------
        nav : Navigator
            Instance of the Navigator class.
        
        main_folder : str
            Path of the main folder.
            
        task_label : str
            Tag of the calculation.
            
        custom_name : str, optional
            Name of the folder.
        
        """
        
        if not isinstance(nav, Navigator):
            raise ValueError("nav parameter has to be an instance of" 
                             " Navigator. \n")
        
        main_folder_dir = Path(main_folder)
        if not main_folder_dir.is_dir():
            raise ValueError("The main folder does not point to a folder."
                             " Please check in 'FT_StartKPointsConvo' why the "
                             " main folder is not correctly created.")
        
        path = nav.find_data(
            collection='tasks',
            fltr={'task_label': task_label})

        if not path:
            log.warning("There is no calculation folder for the task label {}.".format(task_label))
        else:
            src_path = path['dir_name']
            # Remove the name of the machine before the folder
            src_path = src_path.split(":",1)[1]
            
            if custom_name:
                calculations_folder = str(main_folder_dir) + '/' + custom_name
                calculations_folder = calculations_folder.replace(' ', '_')
            else:
                calc_folder_name = path['task_label'].replace(' ', '_')
                calculations_folder = str(main_folder_dir) + '/' + calc_folder_name
            
            calculations_path = FileTools.check_folder(fol_path=calculations_folder)
            
            # Moving all the VASP files and removing the source
            log.warning("Moving all files from {} to {}.".format(src_path, calculations_path))
            copy_tree(src=src_path, dst=calculations_path)
            #remove_tree(directory=src_path)

    @staticmethod
    def check_folder(fol_path):
        """
        Check if a folder exists given its path. If not a new folder will be 
        created.
        
        Parameters
        ----------
        fol_path : str
            Folder path.
        
        Return
        ------
        fol_path : str
            Folder path.
                        
        """
        
        fol_dir = Path(fol_path)

        if not fol_dir.is_dir():
            print("WARNING: There is no folder as the one passed as input.")
            print("Creating the new folder in " + fol_path)
            fol_dir = PurePosixPath(fol_path)
            os.makedirs(fol_dir, exist_ok=True)
            fol_path = Path(fol_dir)
            if not fol_path.is_dir():
                raise RuntimeError('The creation of the folder has failed!')
        
        fol_path = str(fol_dir)
        
        return fol_path
    
    @staticmethod
    def add_datetime(folder):
        """
        Add datetime to the folder name provided by following the notation: 
        mid_day_month_year_hour_minute_second, e.g. folder + '30_04_2021_14_14_35'.

        Parameters
        ----------
        folder : str
            Path to the folder, the datetime will be add at the string end.

        Return
        ------
        name : str
            Name of the folder.

        """

        if not isinstance(folder, str):
            raise ValueError("{} is not a string. Please provide a string".format(folder))

        now = datetime.now()
        now = now.strftime("%d_%m_%Y_%H_%M_%S")
        name = folder + '_' + now

        return name

    @staticmethod
    def copy_general_file(folder_from, folder_to, all=True):
        """
        Copy one file or all the files from a folder to another.
        
        Paramaters
        ----------
        folder_from : str
            Folder where the output files are saved.
        
        folder_to : str
            Folder where to save the output files.
        
        all : bool, by default set to True
            If False only the file specified in the name of folder_from is 
            copied.
        
        """
        
        path_from = Path(folder_from)
        path_to = Path(folder_to)
            
        if not path_from.is_dir() or not path_to.is_dir():
            raise RuntimeError("{} or {} does not exist. Please provide "
                               "the correct location for the folders.".format(folder_from, folder_to))
        
        if all:
            try:
                log.warning("Moving all files from {} to {}.".format(folder_from, folder_to))
                copy_tree(src=folder_from, dst=folder_to)
                remove_tree(directory=folder_from)
            except:
                raise RuntimeError("The files {} do not exist.")
        else:
            try:
                log.warning("Moving the file {} to {}.".format(folder_from, folder_to))
                copy_tree(src=folder_from, dst=folder_to)
                remove_tree(directory=folder_from)
            except:
                raise RuntimeError("The file {} does not exist.")
