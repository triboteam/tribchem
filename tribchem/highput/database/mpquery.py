#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 09:20:33 2021

Functions to query online databases to retrieve structures and material data.

@author: glosi000
"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'March 1st, 2021'

from tribchem.highput.database.navigator import NavigatorMP
from tribchem.highput.utils.errors import NavigatorMPError


# ============================================================================
# Update data in a Database field
# ============================================================================

def query_mp(formula=None, mp_id=None, print_info=False):
    """
    Basic function to retrieve a structure from the Materials Project online
    database. The query priority is given to `mp_id`, if it is None, then the
    function will retrieve the lower energy structure with a given chemical
    formula.

    Parameters
    ----------
        chem_formula : str or None, optional
            Chemical formula of the structure, e.g.: 'NaCl', 'Fe2O3', 'SiO'.

        mp_id : str or None, optional
            Materials Project ID of the structure, e.g.: 'mp-124'. 
            The default is None.
 
        print_info : bool, optional
            Whether to print information about the collected structure. 
            The default is False.

    Returns
    -------
        struct : pymatgen.core.structure.Structure
            Tuple containing several information about the desired structure.

        mp_id : str
            Materials Project ID for the given chemical formula.

    """

    nav_mp = NavigatorMP()

    # Retrieve a material by mp ID
    if mp_id is not None:
        struct, mp_id = nav_mp.get_low_energy_structure(mp_id=mp_id,
                                                        print_info=print_info)

    # Retrieve a material by its chemical formula, with lower energy
    elif formula is not None:
        struct, mp_id = nav_mp.get_low_energy_structure(chem_formula=formula,
                                                        print_info=print_info)

    else:
        NavigatorMPError("Error in querying the MP online database. Either one "
                         "among formula and mp_id should not be None.")
    
    return struct, mp_id
