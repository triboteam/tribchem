#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 16:39:00 2021

Test the if the moving of the vasp files is correctly done by executing the test
fot the k-points convergence.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'April 29th, 2021'

from fireworks import LaunchPad
from fireworks.core.rocket_launcher import rapidfire
from fireworks import Firework, Workflow

from tribchem.highput.database.navigator import NavigatorMP, StructureNavigator
from tribchem.highput.firetasks.run import FT_StartKPointsConvo

# db_file = '/home/gl/Work/WORKFLOW/config/db.json' # Put here your local db
nav_mp = NavigatorMP()
nav_structure = StructureNavigator(high_level='tribchem')

structure, mp_id = nav_mp.get_low_energy_structure(chem_formula='Cu', 
                                                   mp_id='mp-30')
nav_structure.add_bulk_to_db(structure=structure, mid=mp_id, 
                             functional='PBE')

FT_KPts = FT_StartKPointsConvo(mid='mp-30', functional='PBE')
FW_KPts = Firework([FT_KPts], name='Test FT_StartKPointsConvo')
WF_KPts = Workflow([FW_KPts], name='Test FT_StartKPointsConvo')

lpad = LaunchPad.auto_load()
lpad.add_wf(WF_KPts)
#rapidfire(lpad)