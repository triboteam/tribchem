#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 20 15:16:55 2021

In the following class are implemented all the functions necessary to manipulate
the materials project ids. 

@author: omarchehaimi
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'May 20th, 2021'

import os
import json

currentdir = os.path.dirname(__file__)

class Mid:
    """
    This class implements some useful methods for dealing with the creation and
    manipulation of the list of the materials projet ids.
    
    Methods
    -------
    __check_missing(elem)
        Check if the given element has the mp_id missing.

    read_json(name)
        Read the specified json file from highput/database/mid/.

    get_mid_chem(name, fltr=False, missing=False)
        Get the materials project ids from the json saved in 
        highput/database/mid/.
        
     extract_mid_chem(mjson, missing, fltr):
        Extract the materials project ids and the chemical formula.

    """
    
    @staticmethod
    def check_missing(elem):
        """
        Check if the given element has the mp_id missing.
        
        Parameters
        ----------
        elem : dict
            Element of the 'elements.json'.

        Return
        ------
        Raise an error in case the element has a missing mp_id.

        """
        
        if elem['mp_id'] is None:
            raise ValueError("There are some missing ids. Please check and " 
                             "solve the missing values.")

    @staticmethod
    def read_json(name):
        """
        Read the specified json file from highput/database/mid/.
        
        Parameters
        ----------
        name : str
            Name of the json file containing the materials project ids.
            
        Return
        ------
        mid : dict
            The json file as a python dictionary.
        
        """
        with open(currentdir+'/mid/'+name, 'r') as midf:
            mid = json.load(midf)
        
        return mid

    @staticmethod
    def get_mid_chem(name, fltr=False, missing=False):
        """
        Get the materials project ids and the relative chemical formula from the 
        json saved in highput/database/mid/. The returned arrays have consistent
        positions. e.g.: the first element of the materials project ids 
        corresponds to the first element of the chemical formula array, and so on.
        
        Parameters
        ----------
        name : str
            Name of the json file containing the materials project ids.
        
        fltr : dict, optional
            Filter for extracting the element with given property.
            e.g.: If fltr={'crystal_system': 'cubic'} all the elements with that 
            property will be selected.
            
        missing : bool, optional
            By default this flag is set to False. When True raise an error in
            case one or more materials project ids are missing.
        
        Return
        ------
        mid : list
            The list containing all the materials project ids.
            e.g.: ['mp-30', 'mp-134']
        
        chem : list
            The list containing all the chemical formulas.
            e.g.: ['Cu', 'Al']

        """

        mjson = Mid.read_json(name)

        mid, chem = Mid.extract_mid_chem(mjson, missing, fltr)
        
        return mid, chem

    @staticmethod
    def extract_mid_chem(mjson, missing=False, fltr=False):
        """
        Extract the materials project ids and the chemical formula.
        
        Parameters
        ----------
        mjson : list
            List containing all the data in the 'elements.json' file.
        
        missing : bool, optional
            If it is allowed to have some missing materials project ids or not.
        
        fltr : bool, optional
            The filter to select a given property from the elements list.
        """
        
        mid = []
        chem = []
        
        if fltr:
            av_prop = ["crystal_system", "space_group", "point_group"]
            prop = list(fltr.keys())[0]
            # Check if the passed properties are correct
            if prop not in av_prop:
                raise ValueError("{} is not an available property. The "
                                 "availabele properties are: crystal_system,"
                                 " space_group, point_group.".format(prop))
            # Using two different for loops to avoid to check every time the missing flag
            if missing:
                for elem in mjson:
                    Mid.check_missing(elem)
                    if elem[prop] == fltr[prop]:
                        mid.append(elem['mp_id'])
                        chem.append(elem['element'])
            else:
                for elem in mjson:
                    if elem['mp_id'] is None:
                        continue
                    if elem[prop] == fltr[prop]:
                        mid.append(elem['mp_id'])
                        chem.append(elem['element'])
        else:
            # Using two different for loops to avoid to check every time the missing flag
            if missing:
                for elem in mjson:
                    Mid.check_missing(elem)
                    mid.append(elem['mp_id'])
                    chem.append(elem['element'])
            else:
                for elem in mjson:
                    if elem['mp_id'] is None:
                        continue
                    mid.append(elem['mp_id'])
                    chem.append(elem['element'])
        
        return mid, chem
            