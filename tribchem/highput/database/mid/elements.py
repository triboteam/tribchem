#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 26 11:21:00 2021

Just a small script for getting the crystal system of each element.

Author: Omar Chehaimi (@omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

import os

from tribchem.highput.database.navigator import NavigatorMP, use_legacy_matproj
from tribchem.highput.database.mid import Mid

mids, _ = Mid.get_mid_chem(name='elements.json')

nav_mp = NavigatorMP()
for mid in mids:
    if use_legacy_matproj():
        csys = nav_mp.get_property_from_mp(mp_id=mid, properties=['symmetry'])
        print(mid, csys.symmetry.crystal_system,
                   csys.symmetry.symbol,
                   csys.symmetry.point_group)
    else:
        csys = nav_mp.get_property_from_mp(mp_id=mid, properties=['spacegroup'])
    print(mid, csys['spacegroup']['crystal_system'], 
               csys['spacegroup']['symbol'],
               csys['spacegroup']['point_group'])

