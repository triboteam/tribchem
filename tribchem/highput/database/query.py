#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 08:53:10 2021

Functions and utilities to query database with results, based on Navigator cls.

The module contains the following functions:

    - query_field
    - query_field_entry
    - query_structure
    - query_tag
    - update_field
    - update_tag
    - prepare_data_to_update
    - insert_field

    Author: Gabriele Losi (glosi000)
    Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 26th, 2021'

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.database.dbtools import (
    read_multiple_entry,
    write_multiple_entry,
    create_filter
)
from tribchem.highput.utils.tasktools import select_struct_func
from tribchem.highput.utils.errors import NavigatorError


# GLOBAL VARIABLES
# Update operators used to update data in a MongoDB database.
field_update_op = ['$currentDate', '$inc', '$min', '$max', '$mul', 
                   '$rename', '$set', '$setOnInsert', '$unset']
array_update_op = ['$', '$[]', '$[<identifier>]', '$addToSet', '$pop', 
                   '$pull', '$push', '$pullAll', '$each', '$position',
                   '$slice', '$sort']
bitwise_update_op = ['$bit']


# ============================================================================
# Collect data from a Database
# ============================================================================

def query_field(fltr, collection, db_file=None, database=None):
    """
    General query to retrieve a field dictionary from a MongoDB database.

    Parameters
    ----------
    fltr : dict
        Dictionary containing the filter used to match the data of interest.

    collection : str
        Collection in the database where the field is located.

    db_file :
        Path to the database folder on your machine. If None is passed, it will
        search for a local db_file by means of env_check (Atomate function).
        The default is None.
    
    database : str or None, optional
        Name of the database table. If None is passed, the FireWorks database
        is selected. The default is None.

    Returns
    -------
    field : dict or None
        The dictionary field extracted from the database.

    """

    # Initialize the Navigator
    nav = Navigator(db_file=db_file, high_level=database)
    
    # Retrieve the dictionary field from the database
    field = nav.find_data(collection=collection, fltr=fltr)

    return field

def query_field_entry(fltr, collection, entry, db_file=None, database=None):
    """
    Retrieve a field dictionary from a MongoDB database and try to read an
    entry out of it.

    Parameters
    ----------
    fltr : dict
        Dictionary containing the filter used to match the data of interest.

    collection : str
        Collection in the database where the field is located.
    
    entry : str or list or list of lists
        Key(s) used to read the field dictionary extracted from the database. 
        If a str or list is provided, a key or a nested set of keys is read 
        from the field. If a list of lists is provided, a list of many objects
        is extracted from the field.

    db_file :
        Path to the database folder on your machine. If None is passed, it will
        search for a local db_file by means of env_check (Atomate function).
        The default is None.
    
    database : str or None, optional
        Name of the database table. If None is passed, the FireWorks database
        is selected. The default is None.

    Returns
    -------
    field : dict or None
        The dictionary field extracted from the database.

    field_entry : dict or list of dicts or None
        The dictionary field extracted from the database and read for entries.

    """
    
    # Query database to retrieve structure and read nested keys
    field = query_field(fltr, collection, db_file, database)

    if field is not None:
        field_entry = read_multiple_entry(field, entry)
    else:
        field_entry = None

    return field, field_entry

def query_structure(fltr, collection, db_file=None, database=None, entry=None):
    """
    Query a pymatgen structure out of a given database and entry.

    Parameters
    ----------
    fltr : dict
        Dictionary containing the filter used to match the data of interest.

    collection : str
        Collection in the database where the field is located.

    db_file :
        Path to the database folder on your machine. If None is passed, it will
        search for a local db_file by means of env_check (Atomate function).
        The default is None.
    
    database : str or None, optional
        Name of the database table. If None is passed, the FireWorks database
        is selected. The default is None.
    
    entry : str or list or list of lists, optional
        Key(s) used to read the field dictionary extracted from the database. 
        If a str or list is provided, a key or a nested set of keys is read 
        from the field. If a list of lists is provided, a list of many objects
        is extracted from the field. If None is passed, it is assumed the 
        pymatgen structure to occupy entirely the field. The default is None.

    Returns
    -------
    structure : (list of) pymatgen structure(s) or None
        Materials structures converted to pymatgen objects.

    Raises
    ------
    NavigatorError
        If the object extracted from field is not a pymatgen structure dict.

    """
    
    # Initialize default value for pymatgen structure object
    structure = None

    # Query the database to extract the field
    field = query_field(fltr, collection, db_file, database)
    
    # Find structure dictionary and generate the pymatgen object
    if entry is not None:
        field_entry = read_multiple_entry(field, entry)
    else:
        field_entry = field

    if field_entry is not None:
        try: 
            func = select_struct_func(field_entry['@class'])
        except: 
            raise NavigatorError('Input entry not pointing to pymatgen structure')

        if isinstance(field_entry, list):
            structure = [func.from_dict(value) for value in field_entry]
        else:
            structure = func.from_dict(structure)
    
    return structure

def query_tag(collection, db_file=None, database=None, entry=None, **kwargs):
    """
    Query the datababase with a filter which is automatically created from 
    kwargs. It returns the field and the objects contained in a specific
    (list of) entry, if provided. It is a generalization of `query_field_entry`.

    Parameters
    ----------
    collection : str
        Collection in the database where the field is located.

    db_file :
        Path to the database folder on your machine. If None is passed, it will
        search for a local db_file by means of env_check (Atomate function).
        The default is None.
    
    database : str or None, optional
        Name of the database table. If None is passed, the FireWorks database
        is selected. The default is None.
    
    entry : str or list or list of lists or None, optional
        Key(s) used to read the field dictionary extracted from the database. 
        If a str or list is provided, a key or a nested set of keys is read 
        from the field. If a list of lists is provided, a list of many objects
        is extracted from the field. The default is None.

    Returns
    -------
    field : dict or None
        The dictionary field extracted from the database.

    field_entry : dict or list of dicts or None
        The dictionary field extracted from the database and read for entries.

    """
    
    # Create a custom field from kwargs and query the database
    fltr = create_filter(**kwargs)
    field = query_field(fltr, collection, db_file, database)
    
    # Read entry(s) from the field dictionary
    field_entry = None
    if entry is not None:
        field_entry = read_multiple_entry(field, entry)
    
    return field, field_entry


# ============================================================================
# Update data in a Database field
# ============================================================================

def update_field(data, fltr, collection, entry=None, db_file=None, database=None,
                 update_op='$set', upsert=False, to_mongodb=True):
    """
    Update data in the matching field. Data is prepared to be stored in
    database creating a dictionary coherent to MongoDB guidelines. This process
    is controlled by `to_mongodb` parameter and it is necessary to avoiding 
    removing other data present in the field that are not updated.
    The filter is used to match the exact field entry to be update.

    Parameters
    ----------
    data : (list of) python object
        Data to be stored in the database. If a list of objects is provided,
        a dictionary containing different entries is created.

    fltr : dict
        Dictionary containing the filter used to match the data of interest.

    collection : str
        Collection in the database where the field is located.

    entry : str or list or list of lists or None, optional
        Key(s) used to crate a dictionary, conformal to MongoDB, to update a
        field in a database. The default is None.

    db_file :
        Path to the database folder on your machine. If None is passed, it will
        search for a local db_file by means of env_check (Atomate function).
        The default is None.

    database : str or None, optional
        Name of the database table. If None is passed, the FireWorks database
        is selected. The default is None.

    update_op : TYPE, optional
        Update operation to be performed on the database, e.g. '$set', '$inc'. 
        See allowed valued for more information. The default is '$set'.

    upsert : bool
        PyMongo parameter for the update_one function. If True update_one 
        performs an insertion if no documents match the filter.

    to_mongodb : bool, optional
        Decide whether to return a dictionary which is conformal to the queries
        of MongoDB, in order to really update the fields and not 
        overwrite dictionaries.

    """

    # Initialize the Navigator and prepare the data to be updated
    nav = Navigator(db_file, database)
    update_dict = prepare_data_to_update(data, entry, update_op, to_mongodb)

    # Retrieve the dictionary field from the database
    if update_dict is not None:

        # Check if the data is already present in the DB, IF NOT MAKE AN INSERT
        document = query_field(fltr, collection, db_file, database)

        # Manage the update_dict converting it to dictionary
        if not isinstance(update_dict, list):
            update_dict = [update_dict]

        if document is not None:
            # Manage the case of a multiple update of data
            if not isinstance(update_dict, list):
                update_dict = [update_dict]
            if not isinstance(fltr, list):
                fltr = [fltr] * len(update_dict)
            else:
                if len(fltr) != len(update_dict):
                    raise NavigatorError('Wrong input arguments: update_dict, fltr. '
                                         'If lists, they must have the same length.')

            for value, f in zip(update_dict, fltr):
                nav.update_data(collection, f, value, upsert)

        else:
            # Insert a new document in the database
            update_dict = write_multiple_entry(data, entry, False)
            if not isinstance(update_dict, list):
                update_dict.update(fltr)
                update_dict = [update_dict]

            for value in update_dict:
                nav.insert_data(collection, value)

def update_tag(data, collection, entry=None, db_file=None, database=None, 
               update_op='$set', upsert=False, to_mongodb=True, **kwargs):
    """
    It behaves exactly as `update_field`, the only difference is that here the
    filter is defined in a custom way by key-value pair passed as kwargs.

    Parameters
    ----------
    data : (list of) python object
        Data to be stored in the database. If a list of objects is provided,
        a dictionary containing different entries is created.

    collection : str, optional
        Collection in the database where the field is located.

    entry : str or list or list of lists
        Key(s) used to crate a dictionary, conformal to MongoDB, to update a
        field in a database.

    db_file :
        Path to the database folder on your machine. If None is passed, it will
        search for a local db_file by means of env_check (Atomate function).
        The default is None.

    database : str or None, optional
        Name of the database table. If None is passed, the FireWorks database
        is selected. The default is None.

    update_op : TYPE, optional
        Update operation to be performed on the database, e.g. '$set', '$inc'. 
        See allowed valued for more information. The default is '$set'.

    upsert : bool
        PyMongo parameter for the update_one function. If True update_one 
        performs an insertion if no documents match the filter.

    to_mongodb : bool, optional
        Decide whether to return a dictionary which is conformal to the queries
        of MongoDB, in order to really update the fields and not 
        overwrite dictionaries.

    """

    # Create the custom filter from kwargs and update data
    fltr = create_filter(**kwargs)
    update_field(data, fltr, collection, db_file, database, upsert, to_mongodb)

def prepare_data_to_update(data, entry=None, update_op='$set', to_mongodb=True):
    """
    Create a dictionary containing the values to be used to update a field in
    a given database.

    Parameters
    ----------
    data : (list of) python object
        Data to be stored in the database. If a list of objects is provided,
        a dictionary containing different entries is created.

    entry : str or list or list of lists
        Key(s) used to crate a dictionary, conformal to MongoDB, to update a
        field in a database.

    to_mongodb : bool, optional
        Decide whether to return a dictionary which is conformal to the queries
        of MongoDB, in order to really update the fields and not 
        overwrite dictionaries.

    update_op : TYPE, optional
        Update operation to be performed on the database, e.g. '$set', '$inc'. 
        See allowed valued for more information. The default is '$set'.

    Raises
    ------
    NavigatorError
        If an illegal operation for MongoDB is passed by input argument.

    Returns
    -------
    update_dict : dict
        Update dictionary, already set and ready to be put 

    """

    # Check if the operation on the database does exist
    allowed_values = field_update_op + array_update_op + bitwise_update_op
    if update_op not in allowed_values:
        raise NavigatorError("Illegal operation to update database. Allowed "
                             "arguments: {}".format(allowed_values))

    # Prepare the dictionary containing the data
    data_dict = write_multiple_entry(data, entry, to_mongodb)

    if isinstance(data_dict, list):
        update_dict = [{update_op: d} for d in data_dict]
    else:
        update_dict = {update_op: data_dict}

    return update_dict


# ============================================================================
# Insert data in a Database
# ============================================================================

def insert_field(field, collection, db_file=None, database=None, 
                 duplicates=False, message=None):
    """
    Insert a field within a specific database collection. Duplicates determine
    whether to add a field to the database if another one with the same values
    is found.

    Parameters
    ----------
    field : dict or list of dicts
        Object(s) to be saved as field(s) in the database.

    collection : str
        Collection in the database where the field should be located.

    db_file :
        Path to the database folder on your machine. If None is passed, it will
        search for a local db_file by means of env_check (Atomate function).
        The default is None.

    database : str or None, optional
        Name of the database table. If None is passed, the FireWorks database
        is selected. The default is None.

    duplicates : bool, optional
        If True data is saved in database even if there are duplicates.
    
    message : str, optional
        Custom message to write in the console and/or in the log file.

    """
    
    # Initialize the Navigator and prepare the data to be updated
    nav = Navigator(db_file, database)

    # Loop over data values and insert them in database
    if not isinstance(field, list):
        field = [field]    
    for value in field:
        nav.insert_data(collection, value, duplicates, message)
