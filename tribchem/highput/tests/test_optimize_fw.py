#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pymatgen.core.structure import Structure
from pymatgen.core.surface import SlabGenerator
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire
from atomate.vasp.fireworks.core import OptimizeFW
from tribchem.highput.database.navigator import Navigator
from tribchem.highput.fireworks.common import check_inputs_fw
from tribchem.highput.utils.vasp_tools import get_custom_vasp_relax_settings

db_file = '/home/gl/Work/WORKFLOW/config/db.json' # Put here your local db

# Initialize Navigator object
nav = Navigator(db_file)
data = nav.find_data('PBE', {'mpid': 'mp-81'})
miller = [1, 1, 1]
min_thickness = 10
min_vacuum = 20

comp_params = data['comp_params']
comp_params['kdens'] = 1000

prim_bulk = Structure.from_dict(data['structure_equiVol'])
conv_bulk = SpacegroupAnalyzer(prim_bulk).get_conventional_standard_structure()
        
SG = SlabGenerator(initial_structure = conv_bulk,
                   miller_index = miller,
                   min_slab_size = min_thickness,
                   min_vacuum_size = min_vacuum)
slab = SG.get_slabs(bonds=None, ftol=0.1, tol=0.1, max_broken_bonds=0,
                    symmetrize=False, repair=False)[0]

vis = get_custom_vasp_relax_settings(slab, comp_params, 'slab_pos_relax')

Optimize_FW = OptimizeFW(slab, name='Test_relaxation',
                         vasp_input_set = vis,
                         half_kpts_first_relax = True)
WF = Workflow([Optimize_FW], name='OptimizeFW WF')

lpad = LaunchPad.auto_load()
lpad.add_wf(WF)
rapidfire(lpad)
