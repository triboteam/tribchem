#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire
from tribchem.highput.workflows.tribology import heterogenous_wf

inputs = {'material_1': {'formula': 'Pt',
                         'miller': '111',
                         'min_vacuum': 35,
                         'min_thickness': 10
                         },
          'material_2': {'formula': 'Ag',
                         'miller': '111',
                         'mpid': 'mp-124',
                         'min_vacuum': 35,
                         'min_thickness': 10
                         },
          'computational_params':{'functional': 'PBE',
                                  'energy_tol': 0.001,
                                  'volume_tol': 0.001,
                                  'bm_tol': 0.01,
                                  'use_vdw': 'False'},
          'interface_params':{'interface_distance': 2.5,
                              'max_area': 500,
                              'max_area_ratio_tol': 0.05
                              }
          }


WF = heterogenous_wf(inputs)

lpad = LaunchPad.auto_load()
lpad.add_wf(WF)
#rapidfire(lpad)
