#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from fireworks import LaunchPad
from fireworks.core.rocket_launcher import rapidfire
from tribchem.highput.fireworks.subworkflows import calc_pes_swf

db_file = '/home/gl/Work/WORKFLOW/config/db.json' # Put here your local db

WF = calc_pes_swf(interface_name='C001_Ni111_mp-23_mp-48',
                  functional='PBE')


lpad = LaunchPad.auto_load()
lpad.add_wf(WF)
#rapidfire(lpad)