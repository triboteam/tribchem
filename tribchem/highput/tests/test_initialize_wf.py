#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire
from tribchem.highput.fireworks.common import check_inputs_fw

inputs = {'material_1': {'formula': 'C',
                         'miller': '111',
                         'mpid': 'mp-66',
                         'min_vacuum': 25,
                         'min_thickness': 5
                         },
          'material_2': {'formula': 'Au',
                         #'mpid': 'mp-13',
                         'miller': '001',
                         'min_vacuum': 25,
                         'min_thickness': 6
                         },
          'computational_params':{'functional': 'SCAN',
                                  'energy_tol': 0.001,
                                  'volume_tol': 0.001,
                                  'bm_tol': 0.01,
                                  'use_vdw': 'False'},
          'interface_params':{'interface_distance': 2.5,
                              'max_area': 500,
                              'max_area_ratio_tol': 0.05
                              }
          }

Start_WF_FW = check_inputs_fw(mat1_params = inputs['material_1'],
                            mat2_params = inputs['material_2'],
                            compparams = inputs['computational_params'],
                            interface_params = inputs['interface_params'],
                            FW_name = 'Check input parameters FW')

WF = Workflow.from_Firework(Start_WF_FW, name='Test Workflow')

lpad = LaunchPad.auto_load()
lpad.add_wf(WF)
rapidfire(lpad)
