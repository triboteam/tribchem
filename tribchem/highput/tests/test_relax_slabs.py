#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from fireworks import Workflow, LaunchPad
from fireworks.core.rocket_launcher import rapidfire
from tribchem.highput.fireworks.subworkflows import make_relax_slab_swf

db_file = '/home/gl/Work/WORKFLOW/config/db.json' # Put here your local db
mp_id = 'mp-81'
functional = 'PBE'
miller = [1, 1, 1]
min_thickness = 10
min_vacuum = 20

WF = make_relax_slab_swf(mp_id, miller, functional)

lpad = LaunchPad.auto_load()
lpad.add_wf(WF)
#rapidfire(lpad)
