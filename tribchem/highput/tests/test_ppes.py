#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from fireworks import LaunchPad
from fireworks.core.rocket_launcher import rapidfire
from tribchem.highput.fireworks.subworkflows import calc_ppes_swf

db_file = '/home/gl/Work/WORKFLOW/config/db.json' # Put here your local db

WF = calc_ppes_swf(interface_name='C001_Ni111_mp-23_mp-48',
                   functional='PBE',
                   structure_name='unrelaxed_structure',
                   out_name='test_PPES',
                   distance_list=[-0.5, -0.25, 0.0, 0.25, 0.5, 1.0, 3.0, 10.0])


lpad = LaunchPad.auto_load()
lpad.add_wf(WF)
#rapidfire(lpad)
