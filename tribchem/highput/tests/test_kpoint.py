#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 09:00:00 2021

Test the workflow to converge the k-points of a bulk.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'April 22nd, 2021'

from pymatgen.core.structure import Structure
from fireworks import LaunchPad
from fireworks.core.rocket_launcher import rapidfire
from fireworks import Firework, Workflow

from tribchem.highput.database.navigator import (
    NavigatorMP, StructureNavigator, Navigator)
from tribchem.highput.firetasks.run import FT_StartKpointsConvo

# db_file = '/home/gl/Work/WORKFLOW/config/db.json' # Put here your local db
nav_mp = NavigatorMP()
nav_structure = StructureNavigator(high_level='tribchem')
nav = Navigator(high_level='tribchem')

structure, mp_id = nav_mp.get_low_energy_structure(chem_formula='Cu', 
                                                   mp_id='mp-30')
nav_structure.add_bulk_to_db(structure=structure, mid=mp_id, 
                             functional='PBE')
nav.update_data('PBE.bulk', fltr={'mid': mp_id}, 
                new_values={'$set': {'comp_params': {
                    'energy_tolerance': 1
                }}})

FT_KPts = FT_StartKpointsConvo(mid=mp_id, collection='PBE.bulk', flag=mp_id)
FW_KPts = Firework([FT_KPts], name='Test FT_StartKpointsConvo')
WF_KPts = Workflow([FW_KPts], name='Test FT_StartKpointsConvo')

lpad = LaunchPad.auto_load()
lpad.add_wf(WF_KPts)
#rapidfire(lpad)
