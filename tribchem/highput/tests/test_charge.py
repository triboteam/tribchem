#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 24 12:00:00 2021

Test the workflow to for the charge integration in vasp.

Author: Jacopo Mascitelli (jmascitelli)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Jacopo Mascitelli'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'May 24th, 2021'


import os

from fireworks import LaunchPad
from fireworks import Firework, Workflow

from tribchem.highput.firetasks.run import FT_StartChargeDisplacement

chgcar_path = os.getcwd() + '/../calculations/chgcar_test'
print(chgcar_path)
quit()

chgcar_files=[chgcar_path+'/CHGCAR_1',
			  chgcar_path+'/CHGCAR_2',
			  chgcar_path+'/CHGCAR_3']

FT_ChDisp = FT_StartChargeDisplacement(operation_list=['+','-'], 
                                       chgcar_list=chgcar_files, mid_1='mp-148', 
                                       mid_2='mp-148', miller_1=[0,0,1], 
                                       miller_2=[0,0,1], functional='PBE', upsert=True, 
                                       p1=2, rescale=100000, plot=True)
FW_ChDisp = Firework([FT_ChDisp], name='Test FT_StartChargeDisplacement')
WF_ChDisp = Workflow([FW_ChDisp], name='Test FT_StartChargeDisplacement')

lpad = LaunchPad.auto_load()
lpad.add_wf(WF_ChDisp)