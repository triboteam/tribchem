#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 12:00:00 2021

Test the workflow to for the cutoff energy.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'April 22nd, 2021'

from pymatgen.core.structure import Structure
from fireworks import LaunchPad
from fireworks.core.rocket_launcher import rapidfire
from fireworks import Firework, Workflow

from tribchem.highput.firetasks.run import FT_StartEncutConvo
from tribchem.highput.database.navigator import NavigatorMP, StructureNavigator

#db_file = '/home/gl/Work/WORKFLOW/config/db.json' # Put here your local db
nav_mp = NavigatorMP()
nav_structure = StructureNavigator(high_level='tribchem')

structure, mp_id = nav_mp.get_low_energy_structure(chem_formula='Cu',
                                                   mp_id='mp-30')
nav_structure.add_bulk_to_db(structure=structure, mid=mp_id, 
                             functional='PBE')

FT_EnCut = FT_StartEncutConvo(mid='mp-30', formula='Cu', flag='mp-30',
                              collection='PBE.bulk', entry='structure_fromMP',
                              n_converge=1)

FW_EnCut = Firework([FT_EnCut], name='Test FT_StartEncutConvo')
WF_EnCut = Workflow([FW_EnCut], name='Test FT_StartEncutConvo')

lpad = LaunchPad.auto_load()
lpad.add_wf(WF_EnCut)
# rapidfire(lpad)
