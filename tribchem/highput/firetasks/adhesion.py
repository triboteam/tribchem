#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 15:36:52 2021

@author: wolloch
"""

from pymatgen.core.structure import Structure

from fireworks import FWAction
from fireworks.utilities.fw_utilities import explicit_serialize

from tribchem.highput.firetasks.core import FireTaskTribChem
from tribchem.highput.database.dbtools import read_one_entry, read_multiple_entry
from tribchem.physics.base.solidstate import get_cell_info
from tribchem.physics.base.units import Converter
from tribchem.highput.utils.manipulate_struct import structure_to_slab, ManipulateStruct


@explicit_serialize
class FT_AdhesionEnergy(FireTaskTribChem):
    """
    Calculate the adhesion energy after the workflow of the PES is run.

    """

    _fw_name = 'Calculate the Adhesion Energy'
    required_params = ['mid', 'miller', 'collection']
    optional_params = ['formula', 'name', 'db_file', 'database', 'adhesion_type']

    def run_task(self, fw_spec):
        """ Run the FireTask.
        """
        
        # Get the runtask parameters and update the 
        params = self.read_runtask(dfk='AdhesionEnergy', fw_spec=fw_spec,
                                   defaults='defaults.json')
        
        # Compute the adhesion energy of the interface
        adhesion = self.calculate_adhesion(params)
        
        # Update the adhesion energy in the high level db
        self.update_db(adhesion, params)
        
        return FWAction(update_spec=fw_spec)

    def calculate_adhesion(self, p):
        """
        Retrieve the energies from the Database and calculate adhesion energy.

        """

        # Generate the filter
        fltr = self.set_filter(p)

        # Query the database and dowload the document
        field, _ = super().query_db(collection=p['collection'], fltr=fltr,
                                        db_file=p['db_file'], database=p['database'])

        # Retrieve the energies from the database
        adt = p['adhesion_type']
        if p['adhesion_type'] == 'regular':
            read_ene = [['data', 'interface_min', 'energy'],
                        ['data', 'interface_max', 'energy'],
                        ['data', 'top_slab', 'energy'],
                        ['data', 'bot_slab', 'energy']]
        elif p['adhesion_type'] == 'short':
            read_ene = [['data', adt, 'interface_min', 'energy'],
                        ['data', adt, 'interface_max', 'energy'],
                        ['data', adt, 'top_slab', 'energy'],
                        ['data', adt, 'bot_slab', 'energy']]
        ene = read_multiple_entry(field, read_ene)

        inter_min = read_one_entry(field, ['structure', 'opt', adt, 'interface_min'])
        area, _ = get_cell_info(Structure.from_dict(inter_min).lattice.matrix)

        # Calculate adhesion and energy corrugation for min and max sites
        adhesion = {}
        for i, s in enumerate(['min_site', 'max_site']):
            # Calculate adhesion energy and convert it to J/m^2
            adhesion_ev = ene[i] - ene[2] - ene[3]
            adhesion_jm2 = Converter.eV_to_Jm2(adhesion_ev, area * 1e-20)            
            adhesion.update({
                s: {
                    'adhesion_energy_ev': adhesion_ev, 
                    'adhesion_energy_jm2': adhesion_jm2
                    }
                })
        
        # Calculate energetic corrugation and convert it to J/m^2
        corrugation_ev = ene[1] - ene[0]
        corrugation_jm2 = Converter.eV_to_Jm2(corrugation_ev, area * 1e-20)
        adhesion.update({
            'corrugation_ev': corrugation_ev,
            'corrugation_jm2': corrugation_jm2
            })
        
        # Assign a further key depending on the type of calculation
        adhesion = {p['adhesion_type']: adhesion}

        # Update the slab objects to Slab type
        self._change_slabs(field, fltr, p)

        return adhesion

    def _change_slabs(self, field, fltr, p):
        """
        Change the type of the slabs dictionary from pymatgen Structures to
        pymatgen Slabs and recenter the interface to z=0. Necessary after a 
        VASP simulation made with Atomate, which keeps Structure type for 
        everything and apply PBC to the atomic coordinates.

        """
        
        adt = p['adhesion_type']

        # Read input information for slabs and interface
        if adt == 'regular':
            init_bot = read_one_entry(field, ['structure', 'init', 'bot_slab'])
            init_top = read_one_entry(field, ['structure', 'init', 'top_slab'])
            inter_init = read_one_entry(field, ['structure', 'init', 'interface'])
        elif adt == 'short':
            init_bot = read_one_entry(field, ['structure', 'init', 'short', 'bot_slab'])
            init_top = read_one_entry(field, ['structure', 'init', 'short', 'top_slab'])
            inter_init = read_one_entry(field, ['structure', 'init', 'short', 'interface'])
        else:
            raise ValueError('Wrong input adhesion_type: allowed "regular" or "short"')
        
        # Read corresponding output structures
        opt_bot = read_one_entry(field, ['structure', 'opt', adt, 'bot_slab'])
        opt_top = read_one_entry(field, ['structure', 'opt', adt, 'top_slab'])
        inter_min_opt = read_one_entry(field, ['structure', 'opt', adt, 'interface_min'])
        inter_max_opt = read_one_entry(field, ['structure', 'opt', adt, 'interface_max'])

        # Convert the slabs from Structures to Slabs
        opt_bot = structure_to_slab(opt_bot, init_bot, as_dict=True)
        opt_top = structure_to_slab(opt_top, init_top, as_dict=True)

        # TG: Warning here instead of error if fix_interface fails like
        #
        # File "tribchem/highput/utils/manipulate_struct.py", line 401, in fix_interface
        # raise ValueError('Interface is uncoverable: an atom from '
        # ValueError: Interface is uncoverable: an atom from the upper slab is now part of the lower slab
        # 
        # Shifts the slabs again in the right position
        #final_bot = ManipulateStruct.fix_interface(opt_bot, init_bot, as_dict=True) #OMAR
        #final_top = ManipulateStruct.fix_interface(opt_top, init_top, as_dict=True) #OMAR

        # Read the interface on min and max positions and shifts them to zero
        #inter_min = ManipulateStruct.fix_interface(inter_min_opt, inter_init, as_dict=True) #OMAR
        #inter_max = ManipulateStruct.fix_interface(inter_max_opt, inter_init, as_dict=True) #OMAR

        # Update them in the Database
        #slabs = [final_bot, final_top, inter_min, inter_max] #OMAR
        slabs = [opt_bot, opt_top, inter_min_opt, inter_max_opt] #ELISa
        entry = [['structure', 'opt', adt, x] for x in ['bot_slab', 'top_slab', 'interface_min', 'interface_max']]
        for s, en in zip(slabs, entry):
            super().update_db(data=s, collection=p['collection'], entry=en,
                              db_file=p['db_file'], database=p['database'], fltr=fltr)

    def update_db(self, data, p):
        """
        Store the adhesion energy in the Database.

        """

        # Update the energies to the selected database
        super().update_db(data=data, collection=p['collection'], entry='adhesion', 
                          db_file=p['db_file'], database=p['database'], fltr=self.set_filter(p))
        
