#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 11 11:01:02 2021

Handle the cutoff and lattice parameter convergence

@author: glosi000
"""

import numpy as np
from datetime import datetime
from pprint import pprint, pformat

from fireworks import FWAction, Firework, Workflow, FileWriteTask
from fireworks.utilities.fw_utilities import explicit_serialize
from atomate.vasp.config import VASP_CMD, DB_FILE
from atomate.vasp.powerups import add_modify_incar
from atomate.vasp.workflows.base.bulk_modulus import get_wf_bulk_modulus

from tribchem.highput.database.navigator import Navigator, StructureNavigator
from tribchem.highput.database.filetools import FileTools
from tribchem.highput.firetasks.core import FireTaskTribChem
from tribchem.highput.utils.vasp_tools import VaspInputSet, get_emin
from tribchem.highput.utils.io_files import copy_output_files
from tribchem.highput.utils.check_conv import is_list_converged
from tribchem.highput.database.dbtools import convert_dict_to_mongodb


@explicit_serialize
class FT_EnergyCutoff(FireTaskTribChem):
    """
    Converge the encut for a material via fits to an Equation Of State (EOS), 
    raising encut.
    
    Uses the get_bulk_modulus workflow of atomate to fit Birch-Murnaghen EOS
    for increasing values of the
        d = mmdb.collection.find_one(
            {"task_label": {"$regex": "{} bulk_modulus*".format(tag)}})
    energy cutoff. 
    Once bulk modulus and equilibrium volume are converged, the subsequent 
    detours are stopped and the convergence data is passed on.
    
    Parameters
    ----------
    structure : pymatgen.core.structure.Structure
        The structure for which to converge the K-pint grids.
    comp_params : dict
        Dictionary of computational parameters for the VASP calculations.
    tag : str
        String from a uuid4 to identify the shared data entry in the database
        and group the deformations calculations together.
    deformations: list of lists, optional
        List of deformation matrices for the fit to the EOS. Defaults to None,
        which results in 5 volumes from 90% to 110% of the initial volume.
    encut_start : float, optional
        Starting encut value for the first run. Defaults to the largest EMIN
        in the POTCAR.
    main_folder : str
        Path to the main folder related to the upper firetask 
        FT_StartKpointsConvo. Within this folder all the folders related to 
        each VASP calculation will be created.
    encut_incr : float, optional
        Increment for the encut during the convergence. Defaults to 25.
    n_converge : int, optional
        Number of calculations that have to show the same energy as the last
        one as to signify convergence, Defaults to 3.
    db_file : str
        Full path to the db.json file that should be used. Defaults to
        '>>db_file<<', to use env_chk.
        
    Returns
    -------
    FWActions that produce detour subworkflows until convergence is reached.
    
    """

    _fw_name = 'Energy Cutoff Convergence'
    required_params = ['structure', 'mid', 'collection', 'flag', 'tag', 
                       'comp_params', 'main_folder']
    optional_params = ['db_file', 'high_level', 'formula', 'name', 
                       'deformations', 'encut_start', 'encut_incr', 'kdens_default',
                       'bm_tol', 'volume_tol', 'n_converge', 'upsert', 'cluster_params']
    
    def run_task(self, fw_spec):

        # Read required and optional input arguments, get BM data if present
        params, data, bm_tolerance, v0_tolerance = self.read_runtask('EnergyCutoff', fw_spec)
        bm_list, v0_list, encut_list = self.read_data_dict(data)

        if bm_list is None:
            # Set the data to start the convergence process
            bm_wf = self.simulation_from_scratch(params)

            return FWAction(detours=bm_wf, update_spec=fw_spec)

        else:
            bm_tol = bm_list[-1] * bm_tolerance
            v0_tol = v0_list[-1] * v0_tolerance
            
            lists = encut_list, bm_list, v0_list
            tols = bm_tol, v0_tol, bm_tol, v0_tolerance            
            
            if is_list_converged(bm_list, bm_tol, params['n_converge']) and\
               is_list_converged(v0_list, v0_tol, params['n_converge']):

                # Convergence is reached
                wf = self.simulation_converged(lists=lists, tols=tols, p=params)
                
                # Run a workflow to stop the calculation or stop it
                if wf is not None:
                    return FWAction(detours=wf, update_spec=fw_spec)       

                else:
                    return FWAction(update_spec=fw_spec)

            # Run a single new calculation and call the FT recursively
            else:
               wf = self.simulation_one_shot(lists=lists, p=params)

               return FWAction(detours=wf)

    def read_runtask(self, dfk, fw_spec={}, defaults='defaults.json'):
        """
        Read input arguments and extrapolate tolerances on v0 and bm.
        
        """

        p = super().read_runtask(dfk, fw_spec, defaults)

        if p['deformations'] is None:
            deforms = []
            for i in np.arange(0.9, 1.1, 0.05):
                dm = np.eye(3) * i
                deforms.append(dm)
            p['deformations'] = deforms

        # Set defatult values for v0 and bm tolerances
        bm_tolerance = p['bm_tol']
        v0_tolerance = p['volume_tol']
        #uks = {'reciprocal_density': 1000}

        # Get the data arrays from the database (returns None when not there)
        nav = Navigator(p['db_file'])
        data = nav.find_data('bm_data', {'tag': p['tag']})

        return p, data, v0_tolerance, bm_tolerance  # , uks

    def read_data_dict(self, data):
        """
        Define the list of data to understand if they are already calculated.

        """

        # Read BM and encut data stored in data dict
        if bool(data):
            bm_list = data.get('bm_list')
            v0_list = data.get('v0_list')
            encut_list = data.get('encut_list')
        else:
            bm_list = None
            v0_list = None
            encut_list = None
    
        return bm_list, v0_list, encut_list

    def simulation_from_scratch(self, p):
        """
        Start from scratch the evaluation of the energy cutoff and lattice
        parameters.
        
        """

        # Set the correct kspacing from density to deal with deformations
        if 'kdens' in p['comp_params']:
            p['comp_params']['kspacing'] = 1.0 / p['comp_params']['kdens']
        else:
            p['comp_params']['kspacing'] = 1.0 / p['kdens_default']

        # Calculate VASP input set
        visgen = VaspInputSet(p['structure'], p['comp_params'], 'bulk_from_scratch')
        vis = visgen.get_vasp_settings()

        encut_start = p['encut_start']      
        if not encut_start:
            # Get largest EMIN from the potcar and round up to the next whole 25
            emin = get_emin(vis.potcar)
            encut_start = int(25 * np.ceil(emin/25))
        else:
            encut_start = p['encut_start']

        vis.user_incar_settings.update({'ENCUT': encut_start})
        encut_list = [encut_start]

        #uks = {'reciprocal_density': 1000}
        tag_calc = p['tag'] + ' encut ' + str(encut_start)
        bm_wf = get_wf_bulk_modulus(p['structure'], p['deformations'],
                                    vasp_input_set=vis,
                                    vasp_cmd=VASP_CMD, db_file=p['db_file'],
                                    #user_kpoints_settings=uks,
                                    eos='birch_murnaghan', tag=tag_calc)

        formula_update=p['structure'].composition.reduced_formula
        ual_fw = Firework(
            [FT_UpdateBMLists(formula_update=formula_update, 
                              main_folder=p['main_folder'], tag=p['tag'],
                              tag_calc=tag_calc, db_file=p['db_file'],
                              encut=encut_start),
             FT_EnergyCutoff(
                structure=p['structure'],
                tag=p['tag'],
                flag=p['flag'],
                mid=p['mid'],
                main_folder=p['main_folder'],
                formula=p['formula'],
                name=p['name'],
                db_file=p['db_file'],
                high_level=p['high_level'],
                collection=p['collection'],
                deformations=p['deformations'],
                encut_incr=p['encut_incr'],
                encut_start=encut_start,
                n_converge=p['n_converge'],
                bm_tol=p['bm_tol'],
                volume_tol=p['volume_tol'],
                comp_params=p['comp_params'],
                upsert=p['upsert'],
                cluster_params=p['cluster_params'])],
             name='Update BM Lists and Loop')

        bm_wf.append_wf(Workflow.from_Firework(ual_fw), bm_wf.leaf_fw_ids)
        # Use add_modify_incar powerup to add KPAR and NCORE settings
        # based on env_chk in my_fworker.yaml
        bm_wf = add_modify_incar(bm_wf)
        
        # Set up the entry for the data arrays in the database
        nav = Navigator(p['db_file'])
        nav.insert_data('bm_data', 
                        {'tag': p['tag'],
                         'formula': p['formula'],
                         'created_on': str(datetime.now()),
                         'encut_list': encut_list,
                         'bm_list': [],
                         'v0_list': []})
        return bm_wf

    def simulation_converged(self, lists, tols, p):
        """
        Stop the simulation if the convergence is reached.

        """
        
        encut_list, bm_list, v0_list = lists
        bm_tol, v0_tol, bm_tol, v0_tolerance = tols

        final_encut = encut_list[-p['n_converge']]
        final_bm = bm_list[-p['n_converge']]
        final_v0 = v0_list[-p['n_converge']]

        scaled_structure = p['structure'].copy()
        scaled_structure.scale_lattice(final_v0)
        struct_dict = scaled_structure.as_dict()
        
        print('')
        print(' Convergence reached for BM and cell volume. ')
        print(' Final encut = {} eV; Final BM = {} GPa; Final Volume = {} Angstrom³'
              .format(final_encut, final_bm, final_v0))
        print('')
        print(' The scaled output structure is:\n')
        pprint(struct_dict)

        output_dict = {
            'data': {
                'encut_info': {'final_encut': final_encut,
                               'final_bm': final_bm,
                               'final_volume': final_v0,
                               'bm_list': bm_list,
                               'v0_list': v0_list,
                               'encut_list': encut_list,
                               'bm_tol_abs': bm_tol,
                               'bm_tol_rel': bm_tol,
                               'v0_tol_abs': v0_tol,
                               'v0_tol_rel': v0_tolerance},
                'bulk_modulus': final_bm,
                'equil_volume': final_v0
                },
            'structure.opt': struct_dict,
            'comp_params.encut': final_encut
            }

        nav_high = Navigator(db_file=p['db_file'], high_level=p['high_level'])
        fltr = self.set_filter(p, keys=['mid', 'formula', 'name'])
        nav_high.update_data(
            collection=p['collection'],
            fltr=fltr,
            new_values={'$set': convert_dict_to_mongodb(output_dict)},
            upsert=p['upsert'])
        
        nav = Navigator(db_file=p['db_file'])
        nav.update_data(
            collection='bm_data',
            fltr={'tag': p['tag']},
            new_values={'$set': 
                            {'final_encut': final_encut,
                             'final_bm': final_bm,
                             'final_volume': final_v0,
                             'bm_tol_abs': bm_tol,
                             'bm_tol_rel': bm_tol,
                             'v0_tol_abs': v0_tol,
                             'v0_tol_rel': v0_tolerance}})

        # handle file output:
        wf = None
        if 'file_output' in p['cluster_params'].keys():
            if p['cluster_params']['file_output']:                 
                write_FT = FileWriteTask(
                    files_to_write=[{'filename': p['flag']+'_output_dict.txt',
                                     'contents': pformat(output_dict)}])
    
                copy_FT = copy_output_files(
                    file_list=[p['flag']+'_output_dict.txt'],
                    output_dir=p['cluster_params']['output_dir'],
                    remote_copy=p['cluster_params']['remote_copy'],
                    server=p['cluster_params']['server'],
                    user=p['cluster_params']['user'],
                    port=p['cluster_params']['port'])
    
                fw = Firework([write_FT, copy_FT], name='Copy Encut SWF results')
                wf = Workflow.from_Firework(fw, name='Copy Encut SWF results')

        return wf

    def simulation_one_shot(self, lists, p):
        """
        Run the next step in the convergence process

        """

        encut_list, _, _ = lists

        # Set the correct kspacing from density to deal with deformations
        if 'kdens' in p['comp_params']:
            p['comp_params']['kspacing'] = 1.0 / p['comp_params']['kdens']
        else:
            p['comp_params']['kspacing'] = 1.0 / p['kdens_default']
        
        # Calculate the VASP input set
        visgen = VaspInputSet(p['structure'], p['comp_params'], 'bulk_from_scratch')
        vis = visgen.get_vasp_settings()

        encut = encut_list[-1] + p['encut_incr']
        vis.user_incar_settings.update({'ENCUT': encut})
        
        tag_calc = p['tag'] + ' encut ' + str(encut)
        bm_wf = get_wf_bulk_modulus(p['structure'], p['deformations'],
                                    vasp_input_set=vis,
                                    vasp_cmd=VASP_CMD, db_file=DB_FILE,
                                    #user_kpoints_settings=uks,
                                    eos='birch_murnaghan', tag=tag_calc)
        
        formula_update=p['structure'].composition.reduced_formula
        ual_fw = Firework([FT_UpdateBMLists(formula_update=formula_update, 
                                            tag=p['tag'], tag_calc=tag_calc,
                                            main_folder=p['main_folder'], 
                                            db_file=p['db_file'], encut=encut),
                           FT_EnergyCutoff(
                               structure=p['structure'],
                               tag=p['tag'],
                               flag=p['flag'],
                               mid=p['mid'],
                               formula=p['formula'],
                               main_folder=p['main_folder'],
                               name=p['name'],
                               db_file=p['db_file'],
                               high_level=p['high_level'],
                               collection=p['collection'],
                               deformations=p['deformations'],
                               encut_incr=p['encut_incr'],
                               encut_start=p['encut_start'], 
                               n_converge=p['n_converge'],
                               bm_tol=p['bm_tol'],
                               volume_tol=p['volume_tol'],
                               comp_params=p['comp_params'],
                               upsert=p['upsert'],
                               cluster_params=p['cluster_params'])],
                               name='Update BM Lists and Loop')
        
        bm_wf.append_wf(Workflow.from_Firework(ual_fw), bm_wf.leaf_fw_ids)
        # Use add_modify_incar powerup to add KPAR and NCORE settings
        # based on env_chk in my_fworker.yaml
        bm_wf = add_modify_incar(bm_wf)
        
        # Update Database entry for Encut list
        nav = Navigator(db_file=p['db_file'])
        nav.update_data(
            collection='bm_data',
            fltr={'tag': p['tag']},
            new_values={'$push': {'encut_list': encut}})

        return bm_wf

@explicit_serialize
class FT_UpdateBMLists(FireTaskTribChem):
    """
    Fetch information about the EOS fit from the DB and update the lists.

    Used with FT_EnergyCutoff to converge energy cutoffs using the
    get_wf_bulk_modulus workflow of atomate. This Firetasks reads the
    necessary information from the eos collection of the database. Since no
    useful tag is placed, the identification of the correct entry is done
    by the chemical formula and the timestamp. The shared date entry for the
    convergence is then identified via a tag and updated with the new
    equilibrium volume and the bulk modulus.

    Parameters
    ----------
    formula : str
        Chemical formula on the material to be matched with the database.

    tag : str
        String from a uuid4 to identify the shared data entry in the database.
    
    calc_tag : str
        Tag to identify in a safe way the calculations in the tasks collection.

    main_folder : str
        Path to the main folder related to the upper firetask 
        FT_StartKpointsConvo. Within this folder all the folders related to 
        each VASP calculation will be created.

    db_file : str, optional
        Full path to the db.json file detailing access to the database.
        Defaults to '>>db_file<<' to use with env_chk.

    """

    _fw_name = 'Update Bulk Modulus Lists'
    required_params = ['formula_update', 'tag', 'tag_calc', 'main_folder', 
                       'encut']
    optional_params = ['db_file']
    
    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Read required and optional input arguments
        params = self.read_runtask('UpdateBMLists', fw_spec)

        # Update the bulk modulus list
        self.update_bmlist(params)

    def update_bmlist(self, p):

        # Get the first element of the ordered results
        nav_structure = StructureNavigator(db_file=p['db_file'])
        results = nav_structure.get_last_bmd_data_from_db(
            formula=p['formula_update'])

        bm = results['bulk_modulus']
        v0 = results['results']['v0']

        # Update data arrays in the low level database
        nav = Navigator(p['db_file'])
        nav.update_data(collection='bm_data', fltr={'tag': p['tag']}, 
                        new_values={'$push': {'bm_list': bm, 'v0_list': v0}})

        # Create a unique main folder for the corresponding encut value
        main_folder = p['main_folder'] + '/' + str(p['encut'])
        FileTools.check_folder(fol_path=main_folder)
        
        tags = nav.find_many_data(
            collection='tasks', 
            fltr={'task_label': {'$regex': '^' + p['tag_calc']}})
        
        for tag in tags:
            deformation = 'deformation_' + tag['task_label'].split('deformation')[1].strip(' ')
            # Move the VASP output files
            FileTools.move_files(nav, main_folder=main_folder, 
                                 task_label=tag['task_label'], 
                                 custom_name=deformation)
