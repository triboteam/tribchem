#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 20 12:25:32 2021

Firetasks to be used for the bulk

@author: glosi000
"""

import os

from atomate.vasp.fireworks.core import StaticFW, OptimizeFW, ScanOptimizeFW
from atomate.vasp.powerups import add_modify_incar
from pymatgen.core.structure import Structure
from fireworks import (
    Workflow,
    FWAction,
    explicit_serialize
)

from tribchem.highput.utils.tasktools import read_default_params, select_struct_func
from tribchem.highput.firetasks.core import FireTaskTribChem


currentdir = os.path.dirname(__file__)


@explicit_serialize
class FT_CohesionEnergy(FireTaskTribChem):
    
    required_params = ['mid', 'collection_bulk', 'collection_atoms']
    optional_params = ['db_file', 'database_bulk', 'database_atoms',
                       'formula', 'name', 'update_op', 'upsert', 'to_mongodb']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Read required and optional input arguments
        params = self.read_runtask('CohesionEnergy', fw_spec)

        # Calculate the surface energies from the provided entries
        cohesion = self.calculate_cohesion(params)

        # Store surface energies in DB
        self.update_db(cohesion, params)

        return FWAction(update_spec=fw_spec)

    def calculate_cohesion(self, p):
        """
        Calculate the cohesion energy of the bulk.

        """

        # Generate the filter
        fltr = self.set_filter(p)

        # Query the database for the bulk
        field_bulk, _ = super().query_db(collection=p['collection_bulk'],
                                         db_file=p['db_file'],
                                         database=p['database_bulk'],
                                         fltr=fltr)

        bulk = Structure.from_dict(field_bulk['structure']['opt'])
        at_list = []
        for at in bulk.species:
            at_list.append(at.name)
        at_unique_list = list(set(at_list))  # Find out the unique species
    
        # Query the database and dowload the energies for atoms
        energy = [field_bulk['data']['energy']]
        pcopy = p.copy()
        for at in at_unique_list:
            pcopy['mid'] = 'element-' + str(at)
            fltr = self.set_filter(pcopy)
            _, atom_energy = super().query_db(collection=p['collection_atoms'],
                                              db_file=p['db_file'],
                                              database=p['database_atoms'],
                                              entry=['data', 'energy'],
                                              fltr=fltr)
            energy.append(atom_energy)
        
        # Calculate the cohesion energy
        cohesion = energy[0]
        for e, at in zip(energy[1:], at_unique_list):
            cohesion -= (e * at_list.count(at))
        
        return cohesion

    def update_db(self, cohesion, p):
        """
        Upload the final cohesion energy in the database.

        """

        # Generate the filter
        fltr = self.set_filter(p)

        # Update the energies to the selected database
        super().update_db(data=cohesion, collection=p['collection_bulk'],
                          entry=['data', 'cohesion_energy'], db_file=p['db_file'], 
                          database=p['database_bulk'], fltr=fltr, 
                          update_op=p['update_op'], upsert=p['upsert'],
                          to_mongodb=p['to_mongodb'])
