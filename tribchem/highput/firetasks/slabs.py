#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 15:09:03 2021

Firetasks to generate and manipulate crystalline slabs.

The module contains the following Firetasks:

** Slab generation **

    - FT_GenerateSlabs
    Generate a list of slabs out of a structure provided as input and store
    them in a given location inside the database.
    
    - FT_StartThickConvo
    Start a subworkflow to select a desired convergence criterion to calculate
    the slab thickness. Implemented criteria: surface energy.

    - FT_EndThickConvo
    Check the results and call recursively the slab thickness workflow it the
    convergence has not been achieved yet.

    Author: Gabriele Losi (glosi000)
    Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, sUniversity of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 2nd, 2021'


import os
from monty.json import jsanitize

import numpy as np
from fireworks import explicit_serialize, FWAction
from pymatgen.core.structure import Structure, Lattice 
from pymatgen.core.surface import Slab 

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.firetasks.core import FireTaskTribChem
from tribchem.physics.base.solidstate import generate_slabs
from tribchem.physics.tribology.interface_matcher import InterfaceMatcher, flip_slab
from tribchem.highput.utils.manipulate_struct import ManipulateStruct, slab_to_structure, structure_to_slab
from tribchem.highput.utils.errors import GenerateSlabsError, InterfaceMatchError
from tribchem.highput.utils.errors import SlabOptThickError
from tribchem.physics.tribology.highsym import (
    get_slab_hs,
    get_interface_hs,
    pbc_hs_points,
    fix_hs_dicts
    )
from tribchem.highput.database.filetools import FileTools


currentdir = os.path.dirname(__file__)


@explicit_serialize
class FT_GenerateSlabs(FireTaskTribChem):
    """
     Generate a slab or a list of slabs out of a given structure. Parameters 
     hat are taken into account to generate the possible different slabs are: 
     miller, thickness, vacuum, entry. The slabs are generated with 
     SlabGenerator and stored in the database.
    
    """

    required_params = ['structure', 'mid', 'miller', 'collection']
    optional_params = ['formula', 'name', 'db_file', 'database', 'entry',
                       'thickness', 'thick_bulk', 'vacuum', 'symmetrize', 
                       'ext_index', 'in_unit_planes', 'update_op', 'upsert',
                       'to_mongodb', 'comp_params']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """ 

        # Read required and optional input arguments
        params = self.read_runtask('GenerateSlabs', fw_spec)

        # Generate the slabs of interest
        slabs = self.generate_slabs(params)

        # Store the slabs in database, location given by collection, field, entry
        self.update_db(slabs, params)

        return FWAction(update_spec=fw_spec)
    
    def read_runtask(self, dfk, fw_spec, defaults='defaults.json'):
        """
        Call the parent method to read the required and optional parameters 
        passed as input by the user and update some of them to lists.

        Parameters
        ----------
        dfk : str
            Key of the JSON dictionary containing the default values of the
            optional parameters of the instanciated Firetask.

        fw_spec : dict, optional
            Spec of the Firetask. It is used by Atomate to identify the location
            of the database on the local machine (`env_check`), when an input
            argument with name `db_file` is None. The default is default {}.

        defaults : str, optional
            Name of the JSON dictionary. The default is 'defaults.json'.

        Returns
        -------
        dict
            Dictionary containing all the parameters, the optional values which 
            have not been provided by the user are substituted with defaults.

        """
        
        # Read all the parameters calling the parent method
        p = super().read_runtask(dfk, fw_spec, defaults)

        # Update important parameters to lists
        p = self._to_list(p)

        return p

    def _to_list(self, p):
        """
        Convert some parameters of the input dictionary to lists, in order to 
        keep the notation consistent. Updated params: `miller`, `name`,
        `thickness`, `vacuum`, `entry`.

        Returns
        -------
        dict
            Dictionary with the updated parameters

        """

        GenerateSlabsError.check_inputs(p['name'], p['miller'], p['thickness'], 
                                        p['vacuum'], p['entry'], p['comp_params'])
        
        # First level loop, i.e. different mongo documents
        # ================
        
        # Set the miller indexes as reference
        miller = p['miller']
        if not all([isinstance(x, list) for x in miller]):
            miller = [miller]
        
        # Set the proper list of comp_params            
        comp_params = p['comp_params']        
        if comp_params is None:
            comp_params = [comp_params] * len(miller)
        elif not isinstance(comp_params, list):
            comp_params = [comp_params]
        
        # Set the names
        name = p['name']
        if name is None:
            name = [name] * len(miller)
        if not isinstance(name, list):
            name = [name]

        # Second level loop, i.e. within the same mongo document
        # ================
        
        # Set the thickness as reference
        thickness = p['thickness']
        if not isinstance(thickness, list):
            thickness = [thickness]
        
        # Entry should be always provided
        entry = p['entry']
        if not isinstance(entry, list):
            entry = [entry]
        
        # Vacuum can be just a float
        vacuum = p['vacuum']
        if not isinstance(vacuum, list):
            vacuum = [vacuum] * len(thickness)
        
        # Update the params dictionary
        for k, e in zip(['miller', 'comp_params', 'name', 'thickness', 'entry', 'vacuum'],
                        [miller, comp_params, name, thickness, entry, vacuum]):
            p[k] = e

        return p

    def generate_slabs(self, p):
        """
        Generate the slabs out of a list of Miller index, thickness and vacuum.

        Parameters
        ----------
        p : dict
            Parameters of the Firetask.

        Returns
        -------
        list of pymatgen.core.surface.Slab
            List of slabs genereted from a structure, to be stored in database.

        """

        # Generate the slabs, taking into account miller, thickness, vacuum
        
        slabs = []
        for hkl in p['miller']:
            s = generate_slabs(structure=p['structure'], 
                               miller=hkl, 
                               thickness=p['thickness'], 
                               vacuum=p['vacuum'], 
                               thick_bulk=p['thick_bulk'],
                               ext_index=p['ext_index'],
                               in_unit_planes=p['in_unit_planes'])
            slabs.append(s)
 
        return slabs

    def update_db(self, slabs, p):
        """
        Update all the slabs in the selected Database

        Parameters
        ----------
        slabs : list of pymategen.core.slab.Slab
            List of slabs to be updated in the database.

        p : dict
            Input parameters of the Firetask.

        """
        
        # Double loop to save different oriented slabs with different thickness
        for slabs, hkl, cp, name in zip(slabs, p['miller'], p['comp_params'], p['name']):
            for s, entry in zip(slabs, p['entry']):
                
                # Create a custom filter based on mid, formula, name, miller, field
                fltr = {'mid': p['mid'], 'miller': hkl}
                if p['formula'] is not None:
                    fltr.update({'formula': p['formula']})
                if cp is not None:
                    fltr.update({'comp_params': cp})
                if name is not None:
                    fltr.update({'name': name})
    
                # Update each slab in the database
                super().update_db(data=s.as_dict(), collection=p['collection'], 
                                  entry=entry, db_file=p['db_file'], 
                                  database=p['database'], fltr=fltr, 
                                  update_op=p['update_op'], upsert=p['upsert'], 
                                  to_mongodb=p['to_mongodb'])

@explicit_serialize
class FT_GenerateInterface(FireTaskTribChem):
    """
    Matches two slabs to create an interface. You can create either a 
    homogeneous or heterogeneous interface. If the match fails, the accuracy 
    criteria are relaxed in steps of 5% until a match is found.
    Many of the input argument, as stated below, should be provided as a list 
    of two elements, the first one referred to the bottom slab (substrate),
    the second one referred to the upper slab (coating).
    
    The match is done by employing the Zur algorithm [1], as implemented in the
    MPinterfaces python package [2].

    Parameters
    ----------
    mid : list of str
        Material identifiers for the first and second slab.
    
    slabs : list of pymatgen.core.surface.Slab
        Slab objects of the two materials to be matched.
    
    miller : list of lists (of int)
        Miller indexes of the interface, e.g. [[0, 0, 1], [1, 1, 1]].

    collection : str
        Name of the collection in the Database where the interface is stored.

    inter_params : dict or None, optional
        Dictionary containing relevant threshold parameters to match the two
        slabs. Default values are typically fine, in case a matching is not 
        found, thresholds are progressively lightened in a recursive process.
        Default values are substituted from the appositve key in default json.
        Typical default values can be set to:
        {
         "strain_weight_1": 1,
         "strain_weight_2": 2,
         "max_area" : 150,
         "interface_distance" : 2.5,
         "max_angle_tol" : 0.01,
         "max_length_tol" : 0.01,
         "max_area_ratio_tol" : 0.05
        }
    
    formula : str or None, optional
        Chemical formulas of the interface. It is suggested to build it by
        combining the formula of the two slabs. The default is None.
    
    name : str or None, optional
        Third possible optional identifier of the interface. Useful in cases
        of homonymes, e.g. C diamond, C graphene. The default is None.
    
    db_file : str, optional
        Full path to the db.json file which holds the location and access
        credentials to the database. If not given uses env_chk.
    
    database : str or None, optional
        Name of the database, typically a high level area just containing
        clean and final results, where the interface will be inserted.
        The default is 'tribchem'.

    entry : str or list or list of lists, optional
        List of (nested) keys indicating the position of the pymatgeen dict of
        the interface in the MongoDB document. The default is ['']
    
    comp_params : dict or None, optional
        Computational parameters to perform an optimal simulation with VASP,
        relevant information typically are:
            - The kind of XC functional to be used (LDA, PBE, SCAN)
            - If the material is metallic or not, this may affect few INCAR
              parameters and fasten the convergence
            - If the calculation should be spin-polarized or not
            - The kinetic energy cutoff for the plane wave set
            - The density of the k-points grid
            - Eventually, what kind of VdW corrections should be adopted
        A good choice would be to take the "max" values choosing between 
        slab parameters, i.e. the highest encut, spin polarization to True if
        at least one is True.

    References
    ----------
    [1] A. Zur et al., Journal of Applied Phys. 55 (2) (1984) 378–386
    [2] Computational Materials Science 122 (2016) 183–190
    
    """

    _fw_name = 'Build a solid-solid interface'
    required_params = ['slabs', 'mid', 'miller', 'collection', 'inter_params']
    optional_params = ['formula', 'name', 'db_file', 'database', 'entry',
                       'vacuum', 'comp_params', 'update_op', 'upsert', 'to_mongodb']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Read required and optional input arguments
        params = self.read_runtask('GenerateInterface', fw_spec)


        # Generate the slabs of interest and store the interface and alligned slabs in the database
        bot_slab_strain, top_slab_strain = self.generate_interface(params)

        # Update 'slabs' in fw_spec, needed? 
        fw_spec['_tasks'][0]['slabs'][0] = bot_slab_strain.as_dict()
        fw_spec['_tasks'][0]['slabs'][1] = top_slab_strain.as_dict()
        
        return FWAction(update_spec=fw_spec)

    def generate_interface(self, p, factor=1, delta_f=0.01):
        """
        Generate the interface by matching the two slabs with the Zur Algorithm.

        """
        
        # Raise an Error if convergence is not reached after multiple recursions
        if factor >= 1.5:
            raise InterfaceMatchError('Error in generating the interface. '
                                      'Matching not converged')

        # Rescale the interface params for recursion
        p['inter_params']['max_area'] *= factor
        p['inter_params']['max_length_tol'] *= factor
        p['inter_params']['max_angle_tol'] *= factor
        p['inter_params']['max_area_ratio_tol'] *= factor

        # This is necessary to prevent LinAlgError("Singular matrix"),
        # issue present since MPInterfaces 2020.6.19
        if p['inter_params']['max_angle_tol'] > 1.5:
            p['inter_params']['max_angle_tol'] = 0.4
        else:
            p['inter_params']['max_angle_tol'] = 0.4

        # Try to generate the interface
        inter_obj = InterfaceMatcher(
            slab_1=p['slabs'][0], slab_2=p['slabs'][1], **p['inter_params'])
        
        top_aligned, bot_aligned = inter_obj.get_centered_slabs()
        interface = inter_obj.get_interface()
        interface = slab_to_structure(interface)

        # Add the vacuum
        if interface is not None:
            man_struct_obj = ManipulateStruct(structure=interface)
            interface = man_struct_obj.make_vacuum_around(vacuum=p['vacuum'])
            bot_slab, top_slab = man_struct_obj.decompose_structure()

        # bot_aligned, top_aligned, interface = ManipulateStruct.create_interface(
        #     p['slabs'][0], p['slabs'][1], p['vacuum'], p['inter_params'])
        
        # Call the function recursively
        if any([t is None for t in [bot_aligned, top_aligned, interface]]):
            bot_aligned, top_aligned = self.generate_interface(p=p, factor=factor+delta_f, delta_f=delta_f)
            return bot_aligned, top_aligned
        else:
            self.update_db(interface, bot_aligned, top_aligned, p)
            return bot_aligned, top_aligned

    def update_db(self, interface, bot_slab, top_slab, p):
        """
        Update all the slabs in the selected Database

        Parameters
        ----------
        slabs : list of pymategen.core.slab.Slab
            List of slabs to be updated in the database.

        p : dict
            Input parameters of the Firetask.

        """

        # Define the filter to catch the document in the database
        fltr = self.set_filter(p)

        # Define the data to be stored in the database
        data = {
            'interface': interface.as_dict(),
            'bot_slab': bot_slab.as_dict(),
            'top_slab': top_slab.as_dict()
            }

        # Update the interface and its parameters in the database
        for d, entry in zip([data, p['inter_params']], [p['entry'], 'inter_params']):
            super().update_db(data=d, collection=p['collection'], entry=entry,
                              db_file=p['db_file'], database=p['database'], 
                              fltr=fltr, update_op=p['update_op'], 
                              upsert=p['upsert'], to_mongodb=p['to_mongodb'])

@explicit_serialize
class FT_GetHighSymmetryPts(FireTaskTribChem):
    """
    Compute the high symmetry points for a interface.
    
    Finds the high symmetry points of the top side of the bottom slab and the
    bottom side of the top slab. This is done twice, once omitting duplicates,
    and once allowing them. It is made sure that the results are cartesian
    coordinates that lie inside the unit cell. The lists are combined so that
    every combination of unique points for the interface is present. The PES
    section of the high level database is updated with the results and the
    fw_spec is updated with the lateral shifts needed for the PES relaxations
    as well.
    
    Parameters
    ----------
    top_slab : pymatgen.core.surface.Slab
        Top slab of the interface.
    bottom_slab : pymatgen.core.surface.Slab
        Bottom slab of the interface.
    functional : str
        Which functional to use; has to be 'PBE' or 'SCAN'.
    interface_name : str
        Name of the interface in the high-level database.
    db_file : str, optional
        Full path to the db.json file that should be used. Defaults to
        '>>db_file<<', to use env_chk.
        
    Returns
    -------
    FWActions that updates the fw_spec with lateral shifts.

    """

    required_params = ['slabs', 'mid', 'miller', 'collection', 'inter_params']
    optional_params = ['formula', 'name', 'db_file', 'database', 'entry']

    def run_task(self, fw_spec):
    
        # Read required and optional input arguments
        params = self.read_runtask('GetHighSymmetryPts', fw_spec)

        # Calculate the HS points. Functions from physics module are used
        bot_slab_strain, top_slab_strain = self.generate_interface(params)
        params['slabs'] = [bot_slab_strain, top_slab_strain]
        hs = self.get_hspts(params)

        # Update the obtained values in the Database
        fltr = self.set_filter(params)
        super().update_db(hs, params['collection'], params['entry'], 
                          params['db_file'], params['database'], fltr)

        return FWAction(update_spec=fw_spec)


    def generate_interface(self, p, factor=1, delta_f=0.01):
        """
        Generate the interface by matching the two slabs with the Zur Algorithm.

        """
        
        # Raise an Error if convergence is not reached after multiple recursions
        if factor >= 1.5:
            raise InterfaceMatchError('Error in generating the interface. '
                                      'Matching not converged')

        # Rescale the interface params for recursion
        p['inter_params']['max_area'] *= factor
        p['inter_params']['max_length_tol'] *= factor
        p['inter_params']['max_angle_tol'] *= factor
        p['inter_params']['max_area_ratio_tol'] *= factor

        # This is necessary to prevent LinAlgError("Singular matrix"),
        # issue present since MPInterfaces 2020.6.19
        # TG: now max_angle_tol is RELATIVE, change following ABSOLUTE (deg) thresholds?
        if p['inter_params']['max_angle_tol'] > 1.5:
            p['inter_params']['max_angle_tol'] = 0.4
        else:
            p['inter_params']['max_angle_tol'] = 0.4

        # Try to generate the interface
        inter_obj = InterfaceMatcher(
            slab_1=p['slabs'][0], slab_2=p['slabs'][1], **p['inter_params'])
        
        top_aligned, bot_aligned = inter_obj.get_centered_slabs()
        interface = inter_obj.get_interface()
        interface = slab_to_structure(interface)

        # Call the function recursively
        if any([t is None for t in [bot_aligned, top_aligned, interface]]):
            bot_aligned, top_aligned = self.generate_interface(p=p, factor=factor+delta_f, delta_f=delta_f)
            return bot_aligned, top_aligned
        else:
            return bot_aligned, top_aligned


    def get_hspts(self, p):
        """
        Get the high symmetry points for the two slabs and calculate the
        shifts that are needed to be applied to the upper slab to calculate PES.

        Parameters
        ----------
        p : dict
            Dictionary parameters with Firetask inputs.

        Returns
        -------
        hs : dict
            High symmetry dictionary to be stored in the database.

        c_hsp_u : np.ndarray
            Unique shifts for the interface

        """
        
        # Extract the material slabs from the params dictionary
        top_slab = p['slabs'][1]
        bot_slab = p['slabs'][0]

        # Top slab is mirrored to find the high symmetry points at the interface
        flipped_top = flip_slab(top_slab)
        top_hsp_unique, top_hsp_all = get_slab_hs(flipped_top)

        bottom_hsp_unique, bottom_hsp_all = get_slab_hs(bot_slab)

        cell = bot_slab.lattice.matrix

        hsp_unique = get_interface_hs(bottom_hsp_unique, top_hsp_unique, cell)
        hsp_all = get_interface_hs(bottom_hsp_all, top_hsp_all, cell)

        c_hsp_u, c_hsp_a = fix_hs_dicts(hsp_unique, hsp_all,
                                        top_slab, bot_slab)

        b_hsp_u =  pbc_hs_points(bottom_hsp_unique, cell)
        b_hsp_a =  pbc_hs_points(bottom_hsp_all, cell)
        t_hsp_u =  pbc_hs_points(top_hsp_unique, cell)
        t_hsp_a =  pbc_hs_points(top_hsp_all, cell)

        # Create a HS dictionary to be stored in the Database
        # Default location within the MongoDB document is PES.high_symmetry_points
        hs = {
            'bot_unique': b_hsp_u,
            'bot_all': b_hsp_a,
            'top_unique': t_hsp_u,
            'top_all': t_hsp_a,
            'inter_unique': jsanitize(c_hsp_u),
            'inter_all': jsanitize(c_hsp_a)}

        return hs

@explicit_serialize
class FT_StartThickConvo(FireTaskTribChem):
    """
    It starts a subworkflow as a detour to converge the thickness of a slab.
    The thickness can be converged either by evaluating how the surface energy
    or the lattice parameters changes with the number of atomic planes.
    It is the first element of the SlabWF.conv_slabthick_surfene workflow.

    (At the moment only the surface energy convergence is implemented)
    
    Parameters
    ----------
    structure : pymatgen.core.structure.Structure
        Bulk structure to build the slabs.

    mid : str
        MP-id of the structure from the MP database.
    
    miller : list of int, or str
        Miller indexes (h, k, l) to select the slab orientation.
        
    functional : str
        Functional for the pseudopotential to be adopted.
    
    main_folder : str
        Main folder where the VASP output files will be saved.

    db_file : str or None
        Path to the location of the database. If nothing is provided it will be
        searched by env_check from Atomate. The default is None.
        
    low_level : str or None, optional
        Name of the table of the "low level" database, saved in db_file. The 
        intermediate calculations and raw data during will be saved here. If 
        nothing is passed the Firework database is used. The default is None.

    high_level : str, optional
        Name of the table of the "high level" database, saved in db_file.
        The slab optimal thickness will be saved here. The slab energy and
        surface energy will be saved too if conv_kind is 'surfene'.
        The default is 'tribchem'.

    conv_kind : str, optional
        Type of convergence to be performed. Allowed values are: 'surfene', 
        'alat'. The latter is not implemented yet. The default is 'surfene'.

    relax_type : str, optional
        The type of relaxation to be performed during the simulation, to be feed
        to GetCustomVaspRelaxSettings. The default is 'slab_pos_relax'.

    comp_params : dict, optional
        Computational parameters for the VASP simulations. If not set, default 
        parameters will be used instead. The default is {}.
    
    thick_min : int, optional
        Number of atomic layers for the slab to be used as starting point. In
        case of low parallelization this gives the value of atomic layers for
        the only slab which is created and relaxed at each step after the first.
        The default is 4.
        
    thick_max : int, optional
        Maximum number of allowed atomic layers for the slab. If convergence is
        not reached this value is considered the optimal one. The default is 12.

    thick_incr : int, optional
        The incremental number of atomic layers to be added at each step during
        the iterative procedure. The default is 2.

    vacuum : int or float, optional
        Vacuum to be used for creating in the slabs cells. The default is 10.

    in_unit_planes : bool, optional
        Decide if thick_min, thick_max, thick_incr, and vacuum are expressed in
        units of number of atomic layers or Angstrom. The default is True.

    ext_index : int, optional
        Use the ext_index element from SlabGenerator.get_slabs as a slab.
        The default is 0.

    cluster_params : dict, optional
        Dictionary containing cluster-related options to run efficiently the
        VASP simulations on a cluster. The default is {}.

    """
    
    _fw_name = 'Start the slab thickness convergence'
    required_params = ['structure', 'mid', 'miller', 'collection', 'main_folder']
    optional_params = ['formula', 'name', 'db_file', 'database', 'conv_kind', 
                       'calc_type', 'thick_min', 'thick_max', 'thick_incr', 
                       'vacuum', 'in_unit_planes', 'ext_index', 'parallelization',
                       'recursion', 'cluster_params', 'override']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Define the json file containing default values and read parameters
        params = self.read_runtask('StartThickConvo', fw_spec)

        # Select the convergence of interest
        wf = self.run_simulation(params)

        return FWAction(detours=wf, update_spec=fw_spec)

    def run_simulation(self, p):
        
        from tribchem.highput.workflows.surfene_wfs import SurfEneWF

        if p['conv_kind'] == 'surfene':
            wf = SurfEneWF.conv_surface_energy(structure=p['structure'],
                                               mid=p['mid'],
                                               formula=p['formula'],
                                               name=p['name'],
                                               miller=p['miller'],
                                               collection=p['collection'],
                                               main_folder=p['main_folder'],
                                               db_file=p['db_file'], 
                                               database=p['database'],
                                               calc_type=p['calc_type'],
                                               thick_min=p['thick_min'], 
                                               thick_max=p['thick_max'],
                                               thick_incr=p['thick_incr'],
                                               vacuum=p['vacuum'],
                                               in_unit_planes=p['in_unit_planes'],
                                               ext_index=p['ext_index'], 
                                               parallelization=p['parallelization'],
                                               recursion=p['recursion'],
                                               cluster_params=p['cluster_params'],
                                               override=p['override'])
            return wf

        else:
            raise SystemExit('Lattice parameter convergence not yet implemented')

@explicit_serialize
class FT_EndThickConvo(FireTaskTribChem):
    """
    Firetask description...
    
    """

    required_params = ['structure', 'mid', 'miller', 'collection', 'main_folder']
    optional_params = ['formula', 'name', 'db_file', 'database', 'conv_kind',
                       'calc_type', 'thick_min', 'thick_max', 'thick_incr',
                       'vacuum', 'in_unit_planes', 'ext_index', 'conv_thr',
                       'parallelization', 'cluster_params', 'recursion', 'override']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """ 

        # Define the json file containing default values and read parameters
        params = self.read_runtask('EndThickConvo', fw_spec)
        case = self._get_case(params)
        
        # Retrieve the surface energy
        data, index = self.query_db(case, params)

        # Analyze the data, if convergence is reached data is stored in DB
        stop_convergence = self.analyze_data(data, index, case, params)

        # Rerun recursively the surface energy subworkflow within detours
        if not stop_convergence:
            wf = self.call_recursion(fw_spec, params)

            return FWAction(detours=wf, update_spec=fw_spec)

        else:
            # Save the optimal thickness VASP output files
            self.save_opt(p=params)

            return FWAction(update_spec=fw_spec)

    def _get_case(self, p):
        """
        Define the dictionary key to be read from 'conv_kind'.
        
        """
        
        # Select the keys to be used when reading the dictionary
        if p['conv_kind'] == 'surfene':
            case = 'surface_energy_ev'
        elif p['conv_kind'] == 'alat':
            case = 'lattice'
        else:
            raise SlabOptThickError("Wrong argument: 'conv_kind'. Allowed "
                                    "values: 'surfene', 'alat'")
        return case

    def query_db(self, case, p):
        """
        Extract the surface energies from the high level DB.

        case : str
            Dictionary key to identify the type of data to be read from 
            'calc_output' in the nested entry of the pymongo field.

        p : dict
            Dictionary with the input parameters of the Firetask.

        Returns
        -------
        data : list of floats
            Contains all the `case` data calculated for various thicknesses.
        
        index : list of index
            Contains the index referring to the different thicknesses.

        """

        # Call the navigator for retrieving the thickness dict out of DB
        nav = Navigator(db_file=p['db_file'], high_level=p['database'])

        fltr = self.set_filter(p, keys=['mid', 'formula', 'name', 'miller'])
        thickness_dict = nav.find_data(p['collection'], fltr)['thickness']

        # Define the thicknesses that should be taken into account
        data = []
        if p['parallelization'] == 'high':
            thks = list(range(p['thick_min'], p['thick_max']+1, p['thick_incr']))
        else:
            thks = [p['thick_min'], p['thick_max']]

        # Read allowed data for this turn
        for n in thks:
            key = 'data_' + str(n)
            if not key in thickness_dict.keys():
                raise SlabOptThickError('Key data_{} not in thickness subdict. '
                                        'Values present are: {}'
                                        .format(str(n), thickness_dict.keys()))
            if not 'output' in thickness_dict[key].keys():
                raise SlabOptThickError('Calculation with {} layers has no '
                                        'output, VASP failed someway'.format(n))
            data.append(thickness_dict[key]['output'][case])

        # Check for consistency with the value from the max thickness
        dmax = thickness_dict['data_' + str(p['thick_max'])]['output'][case]
        if dmax != data[-1]:
            raise SlabOptThickError("An unexpected error occurred")
        
        return np.array(data), np.array(thks)
    
    def analyze_data(self, data, index, case, p):
        """
        Analyze the data to understand if the convergence has been achieved.
        Update the document in high_level database at the end of the process.

        Parameters
        ----------
        data : list of floats
            List containing the values to be checked for convergence.

        index : list of int
            List containing the thicknesses of the different slabs.

        case : str
            Dictionary key to identify the type of data and store them.

        p : dict
            Dictionary with the input parameters of the Firetask.

        Returns
        -------
        stop_convergence : bool
            Decide the fate of the calculation.

        """

        # Calculate the relative error to the last element
        error_to_last = np.abs((data - data[-1]) / data[-1])

        # Evaluate what is the lower converged data
        i = np.argwhere(error_to_last <= abs(p['conv_thr']))
        index_converged = index[i]
        index_converged = index_converged.flatten()
        
        # If a low parallelization is selected, three calculations are done
        # at the beginning (bulk, min, max), and if convergence is not 
        # achieved, then a calculation is done one by one until convergence
        # is not attained or you go above max
        if p['parallelization'] in [None, 'low']:

            # If length>1, then a material has converged other than max
            if len(index_converged) > 1:
                self.update_db(index_converged[0], p)
                stop_convergence = True

            # If length=1, material is not converged, but if the next step
            # brings you above the max thickness, then convergence is
            # assumed to be reached at thick_max
            elif p['thick_min'] + p['thick_incr'] >= p['thick_max']:
                self.update_db(index_converged[0], p)
                stop_convergence = True
            
            # If length=1 and you are far from thick max, recursion is done
            else:
                stop_convergence = False
        
        # If high level parallelization
        elif p['parallelization'] == 'high':
            self.update_db(index_converged[0], p)
            stop_convergence = True

        return stop_convergence

    def update_db(self, index, p):
        """
        Store the surface energy or the latice parameter that have been
        obtained in a more accessible point within the document in the db.
        If the same convergence workflow is run again, it will be stopped
        when it finds that the data is already there.

        Parameters
        ----------
        index : int
            Index of the thickness indicating the optimal slab.

        """

        # Retrieve the thickness dict 
        fltr = self.set_filter(p, keys=['mid', 'formula', 'name', 'miller'])
        field, thick_dict = super().query_db(collection=p['collection'], 
                                             db_file=p['db_file'], fltr=fltr,
                                             database=p['database'],
                                             entry='thickness')

        # Prepare the dictionary for the update
        if field is None or thick_dict is None:
            raise SlabOptThickError('Something very nasty is happening here. '
                                    'The convergence process is over but the '
                                    'document or the data is missing!')

        # Prepare the data resulting from the VASP simulation
        data = thick_dict['data_' + str(index)]['output']
        data.update({'opt_thickness': int(index)})
        
        # Prepare the init and opt structures for the optimal thickness
        init = thick_dict['data_' + str(index)]['input']
        opt = data['structure']
        _ = data.pop('structure')
        
        # Reconvert the optimal final structure to a slab
        opt = structure_to_slab(opt, init, as_dict=True)

        # Update the final data in a more comfortable location in the field
        super().update_db(data=[init, opt, data], collection=p['collection'], 
                          entry=[['structure', 'init'], ['structure', 'opt'], ['data']],
                          database=p['database'], fltr=fltr, upsert=True)

    def call_recursion(self, fw_spec, p):
        """
        Call the convergence workflow on the slab optimal thickness in a
        recursive way.
        
        """
        
        from tribchem.highput.workflows.slabs_wfs import SlabWF

        # Select the correct function to call the workflow 
        if p['conv_kind'] == 'surfene':
            generate_wf = SlabWF.conv_slabthick_surfene
        else:
            pass
        
        # Generate the workflow for the detour
        wf = generate_wf(structure=p['structure'], mid=p['mid'], formula=p['formula'],
                         name=p['name'], miller=p['miller'], spec=fw_spec,
                         collection=p['collection'], main_folder=p['main_folder'], 
                         db_file=p['db_file'], database=p['database'], 
                         calc_type=p['calc_type'], thick_min=p['thick_min']+p['thick_incr'], 
                         thick_max=p['thick_max'], thick_incr=p['thick_incr'], 
                         vacuum=p['vacuum'], in_unit_planes=p['in_unit_planes'], 
                         ext_index=p['ext_index'], conv_thr=p['conv_thr'], 
                         parallelization=p['parallelization'], 
                         recursion=p['recursion']+1, override=p['override'],
                         cluster_params=p['cluster_params'])

        return wf

    def save_opt(self, p):
        """
        Save the optimal thickness VASP files in output/.

        """
        
        fltr = self.set_filter(p, keys=['mid', 'formula', 'name', 'miller'])
        field, _ = super().query_db(collection=p['collection'], 
                                             db_file=p['db_file'], fltr=fltr,
                                             database=p['database'],
                                             entry='thickness')
        
        opt_thik = field['data']['opt_thickness']
        folder_from = p['main_folder'] + '/' + 'thickness_' + str(opt_thik) + '/'
        folder_to = folder_from + '../../output/'

        FileTools.check_folder(fol_path=folder_to)
        
        FileTools.copy_general_file(folder_from=folder_from, folder_to=folder_to)
