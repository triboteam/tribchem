#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Tue May 11 15:43:31 2021

Workflow concerning the charge displacement calculation.

Author: Jacopo Mascitelli (jmascitelli)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
"""

__author__ = 'Jacopo Mascitelli'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'May 11th, 2021'


import os
from monty.json import jsanitize

import numpy as np
from fireworks import FWAction, explicit_serialize

from tribchem.physics.electronics.charge.chgcar_integrator import (
    extract_chgcar_data,
    chgcar_average_along_axis
    )
from tribchem.highput.database.dbtools import read_multiple_entry
from tribchem.highput.firetasks.core import FireTaskTribChem
from tribchem.physics.electronics.charge.chgcar_integrator import (
    chgcar_algebra_from_file,
    charge_displacement)


currentdir = os.path.dirname(__file__)


@explicit_serialize
class FT_AverageCharge(FireTaskTribChem):
    """
    Calculate the average charge for a structure within a lattice cell, along
    an axis or a plane. The path to the CHGCAR is located within the high level
    database, and the results are stored in a given entry in the same document.
    
    Warning: It currently works only for planar averages, i.e. it stores
    rho(i), with i = x, y, z.
    
    """
    
    _fw_name = 'Calculate average charge'
    required_params = ['mid', 'miller', 'collection']
    optional_params = ['formula', 'name', 'db_file', 'database', 'site', 'axis', 'check_entry']
    
    # Class variable to be used in _make_average_and_update
    axis_converter = {0: 'x', 1: 'y', 2: 'z'}

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Get the runtask parameters
        params = self.read_runtask('AverageCharge', fw_spec)

        # Calculate the charge displacement
        self.calculate_average_charge(params)

        return FWAction(update_spec=fw_spec)

    def calculate_average_charge(self, p):
        """
        Retrieve the location of the CHGCAR from the local computer, then
        calculate and save the charge displacement.

        """

        # Find the document of the interface in the Database
        fltr = self.set_filter(p)
        field, _ = self.query_db(collection=p['collection'], db_file=p['db_file'],
                                 database=p['database'], fltr=fltr)

        # Find out the c-axis for the cell, in order to shift interface z=0
        i = 'interface_min' if p['site'] == 'min_site' else 'interface_max'
        
        if p['check_entry'] == 'short':
            c_2 = field['structure']['opt']['short'][i]['lattice']['c'] / 2 
        else:
            c_2 = field['structure']['opt']['regular'][i]['lattice']['c'] / 2
        
        # Define the location of the CHGCAR files
        entry_chgcar = [['charge', p['check_entry'], 'data', p['site'], x, 'dir_name'] for x in ['interface', 'bot_slab', 'top_slab']]

        # Recover and clean the CHGCAR paths from the database
        chgs = read_multiple_entry(field, entry_chgcar)
        chgs = [c.split(':')[1] + '/CHGCAR' for c in chgs]

        # Gunzip the CHGCAR files
        self.run_command_on_file('gunzip --keep', [c + '.gz' for c in chgs])

        # Load the CHGCAR objects, average them, and store in database
        self._make_average_and_update(chgs, c_2, fltr, p)
        
        # Remove the decompressed temporary files
        self.run_command_on_file('rm ', chgs)

    def _make_average_and_update(self, chgs, c_2, fltr, p):
        """
        Deal with all the CHGCAR files by calculating the averages and storing
        the result in Database. Dependendency of calculate_average_charge.

        """
        
        # Define the entry where average charge is saved
        entry_average = ['interface', 'bot_slab', 'top_slab']

        # Select the axis along which the average should be done
        if p['axis'] == 'all':
            axis = [0, 1, 2]
        else:
            axis = p['axis']
        if not isinstance(axis, list):
            axis = [axis]

        # Extract the CHGCAR objects
        chgcars = extract_chgcar_data(chgs)

        # Calculate the averages
        # average = []
        # entry = []
        data = {'average': {p['site']: {}}}
        for chg, en in zip(chgcars, entry_average):
            data['average'][p['site']][en] = {}
            # tmp_average = []
            # tmp_entry = []
            for ax in axis:

                # Calculate the average values along ax
                avg = chgcar_average_along_axis(chg, ax, to_plot=False)
                if ax == 2:
                    avg[0] = list(np.array(avg[0]) - c_2)

                # Save the data in the right position of the dictionary
                data['average'][p['site']][en][{0: 'x', 1: 'y', 2: 'z'}[ax]] = avg
                
                # import matplotlib.pyplot as plt
                # plt.plot(avg[0], avg[1])
                # plt.show()
                
                # tmp_average.append(avg)
                # tmp_entry.append(en + [str(self.axis_converter[ax])])
            # average.append(tmp_average)
            # entry.append(tmp_entry)

        # Update everything in the Database
        self.update_db(jsanitize(data), db_file=p['db_file'], database=p['database'],
                       collection=p['collection'], entry=['charge', p['check_entry']], fltr=fltr)
        # for avg, en in zip(average, entry):
            # for d, e in zip(avg, en):
                # self.update_db(jsanitize(d), db_file=p['db_file'], database=p['database'],
                                # collection=p['collection'], entry=e, fltr=fltr)

@explicit_serialize
class FT_ChargeDisplacement(FireTaskTribChem):
    """
    Given a list of chgcar objects and a list of operations ('+' or '-'),
    combines objects as indicated. Finds average of the combined chgcar object
    along the given axis, and finally performs an integration of said average.
	Returns the integral divided by the width of the integrated region.
	In short, it gives the average charge between two points along an axis.

    """

    _fw_name = 'Calculate charge displacement'
    required_params = ['mid', 'miller', 'collection', 'operations']
    optional_params = ['formula', 'name', 'db_file', 'database', 'axis', 'site',
                       'integrate_in', 'rescale', 'to_plot', 'check_entry']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Get the runtask parameters
        params = self.read_runtask('ChargeDisplacement', fw_spec)
        
        # Calculate the charge displacement
        self.calculate_charge_displacement(params)

        return FWAction(update_spec=fw_spec)

    def calculate_charge_displacement(self, p):
        """
        Retrieve the location of the CHGCAR from the local computer, then
        calculate and save the charge displacement.

        """

        # Find the document of the interface in the Database
        fltr = self.set_filter(p)
        field, _ = self.query_db(collection=p['collection'], db_file=p['db_file'],
                                 database=p['database'], fltr=fltr)

        # Define the location of the CHGCAR file and where to save data
        entry_chgcar = [['charge', p['check_entry'], 'data', p['site'], x, 'dir_name'] for x in ['interface', 'bot_slab', 'top_slab']]

        # Recover and clean the CHGCAR path to files from the database
        chgs = read_multiple_entry(field, entry_chgcar)

        if chgs is None:
            raise ValueError('Path to CHGCAR file not found. Call the workflow '
                             'with run_vasp=True first')
        chgs = [c.split(':')[1] + '/CHGCAR' for c in chgs]

        # Decompress the CHGCAR file
        self.run_command_on_file('gunzip --keep', [c + '.gz' for c in chgs])

        # Combine the CHGCAR files
        c_chgs = chgcar_algebra_from_file(chgs, p['operations'], p['axis'])

        # Calculate the charge displacement and the interface width
        chgdisp, width, curve, _ = charge_displacement(c_chgs, p['axis'], 
                                                       p['integrate_in'][0],
                                                       p['integrate_in'][1],
                                                       p['rescale'], p['to_plot'])
        # Shift the x of the curve back on zero, so interface is z=0
        i = 'interface_min' if p['site'] == 'min_site' else 'interface_max'

        if p['check_entry'] == 'short':
            c_2 = field['structure']['opt']['short'][i]['lattice']['c'] / 2
        else:
            c_2 = field['structure']['opt']['regular'][i]['lattice']['c'] / 2
        
        curve[0] = list(np.array(curve[0]) - c_2)

        chgdisp_tot, width_tot, _, _ = charge_displacement(c_chgs, p['axis'], 
                                                           rescale=p['rescale'],
                                                           to_plot=p['to_plot'])

        axis = {0: 'x', 1: 'y', 2: 'z'}[p['axis']]
        data = {
             'displacement': {
                p['site']: {
                    axis: {
                        'all':{
                            'integral': chgdisp_tot,
                            'width': width_tot
                        },
                        'interface': {
                            'integral': chgdisp,
                            'width': width,
                            },
                    'grid': jsanitize(curve),
                    'rescale': p['rescale']
                    }
                }
            }
        }

        # Update the charge displacement in the Database
        self.update_db(data, db_file=p['db_file'], database=p['database'],
                       collection=p['collection'], entry=['charge', p['check_entry']], fltr=fltr)

        # Remove the decompressed temporary files
        self.run_command_on_file('rm ', chgs)
