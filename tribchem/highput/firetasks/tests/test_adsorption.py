#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 5th 14:58:00 2021

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'July 5th, 2021'

from fireworks import Workflow, Firework, LaunchPad
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.firetasks.chemabs import FT_AdsorbateSurface

mid = 'mp-134'
formula = 'Al'
miller = [0, 0, 1]
slab_collection = 'PBE.slab_elements'
ad_collection = 'test'
adsorbate = 'O'
both_surfaces = True
database = 'tribchem'

ft_add = FT_AdsorbateSurface(mid=mid, formula=formula, miller=miller,
                             slab_collection=slab_collection, 
                             ad_collection=ad_collection, adsorbate=adsorbate, 
                             both_surfaces=both_surfaces, database=database)

fw_add = Firework([ft_add], spec={}, name='Testing adsorption')

wf = Workflow([fw_add], name='Testing adsorption')

lpad = LaunchPad.auto_load()
lpad.add_wf(wf)
rapidfire(lpad)
