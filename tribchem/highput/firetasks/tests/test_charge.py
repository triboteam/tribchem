#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 24 12:00:00 2021

Test the workflow to for the charge integration in vasp.

Author: Jacopo Mascitelli (jmascitelli)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Jacopo Mascitelli'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'May 24th, 2021'

import os

from pymatgen.core.structure import Structure
from fireworks import LaunchPad
from fireworks import Firework, Workflow

from tribchem.highput.firetasks.charge import FT_ChargeDisplacement
from tribchem.highput.database.navigator import NavigatorMP, StructureNavigator

currentdir = os.getcwd() + '/../../tests'

chgcar_files=[currentdir+'/CHGCAR_1',
			  currentdir+'/CHGCAR_2',
			  currentdir+'/CHGCAR_3']

FT_ChDisp = FT_ChargeDisplacement(operation_list=['+','-'], 
                                  chgcar_list=chgcar_files, mid_1='mp-148', 
                                  mid_2='mp-148', miller_1=[0,0,1], 
                                  miller_2=[0,0,1], functional='PBE', upsert=True)
FW_ChDisp = Firework([FT_ChDisp], name='Test FT_ChargeDisp')
WF_ChDisp = Workflow([FW_ChDisp], name='Test FT_ChargeDisp')

lpad = LaunchPad.auto_load()
lpad.add_wf(WF_ChDisp)