#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 13 09:27:05 2021

Test the Firetasks of the `structure` module.

Author: Omar Chehaimi (@omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna

"""

from fireworks import Firework, Workflow
from fireworks import LaunchPad
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.database.query import insert_field
from tribchem.highput.database.navigator import NavigatorMP
from tribchem.highput.utils.vasp_tools import VaspInputSet
from tribchem.highput.firetasks.structure import FT_RunVaspSimulation, FT_RunVaspOnTheFly

# Get the bulk from the online Database: Materials Project
formula = 'Mg'
mid = 'mp-110'
nav_mp = NavigatorMP()
structure, mid = nav_mp.get_low_energy_structure(
   chem_formula=formula, 
   mp_id=mid)
functional = 'PBE'
insert_field(field={"mid": mid,
                    "formula": formula,
                    "structure_initial": structure.as_dict(), 
                    "comp_params":{
                        "functional": "PBE",
                        "energy_tol": 0.001,
                        "volume_tol": 0.01,
                        "bm_tol": 0.01,
                        "is_metal": True,
                        "use_spin": False,
                        "use_vdw": False
                    }}, 
             collection=functional+'.bulk',
             database='tribchem')
entry = ['structure_initial']

# ft_run = FT_RunVaspSimulation(mid=mid, functional=functional, 
#                               collection=functional+'.bulk', 
#                               entry=entry, tag='test_run_vasp')

vis = VaspInputSet(structure, comp_params={}, calc_type='bulk_from_scratch').get_vasp_settings()
ft_run = FT_RunVaspOnTheFly(structure=[structure]*2, vis=[vis]*2, calc_type='bulk_from_scratch',
                            tag=['test-1']*2)
fw_run = Firework([ft_run], name='Test FT_RunVaspSimulation')
wf_run = Workflow([fw_run], name='Test FT_RunVaspSimulation')

# Run the workflow
lpad = LaunchPad.auto_load()
lpad.add_wf(wf_run)
# rapidfire(lpad)
