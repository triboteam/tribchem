#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 16:56:54 2021

Test the Firetasks of the `core` module.

Author: Gabriele Losi (glosi000)
Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna

"""

from fireworks import Firework, Workflow, LaunchPad
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.firetasks.dbtask import FT_MoveTagResults
from tribchem.highput.database.navigator import Navigator


# Define input parameters
db_file = None

database_from = 'test'
collection_from = 'TEST'
entry_from = 'structure_fromMP'
tag = 'custom-1'
tag_key = 'mid'

mid = 'mp-126'
miller = [0, 1, 2]
database_to = 'test'
collection_to = "PBE.slab_data"
entry_to = 'structure_fromMP'
check_entry = None

override = False
cluster_params = {}

# Initialize the collections for the tests
# nav = Navigator(db_file=db_file)
# data = {'transfer_test': True,
#         'data': {'energy': 1,
#                  'energy2': 2}
# }
# nav.insert_data(collection=collection_from, data=data)

# Instantiate the Firetask and create a WF
ft = FT_MoveTagResults(mid=mid,
                       collection_from=collection_from,
                       collection_to=collection_to,
                       tag=tag,
                       tag_key=tag_key,
                       db_file=db_file,
                       database_from=database_from,
                       database_to=database_to,
                       miller=miller,
                       check_entry=check_entry,
                       entry_to=entry_to,
                       entry_from=entry_from,
                       override=override,
                       cluster_params=cluster_params)
                       
wf = Workflow([Firework([ft])])

# Run the workflow
lpad = LaunchPad.auto_load()
lpad.add_wf(wf)
rapidfire(lpad)
