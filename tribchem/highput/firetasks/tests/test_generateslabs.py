#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 16:27:12 2021

Test the Firetasks of the `slabs` module.

Author: Gabriele Losi (glosi000)
Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna

"""

from pymatgen.core.structure import Structure
from fireworks import Firework, Workflow, LaunchPad
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.firetasks.slabs import FT_GenerateSlabs
from tribchem.highput.database.query import query_field


# Define input parameters
mid = 'mp-66'
collection = 'test'
miller = [[0, 0, 1], [1, 1, 0], [1, 1, 1]]
db_file = None
database = 'test'
thickness = [18]
thick_bulk = 4
vacuum = 10
ext_index = 0
in_unit_planes = True
entry = [['thickness', 'data_' + str(thk), 'init'] for thk in thickness]

# Call the Navigator and retrieve the bulk structure
# field = query_field({'mid': mid}, collection='PBE.bulk_elements', database='tribchem')
# structure = field['structure']['init']
structure = Structure.from_file('POSCAR_bulk_primitive')


# Instantiate the Firetask and create a WF
ft = FT_GenerateSlabs(structure=structure,
                      mid=mid,
                      miller=miller,
                      collection=collection,
                      db_file=db_file,
                      database=database,
                      thickness=thickness,
                      thick_bulk=thick_bulk,
                      vacuum=vacuum,
                      symmetrize=False,
                      ext_index=ext_index,
                      in_unit_planes=in_unit_planes,
                      entry=entry)
wf = Workflow([Firework([ft])])

# Run the workflow
lpad = LaunchPad.auto_load()
lpad.add_wf(wf)
rapidfire(lpad)


for m in miller:
    field = query_field({'mid': mid, 'miller': m}, collection=collection, database=database)
    for t in thickness:
        structure = Structure.from_dict(field['thickness']['data_'+str(t)]['init'])
        structure.to('poscar', 'POSCAR_'+str(t)+'_'+str(m[0])+str(m[1])+str(m[2]))
