#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 15:16:33 2021

Test the Firetask that store a general data in the Database.

@author: glosi000
"""

from fireworks import LaunchPad, Workflow, Firework
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.firetasks.dbtask import FT_DataInDB


db_file = None
database = 'test'
collection = 'TEST'
is_update = True
fltr = {'mid': 'custom-1'}
entry = [['comp_params'], ['material_params']]

data = [
    {
      "bm_tol":0.01,
      "energy_tol":0.001,
      "functional":"PBE",
      "use_spin":False,
      "volume_tol":0.001
    },
    {
      "min_thick":4,
      "min_vacuum":15
   }
]

ft = FT_DataInDB(data=data, collection=collection, db_file=db_file, entry=entry,
                 database=database,  is_update=is_update, fltr=fltr)

fw = Firework([ft], name = 'Data_in_DB_test')
wf = Workflow([fw], name='test_init')

lpad = LaunchPad.auto_load()
lpad.add_wf(wf)
rapidfire(lpad)
