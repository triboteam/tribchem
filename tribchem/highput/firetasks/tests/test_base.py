#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 14:53:22 2021

Test to basic Firetasks constructions.

@author: glosi000

"""


from tribchem.highput.firetasks.core import FireTaskTribChem

ft = FireTaskTribChem()
a = ft.query_db(db_file=None, database='tribchem', collection='PBE.slab_data', entry=None,
                mpid='mp-126', miller=[1, 1, 1])  # kwargs to specify filter
print(a)

