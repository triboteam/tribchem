#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 15:15:58 2021

Test the Firetask that store a material in the Database.

@author: glosi000
"""

from pymatgen.core.surface import Structure, Lattice

from fireworks import LaunchPad, Workflow, Firework
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.firetasks.dbtask import FT_MaterialInDB


# Let's start with fcc Cu
lattice = Lattice.cubic(3.508)
structure = Structure(lattice, ["Cu", "Cu", "Cu", "Cu"],
                [[0,0,0], [0,0.5,0.5], [0.5,0,0.5], [0.5,0.5,0]])
mid = 'custom-1'
db_file = None
database = 'test'
collection = 'TEST'
is_update = True
name = 'Test Cu'
upsert = True

ft = FT_MaterialInDB(mid=mid, structure=structure, collection=collection, 
                     db_file=db_file, database=database, name=name, upsert=upsert)

fw = Firework([ft], name = 'Data_in_DB_test')
wf = Workflow([fw], name='test_init')

lpad = LaunchPad.auto_load()
lpad.add_wf(wf)
rapidfire(lpad)
