
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 18:04:45 2021

Firetasks to manage the interaction with the database and the infrastructures
of the workflow.

@author: glosi000

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Prof. M.C. Righi, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 26th, 2021'


from tribchem.highput.database.navigator import Navigator
from fireworks import explicit_serialize, FWAction

from tribchem.highput.firetasks.core import FireTaskTribChem
from tribchem.highput.database.dbtools import write_multiple_entry
from tribchem.highput.utils.errors import InitError
from tribchem.highput.database.filetools import FileTools


@explicit_serialize
class FT_DataInDB(FireTaskTribChem):

    _fw_name = 'Insert or Update information into a Database'
    required_params = ['data', 'collection']
    optional_params = ['db_file', 'database', 'entry', 'fltr', 'update_op', 
                       'upsert', 'to_mongodb', 'duplicates', 'message']

    def run_task(self, fw_spec):
        """ Run the FireTask.
        """
        
        # Read required and optional input arguments
        params = self.read_runtask('DataInDB', fw_spec)
        
        # Update or insert the data in the database
        self.data_in_db(params)
        
        return FWAction(update_spec=fw_spec)
    
    def data_in_db(self, p):
        """
        Insert a given data dictionary inside the Database.

        """
        
        # Check if filter is defined
        if p['fltr'] is None:
            raise InitError('Wrong input argument. If is_update is True, '
                            'a filter cannnot be None')
        
        doc, _ = self.query_db(p['collection'], p['db_file'], p['database'],  
                               fltr=p['fltr'])
        
        # Make an insert
        if doc is None:
            # Prepare data dictionary and define the field to be stored in DB
            field = write_multiple_entry(p['data'], p['entry'], False)
            field.update(p['fltr'])

            # Store data in DB
            self.insert_db(field, p['collection'], p['db_file'], p['database'], 
                           p['duplicates'], p['message'])
        else:
            # Update data in db
            data = write_multiple_entry(p['data'], p['entry'])
            self.update_db(data, p['collection'], p['entry'], p['db_file'],
                           p['database'], p['fltr'], p['update_op'], 
                           p['upsert'], p['to_mongodb'])

@explicit_serialize
class FT_MaterialInDB(FT_DataInDB):
    """ 
    Put a general material structure in a Database.
    
    Parameters
    ----------
    structure : str
        Location of the dictionary with the material data in the spec.

    comp_paramas : str
        Location of the dictionary with computational parameters in the spec.

    db_file : str or None, optional
        Location of the database. If it is None, it will search for 'localhost'.
    
    """
    
    _fw_name = 'Insert or update a material structure in a Database'
    required_params = ['structure', 'mid']
    optional_params = ['db_file', 'database', 'collection', 'entry', 'formula',
                       'name', 'miller', 'material_params', 'comp_params', 
                       'update_op', 'upsert', 'to_mongodb', 'fill_defaults']

    def run_task(self, fw_spec):
        """ Run the FireTask.
        """

        # Read required and optional input arguments
        params = self.read_runtask('MaterialInDB', fw_spec)
        
        # Put bulk and slab in high level DB
        self.material_in_db(params)

    def material_in_db(self, p):
        """
        Update a material structure, typically a bulk, in a Database. The field
        is identifiable by means of three parameters: 'mid', 'formula', 'name'.
        Only the first one is mandatory, the other are optional. It is also
        possible to provide a set of Miller index, useful to store a slab in
        the Database.

        Parameters
        ----------
        p : dict
            Dictionary with the Firetask parameters

        """

        # Update material and computational parameters, set formula
        formula = p['formula'] if p['formula'] is not None else p['structure'].composition.reduced_formula
        if p['fill_defaults']:
            comp_params = self.read_params(dfk='comp_params', 
                                           dict_params=p['comp_params'], 
                                           allow_unknown=True)
        else:
            comp_params = p['comp_params']

        # Create a filter to update the material in the Database.
        # This filter will unequivocally identify a material: mid, formula, name
        fltr = {
            'mid': p['mid'],
            'formula': formula
            }
        if p['name'] is not None:
            fltr.update({'name': p['name']})
        if p['miller'] is not None:
            fltr.update({'miller': p['miller']})

        # Add computational parameters and material parameters if present
        fltr.update({'comp_params': comp_params})
        if p['material_params'] != {}:
            fltr.update({'material_params': p['material_params']})

        # Create a new dictionary params tp call the parent method and update db
        params = p.copy()
        for key in ['structure', 'mid', 'name', 'comp_params', 'material_params']:
            params.pop(key)

        params['data'] = p['structure'].as_dict()
        params['fltr'] = fltr
        params['duplicates'] = False
        params['message'] = None

        super().data_in_db(params)

@explicit_serialize
class FT_MoveTagResults(FireTaskTribChem):
    """
    Firetask to copy some generic data from a certain location of the database 
    to another one. In principle this Firetask could be used in a very general
    way. The most common usage is to locate the results of an Atomate Workflow
    in the low level database and move output data of interest to the high level
    database of interest. 
    
    The Firetask does the following steps:

    1 - If `check_key` is not None, a control is done to understand if data is 
    already present and eventually stop the transfer to avoid overriding.
    The location of the data in the db is identified by means of: `db_file`, 
    `database_to`, `collection_to`. The MongoDB field is identified with `mid`
    (and `miller`) and data is retrieved. If data_dict['check_key']: stop.

    2 - The field containing the data of interest is extracted. The query is 
    done on: `db_file`, `database_from`, `collection_from`. The correct field
    is identified using the filter:
        {
            `tag_key`: `tag`
        }

    3 - Once the field dictionary has been retrieved, the data to be transferred
    is identied by `entry_from`, which could be: str, list, list of lists.
    To transfer more data it is necessary to use lists of lists, where each
    list contains the nested keys within the extracted dictionary containing 
    the data that I want to transfer.

    4 - The selected data is stored in the destination database, the location is
    identified by: `db_file`, `database_to`, `collection_to`, `entry_to`.
    `entry_to` can be again, str, list or list of lists. To identify a single 
    element it is necessary to use str or list, the latter in the case of a
    data value nested within the source or destination dictionary. However, if
    you want to transfer more data, it is mandatory to use list of lists, and
    in that case len(`entry_from`) == len(`entry_to`).
    Warning: The Firetask does not cancel the source data!

    Examples
    --------
    Here is a simple example explaining how MoveTagResults logically works.

    - Information concerning the source data and location:
        db_file = None
        database_from = "FireWorks"
        collection_from = "coll.tasks"
        entry_from = [['test', 'energy'], ['test', 'energy2']]

    - Information concerning the source tag:
        tag = True
        tag_key = "transfer_test"

    - Information concerning the destination location:
        mid = 'mp-126'
        db_file = None
        database_to = "tribchem"
        collection_to = "PBE.slab_data"
        entry_to = [['energy'], ['data_back', 'energy2']]
        check_key = 'is_done'

    a) The source field would be something like that:
    {
        "_id" : ...,
        "transfer_test" : true,
        "test" : {
            "energy" : 10,
            "energy2" : 50
        }
    }

    b) The destination field would be something like that:
     {
        "_id" : ...,
        "mid" : "mp-126"
    }

    Running the Firetasks you would:

    1. Check b-field to see if a key named 'check_key' is present. It is not, so
       the process will continue.
    2. Identify univocally the a-field to be the correct source containing the 7
       data of interest. This is done matching tag_key and key with the entry
       of the field dictionary: 'transfer_test'.
    3. Extract both energy and energy2 values from the a-field.
    4. Find the exact destination location with a filter based on mid and 
       place there, following the path provided by 'entry_to'.

    In the end, the b-field becomes:
     {
        "_id" : ...,
        "mid": "mp-126",
        "energy" : 10,
        "data_back" : {
            "energy2" : 50,
        }
    }

    """

    required_params = ['mid', 'collection_from', 'collection_to', 'tag']
    optional_params = ['db_file', 'database_from', 'database_to', 'formula', 
                       'name', 'miller', 'adsorbate', 'check_entry', 'entry_to',
                       'entry_from', 'override', 'tag_key', 'cluster_params',
                       'update_op', 'upsert', 'to_mongodb']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Read required and optional input arguments
        params = self.read_runtask('MoveTagResults', fw_spec)

        # Check if a check_key is already present in the destination Database
        is_done = self.is_done(params)
        
        if not is_done or (is_done and params['override']):
            
            # Extract the information and store in destination db
            field, field_entry = self.query_db(params)
            self.update_db(field_entry, params)

            # Manage stdout, save a local poscar with results
        #     wf = self.user_output(field, params, dfl)
        #     if wf is not None:
        #         return FWAction(detours=wf, update_spec=fw_spec)
            
        #     else:
        #         return FWAction(update_spec=fw_spec)
        # else:  
        #     return FWAction(update_spec=fw_spec)
    
    def is_done(self, p):
        """
        Check if there exists an entry called 'check_entry' in the destination
        field. If it is found the transfer is stopped.

        Parameters
        ----------
        p : dict
            All the input parameters of the Firetasks, placed in a dictionary.

        Returns
        -------
        is_done : bool
            If a simulation output is already present in the database. If it
            is True, the calculation will not be performed.

        """
        
        # Check if the destination collection does exist
        #MoveTagResultsError.check_collection(p['collection_to'])

        # Generate a filter to query the database
        fltr = super().set_filter(p)

        # Check if the data is already present in destination
        is_done = super().is_done(db_file=p['db_file'],
                                  collection=p['collection_to'],
                                  database=p['database_to'],
                                  entry=p['check_entry'],
                                  fltr=fltr)

        return is_done

    def query_db(self, p):
        """
        Identify the correct field in `collection_from` by using tags and extract
        the output data of interests with `entry_from`.

        """

        # Define a custom filter from a retrieving tag passed by user
        fltr = {p['tag_key']: p['tag']}

        # Query the database for the tag_key-tag combination as filter
        field, field_entry = super().query_db(db_file=p['db_file'],
                                              collection=p['collection_from'],
                                              entry=p['entry_from'],
                                              database=p['database_from'],
                                              fltr=fltr)

        return field, field_entry

    def update_db(self, field_entry, p):
        """
        Store the results to the destination database and collection.
        
        """

        # Generate a filter to query the database and then store data
        fltr = super().set_filter(p)

        super().update_db(data=field_entry, collection=p['collection_to'],
                          entry=p['entry_to'], db_file=p['db_file'], 
                          database=p['database_to'], fltr=fltr, 
                          update_op=p['update_op'], upsert=p['upsert'], 
                          to_mongodb=p['to_mongodb'])

@explicit_serialize
class FT_SaveVASPOutputs(FireTaskTribChem):
    """
    FireTask to save the VASP output files generated during the execution of the
    calculations.
    
    Author: Omar Chehaimi (omarchehaimi)

    """
    
    required_params = ['tag', 'folder']
    optional_params = ['db_file', 'custom_name']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """
        
        # Read required and optional input arguments
        params = self.read_runtask('SaveVASPOutputs', fw_spec)

        # Move the VASP output files
        nav = Navigator(db_file=params['db_file'])
        if isinstance(params['tag'], list):
            if len(params['tag']) != len(params['custom_name']):
                raise ValueError("The dimension of tag vector is {} and the "
                                 "one of custom name is {}. "
                                 "Please provide for each tag the proper custom name."
                                 .format(len(params['tag']), len(params['custom_name'])))

            tag_name = zip(params['tag'], params['custom_name'])
            for tag, name in tag_name:
                FileTools.move_files(nav, main_folder=params['folder'], 
                                     task_label=tag,
                                     custom_name=name)
        else:    
            FileTools.move_files(nav, main_folder=params['folder'], 
                                 task_label=params['tag'],
                                 custom_name=params['custom_name'])
