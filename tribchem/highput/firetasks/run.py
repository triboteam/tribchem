#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 10:16:55 2021

Collection of simple Firetasks that can be used to start a convergence process, 
a calculation, or a detour within bigger Workflows.

The module contains the following Firetasks:

    - FT_SlabOptThick 
    Starts a subworkflow to perform a convergence process to find the optimal 
    thickness of a slab structure. First step to be called within a workflow.
    
    - FT_StartKpointsConvo
    Start a subworkflow to perform a convergence of the k-points.
    
    - FT_StartAdhesion
    Start the subworkflow to perform the adhesion energy calculation of an 
    interface.

    Author: Gabriele Losi (glosi000), Omar Chehaimi (omarchehaimi), Jacopo Mascitelli (jmascitelli)
    Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi, Omar Chehaimi, Jacopo Mascitelli'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 22nd, 2021'


import os
from pymatgen.core.structure import Structure
from pymatgen.core.surface import Slab
from fireworks import FWAction, explicit_serialize

from tribchem.physics.base.solidstate import generate_slabs
from tribchem.highput.database.navigator import Navigator
from tribchem.highput.firetasks.core import FireTaskTribChem, FireTaskInterface
from tribchem.highput.workflows.encut_wfs import EncutWF
from tribchem.highput.workflows.kpoint_wfs import KpointWF
from tribchem.highput.workflows.interface import InterfaceWF
from tribchem.highput.workflows.electronics import ElectronicsWF
from tribchem.highput.workflows.chemabs_wfs import ChemAdsWF
from tribchem.highput.workflows.cohesion_wfs import BulkWF
from tribchem.highput.workflows.slabs_wfs import SlabWF
from tribchem.highput.utils.tasktools import (
    read_default_params,
    select_struct_func
)
from tribchem.highput.database.dbtools import read_one_entry
from tribchem.highput.utils.errors import (
    SlabOptThickError,
    EncutConvoError,
    InterfaceMatchError
)

from tribchem.highput.utils.manipulate_struct import (
    apply_interface_shifts,
    material_id_from_list, 
    generate_interface_ids,
    generate_interface_comp_params,
    ManipulateStruct,
    structure_to_slab
    )
from tribchem.highput.utils.vasp_tools import VaspInputSet


currentdir = os.path.dirname(__file__)


@explicit_serialize
class FT_StartEncutConvo(FireTaskTribChem):
    """
    Start a subworkflow to converge the encut energy and the lattice parameter
    of a given bulk structure.

    Parameters
    ----------
    mid : str
        Material id.

    collection : str
        Collection of the database where to save the results.
    
    flag : str
        An identifier to find the results in the database. It is strongly
        suggested to use the proper Materials-ID from the MaterialsProject
        if it is known for the specific input structure. Otherwise use something
        unique which you can find again.
    
    formula : str
        Chemical formula of the bulk. The default is None.

    name : str or None, optional
        Third possible optional identifier for a bulk. Useful in cases
        of homonymes, e.g. C diamond, C graphene. The default is None.

    db_file : str
        Location of the database. If it is None, it will be searched for a
        'localhost' on the hosting machine. The default is None.

    low_level : str
        Name of the table of the "low level" database, saved in db_file. The 
        intermediate calculations and raw data during will be saved here. If 
        nothing is passed the Firework database is used. The default is None.

    high_level : str
        Name of the databashigh level database. The default is 'tribchem'.

    entry : list
        List of (nested) keys indicating the position of the pymatgen dict of
        the bulk in the MongoDB document. The default is ['structure', 'init'].

    check_encut : str
        List of nested keys where are saved the results of the energy cutoff
        convergence. In case the value associated to this key is not empty the 
        convergence is done. The default is encut_info.

    deformations : list
        List of deformation matrices for the fit to the EOS. Defaults to None,
        which results in 5 volumes from 90% to 110% of the initial volume.

    encut_start : float
        Starting encut value for the first run. The default is None.

    encut_incr : float
        Increment for the encut during the convergence. The default is 50.

    n_converge : int
        Number of calculations that have to show the same energy as the last
        one as to signify convergence. The default is 3.
        
    kdens_default : float
        K-points density. The default is 10.
    
    upsert : bool
        PyMongo parameter for the update_one function. If True update_one 
        performs an insertion if no documents match the filter.
    
    cluster_params : dict, optional
        Optional params to print data and/or interact with clusters. The default is {}.

    override : bool, optional
        Decide if the dft simulation should be done in any case, despite the
        possible presence of previous results.

    """
    
    _fw_name = 'Start Encut and Lattice Parameter Convergence'
    required_params = ['mid', 'collection', 'flag']
    optional_params = ['formula', 'name', 'db_file', 'low_level', 
                       'high_level', 'entry', 'check_encut', 'deformations',
                       'encut_start', 'encut_incr', 'n_converge', 'kdens_default',
                       'upsert', 'cluster_params', 'override']

    def run_task(self, fw_spec):

        # Read required and optional input arguments
        params = self.read_runtask('StartEncutConvo', fw_spec)

        # Check if cutoff info are already present the high level Database
        is_done = self.is_done(params)

        # Get the main folder where to save all the VASP output files
        if params['formula'] is None:
            raise ValueError('Formula is None, please insert in the bulk entry the formula')
        params['main_folder'] = self.get_main_folder(params)
        
        # Run the lattice parameter and encut convergence
        if params['override'] or not is_done:
            # Add the missing parameters to parmas
            params = self.add_parmas(params)

            # Call the function
            wf = self.run_func(EncutWF.converge_encut_bulk, **params)
            
            return FWAction(detours=wf, update_spec=fw_spec)
        
        # Continue the workflow
        else:
            
            return FWAction(update_spec=fw_spec)

    def is_done(self, p):
        """
        Check if the calculations has already been done

        """

        # Generate a filter to query the database
        fltr = super().set_filter(p)

        # Check if the data is already present in destination
        is_done = super().is_done(db_file=p['db_file'],
                                  collection=p['collection'],
                                  database=p['high_level'],
                                  entry=p['check_encut'],
                                  fltr=fltr)
        
        return is_done

    def query_structure(self, p):
        """
        Retrieve the bulk structure whose cutoff and lattice has to be converged.

        """

        # Define a custom filter from a retrieving tag passed by user
        fltr = super().set_filter(p)

        # Query the database to retrieve the structure
        field, structure = super().query_db(db_file=p['db_file'],
                                            collection=p['collection'],
                                            entry=p['entry'],
                                            database=p['high_level'],
                                            fltr=fltr)

        if field is None:
            raise EncutConvoError('Document not found in Database')
        if structure is None:
            raise EncutConvoError.entry(p['entry'])

        # Define the computational parameters of the slab and the structure
        dfl = currentdir + '/' + 'defaults.json'
        comp_params = field.get('comp_params', {})
        comp_params = read_default_params(dfl, 'comp_params', comp_params,
                                          allow_unknown=True)
        structure = Structure.from_dict(structure)

        return structure, comp_params

    def get_main_folder(self, p):
        """
        Get the main folder where to save the VASP output files.

        """
        
        fol_path = str(p['collection']) + '/' + str(p['formula']) + '/' + 'encut/'
        main_folder = self.save_files(fol_path, datetime=False)
        
        return main_folder
    
    def add_parmas(self, p):
        """
        Add the missing arguments of the function to be called to the dictionary 
        p.

        """
        
        # Get tje structure and the computational parameters
        structure, comp_params = self.query_structure(p)
        
        # Add items to the dictionary
        p['structure'] = structure
        p['comp_params'] = comp_params
        p['bm_tol'] = comp_params['bm_tol']
        p['volume_tol'] = comp_params['volume_tol']
        
        return p

@explicit_serialize
class FT_StartKpointsConvo(FireTaskTribChem):
    """
    Start a subworkflow for converging the k-points.
    
    Parameters
    ----------
    mid : str
        MP-id of the structure from the MP database.
    
    structure : pymatgen.core.structure.Structure
        Bulk structure on which converge the k-points.
    
    db_file : str or None
        Path to the location of the database. If nothing is provided it will be
        searched by env_check from Atomate. The default is None.    
    
    kdens_start : int
        Density of k-points...

    kdens_incr : float

    n_converge : int

    """

    _fw_name = 'Start k-points convergence'
    required_params = ['mid', 'collection', 'flag']
    optional_params = ['formula', 'name', 'db_file', 'low_level', 'high_level',
                       'entry', 'kdens_start', 'kdens_incr', 'n_converge',
                       'upsert', 'override', 'cluster_params']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Get the runtask parameters
        params = self.read_runtask('StartKpointsConvo', fw_spec)
        
        # Query the structure and see if kdens is already done
        structure, comp_params, is_done = self.query_structure(params)
        
        if params['override'] or not is_done:
            # Get the main folder where to save all the VASP output files
            if params['formula'] is None:
                raise ValueError('Formula is None, please insert in the bulk entry the formula')
            params['main_folder'] = self.get_main_folder(params)
            
            # Add the missing parameters to parmas
            params = self.add_parmas(p=params, structure=structure, 
                                     comp_params=comp_params, fw_spec=fw_spec)

            # Create the subworkflow to start the detour
            wf = self.run_func(KpointWF.converge_kpoints, **params)

            return FWAction(detours=wf, update_spec=fw_spec)

        else:
            return FWAction(update_spec=fw_spec)
    
    def query_structure(self, p):

        # Read the structure from the database
        fltr = self.set_filter(p)
        field, structure = self.query_db(collection=p['collection'], fltr=fltr,
                                         db_file=p['db_file'], entry=p['entry'],
                                         database=p['high_level'])
        is_done = False
        if 'data' in field.keys():
            if 'kdens_info' in field['data'].keys(): 
                is_done = True

        comp_params = field['comp_params']
        
        if not structure or not comp_params:
            raise ValueError("structure or comp_parmams is Null.") 
        
        return structure, comp_params, is_done

    def get_main_folder(self, p):
        """
        Get the main folder where to save the VASP output files.

        """
        
        fol_path = str(p['collection']) + '/' + str(p['formula']) + '/' + 'kpoints/'
        main_folder = self.save_files(fol_path, datetime=False)
        
        return main_folder
    
    def add_parmas(self, p, structure, comp_params, fw_spec):
        """
        Add the missing arguments of the function to be called to the dictionary 
        p.

        """
        
        # Get the structure from the dictionary structure
        structure = Structure.from_dict(structure)

        # Read computational parameters and substitute for missing ones
        dfl = currentdir + '/' + 'defaults.json'
        comp_params = read_default_params(dfl, 'comp_params', comp_params,
                                          allow_unknown=True)
        
        # Add items to the dictionary
        p['structure'] = structure
        p['comp_params'] = comp_params
        p['spec'] = fw_spec
        
        return p

@explicit_serialize
class FT_StartCohesionBulk(FireTaskTribChem):
    
    _fw_name = 'Start a subworkflow to calculate cohesion energy for bulk'

    required_params = ['mid', 'collection_bulk', 'collection_atoms']
    optional_params = ['formula', 'name', 'db_file', 'database_bulk', 
                       'database_atoms', 'entry_bulk', 'check_bulk',
                       'check_atoms', 'check_entry', 'cluster_params', 
                       'half_kpts_first_relax', 'override']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """ 

        # Read required and optional input arguments
        params = self.read_runtask('StartCohesionBulk', fw_spec)

        # Check if a convergence calculation of the slab thickness is present
        is_done = self.is_done(params)

        # Get the main folder where to save all the VASP output files for the bulk
        params['main_folder_bulk'] = self.get_main_folder(
            p=params, collection=params['collection_bulk'])
        # For the single atom
        params['main_folder_at'] = self.get_main_folder(
            p=params, collection=params['collection_atoms'])
        
        # Start a subworkflow to converge the thickness if not already done
        if is_done and not params['override']:
            return FWAction(update_spec=fw_spec)  # Continue the Workflow
        
        else:
            # Add the missing parameters to parmas
            params = self.add_parmas(p=params, fw_spec=fw_spec)

            # Create the subworkflow to run a convergence detour
            wf = self.run_func(BulkWF.cohesion_energy, **params)

            return FWAction(detours=wf, update_spec=fw_spec)

    def is_done(self, p):
        """
        Query the database, and check if cohesion energy is already done.

        """

        fltr = self.set_filter(p, keys=['mid', 'name', 'formula'])
        is_done = super().is_done(collection=p['collection_bulk'], fltr=fltr,
                                  db_file=p['db_file'], database=p['database_bulk'],
                                  entry=p['check_entry'])

        return is_done

    def get_main_folder(self, p, collection):
        """
        Get the main folder where to save the VASP output files.

        """
        
        if p['formula']:
            fol_path = str(collection) + '/' + str(p['formula']) + '/output/'
        else:
            nav = Navigator(db_file=p['db_file'], high_level=p['database_bulk'])
            formula = nav.find_data(collection=p['collection_bulk'],
                                    fltr={'mid': p['mid']})
            fol_path = str(collection) + '/' + str(formula) + '/output/'
        
        main_folder = self.save_files(fol_path, datetime=False)
        
        return main_folder
        
    def query_structure(self, p):
        """
        Get the optimal bulk structure from the database, to execute a static
        simulation with VASP, and calculate its properties.

        """
        
        fltr = self.set_filter(p, keys=['mid', 'formula', 'name'])
        field, _ = self.query_db(collection=p['collection_bulk'], fltr=fltr, 
                                 db_file=p['db_file'], database=p['database_bulk'])

        bulk = Structure.from_dict(read_one_entry(field, p['entry_bulk']))
        comp_params = field['comp_params']

        if bulk is None:
            raise ValueError('Bulk has not been found in database:\n'
                             'db_file : {}\ndatabase : {}\ncollection : {}\n'
                             'entry : {}'.format(p['db_file'], p['database_bulk'],
                                                 p['collection_bulk'], 
                                                 p['bulk_entry']))
        if comp_params in [None, {}]:
            raise ValueError('Computational parameters have not been found '
                             'for your bulk')

        return bulk, comp_params

    def add_parmas(self, p, fw_spec):
        """
        Add the missing arguments of the function to be called to the dictionary 
        p.

        """
        
        # Get the structure and the computational parameters
        structure, comp_params = self.query_structure(p)
        
        # Add the items to the dictionary
        p['structure'] = structure
        p['comp_params'] = comp_params
        p['spec'] = fw_spec
        p['check_bulk'] = ['data', 'energy']
        p['check_atoms'] = ['data', 'energy']
        
        return p

@explicit_serialize
class FT_SlabOptThick(FireTaskTribChem):
    """
    Start a subworkflow as a detour to calculate the optimal thickness for a 
    slab generated from a supplied bulk structure and a certain orientation 
    (miller index). The thickness can be converted either by evaluating how the 
    surface energy or the lattice parameters (not implemented) change with the 
    number of atomic planes.

    Parameters
    ----------
    mid : str
        MP-id of the structure from the MP database.

    miller : list of int, or str
        Miller indexes (h, k, l) to select the slab orientation.

    functional : str
        Functional for the pseudopotential to be adopted.

    db_file : str or None
        Path to the location of the database. If nothing is provided it will be
        searched by env_check from Atomate. The default is None.

    low_level : str or None, optional
        Name of the table of the "low level" database, saved in db_file. The 
        intermediate calculations and raw data during will be saved here. If 
        nothing is passed the Firework database is used. The default is None.

    high_level : str, optional
        Name of the table of the "high level" database, saved in db_file.
        The slab optimal thickness will be saved here. The slab energy and
        surface energy will be saved too if conv_kind is 'surfene'.
        The default is 'tribchem'.

    conv_kind : str, optional
        Type of convergence to be performed. Allowed values are: 'surfene', 
        'alat'. The latter is not implemented yet. The default is 'surfene'.

    relax_type : str, optional
        The type of relaxation to be performed during the simulation, to be feed
        to `get_custom_vasp_relax_settings`. The default is 'slab_pos_relax'.

    thick_min : int, optional
        Number of atomic layers for the slab to be used as starting point. In
        case of low parallelization this gives the value of atomic layers for
        the only slab which is created and relaxed at each step after the first.
        The default is 4.
        
    thick_max : int, optional
        Maximum number of allowed atomic layers for the slab. If convergence is
        not reached this value is considered the optimal one. The default is 12.

    thick_incr : int, optional
        The incremental number of atomic layers to be added at each step during
        the iterative procedure. The default is 2.

    vacuum : int or float, optional
        Vacuum to be used for creating in the slabs cells. The default is 10.

    in_unit_planes : bool, optional
        Decide if thick_min, thick_max, thick_incr, and vacuum are expressed in
        units of number of atomic layers or Angstrom. The default is True.
    
    ext_index : int, optional
        Use the ext_index element from SlabGenerator.get_slabs as a slab.
        The default is 0.
    
    conv_thr : float, optional
        Threshold for the convergence process. The default is 0.025.

    bulk_entry : str or list or None, optional
        Name of the custom bulk dictionary, to be retrieved from the high level
        database and to be used to build the slabs. Bulks are identified by 
        mid and functional but there might be different structures of the
        same material. The default is "structure_fromMP".

    check_entry : str or None, optional
        Where to search for the information about the optimal thickness in the
        slab dictionary in the high level database. The default is 'opt_thickness'.
    
    cluster_params : dict, optional
        Optional params to print data and/or interact with clusters. The default is {}.

    override : bool, optional
        Decide if the dft simulation should be done in any case, despite the
        possible presence of previous results.

    """
    
    _fw_name = 'Start a subworkflow to converge slab thickness'

    required_params = ['mid', 'miller', 'collection_bulk', 'collection_slab']
    optional_params = ['formula', 'name_bulk', 'name_slab', 'db_file', 
                       'database', 'conv_kind', 'calc_type', 'thick_min', 
                       'thick_max', 'thick_incr', 'vacuum', 'in_unit_planes',
                       'ext_index', 'conv_thr', 'parallelization',
                       'bulk_entry', 'check_entry', 'cluster_params', 'override',
                       'override_slab_db', 'functional', 'is_metal', 'use_spin', 'vdw']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """ 

        # Read required and optional input arguments
        params = self.read_runtask('SlabOptThick', fw_spec)
        
        # Get the main folder where to save all the VASP output files
        params['main_folder'] = self.get_main_folder(
            p=params, collection=params['collection_slab'])

        # Check if a convergence calculation of the slab thickness is present
        is_done = self.is_done(params)

        # Start a subworkflow to converge the thickness if not already done
        if is_done and not params['override']:
            return FWAction(update_spec=fw_spec)  # Continue the Workflow
        
        else:
            # Add the missing parameters to parmas
            params, func = self.add_parmas(p=params, fw_spec=fw_spec)
            params['collection'] = params['collection_slab']

            # Create the subworkflow to run a convergence detour
            wf = self.run_func(func=func, **params)

            return FWAction(detours=wf, update_spec=fw_spec)

    def get_main_folder(self, p, collection):
        """
        Get the main folder where to save the VASP output files.

        """
        
        miller_str = ''.join([str(i) for i in p['miller']])
        if p['formula']:
            fol_path = str(collection) + '/' + str(p['formula']) + '/' + miller_str + '/surfene/'
        else:
            nav = Navigator(db_file=p['db_file'], high_level=p['database'])
            formula = nav.find_data(collection=p['collection_bulk'],
                                    fltr={'mid': p['mid']})
            fol_path = str(collection) + '/' + str(formula) + '/' + miller_str + '/surfene/'

        main_folder = self.save_files(fol_path, datetime=False)

        return main_folder

    def is_done(self, p):
        """
        Query the database, download the slab dictionary and check if an entry
        for the optimal thickness already exists.

        Parameters
        ----------
        p : dict
            Input parameters of the Firetask.

        Returns
        -------
        is_done : bool
            True if a key named 'opt_thickness' is found in `entry`
        
        comp_params : dict
            Computational parameters to simulate the slab.

        """

        fltr = self.set_filter(p, keys=['mid', 'formula', 'name_slab', 'miller'])
        is_done = super().is_done(db_file=p['db_file'], database=p['database'],
                                  collection=p['collection_slab'], 
                                  entry=p['check_entry'], fltr=fltr)

        return is_done

    def query_structure(self, p):
        """
        Get the bulk structure from the database, to generate the slabs.

        Parameters
        ----------
        p : dict
            Input parameters of the Firetask.

        Returns
        -------
        bulk : pymatgen.core.structure.Structure
            pymatgen bulk structure
        
        comp_params : dict
            Dictionary containing the computational parameters of the bulk

        """
        
        # Retrieve the bulk structure
        fltr = self.set_filter(p, keys=['mid', 'formula', 'name_bulk'])
        field, bulk = self.query_db(collection=p['collection_bulk'], fltr=fltr, 
                                    db_file=p['db_file'], database=p['database'],
                                    entry=p['bulk_entry'])
        bulk = Structure.from_dict(bulk)
        
        # Raise an error if bulk or its field is not found in high level db
        if field is None or bulk is None:
            raise SlabOptThickError('Bulk has not been found in database:\n'
                                    'db_file : {}\ndatabase : {}\ncollection : {}\n'
                                    'entry : {}'.format(p['db_file'], p['database'],
                                     p['collection_bulk'], p['bulk_entry']))
        
        # Check if computational parameters are present for the bulk
        try:
            cp_bulk = read_one_entry(field, 'comp_params')
        except: raise SlabOptThickError('Computational parameters are missing for'
                                        ' bulk: \ndb_file : {}\ndatabase : {} '
                                        '\ncollection : {}\nentry : {}'.format(
                                        p['db_file'], p['database'],
                                        p['collection_bulk'], 'comp_params'))

        # Try to retrieve the slab entries from the low and high level db
        fltr = self.set_filter(p, keys=['mid', 'formula', 'name_slab', 'miller'])
        field, _ = self.query_db(collection=p['collection_slab'], fltr=fltr,
                                 db_file=p['db_file'], database=p['database'])
        
        # Create the eventual dictionary to be inserted in the db
        data_slab = self.set_slab_document(bulk, p, cp_bulk)
        
        # Insert a slab entry in the low and/or high level db if missing
        if field is None or p['override_slab_db']:
            self.insert_db(field=data_slab, collection=p['collection_slab'],
                           db_file=p['db_file'], database=p['database'])         

        return bulk

    def set_slab_document(self, bulk_structure, p, cp_bulk):
        
        # Define the comp_params for the slab
        keys = ['functional', 'encut', 'kdens', 'is_metal', 'use_spin', 'vdw']
        #keys = ['functional', 'encut', 'kdens',  'use_spin', 'vdw'] # For C-slab?
        comp_params = {k: cp_bulk[k] for k in keys}
        
        # Update material parameters for the slab if different from the bulk      
        for k in ['functional', 'is_metal', 'use_spin', 'vdw']:
            if p[k] is not None:
                comp_params.update({k: p[k]})
        
        if not comp_params['functional'] in p['collection_slab']:
            raise SlabOptThickError('There is an inconsistency between the '
                                    'functional and the selected collection for '
                                    'the slab: functional: {}, collection: {}'
                                    .format(comp_params['functional'], 
                                            p['collection_slab']))
        
        # Generate a standard initializing slab to catch default formula
        slab = generate_slabs(structure=bulk_structure, miller=p['miller'], 
                              thickness=p['thick_max'], vacuum=p['vacuum'])
        formula = p['formula']
        if formula is None:
            formula = slab.composition.reduced_formula
        
        # Create the dictionary to be inserted in db
        data_slab = {
            'mid': p['mid'],
            'formula': formula,
            'miller': p['miller'],
            'comp_params': comp_params,
            'structure': {}
            }

        # Add a slab name if provided
        if p['name_slab'] is not None:
            data_slab.update({'name': p['name_slab']})

        return data_slab

    def add_parmas(self, p, fw_spec):
        """
        Add the missing arguments of the function to be called to the dictionary 
        p.
        
        The desired subworkflow is selected from the SlabWFs class, to converge 
        the slab thickness either by evaluating the surface energy or the 
        lattice parameter.

        """
        
        # Get the structure
        structure = self.query_structure(p)
        
        # Check for default values of comp_params and cluster_params
        dfl = currentdir + '/defaults.json'
        cluster_params = read_default_params(dfl, 'cluster_params', 
                                             p['cluster_params'])

        # Select the convergence function based on conv_kind
        if p['conv_kind'] == 'surfene':
            func = SlabWF.conv_slabthick_surfene
        elif p['conv_kind'] == 'alat':
            func = SlabWF.conv_slabthick_alat
        else:
            raise SlabOptThickError("Wrong input argument for conv_kind. "
                                    "Allowed options: 'surfene', 'alat'")
            
        # Add the items to the dictionary 
        p['structure'] = structure
        p['spec'] = fw_spec
        p['cluster_params'] = cluster_params
            
        return p, func

@explicit_serialize
class FT_StartInterfaceMatch(FireTaskTribChem):
    """
    Start a subworkflow as a detour to create a solid-solid interface by
    matching two slabs.
    
    mid, slab, miller, formula, name should be a list of two elements

    """
    
    _fw_name = 'Start interface matching subworkflow'
    required_params = ['mid', 'miller', 'collection_slab', 'collection_inter']
    optional_params = ['formula', 'name', 'db_file', 'database', 'slab_entry',
                       'inter_entry', 'hs_entry', 'inter_params', 'check_entry',
                       'override', 'override_inter_db', 'update_op', 'to_mongodb',
                       'set_bm', 'vacuum', 'is_metal', 'use_spin', 'vdw']
    
    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Get the runtask parameters and check for errors
        params = self.read_runtask('StartInterfaceMatch', fw_spec)
        InterfaceMatchError.check_inputs(params)

        # Query the bottom and top slabs out of the database
        params = self.query_structure(params)

        # Check if the interface is already present in the Database
        is_done = self.is_done(collection=params['collection_slab'], 
                               db_file=params['db_file'], database=params['database'],
                               entry=params['check_entry'], fltr=self.set_filter(params))

        if not is_done or params['override']:
            # Set the document for the interface in the Database if missing
            self.set_interface_document(params)

            # Set the subworkflow to start the detour
            wf = self.run_func(InterfaceWF.match_interface, **params)

            return FWAction(update_spec=fw_spec, detours=wf)

        # Continue the main workflow
        else:
            return FWAction(update_spec=fw_spec)

    def query_structure(self, p):
        """
        Get the slab objects from the database, generate the parameters for
        the interface.

        """

        # Retrieve the slab fields and slab dictionaries
        p_bot = material_id_from_list(p, 0)
        bot_field, bot_slab = self.query_db(collection=p['collection_slab'], 
                                            fltr=self.set_filter(p_bot), 
                                            db_file=p['db_file'], 
                                            database=p['database'],
                                            entry=p['slab_entry'][0])
        bot_slab = Slab.from_dict(bot_slab)

        p_top = material_id_from_list(p, 1)
        top_field, top_slab = self.query_db(collection=p['collection_slab'], 
                                            fltr=self.set_filter(p_top), 
                                            db_file=p['db_file'], 
                                            database=p['database'],
                                            entry=p['slab_entry'][1])
        top_slab = Slab.from_dict(top_slab)

        # Check if fields and slabs are present in the database
        if None in [bot_field, bot_slab, top_field, top_slab]:
            raise InterfaceMatchError('Document or structures of slabs not found in\n'
                                      'db_file : {}\ndatabase : {}\ncollection : {}\n'
                                      'entry : {}'.format(p['db_file'], p['database'],
                                      p['collection_slab'], p['slab_entry']))

        # Convert slabs to pymatgen objects
        p['bot_slab'], p['top_slab'] = bot_slab, top_slab
        p['collection'] = p['collection_inter']

        # Check if computational parameters are present for the slab
        try:
            cp_1 = read_one_entry(bot_field, 'comp_params')
            cp_2 = read_one_entry(top_field, 'comp_params')
        except: 
            raise InterfaceMatchError('Computational parameters missing for'
                                      ' slabs: \ndb_file : {}\ndatabase : {} '
                                      '\ncollection : {}\nentry : {}'.format(
                                      p['db_file'], p['database'],
                                      p['collection_slab'], 'comp_params'))

        # Update comp_params with specific custom values, if provided
        comp_params = generate_interface_comp_params([cp_1, cp_2])
        for k in ['is_metal', 'use_spin', 'vdw']:
            if p[k] is not None:
                comp_params.update({k: p[k]})
        p['comp_params'] = comp_params

        # Set default interface params that are missing
        p['inter_params'] = read_default_params(currentdir+'/defaults.json',
                                                'inter_params',
                                                p['inter_params'])

        # Set the bulk modulus of the two slabs
        p = self.set_bulk_modulus(p)
        
        # Generate the interface ids and OVERWRITE the firetask parameters
        mid, formula, name, miller = generate_interface_ids(p, [bot_slab, top_slab])
        p['mid'], p['formula'], p['name'], p['miller'] = mid, formula, name, miller

        return p

    def set_bulk_modulus(self, p):
        """
        If `set_bm` is True (default), the mid of the two slabs is used to
        recover the bulk modulus of the corresponding bulk from the database.
        The collection of the bulk will be identified by taking the same
        functional of the slabs, e.g. if collection_slab='PBE.slab_elements', 
        it will be searched for 'PBE.bulk_elements'. The database and db_file
        are considered to be the same.
        
        """
        
        if p['set_bm']:
            b_col = p['collection_slab'].replace('slab', 'bulk')
            bms = []
            
            # Loop over the bulk mids and query it
            for m in p['mid']:
                _, bm = self.query_db(collection=b_col, db_file=p['db_file'],
                                      database=p['database'], fltr={'mid': m},
                                      entry=['data', 'bulk_modulus'])
                bms.append(bm)
            
            p['inter_params']['strain_weight_1'] = bms[0]
            p['inter_params']['strain_weight_2'] = bms[1]

            return p
        else:
            return p

    def set_interface_document(self, p):
        """
        Create a template document for the interface in the db.

        """
        
        # Try to retrieve the dictionary entry from the high level DB
        fltr = self.set_filter(p)
        field, _ = self.query_db(collection=p['collection_inter'], fltr=fltr,
                                 db_file=p['db_file'], database=p['database'])
        
        # Insert a interface entry in the high level DB if missing
        if field is None or p['override_inter_db']:
            inter_slab = {
            'mid': p['mid'],
            'formula': p['formula'],
            'miller': p['miller'],
            'structure': {},
            'comp_params': p['comp_params']
            }
            if p['name'] is not None:
                inter_slab.update({'name': p['name']})

            self.insert_db(field=inter_slab, collection=p['collection_inter'],
                           db_file=p['db_file'], database=p['database'])   

@explicit_serialize
class FT_StartPES(FireTaskInterface):
    """
    Start a PES subworkflow from a main workflow as a detour. The structure is
    read from the Database and the calculation is done if no PES data is found.
    
    Parameters
    ----------
    mid : list of str
        List of material IDs composing the interface, e.g. [substrate, coating].

    miller : list of lists (of int)
        List of Miller indexes for the two slabs.

    """

    required_params = ['mid', 'miller', 'collection']
    optional_params = ['formula', 'name', 'db_file', 'database', 'calc_type',
                       'pes_type', 'check_entry', 'bot_slab_entry', 'top_slab_entry',
                       'inter_entry', 'hs_entry', 'run_vasp', 'override', 
                       'layers', 'to_file']

    def run_task(self, fw_spec):

        # Read required and optional input arguments
        params = self.read_runtask('StartPES', fw_spec)
        
        # Get the main folder where to save all the VASP output files
        params['main_folder'] = self.get_main_folder(p=params)
        
        # Recover the slabs and check if the PES has been already calculated
        is_done, params = self.query_interface(params)

        # Run a workflow as a detour to calculate the PES
        if not is_done or params['override']:
            wf = self.run_func(InterfaceWF.calculate_pes, **params)

            return FWAction(update_spec=fw_spec, detours=wf)
        else:
            return FWAction(update_spec=fw_spec)
    
    def query_interface(self, p):
        """
        Special version of the method, it calls the parent one and then shorten
        the layers based on the input value layers. If None is provided, the
        regular interface is kept.

        """

        is_done, p = super().query_interface(p)

        if p['calc_type'] in VaspInputSet.static_types:
            if p['layers'] is not None:

                assert isinstance(p['layers'], list)
                assert len(p['layers']) == 2
                
                i, bs, ts = ManipulateStruct.shorten_interface_slabs(
                    p['interface'],
                    p['layers'][0],
                    p['layers'][1])
                p['interface'], p['bot_slab'], p['top_slab'] = i, bs, ts

        return is_done, p
            
    def get_main_folder(self, p):
        """
        Get the main folder where to save the VASP output files.

        """
        
        miller_str_st = ''.join([str(i) for i in p['miller'][0]])
        miller_str_nd = ''.join([str(i) for i in p['miller'][1]])
        
        nav = Navigator(db_file=p['db_file'], high_level=p['database'])
        formula_st = nav.find_data(collection='PBE.bulk_elements',
                                    fltr={'mid': p['mid'][0]})['formula']
        formula_nd = nav.find_data(collection='PBE.bulk_elements',
                                    fltr={'mid': p['mid'][1]})['formula']
        
        # Define the fol_path based on the type of calculation
        pes_folder = 'pes'
        if p['calc_type'] in VaspInputSet.static_types:
            pes_folder = 'pes_scf'
        fol_path = str(p['collection']) + '/' + \
                    str(formula_st) + miller_str_st + \
                    '-' + str(formula_nd) + miller_str_nd + '_' + \
                    p['mid'][0] + '_' + p['mid'][1] + '/' + pes_folder

        main_folder = self.save_files(fol_path, datetime=False)

        return main_folder

@explicit_serialize
class FT_StartAdhesion(FireTaskInterface):
    """
    Start a subworkflow to calculate the adhesion energy and the corrugation 
    for an interface. It will calculate the adhesion energy for the most stable
    and unstable lateral configuration between two matched slabs.
    
    Parameters
    ----------
    mid : str
        Material Identifier for the interface.
    
    miller : list of lists of int
        Miller indexes (h, k, l) identifying the interface.
    
    formula : str or None, optional
        Chemical formula of the interface.
    
    name : str or None, optional
        Further possible identifier for an interface.
    
    collection : str
        Collection where the data should be searched

    db_file : str or None, optional
        Path to the location of the database. If nothing is provided it will be
        searched by env_check from Atomate. The default is None.
    
    database : str or None, optional
        Database to be selected. The default is 'tribchem'.

    override : bool, optional
        Decide if the simulation should be done in any case, despite the
        possible presence of previous results.

    """
    
    _fw_name = 'Start adhesion calculation'
    required_params = ['mid', 'miller', 'collection']
    optional_params = ['formula', 'name', 'db_file', 'database', 'check_entry',
                       'calc_type', 'pes_type', 'adhesion_type', 'run_vasp', 
                       'override']

    def run_task(self, fw_spec):
        """ Run the FireTask.
        """

        # Get the runtask parameters and update the 
        params = self.read_runtask('StartAdhesion', fw_spec)

        # Check if the calculation is done and update the parameters
        is_done, params = self.query_interface(params)
        
        # Get the main folder where to save all the VASP output files
        params['main_folder'] = self.get_main_folder(p=params)
        
        # Run the Adhesion workflow else continue the outer workflow
        if not is_done or params['override']:
            wf = self.run_func(InterfaceWF.adhesion_energy, **params)

            return FWAction(update_spec=fw_spec, detours=wf)
        else:
            return FWAction(update_spec=fw_spec)

    def query_interface(self, p):
        """
        Check if the adhesion energy is already calculated or not.
    
        """

        # Read the document of the interface
        tmp_p = {'mid': p['mid'][0] + '_' + p['mid'][1], 'miller': p['miller']}
        fltr = self.set_filter(tmp_p)
        field, _ = self.query_db(collection=p['collection'], fltr=fltr,
                                 db_file=p['db_file'], database=p['database'])

        if field is None:
            raise ValueError('The interface you are seeking for is not present '
                             'in the database. Either the document is missing '
                             'or the structure data')
        
        # Read regular slabs to generate the ids
        bs = read_one_entry(field, ["structure", "init", "bot_slab"])
        bs = select_struct_func(bs['@class']).from_dict(bs)
        ts = read_one_entry(field, ["structure", "init", "top_slab"])
        ts = select_struct_func(ts['@class']).from_dict(ts)

        p['mid'], p['formula'], p['name'], p['miller'] = generate_interface_ids(p, [bs, ts])

        if p['pes_type'] == 'pes_scf':
            min_site = read_one_entry(field, ['pes_scf', 'energy', 'min_site'])
            max_site = read_one_entry(field, ['pes_scf', 'energy', 'max_site'])

            if p['adhesion_type'] == 'regular':
                # Obtain the min and max positions out of the big interface
                shifts = field['highsym']['inter_unique']

                # Build the interface by applying the shifts and combine slabs
                mi = apply_interface_shifts(bs, ts, shifts[min_site]).as_dict()
                ma = apply_interface_shifts(bs, ts, shifts[max_site]).as_dict()

            elif p['adhesion_type'] == 'short':
                # Retrievve the interfaces and fix the atomic oordinates
                mi = read_one_entry(field, ["pes_scf", "data", min_site, "input", "structure"])
                mi = ManipulateStruct(mi).fix_interface(mi, as_dict=True)
                ma = read_one_entry(field, ["pes_scf", "data", max_site, "input", "structure"])
                ma = ManipulateStruct(ma).fix_interface(ma, as_dict=True)

                # Decompose the structure and obtain shortened slabs
                # Convert the bs and ts short types to slabs
                bs_short, ts_short = ManipulateStruct(mi).decompose_structure(
                    inter_type='zero', as_dict=True)
                bs = structure_to_slab(bs_short, bs, as_dict=False)
                ts = structure_to_slab(ts_short, ts, as_dict=False)

                # Update the short slabs in structure.init. Not so elegant but
                # functional to rearrange the interface on zero later
                data = {'init': {'short': {'bot_slab': bs.as_dict(),
                                           'top_slab': ts.as_dict(), 
                                           'interface': mi}}}
                self.update_db(data, p['collection'], 'structure', p['db_file'], 
                               p['database'], self.set_filter(p))
            else:
                raise ValueError('Wrong input adhesion_type. It should be "regular" or '
                                 '"short" when pes_type="pes_scf"')
        elif p['pes_type'] == 'pes':
            if p['adhesion_type'] == 'regular':
                mi = read_one_entry(field, ["structure", "opt", "pes", "min"])
                ma = read_one_entry(field, ["structure", "opt", "pes", "max"])
            else:
                raise ValueError('Wrong input adhesion_type. Only "regular" is '
                                 ' implemented with pes_type="pes"')
        else:
            raise ValueError('Error in defining pes_type, it should be either'
                             ' "pes_scf" or "pes"')

        # Update the params dictionary with slab structures, interface, comp_params
        p['bot_slab'], p['top_slab'] = bs, ts
        p['interface'] = [select_struct_func(i['@class']).from_dict(i) for i in [mi, ma]]
        p['comp_params'] = read_one_entry(field, 'comp_params')

        # Check if the PES is done
        is_done = bool(read_one_entry(field, p['check_entry']))

        return is_done, p
    
    def get_main_folder(self, p):
        """
        Get the main folder where to save the VASP output files.

        """

        fol_path = str(p['collection']) + '/' + p['name'] + '_' + p['mid'] + '/output'
        if p['adhesion_type'] == 'short': fol_path += '/short'

        main_folder = self.save_files(fol_path, datetime=False)

        return main_folder

@explicit_serialize
class FT_StartPPES(FireTaskInterface):
    """
    Start a CalcPPES_SWF subworkflow that calculates a PPES.
    
    Parameters
    ----------
    mid : list
        List containing all the materials project ids of the interface.
        E.g.: ['mp-134', 'mp-30']
    
    miller : list
        List containing all the miller indxes of the slabs of the interface.
        E.g.: [[1, 0, 0], [1, 0, 0]]
    
    collection : str
        String with the name of the collection where the interfaces are saved.
    
    functional : str
        Functional for the pseudopotential to be adopted.

    distance_list : list
        Modification of the equilibrium distance between the slabs.
        The default is [-0.5, -0.25, 0.0, 0.25, 0.5, 2.5, 3.0, 4.0, 5.0, 7.5].
    
    db_file : str or None
        Path to the location of the database. If nothing is provided it will be
        searched by env_check from Atomate. The default is None.
    
    database : str, default 'tribchem'
        Name of the database where to save the results.

    formula : list, default None
        List containing the formulas of the slabs which made up the structure.
        E.g.: ['Al', 'Cu']
    
    name : str
        List containing the name of the interface.
    
    slab_entry : str
    
    calc_type : str, default value is 'interface_z_relax'
    
    check_entry : bool, default None
    
    ppes_type : str, default 'energy'
    
    run_vasl : bool, default True
        Is False the vasp calculation will not be done if they are already in
        the database.
    
    fit : str, default 'UBER'
        Name of the mathematical function used for fitting the PPES.

    Returns
    -------
    sfw : fireworks.core.firework.Workflow
        Subworkflow to calculate the PPES for a certain interface.

    """

    required_params = ['mid', 'miller', 'collection']
    optional_params = ['formula', 'name', 'db_file', 'database', 'bot_slab_entry',
                       'top_slab_entry', 'check_entry', 'calc_type', 'ppes_type',
                       'shifts', 'run_vasp', 'site', 'fit', 'override']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Get the runtask parameters
        params = self.read_runtask('StartPPES', fw_spec)

        # Check if the simulation is already done and update parameters
        is_done, params = self.query_interface(params)
        
        # Get the main folder where to save all the VASP output files
        params['main_folder'] = self.get_main_folder(p=params)
        
        # Prepare the wf to start a detour if the calculation is not done
        if not is_done or params['override']:
            wf = self.run_func(InterfaceWF.calculate_ppes, **params)

            return FWAction(update_spec=fw_spec, detours=wf)        
        else:
            return FWAction(update_spec=fw_spec)
    
    def get_main_folder(self, p):
        """
        Get the main folder where to save the VASP output files.

        """

        fol_path = str(p['collection']) + '/' + p['name'] + '_' + p['mid'] + \
                   '/ppes/' + str(p['check_entry']) + '/' + str(p['site'])

        main_folder = self.save_files(fol_path, datetime=False)

        return main_folder

@explicit_serialize
class FT_StartChargeDisplacement(FireTaskInterface):
    """
    Call the subworkflow for calculating the charge displacement.
    
    Parameters
    ----------
    operations : list
        List containing all the operations on CHGCAR files. For example given 
        three files, for calculating the sum of the first file with the second
        and then subtract the result to the third file the list to pass is: 
        ['+', '-'].
    
    mid : list of str
        Material ids of the bottom and to slabs from the Database.
    
    miller : list of int, or str
        Miller indexes (h, k, l) to select the bottom and top slabs orientation.
    
    """
    
    _fw_name = 'Start charge displacement calculation'
    required_params = ['mid', 'miller', 'collection', 'operations']
    optional_params = ['formula', 'name','db_file', 'database', 'check_entry',
                       'bot_slab_entry', 'top_slab_entry', 'site',
                       'calc_type', 'integrate_in', 'axis', 'rescale', 
                       'inter_type', 'run_vasp', 'to_plot', 'override']
    
    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Get the runtask parameters
        params = self.read_runtask('StartChargeDisplacement', fw_spec)

        # Check if the simulation is already done and update parameters
        is_done, params = self.query_interface(params)
        
        # Get the main folder where to save all the VASP output files
        params['main_folder'] = self.get_main_folder(p=params)

        # Prepare the wf to start a detour if the calculation is not done
        if not is_done or params['override']: 
            wf = self.run_func(ElectronicsWF.charge_displacement, **params)

            return FWAction(detours=wf)
        else:
            return FWAction(update_spec=fw_spec)

    def get_main_folder(self, p):
        """
        Get the main folder where to save the VASP output files.

        """

        fol_path = str(p['collection']) + '/' + p['name'] + '_' + p['mid'] + '/charge_dis/' + str(p['check_entry'])

        main_folder = self.save_files(fol_path, datetime=False)

        return main_folder

@explicit_serialize
class FT_StartAdsorbateSurface(FireTaskTribChem):
    """
    Call the subworkflow for calculating the adsoption of an atom or a molecule
    on a surface.
    
    Parameters
    ----------
    mid : list of str
        Material id of the slabs from the Database.
    
    miller : list of int, or str
        Miller indexes (h, k, l) to select the bottom and top slabs orientation.
        
    slab_collection : str
        Name of the collection where the slabs are saved.
    
    ad_collection : str
        Name of the collection where the adsorption results are saved.
        
    'formula': str or None, optional
        The formula of the slab, possible identifier to locate it in the
        database. The default is None.
    
    name : str or None, optional
        Possible name identifier of the slab, located in the database.
        The default is None. Name of the slab+adsorbate is set to 'formula'+'adsorbate'.
        
    'pymatgen_opt' : dict
        Options that can be used by AdsorbateSiteFinder (pymatgen) in order to
        place the desired atomic specie on top of the slab.
        The possible options are:
            - 'selective_dynamics' (False)
            - 'height' (0.9)
            - 'mi_vec' ()
            - 'vacuum' (None)
            - 'min_lw' (5)
            - 'translate' (True)
            - 'reorient' (True)
            - 'find_args' (None)
            - 'a' (None)
            - 'v' (None)
            - 'center' ((0, 0, 0))
            - 'rotate_cell' (False)

    """
    
    _fw_name = 'Start adsorption calculation'
    required_params = ['mid', 'miller', 'adsorbate', 'collection', 
                       'collection_slab', 'collection_atom']
    optional_params = ['formula', 'name', 'db_file', 'database', 'check_key',
                       'run_vasp', 'calc_type', 'override', 'both_surfaces',
                       'coverage', 'sites', 'pymatgen_opt']

    def run_task(self, fw_spec):

        # Get the runtask parameters
        params = self.read_runtask('StartAdsorbateSurface', fw_spec)

        # Check if the workflow has to be run or not
        is_done, params = self.is_done(params)

        # Get the main folder where to save all the VASP output files
        params = self.get_main_folder(p=params)

        # Prepare the wf to start a detour if the calculation is not done
        if not is_done or params['override']:
            wf = self.run_func(ChemAdsWF.adsorbate_atom, **params)

            return FWAction(detours=wf)
        else:
            return FWAction(update_spec=fw_spec)

    def is_done(self, p):
        """
        Check if the calculations are done, retrieve comp_params from slabs.

        """

        # Check if the calculation is done   
        fltr = self.set_filter(p, ['mid', 'formula', 'miller', 'adsorbate'])
        is_done = super().is_done(collection=p['collection'], db_file=p['db_file'],
                                  database=p['database'], entry=p['check_key'],
                                  fltr=fltr)

        # Update comp_params
        fltr = self.set_filter(p, ['mid', 'miller'])
        _, comp_params = self.query_db(collection=p['collection_slab'],
                                       db_file=p['db_file'], entry='comp_params', 
                                       database=p['database'], fltr=fltr)
        comp_params['is_metal'] = True
        comp_params['use_spin'] = True
        p['comp_params'] = comp_params

        return is_done, p

    def get_main_folder(self, p):
        """
        Get the main folders where to save the VASP output files for the adatom,
        the clean slab, and the slab with the adatoms chemisorbed/physisorbed.

        """
        
        # Find the formula name for the folder if it is None
        if not p['formula']:
            formula = self.query_db(collection=p['collection_slab'], 
                                    db_file=p['db_file'], 
                                    database=p['database_slab'], 
                                    fltr={'mid': p['mid']})
            p['formula'] = formula[0]['formula']

        # Create the folders for the atom and the slab if not present
        fol_path_atom = str(p['collection_atom']) + '/' + p['adsorbate']
        fol_path_slab = str(p['collection_slab']) + '/' + p['formula']
        fol_path_slab_atom = str(p['collection']) + '/' + p['adsorbate'] + '_on_' + p['formula']

        main_folder_atom = self.save_files(fol_path_atom, datetime=False)
        main_folder_slab = self.save_files(fol_path_slab, datetime=False)        
        main_folder_slab_atom = self.save_files(fol_path_slab_atom, datetime=False)

        p['main_folder_atom'] = main_folder_atom
        p['main_folder_slab'] = main_folder_slab
        p['main_folder_slab_atom']= main_folder_slab_atom

        return p
