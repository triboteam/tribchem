#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 14:54:53 2021

Firetasks to calculate the surface energy for a slab along a given orientation.

The module contains the following Firetasks:

** Surface Energy evaluation **
    - FT_SurfaceEnergy
    Calculate the surface energy between of one or more slabs with respect to
    the bulk structure having the same orientation.

    Author: Gabriele Losi (glosi000)
    Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 2nd, 2021'

import os
from monty.json import jsanitize

from fireworks.utilities.fw_utilities import explicit_serialize
from fireworks import FWAction
from pymatgen.core.structure import Structure

from tribchem.highput.firetasks.core import FireTaskTribChem
from tribchem.physics.tribology.surface_energy import calculate_surface_energy, fit_surface_energy
# from tribchem.highput.utils.errors import SurfaceEnergyError
from tribchem.physics.base.units import Converter
from tribchem.physics.base.solidstate import get_cell_info


currentdir = os.path.dirname(__file__)


@explicit_serialize
class FT_SurfaceEnergy(FireTaskTribChem):
    """
    Calculate the surface energy between a bulk structure and one or more of the
    slabs oriented in the same way.
    
    """
    
    _fw_name = 'Surface Energy calculation'
    required_params = ['mid', 'collection', 'miller', 'entry']
    optional_params = ['db_file', 'database', 'formula', 'name', 'parallelization',
                       'update_op', 'upsert', 'to_mongodb']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Read required and optional input arguments
        params = self.read_runtask('SurfaceEnergy', fw_spec)

        # Calculate the surface energies from the provided entries
        surfene, surfene_jm2 = self.calculate_surfene(params)

        # Store surface energies in DB
        self.update_db(surfene, params, 'surface_energy_ev')
        self.update_db(surfene_jm2, params, 'surface_energy_jm2')

        return FWAction(update_spec=fw_spec)

    def calculate_surfene(self, p):
        """
        Retrieve the field entry containing the energies of the bulk and the 
        slabs from the database and calculate the surface energies.

        """

        # Check for errors
        # SurfaceEnergyError.check_collection(p['collection'])

        # Generate the filter
        fltr = self.set_filter(p)

        # Query the database and dowload the energies
        field, field_entry = super().query_db(collection=p['collection'],
                                              db_file=p['db_file'],
                                              database=p['database'],
                                              entry=p['entry'], fltr=fltr)

        # Extract the output dictionary containing energies and get surfene
        surfene = calculate_surface_energy(field_entry, sym_surface=True)

        # Convert surfene to jm2
        surfene_jm2 = []
        for i in range(0, len(field_entry[1:])):
            area, _ = get_cell_info(Structure.from_dict(field_entry[1:][i]['structure']).lattice.matrix)
            area = area * 1e-20
            surfene_jm2.append(Converter.eV_to_Jm2(energy=surfene[i], area=area))
        
        # Fit the surfene to a theoretical equation, for high parallelization
        if p['parallelization'] == 'high':       
            self.fit_and_update(field, area, p)

        return surfene, surfene_jm2

    def update_db(self, surfene, p, name):
        """
        Update the surface energies in the database, in the exact location 
        given by `entry`, where the energies of the slabs are taken.
        
        """

        # Extract the surface energies from the provided dictionary entry
        entry = p['entry'][1:]
        _ = [n.append(name) for n in entry]  # Add nested key

        # Generate the filter
        fltr = self.set_filter(p)

        # Update the energies to the selected database
        super().update_db(data=surfene, collection=p['collection'],
                          entry=entry, db_file=p['db_file'], 
                          database=p['database'], fltr=fltr, 
                          update_op=p['update_op'], upsert=p['upsert'],
                          to_mongodb=p['to_mongodb'])
        
        _ = [n.pop() for n in entry]  # pop the last element

    def fit_and_update(self, field, area, p):
        """
        Fit and update the 

        """

        fit_surfene, ebulk, array = fit_surface_energy(field)
        fit_surfene_jm2 = Converter.eV_to_Jm2(energy=fit_surfene, area=area)

        fltr = self.set_filter(p)

        # Update the energies to the selected database
        for s, en in zip([fit_surfene, fit_surfene_jm2, ebulk, jsanitize(array)],
                         ['fit_surface_energy_ev', 'fit_surface_energy_jm2', 'energy_bulk_ev', 'array']):
            super().update_db(data=s, collection=p['collection'], fltr=fltr,
                              entry=['surfene', en], db_file=p['db_file'], 
                              database=p['database'])
        