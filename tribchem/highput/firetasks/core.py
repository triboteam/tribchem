#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 11:17:01 2021

Core Firetasks module for the TribChem project. It contains important features 
and classes to be inherited by any other Firetask of the package.

The module contains:

    - **class FireTaskTribChem**
        - read_runtask
        - is_done
        - query_db
        - update_db
        - insert_db
        - query_mp

    Author: Gabriele Losi (glosi000)
    Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 26th, 2021'


import os
from pathlib import PurePosixPath

from fireworks.utilities.fw_utilities import explicit_serialize
from fireworks import FiretaskBase

from tribchem.highput.database.query import (
    query_field_entry,
    query_tag,
    update_field,
    update_tag,
    insert_field
)
from tribchem.highput.database.mpquery import query_mp
from tribchem.highput.database.dbtools import read_one_entry
from tribchem.highput.utils.tasktools import (
    read_default_params,
    read_runtask_params,
    select_struct_func,
    which_params_attr
)
from tribchem.highput.database.filetools import FileTools
from tribchem.highput.utils.manipulate_struct import generate_interface_ids
from tribchem.utils.utils import FunctionTools, ClassTools


currentdir = os.path.dirname(__file__)


@explicit_serialize
class FireTaskTribChem(FiretaskBase):
    """
    General Firetask for the Tribchem package. Very general methods are defined 
    here. Use it to inherit these methods in more complex physical Firetasks.

    """

    required_params = []
    optional_params = []

    def read_runtask(self, dfk, fw_spec={}, defaults='defaults.json'):
        """
        Read the optional and required parameters of a Firetasks. These ones are
        passed by the user as input arguments when initializing the FT object.
        Optional and Required parameters must be defined as global variables
        within the Firetask.

        Parameters
        ----------
        dfk : str
            Key of the JSON dictionary containing the default values of the
            optional parameters of the instanciated Firetask.

        fw_spec : dict, optional
            Spec of the Firetask. It is used by Atomate to identify the location
            of the database on the local machine (`env_check`), when an input
            argument with name `db_file` is None. The default is default {}.

        defaults : str, optional
            Name of the JSON dictionary. The default is 'defaults.json'.

        Returns
        -------
        dict
            Dictionary containing all the parameters, the optional values which 
            have not been provided by the user are substituted with defaults.

        """
        # Define the json file containing default values and read parameters
        dfl = currentdir + '/' + defaults
        
        p = read_runtask_params(self,
                                required_params=self.required_params, 
                                optional_params=self.optional_params,
                                fw_spec=fw_spec, 
                                default_file=dfl,
                                default_key=dfk)
        return p

    def read_params(self, dfk, dict_params, defaults='defaults.json', 
                    allow_unknown=False):
        """
        Update the parameters of a dictionary object with default values read
        from a dictionary within a JSON file.

        Parameters
        ----------
        dfk : str
            Key of the JSON dictionary containing the default values.

        dict_params : dict
            Dictionary where missing data is update with defaults.

        defaults : str, optional
            Name of the JSON dictionary. The default is 'defaults.json'.

        Returns
        -------
        dict
            Updated dictionary.

        """

        dfl = currentdir + '/' + defaults
        return read_default_params(dfl, dfk, dict_params, allow_unknown)

    def set_filter(self, params, keys=['mid', 'formula', 'name', 'miller', 'adsorbate']):
        """
        Create a filter to query the Database, in order to retrieve/store data.

        Parameters
        ----------
        p : dict
            Parameters of the Firetasks, contains required and optional values.

        keys : list of str, optional
            Keys to be parsed in order to create a filter to query the database.
            If params[key] is not None, it is set as a filter option.
        
        Returns
        -------
        fltr : dict
            Filter to query the database.

        """

        # Create a filter out of required and optional parameters passed in keys
        fltr = {}
        for k in keys:
            if params.get(k, None) is not None:
                fltr.update({k: read_one_entry(params, k)})
        return fltr

    def is_done(self, collection, db_file=None, database=None, entry=None,
                fltr=None, **kwargs):
        """
        Check if there is a specific `entry` within a field that matches the
        filter. Input arguments passed as kwargs are used to create eventually
        the filter. Useful to decide whether to run or stop a job. 

        Parameters
        ----------
        collection : str
            Collection in the database where the field is located.

        db_file :
            Path to the folder containing the MongoDB database. If None is 
            passed, it a local db_file is searched for. The default is None.
        
        database : str or None, optional
            Name of the database table. If None is passed, the FireWorks 
            database is selected. The default is None.
        
        entry : str or list or list of lists or None, optional
            Key(s) to read the field dictionary extracted from the database. 
            If a str or list is provided, a key or a nested set of keys is read 
            from the field. If a list of lists is provided, a list of many 
            objects is extracted from the field. The default is None.
        
        fltr : dict or None, optional
            Dictionary containing the key-value pairs to match a particular 
            field within the database. If it is None, the filter will be 
            automatically created from kwargs arguments.

        Returns
        -------
        is_done : bool
            If a matching field or an entry within it is present in the database.

        """

        # Retrieve a field and one of its entry from the database
        if entry is not None:
            if fltr is not None:
                _, field_entry = query_field_entry(fltr, collection, 
                                                       entry, db_file, database)
            else:
                _, field_entry = query_tag(collection, db_file, database, 
                                               entry, **kwargs)

            # Understand if that entry is already present within the database
            is_done = bool(field_entry)
        else:
            is_done = False
            
        return is_done

    def query_db(self, collection, db_file=None, database=None, entry=None,
                 fltr=None, **kwargs):
        """
        Retrieve data from a MongoDB database. The database location and tables
        are specified with keywords: `db_file`, `database`, `collection`, `entry`.
        Default values will search for a db_file folder in the local machine 
        (by means of env_check function from Atomate), and for the FireWorks
        database. The **kwargs arguments can be used to specify a custom filter.

        Parameters
        ----------
        collection : str
            Collection in the database where the field is located.

        db_file :
            Path to the database folder on your machine. If None is passed, it 
            will search for a local db_file by means of env_check (Atomate 
            function). The default is None.
        
        database : str or None, optional
            Name of the database table. If None is passed, the FireWorks 
            database is selected. The default is None.
        
        entry : str or list or list of lists or None, optional
            Key(s) to read the field dictionary extracted from the database. 
            If a str or list is provided, a key or a nested set of keys is read 
            from the field. If a list of lists is provided, a list of many 
            objects is extracted from the field. The default is None.

        fltr : dict or None, optional
            Dictionary containing the key-value pairs to match a particular 
            field within the database. If it is None, the filter will be 
            automatically created from kwargs arguments.

        Returns
        -------
        field : dict or None
            The dictionary field extracted from the database.

        field_entry : dict or list of dicts or None
            The entries extracted from field.

        Examples
        --------

        >>> ft = FireTaskTribChem()
        >>> a = ft.query_db(db_file=None, database='tribchem', 
                            collection='PBE.slab_data', entry=None, 
                            mp_id='mp-126', miller=[1,1,1])

        """
        
        if fltr is None:
            return query_tag(collection, db_file, database, entry, **kwargs)
        else:
            return query_field_entry(fltr, collection, entry, db_file, database)
    
    def update_db(self, data, collection, entry=None, db_file=None, database=None,
                  fltr=None, update_op='$set', upsert=False, to_mongodb=True, 
                  **kwargs):
        """
        Update data in the matching field. The `data` is prepared to be stored 
        in database creating a dictionary according to the MongoDB guidelines.
        The database field to be updated is located by: `db_file`, `database`, 
        `collection`, `entry`. Default values will search for a database on
        local machine and use the FireWorks database. 
        The **kwargs arguments can be used to specify a filter.

        Parameters
        ----------
        data : (list of) python object
            Data to be stored in the database. If a list of objects is provided,
            a dictionary containing different entries is created.

        collection : str
            Collection in the database where the field is located.

        entry : str or list or list of lists or None, optional
            Key(s) used to crate a dictionary, conformal to MongoDB, to update a
            field in a database. The default is None.

        db_file : str or None, optional
            Path to the database folder on your machine. If None is passed, it 
            will search for a local db_file by means of env_check (Atomate 
            function). The default is None.

        database : str or None, optional
            Name of the database table. If None is passed, the FireWorks
            database is selected. The default is None.

        fltr : dict or None, optional
            Dictionary containing the key-value pairs to match a particular 
            field within the database. If it is None, the filter will be 
            automatically created from kwargs arguments.

        update_op : str, optional
            Update operation to be performed on the database, e.g. '$set', '$inc'. 
            See allowed valued for more information. The default is '$set'.

        upsert : bool
            PyMongo parameter for the update_one function. If True update_one 
            performs an insertion if no documents match the filter.

        to_mongodb : bool, optional
            Decide whether to return a dictionary which is conformal to the 
            queries of MongoDB, in order to really update the fields and not 
            overwrite dictionaries.

        """

        if fltr is None:
            update_tag(data, collection, entry, db_file, database, update_op, 
                       upsert, to_mongodb, **kwargs)
        else:
            update_field(data, fltr, collection, entry, db_file, 
                         database, update_op, upsert, to_mongodb)

    def insert_db(self, field, collection, db_file=None, database=None, 
                  duplicates=False, message=None):
        """
        Add a new field to a database. The precise position within the `db_file`
        folder, where to add the new field is identified by `database` and
        `collection.` Default values will add a field in the FireWorks database
        located on local machine. If `duplicates` is True, a field will be 
        added to the database anyway, even if equal fields are already present.

        Parameters
        ----------
        field : dict or list of dicts
            Object(s) to be saved as field(s) in the database.

        collection : str
            Collection in the database where the field should be located.

        db_file :
            Path to the database folder on your machine. If None is passed, it 
            will search for a local db_file by means of env_check (Atomate 
            function). The default is None.

        database : str or None, optional
            Name of the database table. If None is passed, the FireWorks 
            database is selected. The default is None.

        duplicates : bool, optional
            If True data is saved in database even if there are duplicates.
        
        message : str, optional
            Custom message to write in the console and/or in the log file.

        """

        insert_field(field, collection, db_file, database, duplicates, message)

    def query_mp(self, formula=None, mp_id=None, print_info=False):
        """
        Query the Materials Project Database, searching for a structure with a
        selected `mp_id` or searching for the lowest energy structure given a
        specific chemical formula.

        Parameters
        ----------
            chem_formula : str or None, optional
                Chemical formula of the structure, e.g.: 'NaCl', 'Fe2O3', 'SiO'.

            mp_id : str or None, optional
                Materials Project ID of the structure, e.g.: 'mp-124'. 
                The default is None.
                
            print_info : bool, optional
                Whether to print information about the collected structure. 
                The default is False.

        Returns
        -------
            struct : pymatgen.core.structure.Structure
                Tuple containing several information about the desired structure.

            mp_id : str
                Materials Project ID for the given chemical formula.
        """

        return query_mp(formula, mp_id, print_info)

    def save_files(self, folder, datetime=False):
        """
        Manage all the operations regarding the creation of the folders where to
        save the output files.
        
        Parameters
        ----------
        folder : str
            Path to the folder that will contain the results.
        
        datetime : bool, optional
            Decide to add a unique identifier to the folder path, based on the
            calculation datetime (mid_day_month_year_hour_minute_second).
            The default is False. 
        
        Return
        ------
        main_folder : str
            The path to the main folder where all the subfolders containing the
            VASP output files are saved.

        """

        # Add a datetime to the folder of the last level
        if datetime:
            folder = FileTools.add_datetime(folder=folder)

        # identify the currentdir to match the position of calculations/
        currentdir_object = PurePosixPath(currentdir)

        # Check if the calculations folder has been already created
        calc_folder = str(currentdir_object.parent.parent.parent.parent) \
            + '/calculations/'
        calc_folder = FileTools.check_folder(fol_path=calc_folder)

        # Build the main folder within the results folder: calculations/
        main_folder = str(currentdir_object.parent.parent.parent.parent) \
            + '/calculations/' + folder
        main_folder = FileTools.check_folder(fol_path=main_folder)

        return main_folder

    def run_command_on_file(self, command, filepath):
        """
        Run an OS command on file(s). For instance, you can use it to gunzip
        VASP input and output files from Fireworks, or to create/delete
        temporary files to work on.
        
        """
        
        if not isinstance(filepath, list):
            filepath = [filepath]

        for f in filepath:
            _ = os.system(command + ' ' + f)

    def run_func(self, func, **kwargs):
        """
        Run a function passed by input, using **kwargs as input arguments.

        """
        return FunctionTools._run_func(func, **kwargs)

    @classmethod
    def all_params(cls):
        """
        Return the required and optional parameters of a Firetask.

        Returns
        -------
        list
            Comprehensive list of required and optional parameters of the FT.

        """
        return which_params_attr(cls)

    @classmethod
    def from_locals(cls, **kwargs):
        """
        Initialize the Firetask by using a **kwargs dict as input arguments.
        **Kwargs is filtered by using the FT required and optional params.

        """
        return ClassTools.call_conf(cls, cls.all_params(), **kwargs)

@explicit_serialize
class FireTaskInterface(FireTaskTribChem):
    """
    General Firetask class useful to treat interfaces.

    """

    def query_interface(self, p):
        """
        Check if the adhesion energy is already calculated or not.
    
        """

        # Read the document of the interface
        tmp_p = {'mid': p['mid'][0] + '_' + p['mid'][1], 'miller': p['miller']}
        fltr = self.set_filter(tmp_p)
        field, _ = self.query_db(collection=p['collection'], fltr=fltr,
                                 db_file=p['db_file'], database=p['database'])

        if field is None:
            raise ValueError('The interface you are seeking for is not present '
                             'in the database. Either the document is missing '
                             'or the structure data')

        # Generate the new ids for the interface
        bs = read_one_entry(field, ["structure", "init", "bot_slab"])
        bs = select_struct_func(bs['@class']).from_dict(bs)
        ts = read_one_entry(field, ["structure", "init", "top_slab"])
        ts = select_struct_func(ts['@class']).from_dict(ts)
        p['mid'], p['formula'], p['name'], p['miller'] = generate_interface_ids(p, [bs, ts])

        # Update the params dictionary with slab structures and comp_params
        p['bot_slab'], p['top_slab'] = bs, ts
        p['comp_params'] = read_one_entry(field, 'comp_params')

        # Check if interfaces and/or highsym are required
        if 'site' in p.keys():
            if not p['site'] in ['min_site', 'max_site']:
                raise ValueError('Wrong input argument for site. Allowed values: '
                                 '"min_site", "max_site"')
        
        if 'FT_StartPPES' in str(self.__class__) or 'FT_StartChargeDisplacement' in str(self.__class__):
            i = 'interface_min' if p['site'] == 'min_site' else 'interface_max'
            if p['check_entry'] == 'short':
                inter = read_one_entry(field, ['structure', 'opt', 'short', i])
            else:
                inter = read_one_entry(field, ['structure', 'opt', 'regular', i])   
            p['interface'] = select_struct_func(inter['@class']).from_dict(inter)

        if 'inter_entry' in p.keys():
            # if 'FT_StartAdhesion' in str(self.__class__):
            #     mi = read_one_entry(field, p['inter_entry']+['min'])
            #     ma = read_one_entry(field, p['inter_entry']+['max'])
            #     p['interface'] = [select_struct_func(i['@class']).from_dict(i) for i in [mi, ma]]
            inter = read_one_entry(field, p['inter_entry'])
            p['interface'] = select_struct_func(inter['@class']).from_dict(inter)

        if 'hs_entry' in p.keys():
            p['shifts'] = read_one_entry(field, p['hs_entry'])        

        # Check if the PES is done
        if 'FT_StartChargeDisplacement' in str(self.__class__):
            is_done = bool(read_one_entry(field, ['charge', p['check_entry']]))
        elif 'FT_StartPPES' in str(self.__class__):
            is_done = bool(read_one_entry(field, ['ppes', p['check_entry']]))
        else:
            is_done = bool(read_one_entry(field,  p['check_entry']))
        print('p =', p)
        return is_done, p
