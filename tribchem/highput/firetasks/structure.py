#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 12 16:42:36 2021

Contains firetasks useful to manipulate materials structures and to converge them
with DFT calculations.

    - FT_RunVaspSimulation
    General Firetask to relax a given structure, either bulk, slab or interface.

    Author: Gabriele Losi (glosi000)
    Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'April 12th, 2021'


import os

from fireworks import (
    FWAction,
    explicit_serialize
)

from tribchem.highput.utils.tasktools import read_default_params, select_struct_func
from tribchem.highput.utils.vasp_tools import VaspInputSet
from tribchem.highput.firetasks.core import FireTaskTribChem
from tribchem.highput.workflows.base import run_vasp_wf


currentdir = os.path.dirname(__file__)


# ============================================================================
# Firetasks for DFT simulations
# ============================================================================

@explicit_serialize
class FT_RunVaspSimulation(FireTaskTribChem):
    """
    Retrieve a structure based on `mid` (and `miller`) from a location in the
    database, identified by: `db_file`, `database`, `collection`, `entry`. 
    The structure is converged setting up a VASP calculation based on Atomate
    workflows. The result is stored in the same database location and can be 
    identified by a tag object, provided by the user, associated to a 
    'task_label' entry. The type of the simulation is identified by 'calc_type',
    which can be either a scf-like simulation (static) or a relax/vc-relax
    simulation. The computational parameters of the simulation are searched
    in the first level of the document in Mongodb Database. If not found, a
    set of default values is read.

    Parameters
    ----------
    mid : str
        MP-ID of the structure from the MP database, used to identify the
        structures in local databases.

    collection : str
        Collection where the structure is present. The default collections 
        used in the "tribchem" database are identified as functional+string, 
        where string is usually: '.bulk', 'slab_data', 'interface_name'.

    entry : str or list
        Location of the structure to be retrieved from database and collection.

    tag : str (any python object)
        Defined by user, it is automatically associated from Atomate workflows 
        to an entry, with key 'task_label', in the first layer of the db field 
        containing the output of the VASP simulations. The results are stored in
        the Atomate database. Tag can be used to keep track of where the DFT 
        calculation results have been stored in the database. In principle it 
        can be any python object.

    db_file : str or None, optional
        Path to the location of the database. If nothing is provided it will be
        searched by env_check from Atomate. The default is None.
    
    database : str, optional
        Name of the database where the structure will be retrieved. 
        The default is "tribchem".

    calc_type : str, optional
        The type of calculation to be performed during the simulation, depending
        on the value which is provided, a static or relax vasp calculation is
        run. The default is 'bulk_from_scratch'.

    miller : list of int, optional
        Miller indexes (h, k, l) used to identify unequivocally a slab within
        the database. If nothing is passed, the material will be only identified
        by means of `mid`. The default is None.
    
    check_key : str or list, optional
        Check if a given data is already present within a database location, to
        understand if the DFT simulation has been already done. The location is
        identified by db_file, database, collection, entry. Once the data 
        corresponding to entry is extracted, if data['check_key'] points to
        something in the DB, the simulation will not be done. If it is None, no
        check is done and the simulation is always started. The default is None.

    half_kpts_first_relax : bool, optional
        Use half the kpoints for the first relaxation. The default is False

    """

    _fw_name = 'Run ab initio simulation with VASP'
    required_params = ['mid', 'collection', 'entry', 'tag']
    optional_params = ['formula', 'name', 'miller', 'adsorbate', 'db_file', 
                       'database',  'check_key', 'calc_type', 'override',
                       'half_kpts_first_relax']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Read required and optional input arguments, get default comp_params
        params = self.read_runtask('RunVaspSimulation', fw_spec)

        # Check if a check_key is already present in the destination Database
        is_done = self.is_done(params)

        # Run the simulation if the calculation is not already done
        if not is_done or params['override']:

            # Get the structure and run the wf simulation with detour
            structure, comp_params = self.query_structure(params)
            wf = self.run_simulation(structure, params, comp_params)

            return FWAction(detours=wf, update_spec=fw_spec)

        # Continue the Workflow
        else:
            return FWAction(update_spec=fw_spec)

    def is_done(self, p):
        """
        Decide whether a DFT simulation with VASP should be carried on or not.
        If a `check_key`!=None is passed to the Firetask and point to a non-empty
        element in the matched MongoDB document the calculation is not done.

        Parameters
        ----------
        p : dict
            All the input parameters of the Firetasks, placed in a dictionary.

        Returns
        -------
        structure : dict or pymatgen structure
            Dictionary containing the structure.
        
        is_done : bool
            If a simulation output is already present in the database. If it
            is True, the calculation will not be performed.

        """

        # Generate a filter to query the database
        fltr = super().set_filter(p)

        # Check if the data is already present in destination
        is_done = super().is_done(db_file=p['db_file'],
                                  collection=p['collection'],
                                  database=p['database'],
                                  entry=p['check_key'],
                                  fltr=fltr)

        return is_done

    def query_structure(self, p, defaults='defaults.json'):
        """
        Get the desired atomic structure to be used to make the DFT simulation.
        
        p : dict
            Params with optional and required arguments passed to the Firetask,
        
        dfl : str
            Path to location of defaults, set missing computational parameters.
        
        Returns
        -------
        
        structure : pymatgen.core.structure.Structure or pymatgen.core.surface.Slab
            Pymatgen object containing the crystalline cell of the material to
            be simulated with VASP.
            
        comp_params : dict
            Computational params to set properly the VASP simulation.

        """
        
        # Define a custom filter from a retrieving tag passed by user
        fltr = super().set_filter(p)

        # Query the database to retrieve the structure
        field, structure = super().query_db(db_file=p['db_file'],
                                            collection=p['collection'],
                                            entry=p['entry'],
                                            database=p['database'],
                                            fltr=fltr)

        # Detect the structure type and convert it back to pymatgen object
        func = select_struct_func(structure['@class'])
        structure = func.from_dict(structure)
        
        # Check tag and computational parameters, read defaults for missing values
        dfl = currentdir + '/' + defaults
        comp_params = read_default_params(default_file=dfl, 
                                          default_key='comp_params',
                                          dict_params=field['comp_params'],
                                          allow_unknown=True)

        return structure, comp_params

    def run_simulation(self, structure, p, cp):
        """
        Set necessary VASP parameters and create an Atomate workflow to start a 
        detour and perform a DFT simulation on the given structure structure.

        Parameters
        ----------
        structure : pymatgen structure
            Crystalline atomic structure to perform a Kohn-Sham minimization.

        p : dict
            All the input parameters of the Firetasks, placed in a dictionary.
        
        cp : dict
            Computational params to run the VASP simulation.

        Returns
        -------
        wf : Fireworks.Workflow
            Workflow to start a VASP simulation on your structure.

        """

        # Calculate the VIS
        visgen = VaspInputSet(structure, cp, p['calc_type'])
        vis = visgen.get_vasp_settings()        

        # Create the string name for the calculation
        name = 'VASP simulation ({}) for "{}"'.format(p['calc_type'], p['mid'])
        if p['formula'] is not None:
            name += ', formula: {}'.format(p['formula'])
        if p['name'] is not None:
            name += ', name: {}'.format(p['name'])
        if p['miller'] is not None:
            name += ', miller: {}'.format(tuple(p['miller']))
        if p['adsorbate'] is not None:
            name += ', adsorbate: {}'.format(p['adsorbate'])

        # Create the workflow
        wf = run_vasp_wf([[structure, vis, p['tag']]], p['calc_type'], 
                          p['half_kpts_first_relax'], wf_name=name)

        return wf

@explicit_serialize
class FT_RunVaspOnTheFly(FireTaskTribChem):
    """
    This Firetask is very similar to RunVaspSimulation but it is intended to
    be run on the fly on custom structures, Vasp input sets, and tags that can
    be provided directly from the user.
    If computational parameters are not specified, default values are adopted.

    """
    
    _fw_name = 'Run VASP on the fly'
    required_params = ['structure', 'vis', 'tag', 'calc_type']
    optional_params = ['half_kpts_first_relax', 'wf_name']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Read required and optional input arguments, take shifts from spec
        params = self.read_runtask('RunVaspOnTheFly', fw_spec)
        
        # Prepare the workflow to run the simulation
        wf = self.run_simulation(params)
        
        return FWAction(update_spec=fw_spec, detours=wf)
    
    def run_simulation(self, p):
        """
        Start a subworkflow to run the simulation

        """

        # Check if the provided values are lists
        if not isinstance(p['structure'], list):
            p['structure'] = [p['structure']]
        if not isinstance(p['vis'], list):
            p['vis'] = [p['vis']]
        if not isinstance(p['tag'], list):
            p['tag'] = [p['tag']]
            
        # Check the consistency of the number of calculations
        assert len(p['structure']) == len(p['vis'])
        assert len(p['vis']) == len(p['tag'])

        # Create the data for run_vasp_wf as a list of lists
        data = [[s, v, t] for s, v, t in zip(p['structure'], p['vis'], p['tag'])]
        
        # Create the workflow
        wf = run_vasp_wf(data, p['calc_type'], p['half_kpts_first_relax'], p['wf_name'])
        
        return wf

##############################
# Firetasks to be refactored #
##############################
# @explicit_serialize
# class FT_StartPreRelax(FireTaskTribChem):
#     """ Start a subworkflow as a detour to relax the cell shape and positions
#     of a primitive structure depending on lattice parameters, and then move
#     the optimized primitive structure to the high level database.

#     Parameters
#     ----------
#     mp_id : str
#         ID number for structures in the material project.
#     functional : str
#         functional that is used for the calculation.
#     db_file : str, optional
#         Full path to the db.json file which holds the location and access
#         credentials to the database. If not given uses env_chk.
#     encut : float, optional
#         Energy cutoff for the relaxation run. Defaults to 1000.
#     k_dens : int, optional
#         kpoint density in 1/Angstrom. Defaults to a (quite high) 20.
#     high_level_db : str, optional
#         Name of the high level database the structure should be queried in
#         and later the results written to. Defaults to 'triboflow'.
#     Returns
#     -------
#         Starts a subworkflow as a detour to the current workflow.
#     """
#     _fw_name = 'Start a cell shape relaxation'
#     required_params = ['mp_id', 'functional']
#     optional_params = ['db_file', 'encut', 'k_dens', 'high_level_db']

#     def run_task(self, fw_spec):
#         mp_id = self.get('mp_id')
#         functional = self.get('functional')
#         db_file = self.get('db_file')
#         if not db_file:
#             db_file = env_chk('>>db_file<<', fw_spec)
#         hl_db = self.get('high_level_db', True)

#         # Querying the structure from the high level database.
#         nav_structure = StructureNavigator(
#             db_file=db_file,
#             high_level=hl_db)
#         data = nav_structure.get_bulk_from_db(
#             mp_id=mp_id,
#             functional=functional)

#         prim_struct = data.get('primitive_structure')
#         if not prim_struct:
#             struct = data.get('structure_fromMP')
#             if not struct:
#                 raise LookupError('No structure found in the database that can '
#                                   'be used as input for cell shape relaxation.')
#             struct = Structure.from_dict(struct)
#             prim_struct = SpacegroupAnalyzer(struct).get_primitive_standard_structure()
#             prim_struct = transfer_average_magmoms(struct, prim_struct)
#         else:
#             prim_struct = Structure.from_dict(prim_struct)

#         a = np.round(prim_struct.lattice.a, 6)
#         b = np.round(prim_struct.lattice.b, 6)
#         c = np.round(prim_struct.lattice.c, 6)

#         if data.get('pre_relaxed') or ((a == c) and (b == c)):
#             return FWAction(update_spec=fw_spec)
#         else:

#             # Querying the computational parameters from the high level database
#             # and updating with the optional inputs
#             comp_params = data.get("comp_parameters")
#             encut = self.get('encut', 1000)
#             k_dens = self.get('k_dens', 15)
#             comp_params.update({"encut": encut, "k_dens": k_dens})

#             tag = "CellShapeRelax-{}".format(str(uuid4()))
#             vis = get_custom_vasp_relax_settings(prim_struct, comp_params,
#                                                  'bulk_pos_shape_relax')
#             RelaxWF = dynamic_relax_swf([[prim_struct, vis, tag]])

#             MoveResultsFW = Firework([FT_UpdatePrimStruct(functional=functional,
#                                                           tag=tag, flag=mp_id,
#                                                           high_level_db=hl_db)],
#                                      name='Move pre-relaxed structure for {}'.
#                                      format(prim_struct.formula))
#             MoveResultsWF = Workflow([MoveResultsFW])

#             RelaxWF.append_wf(MoveResultsWF, RelaxWF.leaf_fw_ids)

#             return FWAction(detours=RelaxWF, update_spec=fw_spec)


# @explicit_serialize
# class FT_UpdatePrimStruct(FireTaskTribChem):
#     """ Update the primitive structure in the high level database.

#     Parameters
#     ----------
#     functional : str
#         functional that is used for the calculation.
#     tag : str
#         Unique identifier for the Optimize FW.
#     flag : str
#         An identifyer to find the results in the database. It is strongly
#         suggested to use the proper Materials-ID from the MaterialsProject
#         if it is known for the specific input structure. Otherwise use something
#         unique which you can find again.
#     db_file : str, optional
#         Full path to the db.json file which holds the location and access
#         credentials to the database. If not given uses env_chk.
#     Returns
#     -------
#         Moves the optimized primitive structure from the low level
#         database to the high level database.
#     """
#     _fw_name = 'Update primitive structure in the high level DB'
#     required_params = ['functional', 'tag', 'flag']
#     optional_params = ['db_file', 'high_level_db']

#     def run_task(self, fw_spec):
#         functional = self.get('functional')
#         tag = self.get('tag')
#         flag = self.get('flag')
#         db_file = self.get('db_file')
#         if not db_file:
#             db_file = env_chk('>>db_file<<', fw_spec)
#         hl_db = self.get('high_level_db', True)

#         nav = Navigator(db_file=db_file)
#         calc = nav.find_data('tasks', {'task_label': tag})
#         out = calc['output']

#         struct_dict = {'primitive_structure': out['structure'],
#                        'pre_relaxed': True}

#         nav_high = Navigator(db_file=db_file, high_level=hl_db)
#         nav_high.update_data(
#                     collection=functional+'.bulk_data',
#                     fltr={'mpid': flag},
#                     new_values={'$set': struct_dict},
#                     upsert=False)

#         return FWAction(update_spec=fw_spec)

# @explicit_serialize
# class FT_AddSelectiveDynamics(FireTaskTribChem):
#     """Write a POSCAR with selective dynamics added.
    
#     Parameters
#     ----------
#     structure : pymatgen.core.structure.Structure
#         The structure from which the POSCAR file is written
#     selective_dynamics_array: list, optional
#         Optional Nx3 array of booleans given as a list of lists. N is equal to
#         the number of sites in the structure.
#     ----------
    
#     Returns
#     -------
#     POSCAR: file
#         A POSCAR file is written in the current directory.
    
#     """
    
#     _fw_name = 'Add selective dynamics'
#     required_params = ['structure']
#     optional_params = ['selective_dynamics_array']

#     def run_task(self, fw_spec):
#         struct = self['structure']
        
#         if 'selective_dynamics_array' in self:
#             sd_array = self['selective_dynamics_array']
#             if len(sd_array) != len (struct.sites):
#                 raise SystemExit('Length of provided selective_dynamics array'
#                                  ' is not equal to the number of sites in the'
#                                  ' structure!')
#         else:
#             sd_array = []
#             for i in range(len(struct.sites)):
#                 sd_array.append([False, False, True])
#             #sd_array = np.asarray(sd_array)
        
#         poscar = Poscar(struct, selective_dynamics=sd_array)
#         poscar.write_file('POSCAR')
        
#         spec = fw_spec

#         return FWAction(update_spec = spec)