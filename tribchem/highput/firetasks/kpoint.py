"""Firetasks for converging the k-points convergence in the tribchem project.

Created on Wed Jun 17 15:59:59 2020
@author: mwo
"""

import os
from datetime import datetime
from pprint import pformat

import numpy as np
from fireworks import FWAction, Firework, Workflow, FileWriteTask
from fireworks.utilities.fw_utilities import explicit_serialize
from atomate.utils.utils import env_chk
from atomate.vasp.fireworks.core import StaticFW
from atomate.vasp.powerups import add_modify_incar

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.database.filetools import FileTools
from tribchem.highput.firetasks.core import FireTaskTribChem
from tribchem.highput.utils.check_conv import is_list_converged
from tribchem.highput.utils.vasp_tools import VaspInputSet, MeshFromDensity
from tribchem.highput.utils.io_files import copy_output_files
from tribchem.highput.utils.tasktools import read_default_params
from tribchem.highput.database.dbtools import convert_dict_to_mongodb


currentdir = os.path.dirname(__file__)


@explicit_serialize
class FT_KpointsConvo(FireTaskTribChem):
    """
    Converge the kpoint density by monitoring the total energy.
    
    Runs a series of StaticFWs from atomate with increasing k-mesh density
    and monitors the resulting total energies until convergence is observed.
    
    Parameters
    ----------
    structure : pymatgen.core.structure.Structure
        The structure for which to converge the K-point grids.
    comp_params : dict
        Dictionary of computational parameters for the VASP calculations.

    main_folder : str
            Path to the main folder related to the upper firetask 
            FT_StartKpointsConvo. Within this folder all the folders related to each
            VASP calculation will be created.

    tag : str
        String from a uuid4 to identify the shared data entry in the database
        and group the deformations calculations together.
    kdens_start : int, optional
        Starting k-mesh density value for the first run. Defaults to 500.
    kdens_incr : int, optional
        Increment for the k-mesh density during the convergence.
        Defaults to 50.

    n_converge : int, optional
        Number of calculations that have to show the same energy as the last
        one as to signify convergence, Defaults to 3.

    db_file : str
        Full path to the db.json file that should be used. Defaults to
        '>>db_file<<', to use env_chk.
        
    Returns
    -------
    FWActions that produce detour subworkflows until convergence is reached.
    
    """
    
    _fw_name = 'Kmesh density convergence'
    required_params = ['structure', 'mid', 'collection', 'flag', 'tag', 
                        'comp_params', 'main_folder']
    optional_params = ['formula', 'name', 'db_file', 'high_level', 'kdens_start',
                       'kdens_incr', 'n_converge', 'cluster_params', 'upsert']

    def run_task(self, fw_spec):
        
        # Read required and optional input arguments
        params, data, e_tolerance, e_tol = self.read_runtask('KpointsConvo', fw_spec)
        e_list, kdens_list = self.read_data_dict(data)
        
        if e_list is None:
            params['comp_params']['kdens'] = params['kdens_start']
            k_convo_wf = self.simulation_from_scratch(params)
            
            return FWAction(detours=k_convo_wf)

        else:
            lists= e_list, kdens_list
            tols = e_tol, e_tolerance
            
            # Convergence is reached
            if is_list_converged(e_list, e_tol, params['n_converge']):                
                wf = self.simulation_converged(lists=lists, tols=tols, p=params)
                
                if wf is not None:
                    return FWAction(update_spec=fw_spec, detours=wf)

                else:  
                    return FWAction(update_spec=fw_spec)

            else:
                wf = self.simulation_one_shot(lists=lists, p=params)

                return FWAction(detours=wf)

    def read_runtask(self, dfk, fw_spec={}, defaults='defaults.json'):
        """
        Read input arguments and the energy tolerance.
        
        """

        p = super().read_runtask(dfk, fw_spec, defaults)

        # Read default values for cluster params if missing
        dfl = currentdir + '/' + 'defaults.json'
        cp = p['cluster_params']
        p['cluster_params'] = read_default_params(dfl, 'cluster_params', cp)

        # Get the relative energy tolerance (eV/atom) and the absolute tol.
        e_tolerance = p['comp_params'].get('energy_tol')
        e_tol = e_tolerance * p['structure'].num_sites

        # Get the data arrays from the database (returns None when not there)
        nav = Navigator(db_file=p['db_file'])
        data = nav.find_data(collection='kpoints_data', fltr={'tag': p['tag']})

        return p, data, e_tolerance, e_tol

    def read_data_dict(self, data):
        """
        Define the list of energies and see if kpoints data is already present.

        """
        
        # Read kpoints data stored in data dict
        if data:
            e_list = data.get('e_list')
            kdens_list = data.get('kdens_list')
        else:
            e_list = None
            kdens_list = None
    
        return e_list, kdens_list

    def simulation_from_scratch(self, p):
        """
        Start from scratch the evaluation of the energy cutoff and lattice
        parameters.
        
        """

        label = p['tag'] + ' calc 0'
        visgen = VaspInputSet(p['structure'], p['comp_params'], 'bulk_from_scratch')
        vis = visgen.get_vasp_settings()

        KPTS = MeshFromDensity(p['structure'], p['kdens_start'])
        kpoints = KPTS.get_kpoints()
        #kpoints = get_generalized_kmesh(struct, kdens_start)
        kdens_list = [p['kdens_start']]

        run_vasp_fw = StaticFW(structure=p['structure'], vasp_input_set=vis, name=label)
        
        parse_and_update_fw = Firework(
            [FT_UpdateELists(
                tag=p['tag'],
                tag_calc=label,
                kpoints=kpoints.kpts,
                main_folder=p['main_folder'],
                db_file=p['db_file']),
             FT_KpointsConvo(
                structure=p['structure'],
                mid=p['mid'],
                comp_params=p['comp_params'],
                main_folder=p['main_folder'],
                tag=p['tag'],
                flag=p['flag'],
                collection=p['collection'],
                formula=p['formula'],
                name=p['name'],
                db_file=p['db_file'],
                high_level=p['high_level'],
                kdens_incr=p['kdens_incr'],
                kdens_start=p['kdens_start'],
                n_converge=p['n_converge'],
                upsert=p['upsert'],
                cluster_params=p['cluster_params'])],
            name='Update Energy Lists and Loop')

        k_convo_wf = Workflow([run_vasp_fw, parse_and_update_fw],
                              {run_vasp_fw: [parse_and_update_fw]},
                              name='Kpoint Convergence Loop')

        k_convo_wf = add_modify_incar(k_convo_wf)

        # Set up the entry for the data arrays in the database
        formula=p['structure'].composition.reduced_formula
        set_data = {'tag': p['tag'],
                    'chem_formula': formula,
                    'created_on': str(datetime.now()),
                    'kdens_list': kdens_list,
                    'last_mesh': kpoints.kpts,
                    'e_list': [],
                    'k_meshes': [kpoints.as_dict()]}
        nav = Navigator(p['db_file'])
        nav.insert_data(collection='kpoints_data', data=set_data)

        return k_convo_wf

    def simulation_converged(self, lists, tols, p):
        """
        Stop the simulation if the convergence is reached.

        """
        
        e_list, kdens_list = lists
        e_tol, e_tolerance = tols
        
        final_kdens = kdens_list[-p['n_converge']]
        final_e = e_list[-p['n_converge']]
        print('')
        print(' Convergence reached for total energy per atom.')
        print(' Final kdens = {}; Final energy = {} eV;'
              .format(final_kdens, final_e))
        print('')
        print('')

        out_dict = {
            'data': {
                'kdens_info': 
                    {
                        'final_kdens': final_kdens,
                        'final_energy': final_e,
                        'energy_list': e_list,
                        'kdens_list': kdens_list,
                        'energy_tol_abs': e_tol,
                        'energy_tol_rel': e_tolerance
                    }
                },
            'comp_params.kdens': final_kdens
            }

        # Upload data in high level 
        nav_high = Navigator(db_file=p['db_file'], high_level=p['high_level'])
        fltr = self.set_filter(p, keys=['mid', 'formula', 'name'])
        nav_high.update_data(
            collection=p['collection'],
            fltr=fltr,
            new_values={'$set': convert_dict_to_mongodb(out_dict)},
            upsert=p['upsert'])

        # Update data in low level DB
        nav = Navigator(db_file=p['db_file'])
        nav.update_data(
            collection='kpoints_data',
            fltr={'tag': p['tag']},
            new_values={'$set': {'final_kdens': final_kdens,
                                 'final_energy': final_e,
                                 'energy_list': e_list,
                                 'kdens_list': kdens_list,
                                 'energy_tol_abs': e_tol,
                                 'energy_tol_rel': e_tolerance}})

        # Handle file output if asked
        wf = None
        if p['cluster_params']['file_output']:                 
            write_ft = FileWriteTask(
                files_to_write=[{'filename': p['flag']+'_kpts_out.txt',
                                 'contents': pformat(out_dict)}])
            copy_ft = copy_output_files(
                file_list=[p['flag'] + '_kpts_out.txt'],
                output_dir=p['cluster_params']['output_dir'],
                remote_copy=p['cluster_params']['remote_copy'],
                server=p['cluster_params']['server'],
                user=p['cluster_params']['server'],
                port=p['cluster_params']['port'])
            fw = Firework([write_ft, copy_ft],
                          name='Copy KpointsConvo SWF results')
            wf = Workflow.from_Firework(fw, name='Copy KpointsConve SWF results')

        return wf
    
    def simulation_one_shot(self, lists, p):
        """
        Run the next step in the convergence process

        """
        
        e_list, kdens_list = lists

        calc_nr = len(e_list)
        label = p['tag'] + ' calc ' + str(calc_nr)
        
        kdens = kdens_list[-1] + p['kdens_incr']
        KPTS = MeshFromDensity(p['structure'], kdens,
                               compare_density=kdens_list[-1])

        while KPTS.are_meshes_the_same():
            kdens = kdens + p['kdens_incr']
            KPTS = MeshFromDensity(p['structure'], kdens,
                                   compare_density=kdens_list[-1])
        kpoints = KPTS.get_kpoints()
        
        p['comp_params']['kdens'] = kdens
        visgen = VaspInputSet(p['structure'], p['comp_params'], 'bulk_from_scratch')
        vis = visgen.get_vasp_settings()

        run_vasp_fw = StaticFW(structure=p['structure'], vasp_input_set=vis, name=label)

        parse_and_update_fw = Firework(
            [FT_UpdateELists(
                tag=p['tag'],
                tag_calc=label,
                kpoints=kpoints.kpts,
                main_folder=p['main_folder'],
                db_file=p['db_file']),
             FT_KpointsConvo(
                structure=p['structure'],
                mid=p['mid'],
                comp_params=p['comp_params'],
                tag=p['tag'],
                flag=p['flag'],
                main_folder=p['main_folder'],
                collection=p['collection'],
                formula=p['formula'],
                name=p['name'],
                db_file=p['db_file'],
                high_level=p['high_level'],
                kdens_incr=p['kdens_incr'],
                kdens_start=p['kdens_start'],
                n_converge=p['n_converge'],
                upsert=p['upsert'],
                cluster_params=p['cluster_params'])],
            name='Update Energy Lists and Loop')

        k_convo_wf = Workflow([run_vasp_fw, parse_and_update_fw],
                              {run_vasp_fw: [parse_and_update_fw]},
                              name='Kpoint Convergence Loop')
        
        k_convo_wf = add_modify_incar(k_convo_wf)
        
        # Update Database entry for Encut list
        nav = Navigator(db_file=p['db_file'])
        nav.update_data(
            collection='kpoints_data',
            fltr={'tag': p['tag']},
            new_values={'$push': {'kdens_list': kdens,
                                  'k_meshes': kpoints.as_dict()},
                        '$set': {'last_mesh': kpoints.kpts}})

        return k_convo_wf

@explicit_serialize
class FT_UpdateELists(FireTaskTribChem):
    """
    Fetch information about the last vasp calc and update the lists.
    
    Used with FT_KpointsConvo to converge k-mesh density using total energy
    calculations with vasp. This Firetasks reads the total energy from the
    StaticFW (atomate) with the task_label 'calc_name'. The shared date entry
    for the convergence is then identified via a tag and updated with the new
    total energy.
    
    Parameters
    ----------
    tag : str
        String from a uuid4 to identify the shared data entry in the database.

    calc_label : str
        A label for the specific calculation derived from the tag.
    
    main_folder : str
        Path to the main folder related to the upper firetask 
        FT_StartKpointsConvo. Within this folder all the folders related to each
        VASP calculation will be created.
        
    db_file : str, optional
        Full path to the db.json file detailing access to the database.
        Defaults to '>>db_file<<' to use with env_chk.

    """

    _fw_name = 'Update Energy Lists'
    required_params = ['tag', 'tag_calc', 'main_folder', 'kpoints']
    optional_params = ['db_file']

    def run_task(self, fw_spec):

        # Read required and optional input arguments
        params = self.read_runtask('UpdateELists', fw_spec)

        # Update default params
        self.update_elist(params)

    def update_elist(self, p):

        # Get energy from last vasp run
        nav = Navigator(db_file=p['db_file'])
        vasp_calc = nav.find_data(collection='tasks',
                                  fltr={'task_label': p['tag_calc']})
        energy = vasp_calc['output']['energy']

        # Update data array in the database
        nav.update_data(collection='kpoints_data', fltr={'tag': p['tag']},
                        new_values={'$push': {'e_list': energy}})

        # Create a unique main folder for the corresponding kpoints value
        main_folder = p['main_folder']

        FileTools.move_files(nav, main_folder=main_folder,
                             task_label=p['tag_calc'],
                             custom_name=str(np.array(p['kpoints'])).strip('[').strip(']'))
