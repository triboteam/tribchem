#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 21:49:59 2021

@author: glosi000
"""

from monty.json import jsanitize
from operator import itemgetter

from pymatgen.core.structure import Structure
from fireworks import FWAction, explicit_serialize

from tribchem.highput.firetasks.core import FireTaskTribChem
from tribchem.highput.database.dbtools import read_one_entry
from tribchem.physics.tribology.potensurf import get_pes
from tribchem.highput.utils.tasktools import get_miller_str, select_struct_func
from tribchem.highput.database.dbtools import convert_image_to_bytes
from tribchem.physics.tribology.ppes import fit_ppes
from tribchem.physics.base.solidstate import get_cell_info
from tribchem.physics.base.units import Converter
from tribchem.highput.utils.vasp_tools import VaspInputSet


@explicit_serialize
class FT_PotentialEnergySurface(FireTaskTribChem):
    """
    Collect the data to compute the Potential Energy Surface for a given 
    interface, arrange the data necessary to create it, and eventually plot 
    and save it.

    Uses the perviously computed energies for the unique high-symmetry points
    and copies them to all the correct replica points. Replicates the points
    and fits the PES using radial basis functions. Output is saved in the
    database and if wanted also to files.

    """

    required_params = ['mid', 'miller', 'collection', 'calc_type']
    optional_params = ['formula' ,'name', 'db_file', 'database', 'pes_type',
                       'to_file']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Read required and optional input arguments
        params = self.read_runtask('PotentialEnergySurface', fw_spec)

        # Store the data in a more accessible way in the high level db
        hs_all, unique_values, cell = self.arrange_data(params)

        # Compute and save the PES
        self.compute_pes(hs_all, unique_values, cell, params)

        return FWAction(update_spec=fw_spec)

    def arrange_data(self, p):
        """
        Store the data in the pes, entry reading them from output. This will
        prepare a location in pes_entry related to the particular pes_type
        you want to calculate.

        """

        # Retrieve the document with all the data
        fltr = self.set_filter(p)
        field, _ = self.query_db(collection=p['collection'], fltr=fltr,
                                 db_file=p['db_file'], database=p['database'])

        # Recover the shift names and values
        shifts = read_one_entry(field, ['highsym', 'inter_unique'])
        all_shifts = read_one_entry(field, ['highsym', 'inter_all'])

        # Set the name for the pes based on calc_type
        pes_calc = 'pes_scf' if p['calc_type'] in VaspInputSet.static_types else 'pes'

        # Collect all values associated to shifts and specified by pes_type
        values = []
        for s, d in shifts.items():
            x, y = d[0]
            v = read_one_entry(field, [pes_calc, 'data', str(s), 'output', p['pes_type']])
            values.append([s, x, y, v])
        sorted_values = sorted(values, key=itemgetter(3))

        # Find out the minimum and maximum stacking for values
        min_stack = sorted_values[0][0]
        max_stack = sorted_values[-1][0]
        struct_min = read_one_entry(field, [pes_calc, 'data', str(min_stack), 'output', 'structure'])
        
        # Prepare the grid energy values to be saved in J/m^2
        area, _ = get_cell_info(struct_min['lattice']['matrix'])
        sorted_values_jm2 = sorted_values.copy()
        sorted_values_jm2 = [[s, x, y, Converter.eV_to_Jm2(v, area * 1e-20)]
                             for s, x, y, v in sorted_values]

        # Prepare the data to be stored in the Database
        data = {
            p['pes_type']: {
                'grid': {
                    'hs_unique_ev': sorted_values,
                    'hs_unique_jm2': sorted_values_jm2
                    },
                'max_site': str(max_stack),
                'min_site': str(min_stack)
                }
            }

        # Store information associated to pes_type for min and max stackings
        self.update_db(data, collection=p['collection'], entry=pes_calc,
                       db_file=p['db_file'], database=p['database'], fltr=fltr)

        # Store the minimum energy structure in the high layer of the field
        self._update_opt_structure(min_stack, max_stack, field, pes_calc, fltr, p)

        # Take out the cell of the lattice matrix
        func = select_struct_func(struct_min['@class'])
        cell = func.from_dict(struct_min).lattice.matrix

        return all_shifts, [sorted_values, sorted_values_jm2], cell

    def _update_opt_structure(self, min_stack, max_stack, field, pes_calc, fltr, p):
        """
        Update the optimal structure in the right position of the high level
        database, when pes_type is 'energy'.

        """

        # Make the update only in the case of energy pes_type
        if p['pes_type'] == 'energy':

            # Insert both the minimum and the maximum energy stack
            for s, n in zip([min_stack, max_stack], ['min', 'max']):
                data = read_one_entry(field, [pes_calc, 'data', str(s), 
                                             'output', 'structure'])
                entry = ['structure', 'opt', pes_calc, n]
    
                # Make the update
                self.update_db(data, collection=p['collection'], entry=entry,
                               fltr=fltr, db_file=p['db_file'], database=p['database'])

    def compute_pes(self, hs_all, unique_values, cell, p):
        """
        Compute the PES and save the relevant data.

        """

        # Define the name of the pes objects
        if p['name'] is not None:
            pes_name = str(p['name'])
        else:
            pes_name = p['formula'].split('_')[0] + get_miller_str(p['miller'][0]) +\
                       p['formula'].split('_')[1] + get_miller_str(p['miller'][1])

        # Obtain PES data in eV and J/m^2 interpolating with RBF
        plot_name = './pes_' + pes_name + '.png'
        _, v_list, data = get_pes(hs_all, unique_values[0], cell, 
                                  title=pes_name, to_fig=None)
        _, v_list_jm2, data_jm2 = get_pes(hs_all, unique_values[1], cell, 
                                          title=pes_name, to_fig=plot_name)

        # Prepare data do be stored in the database
        data = {
            p['pes_type']: {
                'grid':
                    {
                        'hs_all_ev': jsanitize(v_list),
                        'hs_all_jm2': jsanitize(v_list_jm2)
                        },
                'pes_ev': jsanitize(data),
                'pes_jm2': jsanitize(data_jm2),
                'image': convert_image_to_bytes('./' + plot_name)
                }
            }

        # Set the name for the pes based on calc_type
        pes_calc = 'pes_scf' if p['calc_type'] in VaspInputSet.static_types else 'pes'

        self.update_db(data, collection=p['collection'], entry=pes_calc,
                       db_file=p['db_file'], database=p['database'], 
                       fltr=self.set_filter(p))

        # Save the grid points to file is asked so
        if p['to_file']:
            data.dump('pes_data' + pes_name + '.dat')
            data.dump('interpolated_pes_data_' + pes_name + '.dat')

@explicit_serialize 
class FT_FitPPES(FireTaskTribChem):
    """
    Retrieve PPES from the high level database and rearrange the data in a 
    more ordered way. The curve is also fitted with a mathematical equation if
    a fit option is provided.

    Parameters
    ----------
    interface : pymatgen.structure
        Interface.

    distance_list : list of float, optional
        Modification of the equilibrium distance between the slabs.

    tag : str
        Unique tag to identify the calculations.

    comp_params : dict
        Dictionary of computational parameters for the VASP calculations.

    structure_name : str
        Name of the structure, which is made up by the materials project ids of 
        the slabs and the miller indexes.
        E.g.: mp-134_mp-30_001_001
    
    mid : list
            List containing all the materials project ids of the interface.
            E.g.: ['mp-134', 'mp-30']
    
    miller : list
        List containing all the miller indxes of the slabs of the interface.
        E.g.: [[1, 0, 0], [1, 0, 0]]

    collection : str
        Name of the collection where to save the fitted value of PPES.

    db_file : str, optional
        Full path to the db.json file that should be used. Defaults to
        '>>db_file<<', to use env_chk.

    database : str, optional
        Database where to save the data.

    """
    
    _fw_name = 'Fit PPES with a custom equation'
    required_params = ['mid', 'miller', 'shifts', 'collection']
    optional_params = ['formula', 'name', 'db_file', 'database', 'ppes_type',
                       'site', 'fit', 'check_entry']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Get the runtask parameters
        params = self.read_runtask('FitPPES', fw_spec, 'defaults.json')
        
        # Perform the fit and save it in the Database
        self.arrange_data(params)
        
        return FWAction(update_spec=fw_spec)

    def arrange_data(self, p):
        """
        Retrieve the data relative to the ppes_type, fit it and store the
        final results in the database.

        """

        # Retrieve the document with all the data
        fltr = self.set_filter(p)
        field, _ = self.query_db(collection=p['collection'], fltr=fltr,
                                 db_file=p['db_file'], database=p['database'])

        # Recover the values of ppes_type to perform the fit
        values = []
        values_jm2 = []
        for s in p['shifts']:
            ts = str(s).replace('.', ',')
            v = read_one_entry(field, ['ppes', p['check_entry'], 'data', p['site'], 'shift_'+ts, 
                                       'output', p['ppes_type']])
            values.append([s, v])

            # Convert to J/m^2 in the energy case
            if p['ppes_type'] == 'energy':
                struct = read_one_entry(field, ['ppes', p['check_entry'],'data', p['site'],
                                                'shift_'+ts, 'output', 'structure'])
                struct = Structure.from_dict(struct)
                area, _ = get_cell_info(struct.lattice.matrix)
                v_jm2 = Converter.eV_to_Jm2(v, area * 1e-20)
                values_jm2.append([s, v_jm2])

        # Try to fit the PPES
        fit_dict = None
        if p['fit']:
            fit_dict = fit_ppes(values, p['fit'])

        # Create the final data dictionary
        if p['ppes_type'] == 'energy':
            values_dict = {'ppes_ev': values, 'ppes_jm2': values_jm2}
        else:
            values_dict = {'ppes': values}
        
        if fit_dict is not None:
            values_dict[p['fit']] = fit_dict

        data = {p['ppes_type']: {p['site']: values_dict}}
        
        # Update them in the Database
        self.update_db(data, collection=p['collection'], entry=['ppes', p['check_entry']],
                       db_file=p['db_file'], database=p['database'], fltr=fltr)
