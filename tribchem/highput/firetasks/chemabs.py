#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 16:08:22 2021

@author: glosi000

"""

from monty.json import jsanitize
from uuid import uuid4

import numpy as np
from fireworks import explicit_serialize, FWAction, Firework, Workflow
from pymatgen.core.surface import Structure, Slab

from tribchem.highput.firetasks.core import FireTaskTribChem
from tribchem.highput.workflows.wfcore import run_and_save
from tribchem.highput.utils.manipulate_struct import generate_chemabs_inputs, structure_to_slab
from tribchem.highput.workflows.wfcore import GeneralTribchemWF as GTWF
from tribchem.highput.firetasks.dbtask import FT_SaveVASPOutputs
from tribchem.highput.utils.vasp_tools import VaspInputSet
from tribchem.highput.database.query import read_multiple_entry
from tribchem.physics.chemabs.adsorption import ChemAbs
from tribchem.physics.base.solidstate import get_cell_info
from tribchem.physics.base.units import Converter


@explicit_serialize
class FT_AdsorbateSurface(FireTaskTribChem):
    """
    Get the adsorption sites of the slab.
    
    Parameters
    ----------
    mid : str
        MP-id of the structure from the MP database.
    
    miller : list of int, or str
        Miller indexes (h, k, l) to select the slab orientation.
    
    slab_collection : str
        Name of the collection in the Database where the interface is stored.
    
    ad_collection : str
        Name of the collection in the database where the adsoption sites are
        saved.
    
    adsorbed : str
        Formula of the adsorbed.
    
    both_surfaces : bool
        If True the adsorbeds are add to both the faces of the slab.
    
    db_file : str or None
        Path to the location of the database. If nothing is provided it will be
        searched by env_check from Atomate. The default is None.
    
    database : str or None, optional
        Name of the database, typically a high level area just containing
        clean and final results, where the interface will be inserted.
        The default is 'tribchem'.

    """
    
    required_params = ['mid', 'miller', 'adsorbate', 'collection', 'collection_slab']
    optional_params = ['formula', 'name', 'db_file', 'database', 'entry',
                       'pymatgen_opt', 'coverage', 'both_surfaces']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """ 

        # Define the json file containing default values and read parameters
        params = self.read_runtask('AdsorbateSurface', fw_spec)

        # Retrieve the slab from the database
        slab, comp_params = self.get_slab(params)

        # Calculate the HS sites and create adsorption structures, save in DB
        self.adsorbate_surface(slab, comp_params, params)

        return FWAction(update_spec=fw_spec)
    
    def get_slab(self, p):
        """
        Get the slab from the database.

        """
        
        # Query the database for the slab
        fltr = self.set_filter(p, ['mid', 'formula', 'name', 'miller'])
        field, slab = self.query_db(collection=p['collection_slab'], fltr=fltr,
                                    db_file=p['db_file'], entry=p['entry'],
                                    database=p['database'])
        
        slab = Slab.from_dict(slab)
        comp_params = field['comp_params']

        return slab, comp_params

    def adsorbate_surface(self, slab, cp, p):
        """
        Generate the surfaces with the adsorption on top of it, and store
        everything in the Database.

        """

        # Calculate the adsorption sites of the slab
        chemabs = ChemAbs(slab)
        chemabs.add_adsorbate(p['adsorbate'], p['coverage'], **p['pymatgen_opt'])

        # Cast to list the values of the dictionary
        sites = {
            'unique': jsanitize(chemabs.sites),
            'all': jsanitize(chemabs.all_sites)
            }

        # Prepare the structures dictionary
        structs = {}
        for k in chemabs.sites.keys():

            # HERE WE SET THE MAGMOMS OF THE ADATOM(S) TO ZERO!!!
            # This is important to prevent Atomate to crash
            ads_slab = chemabs.ads_structure[k].as_dict()
            for a in ads_slab['sites']:
                if 'magmom' in a['properties'].keys():
                    if a['properties']['magmom'] is None: a['properties']['magmom'] = 0
                else:
                    a['properties']['magmom'] = 0

            structs[k] = {
                'init': ads_slab,
                'repeat': jsanitize(chemabs.repeat[k])
                }

        # Define the new name for the slab + adsorbate
        tmp = p['formula'] if p['formula'] is None else slab.composition.reduced_formula
        name = tmp + '-' + p['adsorbate']

        # Update the database for sites and adsorbated structures
        data = {
            'mid': p['mid'],
            'formula': p['formula'],
            'miller': p['miller'],
            'name': name,
            'adsorbate': p['adsorbate'],
            'comp_params': cp,
            'highsym': sites,
            'structure': {
                'coverage_'+str(float(p['coverage'])).replace('.', ','): structs
                }
            }
        fltr = self.set_filter(p, ['mid', 'formula', 'name', 'miller', 'adsorbate'])
        self.update_db(data=data, collection=p['collection'], 
                       db_file=p['db_file'], database=p['database'], fltr=fltr)

@explicit_serialize
class FT_RunCleanSlab(FireTaskTribChem):
    """
    Simulate a clean slab in order to calculate the chemisorption energy of
    an adatom on top of a surface. It checks if the slab has already been
    relaxed, if a slab has been replicated it is created and then relaxed.

    """

    required_params = ['mid', 'miller', 'adsorbate', 'collection', 
                       'collection_slab', 'main_folder_slab']
    optional_params = ['formula', 'db_file', 'database', 'coverage', 'sites',
                       'override', 'calc_type']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """
        
        # Read required and optional input arguments
        params = self.read_runtask('RunCleanSlab', fw_spec)
        
        # Check the slabs and prepare the VASP inputs
        inputs, entries, repeat = self.check_data(params)
        
        # Run the workflow
        wf = self.run_simulation(inputs, entries, repeat, params)

        if wf is None:
            return FWAction(update_spec=fw_spec)
        else:
            return FWAction(detours=wf)

    def check_data(self, p):
        """
        Check if the slab has been already simulated by checking if its final
        energy is present. In case of a replicated slab, the calculation is 
        made from scratch to relax it.

        """

        fltr_slab = self.set_filter(p, ['mid', 'formula', 'miller'])
        field_slab, en_1 = self.query_db(p['collection_slab'], p['db_file'], 
                                         p['database'], entry=['data', 'energy'],
                                         fltr=fltr_slab)
        
        fltr = self.set_filter(p, ['mid', 'formula', 'miller', 'adsorbate'])
        field, _ = self.query_db(p['collection'], p['db_file'], p['database'],
                                 fltr=fltr)

        # Find out the sites that are under study
        cvr = str(float(p['coverage'])).replace('.', ',')
        k = read_multiple_entry(field, ['structure', 'coverage_'+cvr]).keys()
        if p['sites'] == 'all':
            sites = np.array(list(k))
        else:
            sites = [p['sites']] if not isinstance(p['sites'], list) else p['sites']
            if not set(sites).issubset(set(k)):
                raise ValueError('Wrong input argument: sites. It should be a '
                                 'subset of the actual sites in the Database or "all"')
        
        # Grep all the replica options of the cells and select unique ones
        repeat = []
        for s in sites:
            entry = ['structure', 'coverage_'+cvr, s, 'repeat']
            repeat.append(list(read_multiple_entry(field, entry)))
        repeat = [list(x) for x in set(tuple(x) for x in repeat)]
        if [1, 1, 1] in repeat and en_1 is not None: 
            repeat.remove([1, 1, 1])
        
        # Check the slabs that need to be done
        to_do = [True] * len(repeat)
        for i, r in enumerate(repeat):
            str_r = 'replica_{}_{}_{}'.format(str(r[0]), str(r[1]), str(r[2]))
            if str_r in field_slab['data'].keys():
                en = field_slab['data'][str_r].get('energy', None)
            else:
                en = None
            if en is not None: 
                to_do[i] = False
        repeat = [x for i, x in enumerate(repeat) if to_do[i]]

        # Replicate the slabs and then prepare VASP inputs
        if repeat != []:
            if not [1, 1, 1] in repeat:
                slab = Slab.from_dict(field_slab['structure']['opt'])
            else:
                slab = Slab.from_dict(field_slab['structure']['init'])
            comp_params = field_slab['comp_params']
            
            # Build the super-cells
            structures = []
            for r in repeat:
                s = slab.copy()
                s.make_supercell(r)
                structures.append(s)
            
            # Create the VASP input sets to run the simulation
            vis, tags = [], []
            comp_params = field['comp_params']
            for i, s in enumerate(structures):
                label = 'Clean Slab {} - '.format(str(repeat[i])) + str(uuid4())
                v = VaspInputSet(s, comp_params, p['calc_type']).get_vasp_settings()
                vis.append(v)
                tags.append(label)
            
            # Create the entries to store the data
            inputs = [structures, vis, tags]
            entries = GTWF._select_entry('chemabs-slab', repeat=repeat)
        else:
            inputs = None
            entries = None

        return inputs, entries, repeat

    def run_simulation(self, inputs, entries, repeat, p):
        """
        Start a detour and calculate the necessary slab structures.
        
        """
    
        if inputs is None or entries is None:
            return None
        else:
            structure, vis, tags = inputs
            entry_from, entry_to = entries

        # Create the FW to run VASP and save the results
        fw_adslab_1, fw_adslab_2 = run_and_save(
            p['mid'], structure, vis, tags, p['calc_type'],
            p['collection_slab'], entry_from, entry_to, p['db_file'],
            p['database'], formula=p['formula'], miller=p['miller'],
            fw_names=['Simulate clean slab', 'Save clean slab result'])

        # Save the slab with the adsorbate results
        miller_str = ''.join([str(i) for i in p['miller']])

        # Get sites for saving the VASP output for each of them
        # Query the database to find the document
        custom_names = ['/'+miller_str+'/output_replica_{}_{}_{}'.format(str(r[0]), str(r[1]), str(r[2])) for r in repeat]
        
        ft_adslab_3 = FT_SaveVASPOutputs(
            tag=tags, folder=p['main_folder_slab'], 
            custom_name=custom_names, 
            db_file=p['db_file'])
        fw_adslab_3 = Firework(
            [ft_adslab_3],
            name='Save VASP output files for clean slab {}'.format(p['formula']))

        fws = [fw_adslab_1, fw_adslab_2, fw_adslab_3]
        links = {fw_adslab_1: fw_adslab_2, fw_adslab_2: fw_adslab_3}
        
        # Prepare the wf for the detour
        wf = Workflow(fws, links, name='Simulate slab and adatom: {}'.format(p['formula']))
        
        return wf

@explicit_serialize
class FT_RunSlabAdatom(FireTaskTribChem):
    """
    Simulate a Slab with an adatom on top of it, on different sites.
    This is a shortcut necessary to ChemAdsWF.adsorbate_atom, to avoid
    querying the DB for slab+adatom when this structure is not there yet.

    """
    
    required_params = ['mid', 'miller', 'adsorbate', 'collection', 'main_folder_slab_atom']
    optional_params = ['formula', 'db_file', 'database', 'check_entry', 'coverage', 
                       'sites', 'override', 'calc_type']
    
    def run_task(self, fw_spec):
        """ Run the Firetask.
        """
        
        # Read required and optional input arguments
        params = self.read_runtask('RunSlabAdatom', fw_spec)
        
        # Check all the data and prepare the inputs to run VASP
        inputs, entries = self.check_data(params)

        # Run the simulations with VASP
        wf = self.run_simulation(inputs, entries, params)
        
        if wf is None:
            return FWAction(update_spec=fw_spec)
        else:
            return FWAction(detours=wf)
    
    def check_data(self, p):
        """
        Check the sites and if the calculation are done, return all the
        necessary to run a VASP simulation.

        """
        
        # Query the database to find the document
        cvr = str(float(p['coverage'])).replace('.', ',')
        fltr = self.set_filter(p, ['mid', 'miller', 'adsorbate'])
        field, _ = self.query_db(collection=p['collection'], db_file=p['db_file'],
                                 database=p['database'], fltr=fltr)
        
        # Find out the sites and check if they are fine
        k = read_multiple_entry(field, ['structure', 'coverage_'+cvr]).keys()
        if p['sites'] == 'all':
            sites = np.array(list(k))
        else:
            sites = [p['sites']] if not isinstance(p['sites'], list) else p['sites']
            if not set(sites).issubset(set(k)):
                raise ValueError('Wrong input argument: sites. It should be a '
                                 'subset of the actual sites in the Database or "all"')

        # Check if the calculation is already done
        if not p['override'] and p['check_entry']:
            check_entry = [['structure', 'coverage_'+cvr, s, 'opt'] for s in sites]
            if len(check_entry) != len(sites):
                raise ValueError('Check entry and sites should have same length')

            not_done = []
            for s, en in zip(sites, check_entry):
                ntd= not self.is_done(p['collection'], p['db_file'], 
                                      p['database'], en, fltr)
                not_done.append(ntd)
            sites = np.array(sites)[not_done].tolist()

        if len(sites) == 0:
            return None, None
        else:
            # Generate the inputs for VASP
            inputs = generate_chemabs_inputs(field, cvr, sites, calc_type=p['calc_type'])
            entries = GTWF._select_entry('chemabs', coverage=p['coverage'], sites=sites)

            return inputs, entries

    def run_simulation(self, inputs, entries, p):
        """
        Prepare the workflows to run VASP with a detour and save the results.

        """
        
        if inputs is None or entries is None:
            return None
        else:
            structure, vis, tags = inputs
            entry_from, entry_to = entries

        # Create the FW to run VASP and save the results
        fw_adslab_1, fw_adslab_2 = run_and_save(
            p['mid'], structure, vis, tags, p['calc_type'],
            p['collection'], entry_from, entry_to, p['db_file'],
            p['database'], formula=p['formula'], miller=p['miller'],
            adsorbate=p['adsorbate'], 
            fw_names=['Simulate Slab+adsorbate', 'Save Slab+adsorbate result'])

        # Save the slab with the adsorbate results
        miller_str = ''.join([str(i) for i in p['miller']])

        # Get sites for saving the VASP output for each of them
        # Query the database to find the document
        cvr_i = 'coverage_' + str(float(p['coverage']))
        cvr = 'coverage_' + str(float(p['coverage'])).replace('.', ',')
        fltr = self.set_filter(p, ['mid', 'miller', 'adsorbate'])
        sites = self.__get_sites(p, fltr, cvr)
        custom_names = ['/'+miller_str+'/'+cvr_i+'/'+site+'/output' for site in sites]
        
        ft_adslab_3 = FT_SaveVASPOutputs(
            tag=tags, folder=p['main_folder_slab_atom'], 
            custom_name=custom_names, 
            db_file=p['db_file'])
        fw_adslab_3 = Firework(
            [ft_adslab_3],
            name='Save VASP output files for clean slab {}'.format(p['formula']))

        fws = [fw_adslab_1, fw_adslab_2, fw_adslab_3]
        links = {fw_adslab_1: fw_adslab_2, fw_adslab_2: fw_adslab_3}
        
        # Prepare the wf for the detour
        wf = Workflow(fws, links, name='Simulate slab and adatom: {}'.format(p['formula']))
        
        return wf
    
    def __get_sites(self, p, fltr, cvr):
        """ Get the sites"""

        # Find out the sites and check if they are fine
        field, _ = self.query_db(collection=p['collection'], db_file=p['db_file'],
                                 database=p['database'], fltr=fltr)
        k = read_multiple_entry(field, ['structure', cvr]).keys()
        if p['sites'] == 'all':
            sites = np.array(list(k))
        else:
            sites = [p['sites']] if not isinstance(p['sites'], list) else p['sites']
            if not set(sites).issubset(set(k)):
                raise ValueError('Wrong input argument: sites. It should be a '
                                 'subset of the actual sites in the Database or "all"')
        
        return sites

@explicit_serialize
class FT_ChemisorptionEnergy(FireTaskTribChem):
    """
    Calculate the chemisorption energy of a molecule/atom adsorbed on top of
    a surface. It requires the knowledge of the energy of the isolated 
    molecule, the clean slab, and the slab with the adsorbate on it.
    So that the Chemisorption Energy is found as:
        
        E_{slab + molecule} - E_{slab} - E_{molecule}

    """
    
    required_params = ['mid', 'adsorbate', 'miller', 'collection', 
                       'collection_slab', 'collection_atom']
    optional_params = ['formula', 'db_file', 'database', 'coverage', 'sites']

    def run_task(self, fw_spec):
        """ Run the Firetask.
        """

        # Read required and optional input arguments
        params = self.read_runtask('ChemisorptionEnergy', fw_spec)

        # Calculate the Chemisorption Energy
        chemisorption = self.calculate_chemisorption(params)

        # Store surface energies in DB
        self.update_db(chemisorption, params)

        return FWAction(update_spec=fw_spec)

    def calculate_chemisorption(self, p):
        """
        Calculate the chemisorption energy.

        """

        # Retrieve atom information
        _, energy_atom = super().query_db(collection=p['collection_atom'],
                                          db_file=p['db_file'],
                                          database=p['database'],
                                          entry=['data', 'energy'],
                                          fltr={'mid': 'element-'+p['adsorbate']})

        # Retrieve clean slab information
        field_slab, _ = super().query_db(
            collection=p['collection_slab'],
            db_file=p['db_file'],
            database=p['database'],
            fltr={'mid': p['mid'], 'miller': p['miller']})

        # Retrieve slab + adsorbate information
        chemisorption = {}
        cvr = 'coverage_' + str(float(p['coverage'])).replace('.', ',')
        fltr = self.set_filter(p)

        # Get the sites
        sites = self.__get_sites(p, fltr, cvr)

        for site in sites:
            field_adslab, energy = super().query_db(
                collection=p['collection'],
                db_file=p['db_file'],
                database=p['database'],
                entry=['data', cvr, site, 'energy'],
                fltr=fltr)

            # Extract the correct energy of the clean slab
            r = list(field_adslab['structure'][cvr][site]['repeat'])
            if r == [1, 1, 1]:
                energy_slab = field_slab['data']['energy']
                slab = field_slab['structure']['opt']
            else:
                str_r = 'replica_{}_{}_{}'.format(str(r[0]), str(r[1]), str(r[2]))
                energy_slab = field_slab['data'][str_r]['output']['energy']
                slab = field_slab['data'][str_r]['output']['structure']            
            
            # Count atoms of clean slab and slab + adatom
            slab_species = [s['species'][0]['element'] for s in slab['sites']]
            u_slab_species = list(set(slab_species))
            u_slab_count = [slab_species.count(s) for s in u_slab_species]

            adslab = field_adslab['structure'][cvr][site]['opt']
            adslab_species = [s['species'][0]['element'] for s in adslab['sites']]
            u_adslab_species = list(set(adslab_species))
            u_adslab_count = [adslab_species.count(s) for s in u_adslab_species]

            # Count the number of adatoms
            n_atoms = np.sum(u_adslab_count) - np.sum(u_slab_count)
            # for i in range(len(u_adslab_species)):
            #     s = u_adslab_species[i]
            #     count_diff = u_adslab_count[i] - u_slab_count[i]
            #     print(count_diff)
            #     if s == p['adsorbate']:
            #         n_atoms = count_diff
            #     else:
            #         if count_diff != 0:
            #             raise ValueError('You have more than one species, '
            #                              'you should have just one!')

            chem = energy - energy_slab - (energy_atom * n_atoms)
            
            # Prepare the data to be saved in DB
            area, _ = get_cell_info(Structure.from_dict(adslab).lattice.matrix)
            chemisorption[site] = {
                'chemisorption_energy_ev': chem,
                'chemisorption_energy_jm2': Converter.eV_to_Jm2(chem, area * 1e-20)      
                }

            # Update the final structure to be a Slab
            init_adslab = field_adslab['structure'][cvr][site]['init']
            final_adslab = structure_to_slab(adslab, init_adslab, as_dict=True)
            super().update_db(data=final_adslab, collection=p['collection'],
                              entry=['structure', cvr, site, 'opt'], 
                              db_file=p['db_file'], database=p['database'], fltr=fltr)

        return chemisorption

    def update_db(self, chemisorption, p):
        """
        Upload the chemisorption energy in the Database.

        """

        # Generate the filter
        fltr = self.set_filter(p)

        # Update the energies to the selected database
        cvr = 'coverage_' + str(float(p['coverage'])).replace('.', ',')
        
        # Get the sites
        sites = self.__get_sites(p, fltr, cvr)
        
        for site in sites:
            super().update_db(data=chemisorption[site], collection=p['collection'],
                              entry=['data', cvr, site], db_file=p['db_file'],
                              database=p['database'], fltr=fltr)
            
    def __get_sites(self, p, fltr, cvr):
        """ Get the sites"""

        # Find out the sites and check if they are fine
        field, _ = self.query_db(collection=p['collection'], db_file=p['db_file'],
                                 database=p['database'], fltr=fltr)
        k = read_multiple_entry(field, ['structure', cvr]).keys()
        if p['sites'] == 'all':
            sites = np.array(list(k))
        else:
            sites = [p['sites']] if not isinstance(p['sites'], list) else p['sites']
            if not set(sites).issubset(set(k)):
                raise ValueError('Wrong input argument: sites. It should be a '
                                 'subset of the actual sites in the Database or "all"')
        
        return sites
