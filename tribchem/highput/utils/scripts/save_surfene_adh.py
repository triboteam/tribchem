#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 5th 13:20:00 2021

Tool to save the surface energies and adhesion energies for the homogeneous 
slabs.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2022, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'January 19th, 2022'

import os

import pandas as pd

from tribchem.highput.database.navigator import Navigator
from tribchem.core.cli.cli_common import CliCommon

def save_surfene_adh(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    path = CliCommon.get_par(args.path)
    if path[-1] != '/':
        path += '/'
        
    db_file = args.df
    database = args.db
    nav = Navigator(db_file=db_file, high_level=database)

    slabs = nav.find_many_data(collection='PBE.slab_elements', fltr={})

    el = []
    mi = []
    ot = []
    se = []
    ad = []

    for slab in slabs:
        inter_formula = slab['formula'] + '_' +slab['formula']
        inter = nav.find_data(collection='PBE.interface_elements', 
                            fltr={'formula': inter_formula})
        if not inter:
            continue

        el.append(slab['formula'])
        miller = str(slab['miller']).replace(',', '').replace('[', '')
        miller = miller.replace(']', '').replace(' ', '')
        ot.append(slab['data']['opt_thickness'])
        mi.append(miller)
        se.append(round(slab['data']['surface_energy_jm2'], 2))
        
        ad.append(round(
            inter['adhesion']['short']['min_site']['adhesion_energy_jm2'], 2))

    df = pd.DataFrame({'Element': el,
                    'Miller Indices': mi,
                    'Optimal Thickness': ot,
                    'Surface Energy (J/m$^2$)': se,
                    'Adhesion Energy (J/m$^2$)': ad})
    df.to_latex(path+'slab.tex', index=False)