#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 11th 12:00:00 2021

Utility for moving the output files of VASP from the location created by 
FireWorks to a custom one.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'June 11th, 2021'


import argparse
import os
import shutil

import pandas as pd

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.utils.datatools import save_calctags


def update_calc_tags(str_date, db_file=None):
    """
    Update calc_tags.csv with the new tags.

    Parameters
    ----------
    str_date : str
        Contain the date from which to select the data in the collection tasks.
        'from' must be in the form of: yyyy-mm-dd. 
        If 'from' is None all the data in tasks will be selected.
        E.g.: if from is '2021-06-03', then all the new data from that date will
        be added to calc_tags.csv.

    """

    nav = Navigator(db_file=db_file)

    if str_date: 
        fltr = {'completed_at': {'$gt': '2021-05-01'}}
    else:
        fltr = None

    data = nav.find_many_data(collection='tasks', fltr=fltr)

    for datum in data:
        save_calctags(tag=datum['task_label'], collection='tasks', 
                      formula=datum['formula_pretty'], 
                      name=datum['dir_name'],
                      db_file=db_file, database='FireWorks')

par = argparse.ArgumentParser()

par.add_argument('--update',
                 help='True for updateing calc_tags with new data')

par.add_argument('--date',
                 help='Date from which to select all the data from the collection tasks')

par.add_argument('--db_file',
                 help='Path of db.json file')

par.add_argument('--element',
                 help='Chemical element of which moving the files')

par.add_argument('--folder_to',
                 help='Folder to save the VASP files. In our notation the ' 
                      'output files are saved in a folder structure like: '
                      'PBE.bulk_elements/Al/encut  kpoints  output. '
                      'This parameter refers to the innermost folder where to '
                      'save the files (e.g. output, encut/250/deformation_0, kpoints/10_10_10)')

par.add_argument('--collection',
                 help='Name of the collection. E.g.: PBE.bulk_elements or '
                 'PBE.elements')

args = par.parse_args()
inputs = vars(args)

if all(inp==None for inp in inputs.values()):
    raise ValueError("No inputs have been set.")

if inputs['update']=='True':
    print("--- Updating calc_tags.csv")
    update_calc_tags(str_date=inputs['date'], db_file=inputs['db_file'])

currentdir = os.getcwd()
results_dir = currentdir + '/../../../../results/calc_tags.csv'

in_to_check = ['element', 'folder_to', 'collection']
for inp in in_to_check:
    if inputs[inp] is None:
        raise ValueError("{} has not been provided!".format(inp))

tag = 'BM group: 6600ecd2-eb5a-4733-bea6-8c1f4f3e5d72 encut 375 bulk_modulus deformation 4'
pd_csv = pd.read_csv(results_dir)

dir_name = pd_csv[pd_csv['tag']==tag]['name'].values[0]
dir_name = dir_name.split(':')[1]

# Moving files
calculations_dir = currentdir + '/../../../../../calculations/'
folder_to = calculations_dir + inputs['collection'] + '/' + inputs['element'] + '/' + inputs['folder_to']
print('--- Moving files from {} to {}'.format(dir_name, folder_to))

try:
    shutil.copytree(src=dir_name, dst=folder_to, dirs_exist_ok=True)
    print('--- Files moved successfully')
except:
    raise ValueError("An error occurred while moving files")
