#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 4th 09:00:00 2022

Work function analysis.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 4th, 2022'


import os
from tkinter import Y
import zipfile
import shutil
import gzip
import re

from tribchem.highput.utils.plotting import work_function_plot
from tribchem.core.cli.cli_common import CliCommon


def work_function(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """
    
    source = CliCommon.get_par(args.source)
    if source[-1] != '/':
        source += '/'

    struct_fld = CliCommon.get_par(args.struct_fld)
    if struct_fld[-1] != '/':
        struct_fld += '/'
    if struct_fld[0] != '/':
        struct_fld = '/' + struct_fld
    
    save = CliCommon.get_par(args.save)
    if save[-1] != '/':
        save += '/'

    axis = args.a
    compress = args.c
    
    inter_fld = os.listdir(source)
    
    if compress == 'n':
        # To be implemented. It is just enough to open directly the LOCPOT file
        # at the location source+interface+'/'+struct_fld.
        pass
    elif compress == 'y':
        for inter in inter_fld:
            in_inter = os.listdir(source+inter)
            if 'output.zip' not in in_inter:
                print('There is no \'output.zip\' folder. The adhesion for {}'
                      ' has not been calculated.'.format(inter))
                continue
            
            output_ref = zipfile.ZipFile(source+inter+'/output.zip')
            output_ref.extractall(source+inter)
            output_ref.close()
            
            in_output = os.listdir(source+inter+struct_fld)
            if 'LOCPOT.gz' not in in_output:
                print('The LOCPOT file has not been saved for '
                      '{}'.format(inter))
                continue
            else:
                if 'OUTCAR.relax1.gz' in in_output:
                    outcar_com = 'OUTCAR.relax1.gz'
                    outcar_name = 'OUTCAR.relax1'
                else:
                    outcar_com = 'OUTCAR.gz'
                    outcar_name = 'OUTCAR'
                outcar_path = source+inter+struct_fld+outcar_com
                locpot_path = source+inter+struct_fld+'LOCPOT.gz'
                os.system('gunzip '+locpot_path)
                os.system('gunzip '+outcar_path)

                with open(source+inter+struct_fld+outcar_name) as out_f:
                    outcar = out_f.read()
                ef = re.findall(r'E-fermi :\s*([-+]?[0-9]+[.]?[0-9]*([eE][-+]?[0-9]+)?)', outcar)[-1][0] 

                if not os.path.isdir(save+inter):
                    os.mkdir(save+inter)
                
                work_function_plot(
                    source+inter+struct_fld+'LOCPOT', save+inter+'/', inter, 
                    axis, ef
                )

            shutil.rmtree(source+inter+'/output')