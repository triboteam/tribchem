#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 22 11:07:45 2021

@author: glosi000

"""

import sys

from fireworks import LaunchPad
from fireworks.core.rocket_launcher import launch_rocket

argv = sys.argv[1:]

assert(len(argv) == 1)

lpad = LaunchPad.auto_load()

launch_rocket(lpad, fw_id=argv[0])
