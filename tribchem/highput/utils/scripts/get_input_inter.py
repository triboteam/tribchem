#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 26th 11:06:00 2021

Utility to generate the input files necessary to calculate a PES and the 
adhesion energy with VASP.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2022, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'January 26th, 2022'

from copy import copy, deepcopy
from numpy import save

from tribchem.core.cli.calc_interface_cli import CliInterface
from tribchem.highput.firetasks.core import FireTaskInterface
from tribchem.highput.utils.manipulate_struct import (
    ManipulateStruct, generate_pes_inputs, apply_interface_shifts)
from tribchem.highput.utils.input_interface import save_input

def get_input_inter(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    mids = CliInterface.get_list(args.mids)
    formulas = CliInterface.get_list(args.formulas)
    miller = CliInterface.get_miller(args.miller)
    save = CliInterface.get_par(args.save)

    it = CliInterface.get_par(args.type)
    
    if args.s:
        sites = CliInterface.get_list_opt(args.s, 'string')
        min_site = sites[0]
        max_site = sites[1]

    if it not in ['pes', 'adhesion']:
        raise ValueError("The type of input must be either pes or adhesion.") 

    db_file = args.df
    database = args.db

    m1 = str(miller[0]).replace('[', '').replace(']', '').replace(' ', '').replace(',', '')
    m2 = str(miller[1]).replace('[', '').replace(']', '').replace(' ', '').replace(',', '')
    mil = [m1, m2]
    interface_name = formulas[0]+m1+'-'+formulas[1]+m2
    layers = [3 if m=='111' else 2 for m in mil]

    p = {'mid': mids, 'miller': miller, 'formula': formulas, 
         'name': interface_name,
         'collection': 'PBE.interface_elements', 'db_file': db_file, 
         'database': database, 'check_entry': 'pes', 
         'inter_entry': ['structure', 'init', 'interface'], 'layers': layers, 
         'hs_entry': ['highsym', 'inter_unique']}

    pes = FireTaskInterface()
    is_done, p = pes.query_interface(p)

    calc_type_pes = 'slab_pos_relax'
    structure, vis, tags = generate_pes_inputs(
        p['bot_slab'], p['top_slab'], p['shifts'],
        p['comp_params'], calc_type=calc_type_pes)

    # Short slabs
    p_short = deepcopy(p)
    i, bs, ts = ManipulateStruct.shorten_interface_slabs(
                    p['interface'],
                    p['layers'][0],
                    p['layers'][1])
    p_short['interface'], p_short['bot_slab'], p_short['top_slab'] = i, bs, ts

    calc_type_pes_short = 'slab_from_scratch'
    structure_short, vis_short, tags_short = generate_pes_inputs(
        p_short['bot_slab'], p_short['top_slab'], p_short['shifts'],
        p_short['comp_params'], calc_type=calc_type_pes_short)

    # The number of shifts must be the same of the number of structures 
    # generated
    assert len(p['shifts'].keys()) == len(vis)
    assert len(p_short['shifts'].keys()) == len(vis)

    # Add the interface name to the folder
    save += interface_name + '/'

    if it == 'pes':
        for s, v, vs in zip(p['shifts'].keys(), vis, vis_short):
            dd = v.as_dict()
            v.from_dict(dd)
            v.write_input(save+'pes/'+s)
            
            dd_s = vs.as_dict()
            vs.from_dict(dd_s)
            vs.write_input(save+'pes_short/'+s)

    elif it == 'adhesion':
        if min_site not in p['shifts'].keys() and max_site not in p['shifts'].keys():
            raise ValueError('The sites are not in the available shifts for this interface.')

        # Regular
        mi = apply_interface_shifts(p['bot_slab'], p['top_slab'], 
                                    p['shifts'][min_site])
        ma = apply_interface_shifts(p['bot_slab'], p['top_slab'], 
                                    p['shifts'][max_site])
        
        save_input(structure=mi, p=p, calc_type='slab_pos_relax', 
                   save=save+'adhesion/min_site')
        save_input(structure=ma, p=p, calc_type='slab_pos_relax', 
                   save=save+'adhesion/max_site')
        save_input(structure=p['bot_slab'], p=p, calc_type='slab_pos_relax', 
                   save=save+'adhesion/bot_slab')
        save_input(structure=p['top_slab'], p=p, calc_type='slab_pos_relax', 
                   save=save+'adhesion/top_slab')
    
        # Short
        mi_short = apply_interface_shifts(p_short['bot_slab'], 
                                          p_short['top_slab'],
                                          p_short['shifts'][min_site])
        ma_short = apply_interface_shifts(p_short['bot_slab'], 
                                          p_short['top_slab'],
                                          p_short['shifts'][max_site])
        
        save_input(structure=mi_short, p=p_short, calc_type='slab_pos_relax',
                   save=save+'adhesion_short/min_site')
        save_input(structure=ma_short, p=p_short, calc_type='slab_pos_relax',
                   save=save+'adhesion_short/max_site')
        save_input(structure=p_short['bot_slab'], p=p_short, 
                   calc_type='slab_pos_relax',
                   save=save+'adhesion_short/bot_slab')
        save_input(structure=p_short['top_slab'], p=p_short, 
                   calc_type='slab_pos_relax',
                   save=save+'adhesion_short/top_slab')