#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 10th 16:50:00 2021

Write on a file the summary about adhesion calculations.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'November 10th, 2021'


import pandas as pd

from tribchem.highput.database.navigator import Navigator
from tribchem.core.cli.cli_common import CliCommon

def save_inter(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    path = CliCommon.get_par(args.path)
    if path[-1] != '/':
        path += '/'
    db_file = args.df
    database = args.db

    nav = Navigator(db_file=db_file, high_level=database)

    data = nav.find_many_data(collection='PBE.interface_elements', fltr={})

    columns = ['Interface', 'mids', 'PES scf', 'PES', 'Adhesion short',
               'Adhesion regular']

    total = []
    for inter in data:
        name = inter['name']
        mids = inter['mid']
        
        done_pes_scf = True if 'pes_scf' in inter.keys() else False
        done_pes = True if 'pes' in inter.keys() else False
        
        if 'adhesion' not in inter.keys():
            continue
        
        done_adhesion_short = True if 'short' in inter['adhesion'].keys() else False
        done_adhesion_reg = True if 'regular' in inter['adhesion'].keys() else False
        
        total.append([name, mids, done_pes_scf, done_pes, done_adhesion_short, 
                    done_adhesion_reg])

    df = pd.DataFrame(total, columns=columns)
    df.to_csv(path+'summary_interface.csv')