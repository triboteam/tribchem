#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 10:06:48 2021

Save the results of your bulk, slab, interface, whatever, to a nice csv table,
easily accessible by external people.

@author: glosi000
"""


from tribchem.highput.utils.datatools import bulk_elements_table
from tribchem.physics.base.materials import PeriodicTable
from tribchem.core.cli.cli_common import CliCommon

def save_results(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """
    
    path = CliCommon.get_par(args.p)
    if path[-1] != '/':
        path += '/'
        
    db_file = args.df

    print('Try to read:\n')
    print(PeriodicTable.metals+PeriodicTable.semiconductors+PeriodicTable.non_metals)
    print('')

    table = bulk_elements_table(
        db_file=db_file,
        formula=PeriodicTable.metals+PeriodicTable.semiconductors+PeriodicTable.non_metals
    )
    table = table.drop(['Cohesion (eV)', 'Direct Gap'], axis=1)
    table.to_markdown(path+'bulk_elements.md', index=False)
    table.to_latex(path+'bulk_elements.tex', index=False, bold_rows=True)
