#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 20th 16:58:00 2021

Utility to generate the command to be executed for launching the workflow for
the PPES and the charge displacement. The commands are saved in an sh file.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2022, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'January 20th, 2022'

from tribchem.highput.database.navigator import Navigator

db_file = None
high_level = 'tribchem'
nav = Navigator(db_file=db_file, high_level=high_level)

# interfaces = ['Ir111-Cr110', 'Fe110-Mo110', 'Fe110-Pt111', 
#               'Fe110-Cu111', 'Fe110-Al111', 'Fe110-Ag111',
#               'Fe110-Mg001', 'Fe110-Cr110', 'Ti001-Cu111',
#               'Ti001-Al111', 'Ti001-Au111', 'Ni111-Pt111',
#               'Ni111-Cu111', 'Ni111-Mo110', 'Ni111-Ag111',
#               'Ni111-Au111', 'Ni111-Mg001', 'Ni111-Mg001',
#               'Pt111-Au111', 'Cu111-Au111', 'Cu111-Mg001',
#               'Cu111-W110', 'Cu111-Mo110', 'Al111-W110',
#               'Al111-Cu111', 'Al111-Mo110', 'Al111-Mg001',
#               'Al111-Pt111', 'Al111-Cr110', 'Al111-Pt111',
#               'Al111-Ni111', 'Al111-In110', 'Ag111-Au111',
#               'Ag111-Cu111', 'Ag111-Mo110', 'Ag111-Mg001',
#               'Ag111-W110', 'Ag111-Pb111', 'Ag111-Pt111',
#               'Au111-Mg001', 'Zn001-Cr110', 'Zn001-Mo110',
#               'Zn001-Ir111', 'Zn001-Fe110', 'Zn001-Ni111', 
#               'Zn001-Cu111', 'Zn001-Al111', 'Zn001-Ag111', 
#               'Zn001-Au111', 'Zn001-Pb111', 'Zn001-Ga100']
interfaces = ['Cr110-Cr110', 'W110-W110', 'Mo110-Mo110', 'Fe110-Fe110',
              'Ir111-Ir111', 'Ni111-Ni111', 'Ti001-Ti001', 'Pt111-Pt111',
              'Cu111-Cu111', 'Al111-Al111', 'Ag111-Ag111', 'Au111-Au111',
              'Mg001-Mg001', 'Co001-Co001', 'Zn001-Zn001', 'Pb111-Pb111']

ppes = ''
charge = 'python ../../TribChem/tribchem/highput/workflows/scripts/calc_interface.py wf="[charge]"'
min_site = '-pt=pes_scf -ad=short -s=min_site -o=y \n'
max_site = '-pt=pes_scf -ad=short -s=max_site -o=y \n'

full_charge = ''
for i in interfaces:
    inter = nav.find_data(collection='PBE.interface_elements', 
                          fltr={'name': i})
    charge_inter = charge + ' mids="[' + inter['mid'].split('_')[0]
    charge_inter += ' ,' + inter['mid'].split('_')[1] + ']"'
    charge_inter += ' formulas="[' + inter['formula'].split('_')[0]
    charge_inter += ' ,' + inter['formula'].split('_')[1] + ']"'
    charge_inter += ' miller="' + str(inter['miller']) + '" '
    charge_inter_min = charge_inter + min_site
    charge_inter_max = charge_inter + max_site
    full_charge += charge_inter_min
    full_charge += charge_inter_max
    
print(full_charge)
    
