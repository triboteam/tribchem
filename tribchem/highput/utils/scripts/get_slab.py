#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 5th 13:20:00 2021

Tool to save the relaxed slabs with the computational parameters.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2022, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'January 5th, 2022'

import os
import json

from pymatgen.core.structure import Structure

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.utils.datatools import save_structure
from tribchem.core.cli.calc_interface_cli import CliInterface

def get_slab(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    db_file = args.df
    database = args.db
    save = CliInterface.get_par(args.save)
 
    nav = Navigator(db_file=db_file, high_level=database)

    slabs = nav.find_many_data('PBE.slab_elements', fltr={})

    res = {}

    for slab in slabs:
        formula = slab['formula'] + '_' + str(slab['miller']).replace('[', '').replace(']', '').replace(',', '').replace(' ', '')
        opt_th = slab['data']['opt_thickness']
        
        # 18 is the highest number of layer
        if opt_th == 18:
            opt_th = 18
        else:
            opt_th += 1
        opt_th = str(opt_th)
        if formula == 'Si_111' or formula == 'Ge_111' or formula == 'C_111':
            continue
        
        res[formula] = {
            'structure': slab['thickness']['data_'+opt_th]['output']['structure'],
            'comp_params': slab['comp_params'],
            'energy': slab['thickness']['data_'+opt_th]['output']['energy'],
            'energy_per_atom': slab['thickness']['data_'+opt_th]['output']['energy_per_atom']}

    en = open(save+'en.txt', 'a')
    en.write('Element, Energy (J), Energy per Atom (J)')

    for key, value in res.items():
        en.write('\n'+key+', '+str(value['energy'])+', '+str(value['energy_per_atom']))
        formula, miller = key.split('_')
        path = save + formula + '/' + miller
        os.makedirs(path)
        save_structure(value['structure'], 'POSCAR', path+'/', fmt='poscar')
        
        with open(path+'/'+'comp_params.json', 'a') as f:
            json.dump(value['comp_params'], f, indent=4, sort_keys=True)