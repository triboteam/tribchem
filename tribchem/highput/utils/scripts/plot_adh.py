#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sept 9th 10:50:00 2021

Plot the adhesion and corrugation for all the elements.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'September 9th, 2021'


from tribchem.highput.utils.plotting import plot_adhesion, plot_interlayer
from tribchem.core.cli.cli_common import CliCommon

def plot_adh(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    save = CliCommon.get_par(args.p)
    if save[-1] != '/':
        save += '/'
    ron = args.r
    if args.e:
        elements = CliCommon.get_list_opt(args.e)
    else:
        elements = None
    adh_type = args.a
    label_size = args.ls
    annotation_size = args.ans
    db_file = args.df
    database = args.d

    plot_adhesion(save=save, ron=ron, elements=elements, adh_type=adh_type, 
                label_size=label_size, annotation_size=annotation_size, 
                db_file=db_file, database=database)

    plot_interlayer(inter=save+'interlayer.json', ordering=save+'ord.json', 
                    save=save, ron=ron, label_size=label_size, 
                    annotation_size=annotation_size)