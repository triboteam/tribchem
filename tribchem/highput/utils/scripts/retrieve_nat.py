#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  6 11:42:49 2021

@author: glosi000
"""

from tribchem.highput.utils.tasktools import read_json

data = read_json('PBE.interface_elements.json')

interface = []
nat = []
for d in data:
    i2 = d['formula'].split('_')
    if i2[0] != i2[1]:
        interface.append(d['name'])
        nat.append(len(d['structure']['init']['interface']['sites']))

# a = list(zip(interface, nat))
# [x for _, x in sorted(zip(nat, a))]

import numpy
interface = numpy.array(interface)
nat = numpy.array(nat)
inds = nat.argsort()
sorted_interface = interface[inds]
sorted_nat = nat[inds]

for i in range(len(interface)):
    print(interface[i], nat[i])
