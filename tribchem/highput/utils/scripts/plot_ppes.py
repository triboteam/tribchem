#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 8th 16:50:00 2021

Test for the plotting functions.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'November 8th, 2021'


from tribchem.highput.utils.plotting import plot_ppes
from tribchem.core.cli.cli_common import CliCommon

def plot_ppes(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    save = CliCommon.get_par(args.path)
    if save[-1] != '/':
        save += '/'
    ppes_type = args.pt

    if args.e:
        elements = CliCommon.get_list_opt(args.e)
    else:
        elements = None
    db_file = args.df
    database = args.d

    plot_ppes(save=save, ppes_type=ppes_type, elements=elements, 
              db_file=db_file, database=database)