#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 3rd 14:16:00 2021

Utility for saving the structure in a POSCAR file.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'August 3rd, 2021'


from tribchem.highput.utils.datatools import save_interface_structure
from tribchem.core.cli.cli_common import CliCommon

def save_structure(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """
    
    path = CliCommon.get_par(args.p)
    if path[-1] != '/':
        path += '/'
        
    db_file = args.df
    
    adh = args.a
    save =args.s

    save_interface_structure(path=path, db_file=db_file, adh=adh, save=save)