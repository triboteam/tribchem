#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 5th 10:00:00 2021

Utility for dumping the database from the cloud to the local database and
viceversa.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'October 5th, 2021'


import os

def dump_db(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    inputs = vars(args)

    actions = ['to_local', 'to_cloud']

    if inputs['action'] not in actions:
        raise ValueError('The action {} is not availabe!'.format(inputs['action']))

    if inputs['deamon']:
        print('Starting the deamon.')
        deamon_start = 'mongod --config ' + inputs['deamon']
        os.system(deamon_start)

    if inputs['action'] == 'to_local':
        movedir = 'cd ' + inputs['dfolder']
        dump = 'mongodump -d ' + inputs['database'] + '-u ' + inputs['username'] + \
                ' -p ' + inputs['password'] + ' --authenticationDatabase admin --ssl --port '\
                + inputs['port'] + ' -h "' + inputs['address'] + '"'
        os.system(movedir + '&&' + dump)
        upload = 'mongorestore -d ' + inputs['database'] + '-u ' + inputs['username'] + \
                ' -p ' + inputs['password'] + ' --authenticationDatabase admin '\
                + inputs['dfolder']
        os.system(movedir + '&&' + upload)
    elif inputs['action'] == 'to_cloud':
        movedir = 'cd ' + inputs['dfolder']
        dump = 'mongodump -d ' + inputs['database'] + '-u ' + inputs['username'] + \
                ' -p ' + inputs['password'] + \
                ' --authenticationDatabase admin --host localhost --port ' + inputs['port']
        os.system(movedir + '&&' + dump)
        upload = 'mongorestore -d ' + inputs['database'] + '-u ' + inputs['username'] + \
                ' -p ' + inputs['password'] + ' --authenticationDatabase admin --ssl --port '\
                + inputs['port'] + ' -h "' + inputs['address'] + '"'
        os.system(movedir + '&&' + upload)
    else:
        raise ValueError('Something went wrong with the setting of the action.')

    if inputs['deamon']:
        print('Starting the deamon.')
        deamon_stop = 'mongod --config ' + inputs['deamon']
        os.system(deamon_stop)