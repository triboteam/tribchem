#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 22 11:06:56 2021

@author: glosi000

"""

import os
from fireworks import LaunchPad

scripts = ['', '']

lpad = LaunchPad.auto_load()

for s in scripts:
    os.system('python {}'.format(s))
    
fw_ids = lpad.get_fw_ids()

for fw in fw_ids:
    os.system('python m100_launch_rocket.py {}'.format(fw))
