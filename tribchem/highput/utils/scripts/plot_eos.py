#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 3rd 15:00:00 2021

Utility for plotting the equation of state given the VASP output files.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'June 3rd, 2021'


import os
from posixpath import split
import sys

from matplotlib import pyplot as plt

from pymatgen.io.vasp.inputs import Poscar
from pymatgen.io.vasp.outputs import Oszicar

from tribchem.core.cli.cli_common import CliCommon

def plt_eos(data, element, save):
    """
    Plot the energy with respect to the bulk parameter for each cutoff energy.
    
    Parameters
    ----------
    data : dict
        A dictionary containing the lattice parameters and the respective energy
        for each cutoff energy. E.g.: {'encut_450': [[lattice_par], [energy]]}
    
    element : str
        Name of the element.
    
    save : str
        Path where to save the plots.

    """
    
    fig = plt.figure(figsize=(6, 5))
    
    for _, value in data.items():
        plt.plot(value[0], value[1], '--o', markersize=4)
    plt.xlabel('Lattice parameter ($\AA$)')
    plt.ylabel('Energy ($eV$)')
    plt.legend(data.keys(), loc='upper center', shadow=True, fontsize='x-small')
    plt.title('EOS '+element)
    fig.savefig(save+'/'+element+'_enc_'+'.pdf')
    fig.savefig(save+'/'+element+'_enc_'+'.png', dpi=fig.dpi)

def get_eos_plot(encut_path, element, save):
    """
    Get the eos plot for one element.
    
    Parameters
    ----------
    encut_path : str
        Path where the encut folders are saved.
    
    element : str
        Name of the element.
    
    save : str
        Path where to save the plot.

    """

    # Get the cutoff energies
    cutoffs = os.listdir(encut_path)
    cutoffs = sorted(cutoffs)
    results = {}
    for cutoff in cutoffs:
        def_a =[]
        ener_ev = []
        # Get the deformations
        deformations = os.listdir(encut_path+'/'+cutoff)
        main_def_dir = encut_path + '/' + cutoff
        for deformation in deformations:
            files_fol = main_def_dir + '/' + deformation 
            poscar = Poscar.from_file(filename=files_fol+'/POSCAR.gz')
            lattice = poscar.structure.as_dict()
            lattice_par = lattice['lattice']['a']
            def_a.append(lattice_par)
            oszicar = Oszicar(filename=files_fol+'/OSZICAR.gz')
            energy = oszicar.final_energy
            ener_ev.append(energy)
            
            # Order the arrays
            zip_list = zip(def_a, ener_ev)
            srt_list = sorted(zip_list)
            srt_to_list = zip(*srt_list)
            def_a, ener_ev = [list(lst) for lst in  srt_to_list]

        results['encut_'+cutoff] = [def_a, ener_ev]

    plt_eos(data=results, element=element, save=save)

def plt_kpoints(data, element, save):
    """
    Plot the energy with respect to the bulk parameter for each cutoff energy.
    
    Parameters
    ----------
    data : list
        A containing the lists for the kpoints and the energies.
        E.g.: [[kpoints], [energies]]
    
    element : str
        Name of the element.
    
    save : str
        Path where to save the plots.

    """
    
    fig = plt.figure(figsize=(6, 5))
    
    plt.plot(data[0], data[1], '--o', markersize=4)
    plt.xlabel('K-points')
    plt.ylabel('Energy ($eV$)')
    plt.title('K-points vs Energy '+element)
    fig.savefig(save+'/'+element+'_kp_'+'.pdf')
    fig.savefig(save+'/'+element+'_kp_'+'.png', dpi=fig.dpi)

def get_kpoints_plot(kpoints_path, element, save):
    """
    Get the plot for the energy vs kpoints.
    
    Parameters
    ----------
    kpoints_path : str
        Path where the kpints folders are saved.
    
    element : str
        Name of the element.
    
    save : str
        Path where to save the plot.

    """
    
    # Get the kpoints
    kp = []
    ener = []
    kp_folders = os.listdir(kpoints_path)
    for kp_folder in kp_folders:
        kp_path = kpoints_path + '/' + kp_folder
        oszicar = Oszicar(filename=kp_path+'/OSZICAR.gz')
        energy = oszicar.final_energy
        kp_values = kp_folder.split('_')
        kp.append(float(kp_values[0]))
        ener.append(energy)

    # Order the arrays
    zip_list = zip(kp, ener)
    srt_list = sorted(zip_list)
    srt_to_list = zip(*srt_list)
    def_k, ener_ev = [list(lst) for lst in  srt_to_list]
    def_k = [int(k) for k in def_k]
    
    plt_kpoints(data=[def_k, ener_ev], element=element, save=save)

def plot_eos(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    inputs = CliCommon.get_par(args.inputs)
    if inputs[-1] != '/':
        inputs += '/'
        
    if args.elements:
        elements = CliCommon.get_list_opt(args.elements)
    else:
        elements = None
        
    saveplot = CliCommon.get_par(args.saveplot)
    if saveplot[-1] != '/':
        saveplot += '/'

    # Check the main path 
    try:
        dirs = os.listdir(inputs)
    except:
        raise ValueError("The passed folder does not exist. Please provide the "
                        "correct location for the elements.")

    # Check if the folder for the given element exists
    elem = elements
    if elem:
        try:
            elem_path = os.listdir(inputs+'/'+elem)
            encut_path = inputs + '/' + elem + '/encut'
            kpoints_path = inputs + '/' + elem + '/kpoints'
        except:
            raise ValueError("The passed element has some problems. Please make "
                            "sure that both the encut and kpints folder exists.")
        get_eos_plot(encut_path=encut_path, element=elem, save=saveplot)
        get_kpoints_plot(kpoints_path=kpoints_path, element=elem, save=saveplot)
        sys.exit("The plot for '{}' has been saved at '{}'.".format(elem, saveplot))
    
    else:
        try:
            elems_path = os.listdir(inputs)
        except:
            raise ValueError("The passed element has some problems. Please make "
                            "sure that both the encut and kpints folder exists.")
        for elem_path in elems_path:
            encut_path = inputs + '/' + elem_path + '/encut'
            kpoints_path = inputs + '/' + elem_path + '/kpoints'
            get_eos_plot(encut_path=encut_path, element=elem_path, 
                        save=saveplot)
            get_kpoints_plot(kpoints_path=kpoints_path, element=elem_path, 
                            save=saveplot)
        sys.exit("All the plots have been saved at '{}'.".format(saveplot))