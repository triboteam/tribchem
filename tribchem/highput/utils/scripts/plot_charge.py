#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 10th 16:50:00 2021

Plot the charge displacement.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'November 10th, 2021'


from tribchem.highput.utils.plotting import plot_charge, plot_charge_matrix
from tribchem.core.cli.cli_common import CliCommon

def plot_charge(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """
    
    save = CliCommon.get_par(args.p)
    if save[-1] != '/':
        save += '/'
    direction = args.dir
    site = args.s
    if args.e:
        elements = CliCommon.get_list_opt(args.e)
    else:
        elements = None
    db_file = args.df
    database = args.d

    type_plot = args.tp

    if type_plot == 'all':
        plot_charge(
            save=save, direction=direction, site=site, 
            elements=elements, db_file=db_file, database=database
        )
    elif type_plot == 'matrix':
        plot_charge_matrix(
            save=save, ordering=save+'ord.json', elements=elements, 
            db_file=db_file, database=database
        )
    else:
        raise ValueError('The type plot specified is wrong. Select either '
                         '\'matrix\' or \'all\'.')