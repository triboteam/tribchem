#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 13 09:53:00 2021

Save the interface parameters from the standard output.

@author: omarchehaimi
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'September 13th, 2021'


from tribchem.highput.utils.inter_params import get_inter_params
from tribchem.core.cli.cli_common import CliCommon

def save_inter_params(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """
    
    path = CliCommon.get_par(args.s)
    if path[-1] != '/':
        path += '/'
    files =  CliCommon.get_list(args.f)
    save = CliCommon.get_par(args.p)

    get_inter_params(path=path, files=files, save=save)