#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 5th 13:20:00 2021

Tool to save the relaxed interfaces.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2022, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'January 19th, 2022'

import os
import json

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.utils.datatools import save_structure

db_file = None
high_level = 'tribchem'
nav = Navigator(db_file=db_file, high_level=high_level)
inter_type = 'output'

if inter_type == 'input':
    name = 'Fe_Mg'
    path = '/home/omar/Lavoro/Unibo/TribChemProject/interfaces/inter_structures'

    inter = nav.find_data('PBE.interface_elements', fltr={'formula': name})

    structure = inter['structure']['init']['interface']
    inter_params = inter['inter_params']

    path = path + '/' + name
    os.makedirs(path, exist_ok=True)
    save_structure(structure, 'POSCAR', path+'/', fmt='poscar')

    with open(path+'/'+'inter_params.json', 'a') as f:
        json.dump(inter_params, f, indent=4, sort_keys=True)
        
elif inter_type == 'output':
    inter = nav.find_many_data('PBE.interface_elements', fltr={})
    path = '/home/omar/Lavoro/Unibo/TribChemProject/interfaces/output'
    
    for i in inter:
        
        if 'opt' not in i['structure'].keys():
            continue
        if 'short' not in i['structure']['opt'].keys():
            continue
        if 'interface_min' not in i['structure']['opt']['short'].keys():
            continue
        if 'interface_max' not in i['structure']['opt']['short'].keys():
            continue

        min = i['structure']['opt']['short']['interface_min']
        max = i['structure']['opt']['short']['interface_max']
        inter_params = i['inter_params']
    
        path_min = path + '/' + i['name'] + '/min/'
        path_max = path + '/' + i['name'] + '/max/'

        os.makedirs(path_min, exist_ok=True)
        os.makedirs(path_max, exist_ok=True)
        save_structure(min, 'POSCAR', path_min, fmt='poscar')
        save_structure(max, 'POSCAR', path_max, fmt='poscar')

        # path_cp = path + '/' + i['name']
        # with open(path_cp+'/'+'inter_params.json', 'a') as f:
        #     json.dump(inter_params, f, indent=4, sort_keys=True)