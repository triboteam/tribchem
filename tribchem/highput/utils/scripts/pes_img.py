#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23rd 09:45:00 2021

Plot the potential energy surface as saved in the database.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'November 23rd, 2021'

from matplotlib import pyplot as plt

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.database.dbtools import convert_bytes_to_image
from tribchem.core.cli.cli_common import CliCommon

def pes_img(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """
    
    path = CliCommon.get_par(args.p)
    if path[-1] != '/':
        path += '/'
    pes_type = CliCommon.get_par(args.pt)
    interface = CliCommon.get_par(args.i)
    db_file = args.df
    database = args.d 

    nav = Navigator(db_file=db_file, high_level=database)

    imgs = nav.find_many_data(
        collection='PBE.interface_elements', 
        fltr={"name": interface}
    )

    for img in imgs:
        if pes_type == 'pes':
            name = img['name']
            pes_img = img['pes']['energy']['image']
            plt_img = convert_bytes_to_image(pes_img)
            plt_img.save(path+name+'_full_pes.png')
        elif pes_type == 'pes_scf':
            name = img['formula']
            pes_img = img['pes_scf']['energy']['image']
            plt_img = convert_bytes_to_image(pes_img)
            plt_img.save(path+name+'_pes_scf.png')
        else:
            raise ValueError("Neither the pes or the pes scf have been "
                             "calculated for the {} "
                             "interface.".format(interface))
