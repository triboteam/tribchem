#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29Th 14:16:00 2021

Utility for analyzing surface energy results.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'June 29Th, 2021'

import argparse

from tribchem.highput.utils.datatools import surface_energy
from tribchem.core.cli.cli_common import CliCommon

def save_surfene(args):
    """
    Data analysis.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    path = CliCommon.get_par(args.path)
    if path[-1] != '/':
        path += '/'
        
    db_file = args.df

    # If some elements have not be calculated the table can not be generated
    table = surface_energy(db_file=db_file, save=path)
    print(table)