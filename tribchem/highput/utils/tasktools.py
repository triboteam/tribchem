#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 10:49:54 2021

This module contains core functionalities and features common to almost all
the Firetasks. It provides tools to manage the input parameters of a Firetask
and to manipulate the dictionaries to be retrieved and stored to the database

The module contains the following functions:

    - read_json
    - read_runtask_params
    - read_default_params
    - create_tags
    - get_miller_str
    - select_struct_func

    Author: Gabriele Losi (glosi000)
    Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 22nd, 2021'


import json
import os
from uuid import uuid4


from pymatgen.core.surface import Structure, Slab
from atomate.utils.utils import env_chk

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.utils.errors import ReadParamsError
from tribchem.highput.database.dbtools import get_multiple_info_from_dict
from tribchem.utils.utils import ClassTools


project_folder = os.path.dirname(__file__)


# ============================================================================
# Fundamentals tools for Firetasks
# ============================================================================

def read_json(jsonfile):
    """
    Shortcut to easily read a json file.
        
    """
    
    with open(jsonfile, 'r') as f:
        data = json.load(f)  
    return data

def read_runtask_params(obj, fw_spec, required_params, optional_params,
                        default_file, default_key):
    """
    Read the input arguments passed by the user when initializing a Firetask
    instance. It returns a dictionary containing all the required and optional
    parameters to be used within the `run_task` method for further processing.
    Missing data is substituted with default values.

    Parameters
    ----------
    obj : Firetask object
        Instance of a Firetask-derived class, i.e. inherited from FiretaskBase.
        It is necessary to read the arguments passed as input by the user.

    fw_spec : dict
        JSON spec including all the information needed to bootstrap the job.

    required_params : dict
        List of required parameters to the Firetask.

    optional_params : dict
        List of optional parameters to the Firetask.

    default_file : str
        Path to the JSON file containing default values for `optional_params`.

    default_key : str
        Key of the JSON dict containing the default parameters of the Firetask.

    Returns
    -------
    dict
        Dictionary containing all the parameters, the optional values which have
        not been provided by the user are substituted with defaults.

    """

    defaults = read_json(default_file)
    params = {}
    
    # Read required and optional parameters
    for key in required_params:
        params[key] = obj[key]
    for key in optional_params:
        params[key] = obj.get(key, defaults[default_key][key])

    # Clean db_file entry
    if 'db_file' in params.keys():
        if params['db_file'] is None:
            params['db_file'] = env_chk('>>db_file<<', fw_spec)

    # Clean miller index entry
    if 'miller' in params.keys():
        if isinstance(params['miller'], str):
            params['miller'] = [int(k) for k in list(params['miller'])]

    return params

def read_default_params(default_file, default_key, dict_params, allow_unknown=False):
    """
    Read the default argument from a JSON file and compare them with the keys 
    of a dictionary. If some data is missing, is substituted with default
    values. It is a generalization of `read_runtask_params`, because it takes a
    dictionary as input and can be used outside Firetasks.

    Parameters
    ----------
    default_file : str
        Path to the JSON file containing the default values.

    default_key : str
        Key of the JSON dict containing the default parameters.

    dict_params : dict
        Dictionary containing the parameters to be updated with defaults. It
        should be a subset of the dictionary extracted from `default_file`.

    Returns
    -------
    dict
        Final dictionary containing the elements of `dict_params` if present,
        else the default values read from JSON file.

    Raises
    ------
    ReadParamsError
        When unknown keys are present ind `dict_params`.

    """

    # Read the JSON file with defaults and extract the corresponding key
    defaults = read_json(default_file)
    defaults = defaults[default_key]

    # Check if there are some unknown parameters
    if not allow_unknown:
        if not set(dict_params.keys()).issubset(set(defaults.keys())):
            raise ReadParamsError("The values passed as kwargs are not known. "
                                  "Allowed values for '{}' method are: {}"
                                  .format(default_key, list(defaults.keys())))
        params = {}
    else:
        params = dict_params.copy()

    # Set the parameters, missing parameters are substituted with defaults
    for key, value in defaults.items():
        params[key] = dict_params.get(key, value)

    return params


# ============================================================================
# Firetasks tools
# ============================================================================

def create_tags(prefix):
    """
    Generate a tag out of a prefix.

    """

    # Create a list of tags
    if isinstance(prefix, list):
        tag = [n + '_' + str(uuid4()) for n in prefix]

    else:
        tag = prefix + '_' + str(uuid4())
    
    return tag

def get_miller_str(miller):
    """
    Convert a miller index from list to string.

    """
    return ''.join(str(s) for s in miller)

def select_struct_func(struct_kind):
    """
    Select a function to work on pymatgen structure, depending on the value
    of `struct_kind` it returns either `pymatgen.core.surface.Structure` or 
    `pymatgen.core.surface.Slab`.

    """

    if struct_kind in ['Structure', 'structure', 'bulk']:
        func = Structure
    elif struct_kind in ['Slab', 'slab', 'interface']:
        func = Slab
    else:
        ValueError("Wrong argument: struct_kind. Allowed values: "
                   "'bulk', 'slab'. Given value: {}".format(struct_kind)) 
    return func

def retrieve_from_tag(collection, tag, tag_key='task_label', entry=None, 
                      db_file=None, database=None):
    """
    Retrieve a dictionary field out of the database based on the combination
    {tag_key : tag} as filter (fltr). Useful to retrieve quickly the results of a 
    VASP simulation run with Atomate and typically stored in the low level DB.

    Parameters
    ----------
    collection : str
        Collection in the database to parse through.

    tag : any python object
        Object to be found within the database to identify the correct field.
    
    tag_key : str, optional
        Dict key to filter the fields of the dictionary retrieved from the 
        database. The default is 'task_label'.
        
    entry : str or list or list of lists or None, optional
        Key or list of keys to be used to extract a piece of information or 
        multiple values from the `vasp_calc` dictionary. The default is None.

    db_file : str or None
        Location of the database. If it is None, it will be searched for a
        'localhost' on the hosting machine. The default is None.

    database : str or None, optional
        Database to query. The default is None.

    Returns
    -------
    vasp_calc : dict
        Dictionary field retrieved from `database` in `db_file`.
    
    info : any python object
        Content of the `vasp_calc` dictionary, read using entry as a key or
        a list of nested keys. If `entry=None` it returns None.

    """

    # Call the navigator and retrieve the simulation data from tag
    nav = Navigator(db_file=db_file, high_level=database)    
    vasp_calc = nav.find_data(collection, {tag_key: tag})
    
    # Retrieve the correct dictionary and obtain the structure
    info = None
    if entry is not None:
        info = get_multiple_info_from_dict(vasp_calc, entry)
    
    return vasp_calc, info

def which_params_attr(ft):
    """
    Return a unique list for the required and optional params of a Firetask.

    """

    req_params = ClassTools._get_attr(ft, 'required_params')
    opt_params = ClassTools._get_attr(ft, 'optional_params')
    
    return req_params + opt_params
