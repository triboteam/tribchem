#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 14 10:22:23 2021

Test the modification of pymatgen structure

@author: glosi000
"""

import os
import json

from pymatgen.core.structure import Structure
from pymatgen.io.vasp import Poscar

from tribchem.highput.database.query import query_field
from tribchem.highput.utils.manipulate_struct import ManipulateStruct, clean_up_site_properties


def dict_to_json(input_dict, name='dict.json'):
    js = json.dumps(input_dict, indent=4, sort_keys=True)
    f = open(name,"w")
    f.write(js)
    f.close()


field = query_field(fltr={'formula': 'Mg_Mg'}, collection='PBE.interface_elements', 
                    database='test')
interface = Structure.from_dict(field['structure']['init']['interface'])
obj = ManipulateStruct(interface)

# Test the decomposition of the interface, find the slabs
res = obj.decompose_structure(as_dict=False)
res[0].to('poscar', 'slab1_POSCAR')
res[1].to('poscar', 'slab2_POSCAR')

# Test the vacuum extension and reduction
interface_40 = obj.make_vacuum_around(vacuum=40, vac_kind='zero')
obj.make_vacuum_around(vacuum=40, vac_kind='zero', inplace=True)
interface_20 = obj.make_vacuum_around(vacuum=20, vac_kind='zero')
interface.to('poscar', 'interface_POSCAR')
interface_40.to('poscar', 'interface40_POSCAR')
interface_20.to('poscar', 'interface20_POSCAR')


# Try the same thing with a slab
field = query_field(fltr={'formula': 'Mg'}, collection='PBE.slab_elements', 
                    database='test')
structure = Structure.from_dict(field['structure']['init'])
obj = ManipulateStruct(structure)

structure_40 = obj.make_vacuum_around(vacuum=40, vac_kind='default')
obj.make_vacuum_around(vacuum=40, vac_kind='default', inplace=True)
structure_20 = obj.make_vacuum_around(vacuum=20, vac_kind='default')
structure.to('poscar', 'structure_POSCAR')
structure_40.to('poscar', 'structure40_POSCAR')
structure_20.to('poscar', 'structure20_POSCAR')



# Test the vertical shift
ies = []   # Identify the top_slab and the bot_slab by looking at atomic z coordinates
sites_to_shift = []
for i, site in enumerate(interface.sites):
    if site.c > 0:
        sites_to_shift.append(i)
    
    # Loop over all the shifts
    for s in [0, 0.5, 6, -1]:
        # Clean site properties removing NoneType, and translate upper slab
        inter = clean_up_site_properties(interface.copy())
        inter.translate_sites(indices=sites_to_shift, 
                              vector=[0, 0, s],
                              frac_coords=False, 
                              to_unit_cell=False)
        inter.to('poscar', 's_'+str(s)+'_POSCAR')
        ies.append(inter)

# os.system('rm *_POSCAR')

sup = Poscar.from_file('./files/POSCAR_migup').structure
sdw = Poscar.from_file('./files/POSCAR_migdown').structure
s = Poscar.from_file('./files/POSCAR').structure

c = s.lattice.matrix[2, 2]

for select in [sup, sdw]:
    sites_to_shift = []
    for i, site in enumerate(select.sites):
        if site.c < 0:
            sites_to_shift.append(i)
    select.translate_sites(indices=sites_to_shift, vector=[0, 0, c],
                           frac_coords=False, to_unit_cell=False)

sup = ManipulateStruct.fix_interface(sup, s)
sup.to('poscar', 'POSCAR_migup_final')

sdw = ManipulateStruct.fix_interface(sdw, s)
sdw.to('poscar', 'POSCAR_migdw_final')
