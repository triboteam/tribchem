#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sept 9th 10:50:00 2021

Test for the plotting functions.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'September 9th, 2021'

from tribchem.highput.utils.plotting import plot_adhesion

save = '/home/omar/Lavoro/Unibo/TribChemProject/interfaces/adhesion_matrix/'
plot_adhesion(save=save)