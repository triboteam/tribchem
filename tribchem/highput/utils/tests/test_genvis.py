#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 31 17:33:12 2021

@author: glosi000
"""

import json
from pymatgen.core.structure import Structure

from tribchem.highput.database.query import query_field
from tribchem.highput.utils.vasp_tools import VaspInputSet
from tribchem.highput.utils.tests.old_vasp import (
    get_custom_vasp_relax_settings,
    get_custom_vasp_static_settings
    )


def dict_to_json(input_dict, name='dict.json'):
    js = json.dumps(input_dict, indent=4, sort_keys=True)
    f = open(name,"w")
    f.write(js)
    f.close()


field = query_field(fltr={'formula': 'Al'}, collection='PBE.bulk', database='tribchem')
structure = Structure.from_dict(field['structure']['init'])
comp_params = field['comp_params']
calc_type = 'bulk_from_scratch'

comp_params['use_spin'] = True
comp_params['is_metal'] = None

vis_1 = VaspInputSet(structure, comp_params, calc_type=calc_type).get_vasp_settings()
vis_2 = get_custom_vasp_static_settings(structure, comp_params, calc_type)

dict_to_json(vis_1.incar, 'vis_1.json')
dict_to_json(vis_2.incar, 'vis_2.json')
