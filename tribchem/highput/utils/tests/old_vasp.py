#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 08:35:55 2021

Old vasp functions to create

@author: glosi000
"""

from pymatgen.io.vasp.sets import (
    MPStaticSet, 
    MPRelaxSet, 
    MPScanStaticSet, 
    MPScanRelaxSet
    )

from tribchem.highput.utils.vasp_tools import MeshFromDensity

def get_custom_vasp_static_settings(structure, comp_params, static_type,
                                    kdens_default=8):
    """Make custom vasp settings for static calculations.
    
    Parameters
    ----------
    structure : pymatgen.core.structure.Structure
        Structure to be treated
    comp_params : dict
        Computational parameters dictionary which is usually created partly
        by the user and then filled automatically with defaults and after
        convergence tests.
    statc_type : str
        Specifies what system is treated in what way. Check 'allowed_types'
        for a list of choices.
    kdens_default : float, optional
        Specifies the default kpoint density if no kdens or kspacing key
        is found in the comp_params dictionary. The default is 8.

    Raises
    ------
    SystemExit
        If a non-supported static_type is passed, the process terminates.

    Returns
    -------
    vis : pymatgen.io.vasp.sets.MPStaticSet
        
    """

    allowed_types = ['bulk_from_scratch', 'bulk_follow_up', 'bulk_nscf',
                      'slab_from_scratch', 'slab_follow_up', 'slab_nscf']
    
    if static_type not in allowed_types:
        raise SystemExit('static type is not known. Please select from: {}'
                        .format(allowed_types))
        
    #Set user incar settings:
    uis = {}
    uis['NEDOS'] = 3001
    uis['PREC'] = 'Accurate'
    uis['GGA_COMPAT'] = '.FALSE.'
    uis['LASPH'] = '.TRUE.'
    uis['LORBIT'] = 11
    uis['NELMIN'] = 4
    uis['SIGMA'] = 0.05
    uis['ISMEAR'] = -5
    uis['EDIFF'] = 1.0e-6
    #uis['SYMPREC'] = 1e-06
    
    if static_type.endswith('from_scratch'):
        uis['ICHARG'] = 2
        uis['LAECHG'] = '.FALSE.'
    
    if structure.num_sites < 20:
        uis['LREAL'] = '.FALSE.'
        
    #Adjust mixing for slabs that have a very large c axis:
    if structure.lattice.matrix[-1,1] > 50.0:
        uis['AMIN'] = 0.05
        
    # Added in tribchem (is_metal check)
    if 'is_metal' in comp_params:
        if comp_params['is_metal']:
            uis['SIGMA'] = 0.2
            uis['ISMEAR'] = 1
        else:
            uis['SIGMA'] = 0.05
            uis['ISMEAR'] = -5
    else:
        uis['SIGMA'] = 0.1
        uis['ISMEAR'] = 0
    
    if comp_params.get('functional') == 'SCAN':
        uis['ISMEAR'] = 0
        uis['SIGMA'] = 0.1

    if static_type.startswith('slab_'):
        uis['NELMDL'] = -15
        uis['NELM'] = 200
    elif comp_params.get('functional') == 'SCAN':
        uis['NELMDL'] = -10
    else:
        uis['NELMDL'] = -6

    if 'encut' in comp_params:
        uis['ENCUT'] = comp_params['encut']

    if 'use_spin' in comp_params:
        if comp_params['use_spin']:
            uis['ISPIN'] = 2
        else:
            uis['ISPIN'] = 1

    #set van der Waals functional. Note that as of now, 'functional' must be
    #specified for vdw to work!
    if set(('use_vdw', 'functional')) <= comp_params.keys():
        if comp_params['use_vdw']:
            if comp_params['functional'] == 'SCAN':
                vdw = 'rVV10'
            else:
                vdw = 'optB86b'
        else:
            vdw = None
    else:
        vdw = None

    if comp_params.get('functional') == 'SCAN':
        uis['METAGGA'] = 'SCAN'
        uis['ALGO'] = 'All'
        uis['LELF'] = False #otherwise KPAR >1 crashes
        
    if static_type.endswith('follow_up'):
        uis['ISTART'] = 1
        uis['LREAL'] = '.FALSE.'
        uis['NELMDL'] = -1
    elif static_type.endswith('nsfc'):
        uis['ISTART'] = 1
        uis['LREAL'] = '.FALSE.'
        uis['ICHARG'] = 11
        uis['NELMDL'] = -1
    
    material_type = 'auto'
    if 'kspacing' in comp_params:
        uis['KSPACING'] = comp_params['kspacing']
        uis['KGAMMA'] = True
        kpoints = None
    elif 'kdens' in comp_params:
        if static_type.startswith('slab_') or static_type.startswith('interface_'):
            material_type = 'slab'
        KPTS = MeshFromDensity(structure,
                               comp_params['kdens'],
                               material_type=material_type,
                               force_gamma=True)
        kpoints = KPTS.get_kpoints()
    else:
        if static_type.startswith('slab_') or static_type.startswith('interface_'):
            material_type = 'slab'
        KPTS = MeshFromDensity(structure,
                               kdens_default,
                               material_type=material_type,
                               force_gamma=True)
        kpoints = KPTS.get_kpoints()
    uks = kpoints
    
    if comp_params.get('functional') == 'SCAN':
        vis = MPScanStaticSet(structure, user_incar_settings = uis, vdw = vdw,
                              user_kpoints_settings = uks,
                              user_potcar_functional = 'PBE_54')
    else:
        vis = MPStaticSet(structure, user_incar_settings = uis, vdw = vdw,
                          user_kpoints_settings = uks,
                          user_potcar_functional = 'PBE_54')
        
    return vis

def get_custom_vasp_relax_settings(structure, comp_params, relax_type,
                                    kdens_default=8):
    """Make custom vasp settings for relaxations.
    
    Parameters
    ----------
    structure : pymatgen.core.structure.Structure
        Structure to be relaxed.
    comp_params : dict
        Computational parameters dictionary which is usually created partly
        by the user and then filled automatically with defaults and after
        convergence tests.
    relax_type : str
        Specifies what is to be relaxed in what way. Check 'allowed_types'
        for a list of choices.
    kdens_default : float, optional
        Specifies the default kpoint density if no kdens or kspacing key
        is found in the comp_params dictionary. The default is 8.

    Raises
    ------
    SystemExit
        If a non-supported relax_type is passed, the process terminates.

    Returns
    -------
    vis : pymatgen.io.vasp.sets.MPScanRelaxSet or
        pymatgen.io.vasp.sets.MPRelaxSet, depending on input
        A vasp input set for pymatgen.

    """

    allowed_types = ['bulk_full_relax', 'bulk_vol_relax', 'bulk_pos_relax',
                      'bulk_shape_relax', 'slab_shape_relax', 'slab_pos_relax',
                      'interface_shape_relax', 'interface_pos_relax',
                      'interface_z_relax']
    
    if relax_type not in allowed_types:
        raise SystemExit('relax type is not known. Please select from: {}'
                        .format(allowed_types))
    
    #Set user incar settings:
    uis = {}
    uis['NEDOS'] = 3001
    uis['PREC'] = 'Accurate'
    uis['GGA_COMPAT'] = '.FALSE.'
    uis['LASPH'] = '.TRUE.'
    uis['LORBIT'] = 11
    uis['MAXMIX'] = 100
    uis['NELMIN'] = 5
    uis['EDIFF'] = 0.5E-5
    uis['LAECHG'] = '.FALSE.'
    #uis['SYMPREC'] = 1e-06
    
    if structure.num_sites < 20:
        uis['LREAL'] = '.FALSE.'

    #Adjust mixing for slabs that have a very large c axis:
    if structure.lattice.matrix[-1,1] > 50.0:
        uis['AMIN'] = 0.05

    if relax_type.startswith('slab_') or relax_type.startswith('interface_'):
        uis['NELMDL'] = -15
        uis['EDIFFG'] = -0.015
        uis['NELM'] = 200
        #Use a slightly slower but more stable algorithm for the electrons
        uis['ALGO'] = 'Normal'
        # Turn on linear mixing
        # uis['AMIX'] = 0.2
        # uis['BMIX'] = 0.0001
        # uis['AMIX_MAG'] = 0.8
        # uis['BMIX_MAG'] = 0.0001

    else:
        uis['NELMDL'] = -6
        uis['EDIFFG'] = -0.01
        uis['NELM'] = 100
    
    if relax_type.endswith('full_relax'):
        uis['ISIF'] = 3
    elif relax_type.endswith('pos_relax'):
        uis['ISIF'] = 2
    elif relax_type.endswith('z_relax'):
        uis['ISIF'] = 2
        #Set up selective dynamics array for the structrues site property
        sd_array = []
        for i in range(len(structure.sites)):
            sd_array.append([False, False, True])
        structure.add_site_property('selective_dynamics', sd_array)
    elif relax_type.endswith('vol_relax'):
        uis['ISIF'] = 7
    elif relax_type.endswith('shape_relax'):
        uis['ISIF'] = 5

    if 'encut' in comp_params:
        uis['ENCUT'] = comp_params['encut']

    if 'use_spin' in comp_params:
        if comp_params['use_spin']:
            uis['ISPIN'] = 2
        else:
            uis['ISPIN'] = 1

    if 'is_metal' in comp_params:
        if comp_params['is_metal']:
            uis['SIGMA'] = 0.2
            uis['ISMEAR'] = 1
        else:
            uis['SIGMA'] = 0.05
            uis['ISMEAR'] = -5
    else:
        uis['SIGMA'] = 0.1
        uis['ISMEAR'] = 0

    #set van der Waals functional. Note that as of now, 'functional' must be
    #specified for vdw to work!
    if set(('use_vdw', 'functional')) <= comp_params.keys():
        if comp_params['use_vdw']:
            if comp_params['functional'] == 'SCAN':
                vdw = 'rVV10'
            else:
                vdw = 'optB86b'
        else:
            vdw = None
    else:
        vdw = None
    
    material_type = 'auto'
    if 'kspacing' in comp_params:
        uis['KSPACING'] = comp_params['kspacing']
        uis['KGAMMA'] = True
        kpoints = None
    elif 'kdens' in comp_params:
        if relax_type.startswith('slab_') or relax_type.startswith('interface_'):
            material_type = 'slab'
        KPTS = MeshFromDensity(structure,
                                comp_params['kdens'],
                                material_type=material_type,
                                force_gamma=True)
        kpoints = KPTS.get_kpoints()
    else:
        if relax_type.startswith('slab_') or relax_type.startswith('interface_'):
            material_type = 'slab'
        KPTS = MeshFromDensity(structure,
                                kdens_default,
                                material_type=material_type,
                                force_gamma=True)
        kpoints = KPTS.get_kpoints()
    uks = kpoints

    if 'functional' in comp_params:
        if comp_params['functional'] == 'SCAN':
            #Algo All does not play well with tetrahedron method
            if 'is_metal' in comp_params:
                if not comp_params['is_metal']:
                    uis['SIGMA'] = 0.1
                    uis['ISMEAR'] = 0
            uis['METAGGA'] = 'SCAN'
            uis['ALGO'] = 'All'
            uis['LELF'] = False #otherwise KPAR >1 crashes
            vis = MPScanRelaxSet(structure, user_incar_settings = uis,
                                vdw = vdw, user_kpoints_settings = uks,
                                user_potcar_functional = 'PBE_54')
        else:
            vis = MPRelaxSet(structure, user_incar_settings = uis, vdw = vdw,
                            user_kpoints_settings = uks,
                            user_potcar_functional = 'PBE_54')
    else:
        vis = MPRelaxSet(structure, user_incar_settings = uis, vdw = vdw,
                        user_kpoints_settings = uks,
                        user_potcar_functional = 'PBE_54')

    return vis