#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 13 09:53:00 2021

Get the interface parameters from the fireworks collection of the FireWorks
database.

@author: omarchehaimi
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'September 13th, 2021'

import pandas as pd

from tribchem.highput.database.navigator import Navigator

def get_inter_params(path, files, save):
    """
    Get the interface parameters after the match of the slabs.
    
    Parameters
    ----------
    path : str
        The path where the standard output files are located.
    
    files : list
        Name of the files where the standard output is saved.
    
    save : str
        Path where to save the output with all the data related about the
        matching of the interfaces.

    """
    
    db_file = None
    database = 'FireWorks'
    nav = Navigator(db_file=db_file, high_level=database)

    # Example of input to pass to the function
    # path = 'folder where there are the stdoutput/'
    # files = ['FW_job-37534.out', 'FW_job-37536.out', 'FW_job-37538.out', 
    #          'FW_job-37542.out', 'FW_job-37545.out', 'FW_job-37535.out',
    #          'FW_job-37537.out', 'FW_job-37541.out', 'FW_job-37544.out']
    # save = 'path where to save the matching results/interfaces.csv'

    columns = ['mid', 'formula', 'uv1', 'uv2', 'area', 'Lattice mismatch']
    data = []
    res_files = {}
    for file in files:
        with open(path+file, 'r') as f:
            previous = ''
            for line in f:
                if 'fw_id' in line:
                    fw_id = line.split()
                    inter = nav.find_data(collection='fireworks', 
                            fltr={'fw_id': int(fw_id[fw_id.index('fw_id:')+1])})
                    if not inter or not inter['name'] == 'Build and store a interface':
                        continue
                    mid = inter['spec']['_tasks'][0]['mid']
                    formula = inter['spec']['_tasks'][0]['formula']
                    res_files['mid'] = mid
                    res_files['formula'] = formula
                
                if 'uv1:' in previous:
                    sanitized = line.split('(')
                    sanitized = [i.split(')') for i in sanitized]
                    res_files['uv1'] = str(sanitized[1][0]) + ', ' + str(sanitized[2][0])  
                
                if 'uv2:' in previous:
                    sanitized = line.split('(')
                    sanitized = [i.split(')') for i in sanitized]
                    res_files['uv2'] = str(sanitized[1][0]) + ', ' + str(sanitized[2][0])
                
                if 'area' in previous:
                    res_files['area'] = line.split()[0]
                
                if 'Lattice mismatch[u, v & alpha]:' in previous:
                    mis = line.split()
                    sanitized = str(mis[0]) + ', ' + str(mis[2]) + ', ' + \
                                str(mis[4]) + ' ' + str(mis[5]) 
                    res_files['Lattice mismatch'] = sanitized
                    data.append(list(res_files.values()))
                    res_files = {}
                
                previous = line

    df = pd.DataFrame(data, columns=columns)
    print("Saving the results in: "+save)
    df.to_csv(save, index=False)