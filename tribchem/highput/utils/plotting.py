#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 15:46:24 2020

Utility tools for plotting.

Author: Gabriele Losi (glosi000), Omar Chehaimi (omarchehaimi)
 
"""

__author__ = 'Gabriele Losi, Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 8th, 2021'

from dis import dis
import json
from turtle import st

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.colors import LogNorm
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
from pymatgen.analysis.adsorption import plot_slab
from ase.calculators.vasp import VaspChargeDensity

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.database.dbtools import read_multiple_entry
from tribchem.highput.database.mid import Mid

from tribchem.physics.base.manist import estimate_interface


# =============================================================================
# PLOTTING TOOLS
# =============================================================================


def plot_slab_hs(hs, slab, to_fig=None):
    """
    Plot the slab, displaying the atoms and the HS sites of the surface
    
    Parameters
    ----------
    slab : pymatgen.core.surface.Slab 
        The slab to be displayed
        
    hs : dict
        HS sites of the slab.
        
    name_fig : string, optional
        Name of the image that you want to save, it will be: 'name_fig'+'.pdf' 
        Suggested name: 'Name of the material' + 'Miller index'.
        The default is None and no image is saved.     
        
    Returns
    -------
    None.

    """
    
    from tribchem.physics.tribology.highsym import hs_dict_converter

    # Check the type of the hs points
    typ = list( set(type(k) for k in hs.values()) )[0]
    if typ == list:
        hs = hs_dict_converter(hs, to_array=True)
    
    # Extract the lattice vector of the basal plane
    a = slab.lattice.matrix[0, :]
    b = slab.lattice.matrix[1, :]
    
    # plot the atoms and the lattice cell
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    plot_slab(slab, ax, scale=0.8, repeat=3, window=2.25, 
              draw_unit_cell=True, decay=0.2, adsorption_sites=False)
    ax.set(xlim = ( -0.1*(a[0] + b[0]), 1.1*(a[0] + b[0]) ), 
           ylim = ( -0.1*(a[1] + b[1]), 1.1*(a[1] + b[1]) ))
    
    # Add the HS sites with the proper labels
    for k in hs.keys():
        data = hs[k]
        if len(data.shape) == 1:
            plt.plot(data[0], data[1], marker='o', markersize=12, mew=3, 
                     linestyle='', zorder=10000, label=k)     
        else:
            plt.plot(data[:,0], data[:,1], marker='o', markersize=12, mew=3, 
                     linestyle='', zorder=10000, label=k)
 
    plt.legend(bbox_to_anchor=(1.025, 1), loc='upper left')
    
    plt.rcParams.update({'font.size': 18})
    plt.tick_params(
    axis='both',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    left=False,
    right=False,
    labelbottom=False,
    labelleft=False) # labels along the bottom edge are off
    
    if to_fig != None:
        plt.savefig(to_fig+'.png', dpi=300, bbox_inches='tight')
    
    plt.show()


def plot_pes(data, lattice, to_fig=None, vmin=None, vmax=None):
    """
    Plot the PES and eventually save it

    """

    a = lattice[0]
    b = lattice[1]
    x = data[0]
    y = data[1]
    E = data[2]

    fact=1.
    n = 51
    if vmin and vmax:
        levels = np.linspace(vmin, vmax, n)
    else:
        levels = np.linspace(np.amin(E), np.amax(E), n)
    fig = plt.figure(figsize=(7, 7), dpi=150)
    ax = fig.add_subplot(111)
    ax.set_aspect('equal')
    anglerot='vertical'
    shrin=1.
    #zt1=plt.contourf(x, y, E, level, extent=(-fact*a, fact*a, -fact*b, fact*b), cmap=plt.cm.RdYlBu_r)
    zt1=plt.contourf(x, y, E, levels, cmap=plt.cm.RdYlBu_r)
    cbar1=plt.colorbar(zt1,ax=ax,orientation=anglerot,shrink=shrin)
    cbar1.set_label(r'$E_{adh} (J/m^2)$', rotation=270, labelpad=20,fontsize=15,family='serif')

    #ax.quiver(0. , 0., 1., 0.,scale=1.,scale_units='inches',width=0.01,color='white')
    #ax.quiver(0. , 0., 0., 1.,scale=1.,scale_units='inches',width=0.01,color='white')
    ax.plot(0.,0.,'w.',ms=7)
    #ax.text(0.5,-0.5,'[1 0 1]',rotation='horizontal',color='white', fontsize=14)
    #ax.text(-0.5,1.,'[1 2 1]',rotation='vertical',color='white', fontsize=14)
    #ax.axis([-fact*min(a), fact*max(a), -fact*min(b), fact*max(b)])
    plt.xlabel(r"distance ($\AA$)",fontsize=12,family='serif')
    plt.ylabel(r"distance ($\AA$)",fontsize=12,family='serif')

    for zt1 in zt1.collections:
       zt1.set_edgecolor("face")
       zt1.set_linewidth(0.000000000001)

    if to_fig != None:
        plt.title("PES for " + str(to_fig), fontsize=15, family='serif')
        plt.savefig('PES_' + str(to_fig) + '.png', dpi=300)
    
def plot_uniform_grid(grid, cell, n_a, n_b):
    """
    Plot an uniform grid of n_aXn_b points on the planar base of a lattice 
    
    """
    
    a = cell[0, :]
    b = cell[1, :]
    v = np.cross(a, b)
    
    mod_a = np.sqrt(a[0]**2. + a[1]**2. + a[2]**2.)
    mod_b = np.sqrt(b[0]**2. + b[1]**2. + b[2]**2.)
    A = np.sqrt(v[0]**2. + v[1]**2. + v[2]**2.)
    
    N = n_a * n_b
    density = N / A
    
    # Print information
    print("1st vector:  {:} -> norm: {:.3f}".format(a, mod_a))
    print("2nd vector:  {:} -> norm: {:.3f}".format(b, mod_b))
    print("N pts: {:}   Area: {:.3f}   Density: {:.3f}".format(N, A, density))
    print("\nUniform {0}x{1} grid\n".format(n_a, n_b))
    print(grid)      
    
    # Projection on the plane, top view
    plt.title("Projection on xy plane")
    plt.plot(grid[:, 0], grid[:, 1], 'o')
    
    # 3D plot
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(grid[:,0], grid[:,1], grid[:,2], 
               c='r', marker='o', label="3D grid")
    
    # Plot the lattice edge of the plane
    x = [0, a[0], a[0]+b[0], b[0],0]
    y = [0, a[1], a[1]+b[1], b[1],0]
    z = [0, a[2], a[2]+b[2], b[2],0]
    
    ax.plot(x, y, z)
    plt.show()

def plot_adhesion(save, elements=None, ron=2, adh_type='short', label_size=16, 
                  annotation_size=18, db_file=None, database='tribchem'):
    """
    Plot the adhesion energy for every element against all the other ones.
    author: Omar Chehaimi
    
    Parameters
    ----------
    save : str
        Path where to save the matrix.
    
    elements : list, optional
        If none all the elements available in the database will be plotted, 
        otherwise this list contains all the elements to plot. An element is
        identified with the following notation: 'Al111'.
    
    ron : int, optional
        Number of decimal digits to keep while rounding.
    
    adh_type : str, optional
        Type of adhesion. It can be only regular or short.
        
    label_size : int, optional
        Fontsize of the label.
        
    annotation_size : int, optional
        Fontsize of the annotation.
        
    db_file : str, optional
        Path where the database is located.
    
    database : str, optional
        Name of the database (by default is tribchem).

    """
    
    if elements and not isinstance(elements, list):
        raise ValueError("The input {} is not a list!".format(elements))
    
    nav = Navigator(db_file=db_file, high_level=database)
    
    data = nav.find_many_data(collection='PBE.interface_elements', fltr={})
    
    adh = {}
    
    for inter in data:
        # Check if the adhesion has been calculated
        if 'adhesion' not in inter.keys():
            continue

        if elements:
           for el in elements:
               if not inter['name'].find(el):
                   adh[inter['name']] = [inter['adhesion'][adh_type]['min_site']['adhesion_energy_jm2'], 
                                         inter['adhesion'][adh_type]['corrugation_jm2']]
        else:
            adh[inter['name']] = [inter['adhesion'][adh_type]['min_site']['adhesion_energy_jm2'], 
                                  inter['adhesion'][adh_type]['corrugation_jm2']]
    
    tot_adh = {}
    # Initialization of the empty dictionaries
    for k, v in adh.items():
       elements = k.split('-')
       tot_adh[elements[0]] = {}
       tot_adh[elements[1]] = {}
    
    homo_adh = {}
    for k, v in adh.items():
        elements = k.split('-')
        tot_adh[elements[0]][elements[1]] = v
        tot_adh[elements[1]][elements[0]] = v
        # For ordering the matrix with respect the adhesion energy of the
        # homogenous interfaces
        if elements[0] == elements[1]:
            homo_adh[elements[0]] = v[0]
            
    homo_adh = dict(sorted(homo_adh.items(), key=lambda x:x[1]))

    # Using the sorted elements
    label = homo_adh.keys()
    matrix_adh = []
    matrix_cor = []
    for element in label:
        row_adh = []
        row_cor = []
        adh_en = tot_adh[element]
        for el in label:
            if el == element:
                if el in adh_en.keys():
                    row_adh.append(adh_en[el][0])
                    row_cor.append(adh_en[el][1])
                else:
                    row_adh.append(np.nan)
                    row_cor.append(np.nan)
            else:
                if el not in adh_en.keys():
                    row_adh.append(np.nan)
                    row_cor.append(np.nan)
                else:
                    row_adh.append(adh_en[el][0])
                    row_cor.append(adh_en[el][1])

        matrix_adh.append(row_adh)
        matrix_cor.append(row_cor)

    df_adh = pd.DataFrame(matrix_adh, columns=label)
    df_cor = pd.DataFrame(matrix_cor, columns=label)

    matrix_avg = []
    matrix_abs_avg = []
    for i in range(0, len(matrix_adh)):
        row_avg = []
        row_abs_avg = []
        for j in range(0, len(matrix_adh)):
            if matrix_adh[i][j] == np.nan:
                res_avg = np.nan
                res_avg_abs = np.nan
            else:
                avg = np.mean([matrix_adh[i][i], matrix_adh[j][j]])
                res_avg = matrix_adh[i][j] - avg
                res_avg_abs = (matrix_adh[i][j] - avg)/avg
                res_avg_abs = res_avg_abs*100
            row_avg.append(res_avg)
            row_abs_avg.append(res_avg_abs)
        matrix_avg.append(row_avg)
        matrix_abs_avg.append(row_abs_avg)
    
    df_avg = pd.DataFrame(matrix_avg, columns=label)
    df_abs_avg = pd.DataFrame(matrix_abs_avg, columns=label)
    
    matrix_text_adh = []
    matrix_text_cor = []
    matrix_text_avg = []
    matrix_text_abs_avg = []
    # Add the text in the matrix
    for i in range(0, len(label)):
        row_adh = []
        row_cor = []
        row_avg = []
        row_abs_avg = []
        for j in range(0, len(label)):
            if matrix_adh[i][j] == np.nan:
                row_adh.append('N/A')
                row_cor.append('N/A')
                row_avg.append('N/A')
                row_abs_avg.append('N/A')
            else:
                row_adh.append(str(round(matrix_adh[i][j], ron)))
                row_cor.append(str(round(matrix_cor[i][j], ron+1)))
                row_avg.append(str(round(matrix_avg[i][j], ron)))
                row_abs_avg.append(str(round(matrix_abs_avg[i][j], ron)))
        matrix_text_adh.append(row_adh)
        matrix_text_cor.append(row_cor)
        matrix_text_avg.append(row_avg)
        matrix_text_abs_avg.append(row_abs_avg)
    
    df_adh = pd.DataFrame.to_numpy(df_adh)
    df_cor = pd.DataFrame.to_numpy(df_cor)
    df_avg = pd.DataFrame.to_numpy(df_avg)
    df_abs_avg = pd.DataFrame.to_numpy(df_abs_avg)
    mask = np.triu(np.ones_like(df_adh, dtype=bool), k=1)
    
    # Save the adhesion energies and the ordering
    with open(save+'adh.json', 'w') as f_adh:
        json.dump(tot_adh, f_adh, indent=4, sort_keys=True)
    
    with open(save+'ord.json', 'w') as f_ord:
        json.dump(homo_adh, f_ord, indent=4, sort_keys=True)
    
    # Save the plots
    plot_matrix(pd_dataframe=df_adh, annotation=matrix_text_adh, mask=mask, 
                annotation_size=annotation_size, label=label, 
                label_size=label_size, title='Adhesion Energy ($J/m^2$)', 
                save=save+'matrix_adh')
    
    plot_matrix(pd_dataframe=df_cor, annotation=matrix_text_cor, mask=mask, 
                annotation_size=annotation_size, label=label, 
                label_size=label_size, title='Corrugation ($J/m^2$)', 
                save=save+'matrix_corr', corr=True)
    
    plot_matrix(pd_dataframe=df_avg, annotation=matrix_text_avg, mask=mask, 
                annotation_size=annotation_size, label=label, 
                label_size=label_size, title='Error ($J/m^2$)',
                save=save+'matrix_err')
    
    plot_matrix(pd_dataframe=df_abs_avg, annotation=matrix_text_abs_avg, 
                mask=mask, annotation_size=annotation_size, label=label, 
                label_size=label_size, title='Error (%)',
                save=save+'matrix_per_err')

def plot_interlayer(inter, ordering, save, ron=2, label_size=16, 
                    annotation_size=18):
    """
    Plot the interlayer distance on a matrix given a json file.
    
    Parameters
    ----------
    inter : str
        Path for the json file containing the data.
    
    ordering : str
        Path for the json file containing the ordering of the interfaces.

    save : str
        Path where to save the plots.
    
    ron : int, optional
        Number of decimal digits to keep while rounding.
        
    label_size : int, optional
        Fontsize of the label.
        
    annotation_size : int, optional
        Fontsize of the annotation.

    """
    
    with open(inter, 'r') as f_d:
        dist = json.load(f_d)
        
    with open(ordering, 'r') as f_o:
        ord = json.load(f_o)
    
    ord = dict(sorted(ord.items(), key=lambda x:x[1]))
    
    label = ord.keys()
    d_max = {}
    d_min = {}
    
    for e in label:
        d_max[e] = {}
        d_min[e] = {}
        for d in dist.keys():
            if e in d:
                name = d.replace(e, '').replace('-', '')
                if name == '':
                    d_max[e][e] = dist[d]['max']
                    d_min[e][e] = dist[d]['min']
                else:
                    d_max[e][name] = dist[d]['max']
                    d_min[e][name] = dist[d]['min']
    
    m_max = []
    m_min = []

    for element in label:
        row_max = []
        row_min = []
        for el in label:
            if el == element:
                if el in d_max[element].keys():
                    row_max.append(d_max[element][el])
                else:
                    row_max.append(np.nan)

                if el in d_min[element].keys():
                    row_min.append(d_min[element][el])
                else:
                    row_min.append(np.nan)
            else:
                if el not in d_max[element].keys():
                    row_max.append(np.nan)
                else:
                    row_max.append(d_max[element][el])
    
                if el not in d_min[element].keys():
                    row_min.append(np.nan)
                else:
                    row_min.append(d_min[element][el])

        m_max.append(row_max)
        m_min.append(row_min)
        
    df_max = pd.DataFrame(m_max, columns=label)
    df_min = pd.DataFrame(m_min, columns=label)
    
    m_max_text = []
    m_min_text = []
    for i in range(0, len(label)):
        row_max_text = []
        row_min_text = []
        for j in range(0, len(label)):
            if m_max[i][j] == np.nan:
                row_max_text.append('N/A')
            else:
                row_max_text.append(str(round(m_max[i][j], ron)))
            
            if m_min[i][j] == np.nan:
                row_min_text.append('N/A')
            else:
                row_min_text.append(str(round(m_min[i][j], ron)))
        
        m_max_text.append(row_max_text)
        m_min_text.append(row_min_text)
        
    df_max = pd.DataFrame.to_numpy(df_max)
    df_min = pd.DataFrame.to_numpy(df_min)
    
    mask_max = np.triu(np.ones_like(df_max, dtype=bool), k=1)
    mask_min = np.triu(np.ones_like(df_min, dtype=bool), k=1)
    
    plot_matrix(pd_dataframe=df_max, annotation=m_max_text, mask=mask_max, 
                annotation_size=annotation_size, label=label, 
                label_size=label_size, title='Interlayer Distance Max (Å)', 
                save=save+'inter_max')
    
    plot_matrix(pd_dataframe=df_min, annotation=m_min_text, mask=mask_min, 
                annotation_size=annotation_size, label=label, 
                label_size=label_size, title='Interlayer Distance Min (Å)', 
                save=save+'inter_min')

def plot_matrix(pd_dataframe, annotation, mask, annotation_size, label, 
                label_size, title, save, corr=False, invert=False):
    """
    Plot the matrix containing the adhesion results.
    
    Parameters
    ----------
    pd_dataframe : Pandas Dataframe
        Pandas dataframe containing the data to plot.
    
    annotation : list of list
        Matrix containing the numbers to plot.
        
    mask : list of list
        Mask to remove the upper triangle.
    
    annotation_size : int 
    
    label : str
        Elements.
    
    label_size : int
    
    title : str
    
    save : str
        Name of the file where to save the matrix.
    
    corr : bool
        Set True to plot the corrugation.
    
    invert : bool
        Set True to invert the color of the matrix.

    """
    
    fig, ax = plt.subplots(figsize=(20, 16))
    cmap = sns.cm.rocket_r
    
    if corr:
        sns.heatmap(pd_dataframe, annot=annotation, fmt="", ax=ax, cbar=False, 
                    mask=mask, annot_kws={"fontsize":annotation_size}, linewidths=0.5, 
                    norm=LogNorm(), cmap=cmap)
    elif invert:
        sns.heatmap(pd_dataframe, annot=annotation, fmt="", ax=ax, cbar=False, 
                    mask=mask, annot_kws={"fontsize":annotation_size}, linewidths=0.5, 
                    cmap=cmap)
    else:
        sns.heatmap(pd_dataframe, annot=annotation, fmt="", ax=ax, cbar=False, 
                    mask=mask, annot_kws={"fontsize":annotation_size}, linewidths=0.5)
  
    ax.set_yticklabels(label, rotation="horizontal", fontsize=label_size)
    ax.set_xticklabels(label, rotation="horizontal", fontsize=label_size)
    ax.set_title(title, fontsize=26, y=0.96, fontweight='bold')
    plt.savefig(save+'.png', bbox_inches='tight', pad_inches=0.0)
    plt.savefig(save+'.pdf', bbox_inches='tight', pad_inches=0.0)

def plot_ppes(save, ppes_type='regular', elements=None, db_file=None, 
              database='tribchem'):
    """
    Plot the perpendicular potential energy surface.
    author: Omar Chehaimi
    
    Parameters
    ----------
    save : str
        Path where to save the matrix.
    
    elements : list, optional
        If none all the elements available in the database will be plotted, 
        otherwise this list contains all the elements to plot. An element is
        identified with the following notation: 'Al111'.
    
    site : str, optional
        Site of the ppes. It can be only min_site or max_site.
        
    plot_homo : bool, optional
        Set to True to plot also the PPES of the respective homogeneous 
        interface.
        
    db_file : str, optional
        Path where the database is located.
    
    database : str, optional
        Name of the database (by default is tribchem).

    """
    
    if elements and not isinstance(elements, list):
        raise ValueError("The input {} is not a list!".format(elements))
        
    if ppes_type not in ['regular', 'short']:
        raise ValueError("The {} ppes_type is not valid. "
                         "Select either 'regular' or 'short'.".format(ppes_type))
    
    nav = Navigator(db_file=db_file, high_level=database)
    data = nav.find_many_data(collection='PBE.interface_elements', fltr={})
    
    ppes = {}
    for inter in data:
        
        if 'ppes' not in inter.keys():
            continue
        
        if ppes_type == 'regular':
            ppes[inter['name']] = {'structure': inter['structure'],
                                   'adhesion': inter['adhesion'], 
                                   'ppes': inter['ppes']}
        else:
            ppes[inter['name']] = {'structure': inter['structure'],
                                   'adhesion': inter['adhesion']['short'], 
                                   'ppes': inter['ppes']}
    
    ppes_tr = {}
    for key, value in ppes.items():
        
        if ppes_type == 'regular':
            dz_max = estimate_interface(value['structure']['opt']['interface_max'])[0]
            dz_min = estimate_interface(value['structure']['opt']['interface_min'])[0]
            
            # In the future there will be the field for the short ppes, here is why this
            # instructions are repeated exactly later.
            max_site_p, max_site_e = extract_p_e(
                value['ppes']['energy']['max_site']['ppes_jm2'])
            min_site_p, min_site_e = extract_p_e(
                value['ppes']['energy']['min_site']['ppes_jm2'])

            shifted_max_p, shifted_max_e = get_shifts(max_site_p, max_site_e, 
                                                      dz_max, value, 'max_site')
            shifted_min_p, shifted_min_e = get_shifts(min_site_p, min_site_e, 
                                                      dz_min, value, 'min_site')
        else:
            dz_max = estimate_interface(value['structure']['opt']['short']['interface_max'])[0]
            dz_min = estimate_interface(value['structure']['opt']['short']['interface_max'])[0]
            
            max_site_p, max_site_e = extract_p_e(
                value['ppes']['energy']['max_site']['ppes_jm2'])
            min_site_p, min_site_e = extract_p_e(
                value['ppes']['energy']['min_site']['ppes_jm2'])

            shifted_max_p, shifted_max_e = get_shifts(max_site_p, max_site_e, 
                                                      dz_max, value, 'max_site')
            shifted_min_p, shifted_min_e = get_shifts(min_site_p, min_site_e, 
                                                      dz_min, value, 'min_site')
            ppes_tr[key] = {'max': [shifted_max_p, shifted_max_e],
                            'min': [shifted_min_p, shifted_min_e]}
        
    ppes_plot_all(ppes_tr, 'min', save)
    ppes_plot_all(ppes_tr, 'max', save)
        
def extract_p_e(pe):
    """
    Extract the position and the energy from a list of the type:
    [[p1, e1], [p2, e2], ...] to [p1, p2, ...], and [e1, e2, ...].
    The order is kept.
    
    Parameters
    ----------
    pe : list of lists
        List of lists containing the postions and energies.
        
    Return
    ------
    p : list
        Positions.

    e : list
        Energies.

    """
    
    p = []
    e = []
    
    for i in pe:
        p.append(i[0])
        e.append(i[1])
    
    return p, e

def get_shifts(p, e, dz, value, site):
    """
    Get the shifted values for the energies and positions.
    """
    
    if site not in ['min_site', 'max_site']:
        raise ValueError("The site must be either 'min_site' or 'max_site' not {}.".format(site))

    # Shfting positions
    shifted_p = [i - dz for i in p]
    
    # Shifting energies
    shift_e = e[p.index(0)] - value['adhesion'][site]['adhesion_energy_jm2']
    shifted_e = [i - shift_e for i in e]
    
    return shifted_p, shifted_e

def ppes_plot_all(ppes_tr, position, save):
    """
    Plot and save the ppes for all the elements.
    """

    lab_p = {'min': 'Min', 'max': 'Max'}
    for key, value in ppes_tr.items():

        el1, el2 = key.split('-')
        if el1 == el2:
            continue

        hom1 = el1 + '-' + el1
        hom2 = el2 + '-' + el2

        if hom1 not in ppes_tr.keys():
            raise ValueError("The homogeneous interface {} has not been "
                             "calculated.".format(hom1))
        if hom2 not in ppes_tr.keys():
            raise ValueError("The homogeneous interface {} has not been "
                             "calculated.".format(hom2))

        fig, ax = plt.subplots(figsize=(12, 8))
        ax.plot(ppes_tr[hom1][position][0], ppes_tr[hom1][position][1], 
                marker='^', label=lab_p[position]+' '+hom1)
        ax.plot(value[position][0], value[position][1], 
                marker='v', label=lab_p[position]+' '+key)
        ax.plot(ppes_tr[hom2][position][0], ppes_tr[hom2][position][1], 
                marker='^', label=lab_p[position]+' '+hom2)
        ax.legend(fontsize=14)
        ax.set_ylabel('PPES (J/m$^2$)', rotation="vertical", fontsize=20)
        ax.set_xlabel('Position (Å)', rotation="horizontal", fontsize=20)
        ax.tick_params(axis='y', which='major', labelsize=14)
        ax.tick_params(axis='x', which='major', labelsize=14, rotation=45)
        plt.xticks(value[position][0])
        plt.subplots_adjust(bottom=0.15)
        ax.set_title(key+' Perpendicular Potential Energy Surface', fontsize=20)
        fig.savefig(save+'ppes_'+position+'_'+key+'.png')
        fig.savefig(save+'ppes_'+position+'_'+key+'.pdf')

def plot_charge(save, direction, site='all', elements=None, db_file=None, 
                database='tribchem'):
    """
    Plot the charge displacement along a given direction.
    
    author: Omar Chehaimi
    
    Parameters
    ----------
    save : str
        Path where to save the matrix.
    
    direction : str
        Direction along with calculate the charge displacement.
    
    site : str, optional
        Site of the ppes. It can be only min_site or max_site.
    
    elements : list, optional
        If none all the elements available in the database will be plotted, 
        otherwise this list contains all the elements to plot. An element is
        identified with the following notation: 'Al111'.
        
    db_file : str, optional
        Path where the database is located.
    
    database : str, optional
        Name of the database (by default is tribchem).

    """
    
    if elements and not isinstance(elements, list):
        raise ValueError("The input {} is not a list!".format(elements))
    
    if site == 'all':
        raise ValueError("Not implemented yet the automatic plotting of all sites!")

    if site not in ['all', 'min_site', 'max_site']:
        raise ValueError("The site {} is not correct."
                         "Available sites are 'min_site', and 'max_site."
                         .format(site))
    
    nav = Navigator(db_file=db_file, high_level=database)
    data = nav.find_many_data(collection='PBE.interface_elements', fltr={})
    
    charge = {}
    
    for inter in data:
        
        if 'charge' not in inter.keys():
            continue
        
        if 'displacement' not in inter['charge'].keys():
            continue

        positions = inter['charge']['displacement'][site][direction]['grid'][0]
        charge = inter['charge']['displacement'][site][direction]['grid'][1]
        
        fig, ax = plt.subplots(figsize=(12, 8))
        ax.plot(positions, charge)
        ax.set_ylabel('Charge Displacement (e$^-$/Å$^3$)', rotation="vertical", fontsize=20)
        ax.set_xlabel('Displacement (Å)', rotation="horizontal", fontsize=20)
        ax.tick_params(axis='y', which='major', labelsize=14)
        ax.tick_params(axis='x', which='major', labelsize=14)
        plt.subplots_adjust(bottom=0.15)
        ax.set_title('Charge displacement for '+inter['name']+' at '+site+ ' along '+direction, 
                     fontsize=20)
        fig.savefig(save+'charge_disp_'+inter['name']+'_'+site+'.png')
        fig.savefig(save+'charge_disp_'+inter['name']+'_'+site+'.pdf')
        plt.close()
        
def plot_charge_matrix(save, ordering, elements=None, ron=3, label_size=16, 
                       annotation_size=18, db_file=None, database='tribchem'):
    """
    Plot the adhesion energy for every element against all the other ones.
    author: Omar Chehaimi
    
    Parameters
    ----------
    save : str
        Path where to save the matrix.
        
    ordering : str
        Path for the json file containing the ordering of the interfaces.
    
    elements : list, optional
        If none all the elements available in the database will be plotted, 
        otherwise this list contains all the elements to plot. An element is
        identified with the following notation: 'Al111'.
    
    ron : int, optional
        Number of decimal digits to keep while rounding.
    
    adh_type : str, optional
        Type of adhesion. It can be only regular or short.
        
    label_size : int, optional
        Fontsize of the label.
        
    annotation_size : int, optional
        Fontsize of the annotation.
        
    db_file : str, optional
        Path where the database is located.
    
    database : str, optional
        Name of the database (by default is tribchem).

    """
    
    if elements and not isinstance(elements, list):
        raise ValueError("The input {} is not a list!".format(elements))
    
    nav = Navigator(db_file=db_file, high_level=database)
    
    data = nav.find_many_data(collection='PBE.interface_elements', fltr={})
    
    chr = {}
    
    for inter in data:
        # Check if the adhesion has been calculated
        if 'charge' not in inter.keys():
            continue

        if elements:
           for el in elements:
               if not inter['name'].find(el):
                   if 'min_site' not in inter['charge']['displacement'].keys():
                       mis = np.nan
                   else:
                       mis = inter['charge']['displacement']['min_site']['z']['interface']['integral']
                   if 'max_site' not in inter['charge']['displacement'].keys():
                       mxs = np.nan
                   else:
                       mxs = inter['charge']['displacement']['max_site']['z']['interface']['integral']
                   chr[inter['name']] = [mis, mxs]
        else:
            if 'min_site' not in inter['charge']['displacement'].keys():
                mis = np.nan
            else:
                mis = inter['charge']['displacement']['min_site']['z']['interface']['integral']
            if 'max_site' not in inter['charge']['displacement'].keys():
                mxs = np.nan
            else:
                mxs = inter['charge']['displacement']['max_site']['z']['interface']['integral']
            chr[inter['name']] = [mis, mxs]
    
    tot_chr = {}
    # Initialization of the empty dictionaries
    for k, v in chr.items():
       elements = k.split('-')
       tot_chr[elements[0]] = {}
       tot_chr[elements[1]] = {}
    
    for k, v in chr.items():
        elements = k.split('-')
        tot_chr[elements[0]][elements[1]] = v
        tot_chr[elements[1]][elements[0]] = v
    
    with open(ordering, 'r') as f_o:
        ord = json.load(f_o)
    
    ord = dict(sorted(ord.items(), key=lambda x:x[1]))
    
    label = ord.keys()
    c_max = {}
    c_min = {}
    
    for e in label:
        c_max[e] = {}
        c_min[e] = {}
        for d in tot_chr.keys():
            if d == e:
                if d not in tot_chr[e].keys():
                    continue
                c_min[e][e] = tot_chr[e][d][0]
                c_max[e][e] = tot_chr[e][d][1]
            elif d not in tot_chr[e].keys():
                continue
            else:
                c_min[e][d] = tot_chr[e][d][0]
                c_max[e][d] = tot_chr[e][d][1]
    
    m_max = []
    m_min = []

    for element in label:
        row_max = []
        row_min = []
        for el in label:
            if el == element:
                if el in c_max[element].keys():
                    row_max.append(c_max[element][el])
                else:
                    row_max.append(np.nan)

                if el in c_min[element].keys():
                    row_min.append(c_min[element][el])
                else:
                    row_min.append(np.nan)
            else:
                if el not in c_max[element].keys():
                    row_max.append(np.nan)
                else:
                    row_max.append(c_max[element][el])
    
                if el not in c_min[element].keys():
                    row_min.append(np.nan)
                else:
                    row_min.append(c_min[element][el])

        m_max.append(row_max)
        m_min.append(row_min)
        
    df_max = pd.DataFrame(m_max, columns=label)
    df_min = pd.DataFrame(m_min, columns=label)
    
    m_max_text = []
    m_min_text = []
    for i in range(0, len(label)):
        row_max_text = []
        row_min_text = []
        for j in range(0, len(label)):
            if m_max[i][j] == np.nan:
                row_max_text.append('N/A')
            else:
                row_max_text.append(str(round(m_max[i][j], ron)))
            
            if m_min[i][j] == np.nan:
                row_min_text.append('N/A')
            else:
                row_min_text.append(str(round(m_min[i][j], ron)))
        
        m_max_text.append(row_max_text)
        m_min_text.append(row_min_text)
        
    df_max = pd.DataFrame.to_numpy(df_max)
    df_min = pd.DataFrame.to_numpy(df_min)
    
    mask_max = np.triu(np.ones_like(df_max, dtype=bool), k=1)
    mask_min = np.triu(np.ones_like(df_min, dtype=bool), k=1)
        
    plot_matrix(pd_dataframe=df_max, annotation=m_max_text, mask=mask_max, 
                annotation_size=annotation_size, label=label, 
                label_size=label_size, title='Average Charge ($e^-$/$Å^3$))', 
                save=save+'chr_max', invert=True)
    
    plot_matrix(pd_dataframe=df_min, annotation=m_min_text, mask=mask_min, 
                annotation_size=annotation_size, label=label, 
                label_size=label_size, title='Average Charge ($e^-$/$Å^3$)', 
                save=save+'chr_min', invert=True)
    
    # Save the adhesion energies and the ordering
    with open(save+'chr.json', 'a') as f_chr:
        json.dump(tot_chr, f_chr, indent=4, sort_keys=True)
        
def work_function_plot(locpot, save, inter_name, axes, ef):
    """
    Plot the potential energy along a given direction by reading the file 
    LOCPOT.
    
    Parameters
    ----------
    
    locpot : str
        Path where the LOCPOT file is saved.
    
    save : str
        Path where to save the results. It will save the plots and the csv file 
        containing the values of the potential for each point along the given
        axes.
    
    inter_name : str
        Name of the interface.
    
    axes : str
        Axes along which to calculate the energy.
    
    ef : double
        Fermi energy.
    
    Return
    ------
    None.

    """
    
    """
    Code taken from: https://gist.github.com/Ionizing/1ac92f98e8b00a1cf6f16bd57694ff03'.
    """

    locd = VaspChargeDensity(locpot)
    cell = locd.atoms[0].cell
    latlens = np.linalg.norm(cell, axis=1)
    vol = np.linalg.det(cell)

    iaxis = ['x', 'y', 'z'].index(axes.lower())
    axes = [0, 1, 2]
    axes.remove(iaxis)
    axes = tuple(axes)

    locpot = locd.chg[0]
    mean = np.mean(locpot, axes) * vol

    xvals = np.linspace(0, latlens[iaxis], locpot.shape[iaxis])
    
    fig, ax = plt.subplots(figsize=(12, 10))
    ax.plot(xvals, mean)
    ax.set_title(inter_name+' E$_{Fermi}$='+ef+' (eV)', fontsize=16)
    ax.set_ylabel('Energy ($eV$)', fontsize=16)
    ax.set_xlabel('Distance ($\AA$)', fontsize=16)
    ax.tick_params(axis='both', labelsize=16)
    fig.savefig(save+inter_name+'.pdf', bbox_inches='tight')
    
    total = np.column_stack((xvals, mean))
    np.savetxt(save+inter_name+'.csv', total, fmt='%1.3f', 
               header='Distance($\AA$) Energy(eV)')
    
    return xvals, mean