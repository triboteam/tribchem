#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 1 09:08:36 2021

Tools to save the results of the VASP simulation in a more accessible way,
create few tables that are useful to look at them.

@author: glosi000, omarchehaimi
"""

__author__ = 'Gabriele Losi, Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'June 1st, 2021'

import os
import json
from pathlib import Path, PurePosixPath

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from pymatgen.core.structure import Structure

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.utils.vasp_tools import VaspInputSet
from tribchem.physics.base.units import PhysConstants


currentdir = os.path.dirname(__file__)


# ============================================================================
# Secondary tools to work with input parameters and functions
# ============================================================================

def save_calctags(tag, collection, formula=None, mpid=None, miller=None, 
                  name=None, db_file=None, database='tribchem'):
    """
    Store in a csv file the tags of a calculation which was succesfully done by
    vasp. Useful to retrieve later the tags in order to have a complete access
    to the results data stored in the low level datababase.

    """
    
    # Check the folder containing calculation tags, if not present create it
    folder_object = PurePosixPath(currentdir)
    folder = str(folder_object.parent.parent.parent) + '/results/'
    path = Path(folder)
    if not path.is_dir():
        print("WARNING: There is no folder for calculation tags.")
        print("Creating a new mp_structures folder in " + folder)
        folder = PurePosixPath(folder)
        os.mkdir(folder)
        path = Path(folder)
        if not path.is_dir():
            raise RuntimeError('The creation of struct path has failed!')
            
    # Create the path to the csv file
    path = str(path)
    csv_file = path + '/calc_tags.csv'
    columns = ['tag', 'formula', 'mpid', 'miller', 'name',
               'db_file', 'database', 'collection']

    # Create the new row for the Dataframe 
    df_new = pd.DataFrame([[tag, formula, mpid, miller, name, db_file, 
                           database, collection]], columns=columns)
    
    # Check if the csv table does exist, if not create it, else append df_new
    if not os.path.exists(csv_file):
        df_new.to_csv(csv_file, index=False)
    else:
        df = pd.read_csv(csv_file)
        df = df.append(df_new)
        df.to_csv(csv_file, index=False)

def bulk_elements_table(formula, db_file=None, database='tribchem', 
                        collection='PBE.bulk_elements'):
    """
    Read data saved for elemental bulks and save a table containing data.

    """

    # Load all the elements and the corresponding mids
    all_elements = pd.read_csv(currentdir + '/../../physics/base/ptable.csv', sep=' ', index_col=False)
    with open(currentdir + '/../database/mid/elements.json', 'r') as midf:
        all_mids = json.load(midf)

    # Start the navigator, to read one by one each bulk
    nav = Navigator(db_file=db_file, high_level=database)

    # Define empty lists to be saved
    fs = []
    z = []
    group = []
    csystem = []
    mids = []
    alat = []
    encut = []
    kpoints = []
    bandgap = []
    cohesion = []
    bm = []
    is_gap_direct = []

    for el in formula:
        
        # Read the data from the Database, pass the iteration if missing
        field = nav.find_data(collection=collection, fltr={'formula': el})
        
        if field is not None:
            # Save mid and lattice parameter
            mids.append(field['mid'])
            opt_struct = field['structure'].get('opt', None)

            # Get the K-points density
            kd = field['comp_params'].get('kdens', None)
            if kd is not None:
                v = VaspInputSet(Structure.from_dict(field['structure']['opt']), 
                                 field['comp_params'],
                                 calc_type='bulk_from_scratch').get_vasp_settings()
                kd = np.round(kd, 2)
                kgr = str(np.array(v.kpoints.as_dict()['kpoints'][0])).strip('[').strip(']')
                kgr = kgr + ' (' + str(kd) + ')'
            else:
                kgr = None

            # Save general info on the element
            fs.append(el)
            z.append(all_elements[all_elements['element'] == el].iloc[0]['Z'])
            for d in all_mids:
                if d['element'] == el:
                    group.append(d['space_group'])
                    if d['space_group'] == 'Fm-3m' or d['space_group'] == 'Fd-3m':
                        d['crystal_system'] = 'fcc'
                        if opt_struct is not None:
                            al = opt_struct['lattice']['a'] * np.sqrt(2)
                            al = np.round(al, 3)
                        else:
                            al = '/'
                        # Rescale the k-points accordingly to the lattice parameter
                        if kd and al != '/':
                            kgr = [int(np.round(i, 0)) for i in [al*kd]*3]
                            kgr = str(kgr).strip('[').strip(']') + ' (' + str(kd) + ')'
                        else:
                            kgr = None

                    elif d['space_group'] == 'Im-3m':
                        d['crystal_system'] = 'bcc'
                        if opt_struct is not None:
                            al = opt_struct['lattice']['a'] * 2 / np.sqrt(3)
                            al = np.round(al, 3)
                        else:
                            al = '/'
                        # Rescale the k-points accordingly to the lattice parameter
                        if kd and al != '/':
                            kgr = [int(np.round(i, 0)) for i in [al*kd]*3]
                            kgr = str(kgr).strip('[').strip(']') + ' (' + str(kd) + ')'
                        else:
                            kgr = None

                    elif d['space_group'] == 'P6_3/mmc':
                        d['crystal_system'] = 'hexagonal'
                        if opt_struct is not None:
                            al_a = np.round(opt_struct['lattice']['a'], 3)
                            al_c = np.round(opt_struct['lattice']['c'], 3)
                            al = str(al_a) + ', ' + str(al_c)
                        else:
                            al = '/'
                   
                    csystem.append(d['crystal_system'])
                    alat.append(al)

            # Add K-points data
            kpoints.append(kgr)
            
            # Save kpoints and encut
            en = field['comp_params'].get('encut', None)
            # if en is not None:
            #     en /= PhysConstants.ry_to_eV
            encut.append(np.round(en, 3) if en is not None else en)

            # Read physical data
            if 'data' in field.keys():
                bg = field['data'].get('bandgap', '/')
                if bg != '/':
                    bg = np.round(bg, 3)
                bandgap.append(bg)
                cohesion.append(field['data'].get('cohesion_energy', '/'))
                bm.append(np.round(field['data'].get('bulk_modulus', '/'), 2))
            else:
                bandgap.append('/')
                cohesion.append('/')
                bm.append('/')
            if bandgap[-1] == 0:
                is_gap_direct.append('/')
            else:
                if 'data' in field.keys():
                    is_gap_direct.append(field['data'].get('is_gap_direct', '/'))
                else:
                    is_gap_direct.append('/')

    # Define the pandas table
    cols = ['Z', 'Element', 'Material ID', 'Space Group', 'Crystal System', 
            'Lattice constant (Å)', 'Bulk Modulus (GPa)', 'Cohesion (eV)', 
            'Bandgap (eV)', 'Direct Gap', 'Ecut (Ry)', 'K-points']
    all_columns = [z, fs, mids, group, csystem, alat, bm, cohesion, 
                   bandgap, is_gap_direct, encut, kpoints]

    data = [[d[i] for d in all_columns] for i in range(len(z))]

    table = pd.DataFrame(data, columns=cols)
    table.sort_values(by='Z', ascending=True, inplace=True, ignore_index=True)

    return table

def surface_energy(db_file=None, database='tribchem', 
                   collection='PBE.slab_elements', save=None):
    """
    Retrive the surface energy data for all the available slabs in the database.
    
    Parameters
    ----------
    db_file : str, optional
        Location of the database configuration file.
        
    database : str, optional
        Name of the database.
        
    collection : str, optional
        Name of the collection.
        
    save : str, optional
        Path where to save the plots and the POSCAR of the delta surface energy 
        vs the thickness.

    Return
    ------
    table : pandas dataframe
        Table containing the retrived data.
        E.g.:
        | Element | Miller indices | Optimal thickness | Surface energy (eV) | delta E (%) |
        | Cu      | 111            | 12                | 0.4222              | -4.4822     |
    
    """    

    nav = Navigator(db_file=db_file, high_level=database)
    
    field = nav.find_many_data(collection=collection, fltr={})

    elements = []
    miller_ind =[]
    opt_thk = []
    surfene_ev = []
    surfene_jm2 = []
    delta_e = []
    delta_e_all = []

    for el in field:
        element = el['formula']
        # In case the optimal thickess has not been found yet skip the element
        if 'data' not in el.keys():
            continue
        opt = el['data']['opt_thickness']
        miller = ''.join([str(i) for i in el['miller']])

        elements.append(element)
        miller_ind.append(miller)
        opt_thk.append(opt)
        data_name = 'data_' + str(el['data']['opt_thickness'])
        surfene_ev.append(el['thickness'][data_name]['output']['surface_energy_ev'])
        surfene_jm2.append(el['thickness'][data_name]['output']['surface_energy_jm2'])
        
        if save:
            if not os.path.isdir(save):
                raise ValueError("The path {} is not a valid path where to save the plots!".format(save))

            if not os.path.isdir(save+'/'+element):
                os.mkdir(save+'/'+element)

            if not os.path.isdir(save+'/'+element+'/'+miller+'/'):
                os.mkdir(save+'/'+element+'/'+miller+'/')
            sur_path = save+'/'+element+'/'+miller+'/'

        try:
            thk = el['thickness']
            thk_list = []
            e_list = []
            for t, v in thk.items():
                # If the optimal structure has not been found yet skip it
                if 'output' not in v.keys():
                    continue
                if t == 'data_0':
                    if save:
                        save_structure(
                            v['output']['structure'],
                            fmt='poscar',
                            name=element+'_'+miller+'_'+t.split('_')[1]+'_POSCAR',
                            path=sur_path)
                    continue
                if save:
                    save_structure(
                        v['output']['structure'],
                        fmt='poscar',
                        name=element+'_'+miller+'_'+t.split('_')[1]+'_POSCAR',
                        path=sur_path)
                thk_list.append(int(t.split('_')[1]))
                e_list.append(v['output']['surface_energy_ev'])
        except:
            pass
            
        # Get the dictionary (thickness, surface energy)
        res = dict(sorted(zip(thk_list, e_list)))

        # Get the surface energies and names
        max_thk = max(res, key=int)
        min_thk = min(res, key=int)
        max_e = res[max_thk]
        min_e = res[min_thk]
        delta_e.append((min_e-max_e)/min_e*100)
        e_list = list(res.values())
        delta_e_elem = [(e_list[i]-max_e)/e_list[i]*100 for i in range(0, len(e_list))]
        delta_e_all.append(delta_e_elem)
        
        if save:

            fig_sur = plt.figure(figsize=(6, 5))
            plt.plot(res.keys(), delta_e, '--o', markersize=4)
            plt.xlabel('Thickness')
            plt.ylabel('$\Delta$ E (%)')
            plt.xticks(list(res.keys()))
            for i, txt in enumerate(res.values()):
                plt.annotate(str(round(txt, 4))+' eV', (list(res.keys())[i], delta_e[i]+0.1))
            plt.title('Surface energy '+element+' '+miller+', optimal thickness: '+str(opt))
            fig_sur.savefig(sur_path+'/'+element+'_'+miller+'.pdf')
            fig_sur.savefig(sur_path+'/'+element+'_'+miller+'.png', dpi=fig_sur.dpi)

            plt.clf()
            
    cols_name = ['Element', 'Miller indices', 'Optimal thickness', 
                 'Surface energy (eVm2)', 'Surface energy (Jm2)', 'delta E (%)']
    cols = [elements, miller_ind, opt_thk, surfene_ev, surfene_jm2, delta_e]
    
    data = [[d[i] for d in cols] for i in range(len(elements))]
    table = pd.DataFrame(data, columns=cols_name)

    return table   

def save_structure(structure, name, path, fmt='poscar'):
    """
    Save a structure .
    
    Inputs
    ------
    structure : dict
        Dictionary of the structure.
    
    name : str
        Name of the file.

    path : str
        Path where to save the structure.

    fmt : str, default poscar
        Format of the file where to save the structure. 
        It might be: "cif", "poscar", "cssr", "json".
        For more details check the documentation of Structure class at 
        /pymatgen/core/structure.py.

    """
    
    poscar = Structure.from_dict(structure)
    poscar.to(fmt=fmt, filename=path+name)
    
def save_interface_structure(path, db_file=None, database='tribchem', 
                             collection='PBE.interface_elements', adh='regular',
                             save='init'):
    """
    Save the structure of an interface in a POSCAR file.
    
    Parameters
    ----------
    path : str
        Main path where to save the structures.
        The final structure will be: ../path/structure_1/POSCAR_file.
        
    db_file : str, optional
        Location of the database.
    
    database : str, optional
        Name of the database where to look at for the data.
    
    collection : str, optional
        Collection where the data are saved.
    
    adh : str, optional
        Adhesion type. It can be either \'regular\' or \'short\'.

    save : str, optional
        Set \'init\' to save the initial structure, \'opt\' to save the relaxed
        structure. 

    """
    
    # Check if the path exists
    if not os.path.isdir(path):
        raise ValueError("The path {} is not a valid path where to save the plots!".format(path))
    
    if save not in ['init', 'opt']:
        raise ValueError("{} is not an available option. Select either "
                         "\'init\' or \'opt\'.".format(save))
    
    if adh not in ['regular', 'short']:
        raise ValueError("{} is not an available option. Select either "
                         "\'regular\' or \'short\'.".format(adh))

    # Get all the structures from the database
    nav = Navigator(db_file=db_file, high_level=database)
    interfaces = nav.find_many_data(collection=collection, fltr={})
    
    if save == 'init':
        save_inint_interfaces(interfaces, path)
    elif save == 'opt':
        save_opt_interfaces(interfaces, path, adh)
    
        
def save_inint_interfaces(interfaces, path):
    """
    Save the initial structure of all the interfaces.
    
    Parameters
    ----------
    interfaces : Navigator
        Navigator object.
    
    path : str
        Main path where to save the structures.
        The final structure will be: ../path/structure_1/POSCAR_file.
    
    Return
    ------
    None.

    """
    
    for interface in interfaces:
        
        inter_name = interface['formula']
        miller = interface['miller']
        miller_name = ''.join([str(i) for i in miller[0]])
        miller_name += '_' + ''.join([str(i) for i in miller[1]])
        
        if not os.path.isdir(path+'/'+inter_name+'/'+miller_name+'/bot_slab'):
            os.makedirs(path+'/'+inter_name+'/'+miller_name+'/bot_slab')
            path_bot = path+'/'+inter_name+'/'+miller_name+'/bot_slab/'
        
        save_structure(structure=interface['structure']['init']['bot_slab'],
                       name='POSCAR', path=path_bot, fmt='poscar')
        
        if not os.path.isdir(path+'/'+inter_name+'/'+miller_name+'/top_slab'):
            os.makedirs(path+'/'+inter_name+'/'+miller_name+'/top_slab')
            path_top = path+'/'+inter_name+'/'+miller_name+'/top_slab/'
        
        save_structure(structure=interface['structure']['init']['top_slab'],
                       name='POSCAR', path=path_top, fmt='poscar')
        
        if not os.path.isdir(path+'/'+inter_name+'/'+miller_name+'/interface'):
            os.makedirs(path+'/'+inter_name+'/'+miller_name+'/interface')
            path_inter = path+'/'+inter_name+'/'+miller_name+'/interface/'
        
        save_structure(structure=interface['structure']['init']['interface'],
                       name='POSCAR', path=path_inter, fmt='poscar')

def save_opt_interfaces(interfaces, path, adh):
    """
    Save the initial structure of all the interfaces.
    
    Parameters
    ----------
    interfaces : Navigator
        Navigator object.
    
    path : str
        Main path where to save the structures.
        The final structure will be: ../path/structure_1/POSCAR_file.
    
    adh : str, optional
        Adhesion type. It can be either \'regular\' or \'short\'.
    
    Return
    ------
    None.

    """
    
    for interface in interfaces:
        
        if 'opt' not in interface['structure'].keys():
            continue
        
        if adh == 'regular' and 'regular' not in interface['structure']['opt'].keys():
            continue
        
        if adh == 'short' and 'short' not in interface['structure']['opt'].keys():
            continue
    
        inter_name = interface['formula']
        miller = interface['miller']
        miller_name = ''.join([str(i) for i in miller[0]])
        miller_name += '_' + ''.join([str(i) for i in miller[1]])
        
        if not os.path.isdir(path+'/'+inter_name+'/'+miller_name+'/bot_slab'):
            os.makedirs(path+'/'+inter_name+'/'+miller_name+'/bot_slab')
            path_bot = path+'/'+inter_name+'/'+miller_name+'/bot_slab/'
        
        save_structure(structure=interface['structure']['opt'][adh]['bot_slab'],
                       name='POSCAR', path=path_bot, fmt='poscar')
        
        if not os.path.isdir(path+'/'+inter_name+'/'+miller_name+'/top_slab'):
            os.makedirs(path+'/'+inter_name+'/'+miller_name+'/top_slab')
            path_top = path+'/'+inter_name+'/'+miller_name+'/top_slab/'
        
        save_structure(structure=interface['structure']['opt'][adh]['top_slab'],
                       name='POSCAR', path=path_top, fmt='poscar')
        
        if not os.path.isdir(path+'/'+inter_name+'/'+miller_name+'/interface_max'):
            os.makedirs(path+'/'+inter_name+'/'+miller_name+'/interface_max')
            path_inter = path+'/'+inter_name+'/'+miller_name+'/interface_max/'
        
        save_structure(structure=interface['structure']['opt'][adh]['interface_max'],
                       name='POSCAR', path=path_inter, fmt='poscar')
        
        if not os.path.isdir(path+'/'+inter_name+'/'+miller_name+'/interface_min'):
            os.makedirs(path+'/'+inter_name+'/'+miller_name+'/interface_min')
            path_inter = path+'/'+inter_name+'/'+miller_name+'/interface_min/'
        
        save_structure(structure=interface['structure']['opt'][adh]['interface_min'],
                       name='POSCAR', path=path_inter, fmt='poscar')
