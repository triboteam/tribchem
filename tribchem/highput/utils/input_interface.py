#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27th 15:38:00 2021

Utility to generate the input files necessary to calculate a PES and the 
adhesion energy with VASP.

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna
 
"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2022, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'January 27th, 2022'

from tribchem.highput.utils.vasp_tools import VaspInputSet

def save_input(structure, p, calc_type, save):
    """
    Save the inputs of VASP.
    
    Parameters
    ----------
    structure : pymatgen
        Structure.
        
    p : dict
        Dictionary containing all the parameters of the slabs and interface.
    
    calc_type : str
        Calculation type.
    
    save : str
        Location where to save the results.
        
    Return
    ------
    None

    """
    
    vis = VaspInputSet(structure, p['comp_params'], calc_type).get_vasp_settings()
    str_dic = vis.as_dict()
    vis.from_dict(str_dic)
    vis.write_input(save)