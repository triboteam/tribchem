#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 16:41:03 2021

Workflows useful to insert materials from different sources to Database.

@author: glosi000
"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Prof. M.C. Righi, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'January 20th, 2021'


import os

from fireworks import Workflow, Firework
from pymatgen.io.vasp.inputs import Poscar
from pymatgen.core.structure import Structure

from tribchem.highput.firetasks.dbtask import FT_MaterialInDB
from tribchem.highput.database.navigator import NavigatorMP
from tribchem.highput.utils.errors import InitError


class InitWF:
    """
    This is a collection of static methods that check the input parameters for 
    a Workflow and check their completeness, adding default values when needed.

    """

    @staticmethod
    def initialize_from_mp(mid, collection, entry='init_structure', 
                           db_file=None, database='tribchem', name=None, 
                           miller=None, comp_params={}, update_op='$set',
                           upsert=True, to_mongodb=True):

        nav_mp = NavigatorMP()
        structure = []
        formula = []
        for m in mid:
            s = nav_mp.get_structure_mpid(m)
            structure.append(s)
            formula.append(s.composition.reduced_formula)

        wf = InitWF.initialize_materials(structure, mid, formula, collection,
                                         entry, db_file, database, name, miller,
                                         comp_params, update_op, upsert, to_mongodb)

        return wf

    @staticmethod
    def initialize_from_local(poscars, mid, collection, entry='init_structure', 
                              db_file=None, database='tribchem', name=None, 
                              miller=None, comp_params={}, update_op='$set',
                              upsert=True, to_mongodb=True):
        
        if not isinstance(poscars, list):
            poscars = [poscars]

        structure = []
        formula = []
        for p in poscars:
            s = p.structure
            structure.append(s)
            formula.append(s.formula)

        wf = InitWF.initialize_materials(structure, mid, formula, collection,
                                         entry, db_file, database, name, miller,
                                         comp_params, update_op, upsert, to_mongodb)

        return wf

    @staticmethod
    def initialize_materials(structure, mid, formula, collection, entry,
                             db_file=None, database='tribchem', name=None, 
                             miller=None, comp_params={}, update_op='$set',
                             upsert=True, to_mongodb=True):

        res = check_init_args(structure, mid, formula, name, miller, comp_params)
        structure, mid, formula, name, miller, comp_params = res

        fts = []
        for i in range(len(structure)):
            ft = FT_MaterialInDB(structure=structure[i], mid=mid[i],
                db_file=db_file, database=database, collection=collection,
                entry=entry, formula=formula[i], name=name[i], miller=miller[i],
                comp_params=comp_params[i], update_op=update_op, upsert=upsert,
                to_mongodb=to_mongodb)
            fw = fts.append
            fts.append(ft)
        
        fw = Firework(fts, name='Store {} materials in {}, {}'
            .format(len(mid), database, collection))

        wf = Workflow([fw], name='Initialize Materials WF')

        return wf

def check_init_args(structure, mid, formula, name, miller, comp_params):

    # Convert input arguments to lists
    if not isinstance(structure, list):
        structure = [structure]
    if not isinstance(mid, list):
        mid = [mid]
    if not isinstance(formula, list):
        formula = [formula]

    if name is None:
        name = [name] * len(mid)
    elif not isinstance(name, list):
        name = [name]

    if miller is None:
        miller = [miller] * len(mid)
    else:
        miller = list(miller)
        if len(miller) == 3:
            miller = [miller] * len(mid)

    if comp_params == {}:
        comp_params = [comp_params] * len(mid)
    if not isinstance(comp_params, list):
        comp_params = [comp_params]

    # More mids need might lists with the same lengths as input arguments
    arg = None
    if len(structure) != len(mid) : arg = ('structure', structure)
    elif len(formula) != len(mid) : arg = ('formula', formula)
    elif len(name) != len(mid): arg = ('name', name)
    elif len(miller) != len(mid) : arg = ('miller', miller)
    elif len(comp_params) != len(mid) : arg = ('comp_params', comp_params)
    if arg is not None:
        raise InitError('Error in defining the input arguments, mid length '
                        'is {}, {} length is {}'.format(len(mid), len(arg[0]),
                                                        len(arg[1])))        
        
    return structure, mid, formula, name, miller, comp_params


#     @staticmethod
#     def checkinp_hetero_interface(material_1, material_2, computational,
#                                   interface, fw_name='Check input parameters'):        
#         """
#         Create a Fireworks to check if the necessary input for a heterogenous
#         workflow are given and assignes default values to optional parameters. 
#         Input parameters are checked for correct type and location in the spec.
        
#         Parameters
#         ----------
#         material_1 : list of str
#             Keys list in fw_spec pointing to the input dict of first material.
#         material_2 : list of str
#             Keys list in fw_spec pointing to the input dict of second material.
#         computational : list of str
#             Keys list in fw_spec pointing to the computational parameters.
#         interface : list of str
#             Keys list in fw_spec pointing to the interface parameters, which
#             are needed for interface matching.
#         fw_name : str
#             Name of the returned FireWork

#         Returns
#         -------
#         FW : fireworks.core.firework.Firework
#             Firework that checks all input parameters for an heterogeneous WF.
            
#         """

#         # Firetasks checking the materials parameters
#         ft_mat1 = FTCheckInput(input_dict = material_1, read_key = 'material',
#                                output_dict_name = 'mat_1')
#         ft_mat2 = FTCheckInput(input_dict = material_2, read_key = 'material',
#                                output_dict_name = 'mat_2')
        
#         # Firetask checking the computational parameters
#         ft_computation = FTCheckInput(input_dict = computational, 
#                                       read_key = 'computational',
#                                       output_dict_name = 'comp')
        
#         # Firetask checking the interfacial matching parameters
#         ft_interface = FTCheckInput(input_dict = interface,
#                                     read_key = 'interface',
#                                     output_dict_name = 'inter')
        
#         # Put materials bulk and slab in DB
#         ft_mat1_db = FTPutMaterialInDB(mat = 'mat_1', comp_params = 'comp')
#         ft_mat2_db = FTPutMaterialInDB(mat = 'mat_2', comp_params = 'comp')
        
#         # Put the parameters to build the interface in DB
#         ft_interface_db = FTPutInterfaceInDB(mat_1 = 'mat_1', mat_2 = 'mat_2',
#                                              comp_params = 'comp',
#                                              inter_params = 'inter')
        
#         fw = Firework([ft_mat1, ft_mat2, ft_computation, ft_interface, 
#                        ft_mat1_db, ft_mat2_db, ft_interface_db], name = fw_name)
        
#         return fw

#     @staticmethod
#     def checkinp_homo_interface(material, computational, interface, 
#                                 fw_name='Check input parameters'):
#         """
#         Create a Fireworks to check if the necessary input for a homogeneous
#         workflow are given and assignes default values to optional parameters. 
#         Input parameters are checked for correct type and location in the spec.
        
#         Parameters
#         ----------
#         material : list of str
#             Keys list in fw_spec pointing to the input dict of the material.
#         computational : list of str
#             Keys list in fw_spec pointing to the computational parameters.
#         interface : list of str
#             Keys list in fw_spec pointing to the interface parameters, which
#             are needed for creating the interface.
#         fw_name : str
#             Name of the returned FireWork
    
#         Returns
#         -------
#         fw : fireworks.core.firework.Firework
#             Firework that checks all input parameters for an heterogeneous WF.
            
#         """
        
#         # Firetasks checking the material parameters
#         ft_mat = FTCheckInput(input_dict = material, read_key = 'material',
#                                output_dict_name = 'mat')
        
#         # Firetask checking the computational parameters
#         ft_computation = FTCheckInput(input_dict = computational, 
#                                       read_key = 'computational',
#                                       output_dict_name = 'comp')
        
#         # Firetask checking the interfacial matching parameters
#         ft_interface = FTCheckInput(input_dict = interface,
#                                     read_key = 'interface',
#                                     output_dict_name = 'inter')
        
#         # Put materials bulk and slab in DB
#         ft_mat_db = FTPutMaterialInDB(mat = 'mat', comp_params = 'comp')
        
#         # Put the parameters to build the interface in DB
#         ft_interface_db = FTPutInterfaceInDB(mat_1 = 'mat', mat_2 = 'mat',
#                                              comp_params = 'comp',
#                                              inter_params = 'inter')
        
#         fw = Firework([ft_mat, ft_computation, ft_interface, ft_mat_db, 
#                        ft_interface_db], name = fw_name)
        
#         return fw
