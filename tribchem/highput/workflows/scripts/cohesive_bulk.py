#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 26 11:11:40 2021

Script to calculate the cohesive energy of a bulk. This is done by doing a 
static simulation on the bulk, which also let you to store all its physical 
data to the high level database. You then subtract from this energy the
energy of the calculated isolated atoms.

@author: glosi000, omarchehaimi

"""

__author__ = 'Gabriele Losi, Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'May 26th, 2021'

import argparse

import numpy as np

from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.firetasks.run import FT_StartCohesionBulk
from tribchem.highput.database.mid import Mid
from tribchem.core.cli.cohesion_bulk_cli import CliCohesionBulk

def cohesive_bulk(args):
    """
    Generate the workflow.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    debug_mode = args.d
    override = args.o

    if args.mids == 'mids=all' and args.formulas == 'formulas=all':
        all_element = True
    else:
        all_element = False
        mids = np.array(CliCohesionBulk.get_list(args.mids))
        formulas = np.array(CliCohesionBulk.get_list(args.formulas))

    filter_name = args.fln
    fltr = args.fl
    half_kpts_first_relax = args.hk
    db_file = args.dbf
    database_bulk = args.dbb
    database_atoms = args.dba
    collection_bulk = args.cb
    collection_atoms = args.ca

    check_entry = CliCohesionBulk.get_list_opt(args.ce)
    entry_bulk = CliCohesionBulk.get_list_opt(args.eb)
    check_bulk = CliCohesionBulk.get_list_opt(args.chb)
    check_atoms = CliCohesionBulk.get_list_opt(args.cha)

    if all_element:
        # Set fltr to None to take all the available element in 
        # tribchem/database/mid/elements.json
        if fltr:
            mids, formulas = Mid.get_mid_chem(
                name='elements.json', fltr={filter_name: fltr}
            )
        else:
            mids, formulas = Mid.get_mid_chem(name='elements.json')

        mids = np.array(mids)
        formulas = np.array(formulas)

    CliCohesionBulk.check_mids_formulas(mids, formulas)

    name = [None] * len(mids)

    CliCohesionBulk.print_messsage(formulas)

    # Prepare the workflows
    wfs = []
    for i in range(len(mids)):
        fw = Firework(
            FT_StartCohesionBulk(
                mid=mids[i], collection_bulk=collection_bulk,
                collection_atoms=collection_atoms, formula=formulas[i], 
                name=name[i], db_file=db_file, database_bulk=database_bulk, 
                database_atoms=database_atoms, entry_bulk=entry_bulk, 
                check_bulk=check_bulk, check_atoms=check_atoms, 
                check_entry=check_entry, override=override, 
                half_kpts_first_relax=half_kpts_first_relax),
            name='Calculate Cohesion energy for {}'.format(mids[i]))

        wfs.append(Workflow([fw], name='Calculate Cohesion energy and bulk '
                            'properties for {}'.format(mids[i])))

    # Run the workflow
    lpad = LaunchPad.auto_load()
    for wf in wfs:
        lpad.add_wf(wf)

    if debug_mode:
        # DO NOT RUN THIS ON THE LOGIN NODE !!!
        rapidfire(lpad)
