#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 26 17:19:21 2021

Create a slab for a specific bulk, oriented along a given list of Miller indexes.
With this script you do the following steps:
    - Create the slab out of a bulk
    - Relax its structure and collect the physical results
    - Calculate the optimal number of atomic layers (thickness)
    - Calculate the surface energy

WARNING: The generation of the slabs and the convergence of their thickness are
guaranteed to work properly only for bulks with monoatomic basis.

@author: glosi000, omarchehaimi

"""

__author__ = 'Gabriele Losi, Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'May 26th, 2021'


from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.firetasks.run import FT_SlabOptThick
from tribchem.core.cli.converge_slab_cli import CliConvergeSlab

def converge_slab(args):
    """
    Generate the workflow.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    debug_mode = args.d
    override = args.o
    override_slab = args.os

    mids = CliConvergeSlab.get_list(args.mids)
    name_bulk = [None] * len(mids)
    name_slab = [None] * len(mids)
    formulas = CliConvergeSlab.get_list(args.formulas)
    miller = CliConvergeSlab.get_miller(args.miller)

    CliConvergeSlab.check_mids_formulas_miller(mids, formulas, miller)

    db_file = args.dbf
    database = args.db
    collection_bulk = args.cb
    collection_slab = args.cs
    check_entry = CliConvergeSlab.get_list_opt(args.ce)
    bulk_entry = CliConvergeSlab.get_list_opt(args.be)

    parallelization = args.p
    override_slab_db = args.os

    is_metal = CliConvergeSlab.get_atom_comp_par(args.im, len(mids))
    use_spin = CliConvergeSlab.get_atom_comp_par(args.us, len(mids))
    vdw = CliConvergeSlab.get_atom_comp_par(args.vdw, len(mids))

    conv_thr = args.ct

    thick_min = CliConvergeSlab.get_int(args.tmin)
    thick_max = CliConvergeSlab.get_int(args.tmax)

    thick_incr = args.ti
    vacuum = args.va
    in_unit_planes = args.up
    calc_type = args.cct

    ext_index = args.ei

    CliConvergeSlab.print_messsage(formulas, miller)

    # Prepare the workflows
    wfs = []
    for i in range(len(mids)):
        ft = FT_SlabOptThick(
            mid=mids[i], formula=formulas[i], name_bulk=name_bulk[i], 
            name_slab=name_slab[i], miller=miller[i], 
            collection_bulk=collection_bulk, collection_slab=collection_slab, 
            db_file=db_file, database=database, conv_kind='surfene', 
            calc_type=calc_type, thick_min=thick_min, thick_max=thick_max, 
            thick_incr=thick_incr, vacuum=vacuum, in_unit_planes=in_unit_planes, 
            ext_index=ext_index, conv_thr=conv_thr, 
            parallelization=parallelization, bulk_entry=bulk_entry, 
            check_entry=check_entry, override=override, 
            override_slab_db=override_slab_db, is_metal=is_metal[i], 
            use_spin=use_spin[i], vdw=vdw[i])

        fw = Firework(
            ft, name='Converge the slab thickness for {} '
                     '{}'.format(mids[i], tuple(miller[i])))
        wfs.append(
            Workflow([fw], name = 'Calculate the optimal slab thickness and '
                                  'surface energy for {} '
                                  '{}'.format(mids[i], tuple(miller[i]))))

    # Run the workflow
    lpad = LaunchPad.auto_load()
    for wf in wfs:
        lpad.add_wf(wf)

    if debug_mode:
        # DO NOT RUN THIS ON THE LOGIN NODE !!!
        rapidfire(lpad)