#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 9 13:52:00 2021

Start the workflows for calculating the adsorption of atoms on a surface. 

@author: omarchehaimi, glosi000
"""

__author__ = 'Gabriele Losi, Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'July 9th, 2021'


from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.firetasks.run import FT_StartAdsorbateSurface
from tribchem.core.cli.calc_adsorbate_cli import CliCalcAdsorbate


def calc_adsorbate(args):
    """
    Generate the workflow.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    debug_mode = args.d
    override = args.o
    run_vasp = CliCalcAdsorbate.set_run_vasp(args.rv)

    mid = CliCalcAdsorbate.get_list(args.mid)
    formula = CliCalcAdsorbate.get_list(args.formula)
    miller = CliCalcAdsorbate.get_miller(args.miller)[0]
    adsorbate = CliCalcAdsorbate.get_par(args.adsorbate)
    sites = CliCalcAdsorbate.get_par(args.sites)

    collection = args.c
    collection_slab = args.cs
    collection_atom = args.ca

    db_file = args.dbf
    database = args.db
    check_key = CliCalcAdsorbate.get_list_opt(args.ck)

    coverage = args.co

    calc_type = args.cct
    both_surfaces = args.bs

    # Pymatgen optional
    selective_dynamics = args.sd
    height = args.he
    translate = args.t
    reorient = args.r
    mi_vec = CliCalcAdsorbate.get_mv(args.mv)
    min_lw = args.ml
    find_args = CliCalcAdsorbate.get_fa(args.fa)
    a = args.a
    v = args.vv
    center = CliCalcAdsorbate.get_par_list_opt(args.cen)
    rotate_cell = args.rc
    pymatgen_opt = {
        "selective_dynamics":selective_dynamics,
        "height":height,
        "mi_vec":mi_vec,
        "min_lw":min_lw,
        "translate":translate,
        "reorient":reorient,
        "find_args":find_args,
        "a":a,
        "v":v,
        "center":center,
        "rotate_cell":rotate_cell,
    }

    name = [None] * len(mid)

    ft_ad = FT_StartAdsorbateSurface(
        mid=mid, miller=miller, adsorbate=adsorbate, collection=collection,
        collection_slab=collection_slab, collection_atom=collection_atom, 
        formula=formula, name=name, db_file=db_file, database=database,
        check_key=check_key, run_vasp=run_vasp, calc_type=calc_type,
        override=override, both_surfaces=both_surfaces, coverage=coverage,
        sites=sites, pymatgen_opt=pymatgen_opt)

    fw_ad = Firework([ft_ad], name="Adsorption workflow")
    wf = Workflow([fw_ad], name="Adsorption workflow")

    lpad = LaunchPad.auto_load()
    lpad.add_wf(wf)

    if debug_mode:
        # DO NOT RUN THIS ON THE LOGIN NODE !!!
        rapidfire(lpad)