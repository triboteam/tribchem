#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  7 13:30:13 2021

Script to run a convergence on the kinetic energy cutoff and the k-points grid
for a set of bulks.

Generate the ID for the list of materials that we are interested in.
Mid is mandatory, eventually you can also set a further identifier for the 
structure (a name), which may have the following notation: formula - name. 
Example: for diamond you can write: 'C - diamond' or just 'diamond'.

WARNING: Use always lowe case letters for variables and string names, except
where it cannot be avoided.

@author: glosi000, omarchehaimi

"""

__author__ = 'Gabriele Losi, Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'May 7th, 2021'


from logging import debug
import numpy as np

from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.firetasks.run import (
    FT_StartEncutConvo, 
    FT_StartKpointsConvo
)
from tribchem.highput.database.mid import Mid
from tribchem.core.cli.converge_bulk_cli import CliConvergeBulk

def converge_bulk(args):
    """
    Generate the workflow.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """
    
    if args.mids == 'mids=all' and args.formulas == 'formulas=all':
        all_element = True
    else:
        all_element = False
        mids = np.array(CliConvergeBulk.get_list(args.mids))
        formulas = np.array(CliConvergeBulk.get_list(args.formulas))

    debug_mode = args.d
    override = args.o

    filter_name = args.fln
    fltr = args.fl
    check_encut = CliConvergeBulk.get_list_opt(args.ce)
    encut_start = args.es
    encut_incr = float(args.ei)
    kdens_start = float(args.ks)
    kdens_incr = float(args.ki)
    n_converge = int(args.nc)

    db_file = args.dbf
    database = args.db
    functional = args.f
    collection = functional + '.bulk_elements'
    entry_encut = CliConvergeBulk.get_list_opt(args.ee)
    entry_kpts = CliConvergeBulk.get_list_opt(args.ek)
    update_op = args.uo
    upsert = args.up

    if all_element:
        # Set fltr to None to take all the available element in 
        # tribchem/database/mid/elements.json
        if fltr:
            mids, formulas = Mid.get_mid_chem(
                name='elements.json', fltr={filter_name: fltr}
            )
        else:
            mids, formulas = Mid.get_mid_chem(name='elements.json')
            print(mids)

        mids = np.array(mids)
        formulas = np.array(formulas)

    name = [None] * len(mids)
    flag = mids

    CliConvergeBulk.check_mids_formulas(mids, formulas)

    # Define the deformations, i.e. number of calculations, for the Murnaghan 
    # and BM
    deforms = []
    for i in np.arange(0.94, 1.06, 0.02):
        dm = np.eye(3) * i
        deforms.append(dm)

    CliConvergeBulk.print_messsage(formulas)

    # Prepare the workflows
    wfs = []
    for i in range(len(mids)):

        encut = Firework(
            FT_StartEncutConvo(
                mid=mids[i], collection=collection, formula=formulas[i], 
                name=name[i], flag=flag[i], db_file=db_file, low_level=None, 
                high_level=database, entry=entry_encut, check_encut=check_encut, 
                encut_start=encut_start, encut_incr=encut_incr, 
                n_converge=n_converge, override=override, deformations=deforms),
            name = 'Converge the cutoff energy for {}'.format(mids[i])
            )

        kpts = Firework(
            FT_StartKpointsConvo(
                mid=mids[i], flag=flag[i], db_file=db_file, 
                collection=collection, low_level=None, formula=formulas[i], 
                high_level=database, kdens_start=kdens_start, 
                kdens_incr=kdens_incr, n_converge=n_converge, override=override, 
                entry=entry_kpts),
            name = 'Converge the k-points for {}'.format(mids[i])
            )

        wfs.append(Workflow([encut, kpts], {encut: kpts}, 
                            name='Converge cutoff energy and k-points '
                                 'for {}'.format(mids[i])))

    # Run the workflow
    lpad = LaunchPad.auto_load()
    for wf in wfs:
        lpad.add_wf(wf)

    if debug_mode:
        # DO NOT RUN THIS ON THE LOGIN NODE !!!
        rapidfire(lpad)
