#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 21 13:52:00 2021

Start the workflows for calculating the high symmetry points, the PES, and the 
adhesion energy.

Parameters
----------
debug_mode : bool, default False
    If True the script will launch the workflow directly on the machine where
    the following script is executed. This is useful for doing rapid tests of 
    the workflows or to execute fast calculations without sumbitting them to the
    calculation nodes.

mid : list
    Material id of the two elements of the interface.
    
formula : list
    Formula of the two elements. The order must be consistent with the one of
    the mid.
    
miller : list
    Miller indexes of the two slabs creating the interface.
    
collection_slab : str
    Name of the collection where the data about the slabs are saved.
    
collection_inter : str
    Name of the collection where the data abaout the interface are saved.
    
use_spin : bool
    Set True if at least of one of the two slabs is a magnetic element.

operations : list
    Operations to perform for evaluating the charge displacement.

override : bool
    Set to True to repeat all the calculation even if they have been already done.

layers : list
    Number of layers for each slab.

inter_params : dict
    Dictionary containing the parameters for the interfaces. The available
    parameters are: 
        -max_area
        -interface_distance
        -max_angle_tol
        -max_length_tol
        -max_area_ratio_tol
        -best_match

calc_type_pes : str
    Calculation type for the PES. Refer to the class 'VaspInputSet' implemented 
    in the script 'highput/utils/vasp_tools.py' for a complete list of all 
    available type of calculations.

calc_type_adhesion : str
    Calculation type for the adhesion energy. Refer to the class 'VaspInputSet' 
    implemented in the script 'highput/utils/vasp_tools.py' for a complete list 
    of all available type of calculations.
    
check_entry_pes : str
    Field of the database where to check the data about the calculated PES.

adhesion_type : str
    Type of the adhesion. To speed up the calculations of the adhesion energy
    it is possible to set 'adhesion_type' to 'short'.
    
pes_type : str
    Type of the calculated PES. If the PES was obtained with a static
    calculation the name is pes_scf.

@author: glosi000, omarchehaimi

"""

__author__ = 'Gabriele Losi, Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'July 21st, 2021'

import argparse

from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.firetasks.run import (
    FT_StartInterfaceMatch,
    FT_StartPES, 
    FT_StartAdhesion, 
    FT_StartPPES, 
    FT_StartChargeDisplacement
    )
from tribchem.core.cli.calc_interface_cli import CliInterface
from tribchem.highput.workflows.wfcore import run_vasp_simulation

def calc_interface(args):
    """
    Generate the workflow.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    debug_mode = args.d
    override = args.o
    run_vasp = CliInterface.set_run_vasp(args.rv)

    wf = CliInterface.get_wfs(args.wf)
    wf = CliInterface.check_workflows(wf)
    mids = CliInterface.get_list(args.mids)
    formulas = CliInterface.get_list(args.formulas)
    miller = CliInterface.get_miller(args.miller)

    CliInterface.check_mids_formulas_miller(mids, formulas, miller)

    collection_slab = args.cs
    collection_inter = args.ci
    use_spin, inter_params = CliInterface.set_interface(
        args.spin, max_area=args.marea, interface_distance=args.idist, 
        max_angle_tol=args.max_angle_tol, max_length_tol=args.max_length_tol, max_area_ratio_tol=args.max_area_ratio_tol, 
        best_match=args.bm)

    shifts = CliInterface.get_list_opt(args.sh, t='float')

    operations = CliInterface.get_list_opt(args.op)
    calc_type_pes, check_entry_pes = CliInterface.set_pes(args.pt)
    calc_type_adhesion, adhesion_type, pes_type = CliInterface.set_adh(args.ad)
    calc_type_ppes, check_entry_ppes = CliInterface.set_ppes(args.ppt)
    check_entry_chg = args.chgtype
    site = args.s
    to_plot = args.tp
    layers = [3 if m==[1,1,1] else 2 for m in miller]

    CliInterface.print_messsage(wf, formulas, mids, miller)

    fw_interface = Firework(
        FT_StartInterfaceMatch(
            mid=mids, miller=miller, collection_slab=collection_slab,
            collection_inter=collection_inter, override=override, 
            inter_params=inter_params, use_spin=use_spin),
        name="Interface calculation for {}".format(mids[0]+'_'+mids[1])
    )

    fw_pes = Firework(
        FT_StartPES(mid=mids, formula=formulas, miller=miller, 
                    collection=collection_inter, layers=layers, 
                    override=override, calc_type=calc_type_pes, 
                    check_entry=check_entry_pes, run_vasp=run_vasp), 
        name="PES calculation for {}".format(mids[0]+'_'+mids[1])
    )

    fw_adh = Firework(
        FT_StartAdhesion(mid=mids, miller=miller, formula=formulas, 
                        collection=collection_inter, override=override, 
                        calc_type=calc_type_adhesion, 
                        adhesion_type=adhesion_type, pes_type=pes_type, 
                        run_vasp=run_vasp),
        name="Adhesion calculation for {}".format(mids[0]+'_'+mids[1])
    )

    fw_ppes = Firework(
        FT_StartPPES(mid=mids, miller=miller, collection=collection_inter, 
                     formula=formulas, override=override, 
                     calc_type=calc_type_ppes, check_entry=check_entry_ppes, 
                     run_vasp=run_vasp, shifts=shifts, site=site),
        name="PPES calculation for {}".format(mids[0]+'_'+mids[1])
    )

    fw_charge = Firework(
        FT_StartChargeDisplacement(mid=mids, miller=miller, 
                                   collection=collection_inter, 
                                   operations=operations, site=site,
                                   formula=formulas, to_plot=to_plot, 
                                   check_entry=check_entry_chg, 
                                   override=override, run_vasp=run_vasp),
        name="Charge displacement calculation for {}".format(mids[0]+'_'+mids[1])
    )

    wf_list, wf_dict = CliInterface.get_wf_variables(
        wf, fw_interface=fw_interface, 
        fw_pes=fw_pes, fw_adh=fw_adh,
        fw_ppes=fw_ppes, fw_charge=fw_charge
    )

    # E.g.: wf = Workflow([fw_pes, fw_adh], links_dict={fw_pes: fw_adh}, name='Run Workflow') 
    wf = Workflow(wf_list, links_dict=wf_dict, name='Run Workflow')

    lpad = LaunchPad.auto_load()
    lpad.add_wf(wf)

    if debug_mode:
        # DO NOT RUN THIS ON THE LOGIN NODE !!!
        rapidfire(lpad)
