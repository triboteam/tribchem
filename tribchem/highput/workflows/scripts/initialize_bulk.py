#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  7 11:13:03 2021

Script to store bulk structure from the Materials Project (https://materialsproject.org/) 
database to the local one.

Generate the ID for the list of materials that we are interested in.
Mid is mandatory, eventually you can also set a further identifier for the 
structure (a name), which may have the following notation: formula - name. 
Example: for diamond you can write: 'C - diamond' or just 'diamond'.

Default values for functional is PBE, the thresholds for the energy, volume,
and bm are generally 0.025, 0.01, 0.01 and can be kept more or less to be
constant for all the elements. Functional should be the same for all the 
materials that you are uploading, because it is used to identify the collection.
use_vdw and use_spin should be set to True for materials with vdw interactions, 
e.g. 2D materials, and magnetic e.g. Fe...
Be careful to set correctly use_spin and is_metal, otherwise you run the
structure relaxations in a bad way.

@author: glosi000, omarchehaimi

"""

__author__ = 'Gabriele Losi, Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'May 7th, 2021'


import numpy as np

from fireworks import LaunchPad
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.workflows.initialize import InitWF
from tribchem.highput.database.mid import Mid
from tribchem.physics.base.materials import PeriodicTable
from tribchem.core.cli.initialize_bulk_cli import CliInitBulk

def initialize_bulk(args):
    """
    Generate the workflow.
    
    Parameters
    ----------
    args : argparse
        Arguments of the CLI.
        
    Return
    ------
    None.

    """

    if args.mids == 'mids=all' and args.formulas == 'formulas=all':
        all_element = True
    else:
        all_element = False
        mids = np.array(CliInitBulk.get_list(args.mids))
        formulas = np.array(CliInitBulk.get_list(args.formulas))

    debug_mode = args.d

    filter_name = args.fln
    fltr = args.fl

    if all_element:
        # Set fltr to None to take all the available element in 
        # tribchem/database/mid/elements.json
        if fltr:
            mids, formulas = Mid.get_mid_chem(
                name='elements.json', fltr={filter_name: fltr}
            )
        else:
            mids, formulas = Mid.get_mid_chem(name='elements.json')

        mids = np.array(mids)
        formulas = np.array(formulas)

    name = [None] * len(mids)

    CliInitBulk.check_mids_formulas(mids, formulas)

    func = args.f
    functional = [func] * len(mids)  # Only change func
    energy_tol = [0.005] * len(mids)
    volume_tol = [0.01] * len(mids)
    bm_tol = [0.01] * len(mids)
    use_vdw = [None] * len(mids)
    use_spin = [True] * len(mids)

    db_file = args.dbf
    database = args.db
    collection = func + '.bulk_elements'
    entry = CliInitBulk.get_list_opt(args.ce)

    update_op = args.uo
    upsert = args.up
    to_mongodb = args.tm

    miller = None # Only useful for slabs

    # Create a list of dicts
    comp_params = []
    for i in range(len(mids)):
        d = {
            'functional': functional[i],
            'energy_tol': energy_tol[i],
            'bm_tol': bm_tol[i],
            'volume_tol': volume_tol[i],
            'is_metal': True if formulas[i] in PeriodicTable.metals else False,
            'vdw': use_vdw[i],
            'use_spin': use_spin[i]
            }
        comp_params.append(d)

    # Check if all the length are coherent
    assert len(mids) == len(name)
    assert len(mids) == len(comp_params)

    init = InitWF.initialize_from_mp(
        mids, collection, entry, db_file, database, name, miller, comp_params, 
        update_op, upsert, to_mongodb)

    # Run the workflow
    lpad = LaunchPad.auto_load()
    lpad.add_wf(init)

    if debug_mode:
        # DO NOT RUN THIS ON THE LOGIN NODE !!!
        rapidfire(lpad)
