#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 24 14:24:46 2021

General functionalities for the workflows of the Tribchem package.

@author: glosi000

"""

from uuid import uuid4

from fireworks import Workflow, Firework

from tribchem.highput.database.navigator import NavigatorMP, use_legacy_matproj
from tribchem.highput.utils.io_files import copy_output_files
from tribchem.highput.firetasks.structure import FT_RunVaspOnTheFly, FT_RunVaspSimulation
from tribchem.highput.firetasks.dbtask import FT_MoveTagResults, FT_SaveVASPOutputs


class GeneralTribchemWF:
    """
    General class with common methods and attributes useful to create any
    kind of custom Workflow for TribChem calculations.
    
    """

    # Dictionary containing the data
    transfer_data_to = [['data', 'nsites'],
                        ['data', 'density'],
                        ['data', 'energy'],
                        ['data', 'energy_per_atom'],
                        ['data', 'bandgap'],
                        ['data', 'cbm'],
                        ['data', 'vbm'],
                        ['data', 'is_gap_direct'],
                        ['data', 'forces'],
                        ['data', 'stress'],
                        ['data', 'pseudo_potential'],
                        ['data', 'dir_name'],
                        ['data', 'task_id'],
                        ['data', 'task_label']]

    transfer_data_from = [['nsites'],
                          ['output', 'density'],
                          ['output', 'energy'],
                          ['output', 'energy_per_atom'],
                          ['output', 'bandgap'],
                          ['output', 'cbm'],
                          ['output', 'vbm'],
                          ['output', 'is_gap_direct'],
                          ['output', 'forces'],
                          ['output', 'stress'],
                          ['input', 'pseudo_potential'],
                          ['dir_name'],
                          ['task_id'],
                          ['task_label']]
    
    @staticmethod
    def _select_entry(kind, **kwargs):
        """
        Generate entry_from and entry_to, in order to use the run_and_save
        workflow. With kind you can specify the kind of calculations, allowed
        values at the moment are: pes, adhesion, ppes. 
        
        Specific arguments for each calculation type are passed as **kwargs.
            - kind = 'pes' : `shifts`
            - kind = 'adhesion' : `labels`, `adhesion_type`
            - kind = 'ppes' : `shifts`, `site`
            - kind = 'chgdisp' : `labels`, `site`
            - kind = 'chemabs' : `sites`, `coverage`
            - kind = 'chemabs-slab' : `repeat`

        """
        
        allowed = ['pes', 'adhesion', 'ppes', 'chgdisp', 'chemabs', 'chemabs-slab']
        if not kind in allowed:
            raise ValueError('Wrong input argument: kind. Provided: {}. '
                             'Allowed: {}'.format(kind, allowed))

        entry_from = []
        entry_to = []

        # PES CASE
        if kind == 'pes':
            for s in kwargs['shifts']:
                tmp = GeneralTribchemWF.transfer_data_to.copy()
                en_t = [['pes', 'data', str(s), 'output', d[-1]] for d in tmp]
                en_t += [['pes', 'data', str(s), 'output', 'structure'],
                         ['pes', 'data', str(s), 'input', 'structure']]
                
                en_f = GeneralTribchemWF.transfer_data_from.copy()
                en_f += [['output', 'structure'], ['input', 'structure']]
                
                entry_from.append(en_f)
                entry_to.append(en_t)

        # ADHESION CASE
        elif kind == 'adhesion':
            adt = kwargs['adhesion_type']
            for l in kwargs['labels']:
                tmp = GeneralTribchemWF.transfer_data_to.copy()
                en_t = [['data', str(l), d[-1]] if adt == 'regular' else ['data', adt, str(l), d[-1]] for d in tmp]
                en_t += [['structure', 'opt', adt, str(l)]]

                en_f = GeneralTribchemWF.transfer_data_from.copy()
                en_f += [['output', 'structure']]

                entry_from.append(en_f)
                entry_to.append(en_t)

        # PPES CASE
        elif kind == 'ppes':
            site = kwargs['site']
            ppt = kwargs['check_entry']
            for s in kwargs['shifts']:
                tmp = GeneralTribchemWF.transfer_data_to.copy()
                ts = str(s).replace('.', ',')
                en_t = [['ppes', ppt, 'data', site, 'shift_'+ts, 'output', d[-1]] for d in tmp]
                en_t += [['ppes', ppt, 'data', site, 'shift_'+ts, 'output', 'structure'],
                         ['ppes', ppt, 'data', site, 'shift_'+ts, 'input', 'structure']]
    
                en_f = GeneralTribchemWF.transfer_data_from.copy()
                en_f += [['output', 'structure'], ['input', 'structure']]
    
                entry_from.append(en_f)
                entry_to.append(en_t)

        # CHARGE DISPLACEMENT CASE
        elif kind == 'chgdisp':
            site = kwargs['site']
            chgtype = kwargs['check_entry']
            for l in kwargs['labels']:
                tmp = GeneralTribchemWF.transfer_data_to.copy()
                en_t = [['charge', chgtype, 'data', site, str(l), d[-1]] for d in tmp]
                en_t += [['charge', chgtype, 'data', site, str(l), 'structure']]

                en_f = GeneralTribchemWF.transfer_data_from.copy()
                en_f += [['output', 'structure']]

                entry_from.append(en_f)
                entry_to.append(en_t)

        # CHEMISORPTION OF ADATOM ON A SURFACE CASE
        elif kind == 'chemabs':
            cvr = str(float(kwargs['coverage'])).replace('.', ',')
            for s in kwargs['sites']:
                tmp = GeneralTribchemWF.transfer_data_to.copy()
                en_t = [['data', 'coverage_'+cvr, s, d[-1]] for d in tmp]
                en_t += [['structure', 'coverage_'+cvr, s, 'opt']]

                en_f = GeneralTribchemWF.transfer_data_from.copy()
                en_f += [['output', 'structure']]

                entry_from.append(en_f)
                entry_to.append(en_t)
        
        # CHEMISORPTION CASE FOR CALCULATING A CLEAN SLAB REPLICATED
        elif kind == 'chemabs-slab':
            for r in kwargs['repeat']:
                str_r = 'replica_{}_{}_{}'.format(str(r[0]), str(r[1]), str(r[2]))

                tmp = GeneralTribchemWF.transfer_data_to.copy()
                en_t = [['data', str_r, 'output', d[-1]] for d in tmp]
                en_t += [['data', str_r, 'input', 'structure'],
                         ['data', str_r, 'output', 'structure']]

                en_f = GeneralTribchemWF.transfer_data_from.copy()
                en_f += [['input', 'structure'], ['output', 'structure']]

                entry_from.append(en_f)
                entry_to.append(en_t)

        return entry_from, entry_to

    @staticmethod
    def check_mid(structure, mid):
        """
        Check if the chemical formula passed is the same on MP database.

        """
        
        if mid.startswith('mp-') and mid[3:].isdigit():
            nav_mp = NavigatorMP()
            formula_from_struct = structure.composition.reduced_formula
            if use_legacy_matproj():
                formula_from_flag = nav_mp.get_property_from_mp(mid, ['pretty_formula'])
                formula_from_flag = formula_from_flag['pretty_formula']
            else:
                formula_from_flag = nav_mp.get_property_from_mp(mid, ['formula_pretty']).formula_pretty
            if not formula_from_flag == formula_from_struct:
                raise SystemExit('The chemical formula of your structure ({}) '
                                 'does not match the chemical formula of the flag '
                                 '(mid) you have chosen, which corresponds '
                                 'to {}.\n'.format(
                                 formula_from_struct, formula_from_flag))

    @staticmethod
    def check_comp_params(comp_params):
        """
        Check computational parameters and print a message to stdout.

        """

        # Check computational parameters and use defaults if necessary
        if comp_params in [{}, None]:
            print('\nNo computational parameters is defined, defaults are used\n'
                'It is reccomended to pass a comp_params dictionary like:\n'
                '   {"use_vdw": <True/False>,\n'
                '    "use_spin": <True/False>,\n'
                '    "is_metal": <True/False>,\n'
                '    "encut": <int>,\n'
                '    "kdens": <int>}\n')

    @staticmethod
    def print_help(cluster_params):
        """
        Check if the print help option of the cluster parameters is True.

        """

        if cluster_params['print_help']:
            print('Results can be accessed in a simple way, via:\n\n'
                  'import pprint\n'
                  'from tribchem.highput.database.navigator import Navigator\n\n'
                  'nav = Navigator(db_file=<YourDBFile>)\n'
                  'data = nav.find_data(collection=<YourCollection>, '
                  'fltr=dict("mid": <YourMid>, "miller": <YourMiller>))\n'
                  'pprint.pprint(data)\n')

    @staticmethod
    def make_local_copy(output_files, cluster_params):
        """
        Make a local copy of the generated output files.

        """        
        ft = None
        if cluster_params['file_output']: 
            ft = copy_output_files(file_list=output_files,
                                   output_dir=cluster_params['output_dir'],
                                   remote_copy=cluster_params['remote_copy'],
                                   server=cluster_params['server'],
                                   user=cluster_params['server'],
                                   port=cluster_params['port'])       

        return ft

def run_and_save(mid, structure, vis, tags, calc_type, collection, entry_from,
                 entry_to, db_file=None, database='tribchem', formula=None,
                 name=None, miller=None, adsorbate=None, fw_names=None, to_wf=False):
    """
    Run a calculation by calling vasp on the fly on different structures and
    then store the results in a database. It can either return a tuple of
    Fireworks objects, that can be used in other workflows, or a complete workflow
    that can be run.

    """
    
    if fw_names is not None:
        try:
            assert isinstance(fw_names, list)
            assert len(fw_names) == 2
            assert all([isinstance(n, str) for n in fw_names])
        except:
            raise ValueError('Wrong input argument: fw_names. It should be a list of '
                             'two strings or None')
    else:
        fw_names = [None, None]
    
    try:
        if isinstance(entry_to, list): 
            assert isinstance(entry_from, list)
            assert len(entry_to) == len(entry_from)
        if isinstance(structure, list): 
            assert len(structure) == len(entry_to)
    except:
        raise ValueError('Wrong input argument: entry_to, entry_from. '
                         'WARNING: Although you might have the same entries for '
                         'all the structures that are run, you should provide a '
                         'list of entries, one for each structure.')

    # Run the simulations on the fly
    ft_1 = FT_RunVaspOnTheFly(structure=structure, vis=vis, tag=tags,
                              calc_type=calc_type, wf_name=fw_names[0])
    f1n = fw_names[0] + ' - fw' if fw_names[0] is not None else 'Run Vasp on the fly - fw'
    fw_1 = Firework([ft_1], name=f1n)
    
    # Check for type consistency
    if not isinstance(entry_to, list): entry_to = [entry_to]
    if not isinstance(entry_from, list): entry_from = [entry_from]
    if not isinstance(tags, list): tags = [tags]

    # Save all the data for the various calculations
    fts = []
    for t, ef, et in zip(tags, entry_from, entry_to):
        # Create and save the actual data in the location provided by input
        ft = FT_MoveTagResults(mid=mid,
                               formula=formula,
                               name=name,
                               collection_from='tasks',
                               collection_to=collection,
                               tag=t,
                               db_file=db_file,
                               database_from=None,
                               database_to=database,
                               miller=miller,
                               adsorbate=adsorbate,
                               entry_from=ef,
                               entry_to=et)
        fts.append(ft)
    f2n = fw_names[1] + ' - fw' if fw_names[1] is not None else 'Save data in db - fw'
    fw_2 = Firework(fts, name=f2n)
    
    if to_wf:
        wf = Workflow([fw_1, fw_2], {fw_1: [fw_2]}, name='Run and save - WF')
        return wf
    else:
        return fw_1, fw_2

def run_vasp_simulation(mid, collection, entry_from, entry_to, calc_type,
                        entry_vasp=['structure', 'init'],
                        formula=None, miller=None, adsorbate=None, name=None,
                        db_file=None, database='tribchem', check_key=None, 
                        check_entry=None, tag=None, override=False, to_wf=False):
    """
    Run a VASP simulation for a given material already stored in the Database.
    It call consecutively RunVaspSimulation and MoveTagResults to perform a 
    SINGLE DFT calculation. If you want to run more than one, call the function
    in a loop. Either a tuple of Fireworks objects or a workflow is returned.

    """
    
    # Define the tag
    if tag is None:
        tag = str(uuid4())
    
    # Prepare the VASP simulation 
    ft_vasp = FT_RunVaspSimulation(mid=mid,
                                   formula=formula,
                                   miller=miller,
                                   name=name,
                                   db_file=db_file,
                                   database=database,
                                   collection=collection,
                                   entry=entry_vasp,
                                   tag=tag,
                                   calc_type=calc_type,
                                   override=override,
                                   check_key=check_key)
    fw_vasp = Firework(ft_vasp, name='Run VASP - ' + tag)

    ft_mv = FT_MoveTagResults(mid=mid,
                              collection_from='tasks',
                              collection_to=collection,
                              tag=tag,
                              db_file=db_file,
                              database_from=None,
                              database_to=database,
                              entry_to=entry_to,
                              entry_from=entry_from,
                              check_entry=check_entry,
                              override=override)
    fw_mv = Firework(ft_mv, name='Save data in DB - ' + tag)

    if to_wf:
        wf = Workflow([fw_vasp, fw_mv], {fw_vasp: [fw_mv]}, name='Run Vasp simulation - WF')
        return wf
    else:
        return fw_vasp, fw_mv

def run_and_save_files(mid, structure, vis, tags, calc_type, collection, entry_from,
                       entry_to, main_folder, custom_names, db_file=None, 
                       database='tribchem', formula=None, name=None, miller=None, 
                       adsorbate=None, fw_names=None, to_wf=False):
    """
    Modification of run_and_save which allows also to save the VASP output files.
    """
    
    if fw_names is not None:
        try:
            assert isinstance(fw_names, list)
            assert len(fw_names) == 2
            assert all([isinstance(n, str) for n in fw_names])
        except:
            raise ValueError('Wrong input argument: fw_names. It should be a list of '
                             'two strings or None')
    else:
        fw_names = [None, None]
    
    try:
        if isinstance(entry_to, list): 
            assert isinstance(entry_from, list)
            assert len(entry_to) == len(entry_from)
        if isinstance(structure, list): 
            assert len(structure) == len(entry_to)
        if isinstance(custom_names, list):
            assert len(custom_names) == len(tags)
    except:
        raise ValueError('Wrong input argument: entry_to, entry_from or custom_names. '
                         'WARNING: Although you might have the same entries for '
                         'all the structures that are run, you should provide a '
                         'list of entries, one for each structure.')

    # Run the simulations on the fly
    ft_1 = FT_RunVaspOnTheFly(structure=structure, vis=vis, tag=tags,
                              calc_type=calc_type, wf_name=fw_names[0])
    f1n = fw_names[0] + ' - fw' if fw_names[0] is not None else 'Run Vasp on the fly - fw'
    fw_1 = Firework([ft_1], name=f1n)
    
    # Check for type consistency
    if not isinstance(entry_to, list): entry_to = [entry_to]
    if not isinstance(entry_from, list): entry_from = [entry_from]
    if not isinstance(tags, list): tags = [tags]

    # Save all the data for the various calculations
    fts = []
    ftv = []
    for t, ef, et, cn in zip(tags, entry_from, entry_to, custom_names):
        # Create and save the actual data in the location provided by input
        ft = FT_MoveTagResults(mid=mid,
                               formula=formula,
                               name=name,
                               collection_from='tasks',
                               collection_to=collection,
                               tag=t,
                               db_file=db_file,
                               database_from=None,
                               database_to=database,
                               miller=miller,
                               adsorbate=adsorbate,
                               entry_from=ef,
                               entry_to=et)
        fts.append(ft)
        ft_v = FT_SaveVASPOutputs(tag=t, folder=main_folder, custom_name=cn,
                                  db_file=db_file)
        ftv.append(ft_v)

    f2n = fw_names[1] + ' - fw' if fw_names[1] is not None else 'Save data in db - fw'
    fw_2 = Firework(fts, name=f2n)
    
    fw_3 = Firework(ftv, name='Save VASP output files')
    
    if to_wf:
        wf = Workflow([fw_1, fw_2, fw_3], {fw_1: [fw_2], fw_2: [fw_3]}, name='Run and save - WF')
        return wf
    else:
        return fw_1, fw_2, fw_3
