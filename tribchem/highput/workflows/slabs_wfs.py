#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 15:02:48 2021

Collection of Workflow to study the properties and structures of crystal slabs.

The module contains:

** SlabWF **:
    General class to work on crystalline slabs, workflows are static method.
    It includes the following methods:
        - conv_slabthick_surfene
        - conv_slabthick_alat
        - _check_subwf_params

    Author: Gabriele Losi (glosi000)
    Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 2nd, 2021'


import os
from fireworks import Workflow, Firework

from tribchem.highput.utils.tasktools import read_default_params
from tribchem.highput.workflows.wfcore import GeneralTribchemWF
from tribchem.highput.firetasks.slabs import (
    FT_StartThickConvo,
    FT_EndThickConvo
)


currentdir = os.path.dirname(__file__) + '/../firetasks'


class SlabWF(GeneralTribchemWF):
    """
    Collection of static methods to create, cut, modify, merge, and generally
    manipulate slab structures. It also contains methods to manage the 
    optimization of a slab thickness, based on the evaluation of the surface 
    energy and the lattice parameter.

    """
    
    @staticmethod
    def conv_slabthick_surfene(structure, mid, miller, collection, main_folder,
                               formula=None, name=None, spec={}, db_file=None,
                               database='tribchem', calc_type='slab_pos_relax',
                               thick_min=4, thick_max=12, thick_incr=2, vacuum=10,
                               in_unit_planes=True, ext_index=0, conv_thr=0.025,
                               parallelization='low', recursion=0,
                               cluster_params={}, override=False):
        """ 
        Function to set the computational and physical parameters and start a 
        workflow to converge the thickness of the provided slabs.

        """

        # Set the cluster parameters
        dfl = currentdir + '/defaults.json'
        cluster_params = read_default_params(default_file=dfl, 
                                             default_key='cluster_params', 
                                             dict_params=cluster_params)

        # Print relevant information and raise errors based on parameters
        SlabWF.check_mid(structure, mid)
        SlabWF.print_help(cluster_params)

        # Create two fws to handle the convergence of the optimal thickness
        conv_kind = 'surfene'
        fw_1 = Firework(FT_StartThickConvo.from_locals(**locals()),
                        name = 'Start slab thickness convergence WF')

        fw_2 = Firework(FT_EndThickConvo.from_locals(**locals()),
                        name = 'End slab thickness convergence WF')

        # Set the name of the Workflow
        name_wf = 'Slab Thickness optimization of ' + \
                  structure.composition.reduced_formula + ' ' + str(miller)

        wf = Workflow([fw_1, fw_2], {fw_1: fw_2}, name=name_wf)

        return wf

    @staticmethod
    def conv_slabthick_alat():        
        """
        Subworkflow to converge the slab thickness by converging the lattice
        parameters. Not implemented yet.
        """
        pass
