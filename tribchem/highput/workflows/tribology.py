"""
Workflow to calculate tribological interface of hetero interfaces

"""

from fireworks import Workflow, Firework

from tribchem.highput.fireworks.common import check_inputs_fw
from tribchem.highput.firetasks.encut import FTStartEncutConvo
from tribchem.highput.firetasks.kpoints import FTStartKpointsConvo
from tribchem.highput.firetasks.structure import (
    FTStartSlabRelaxSWF,
    FTMakeHeteroStructure,
    interface_name,
)
from tribchem.highput.firetasks.iocheck import FTUpdateCompParams
from tribchem.highput.database.navigator import NavigatorMP


def heterogenous_wf(inputs):
    """
    Return main workflow for heterogeneous interfaces within Triboflow.

    Parameters
    ----------
    inputs : dict
        Dictionary containing sub-dictionaries with material information and
        parameters for the interface creation and computational settings.

    Returns
    -------
    wf : FireWorks Workflow
        Main Triboflow workflow for heterogeneous interfaces.

    """   
    
    mat_1 = inputs.get('material_1')
    mat_2 = inputs.get('material_2')
    comp_params = inputs.get('computational_params')
    inter_params = inputs.get('interface_params')
    
    if not all([mat_1, mat_2, comp_params, inter_params]):
        raise SystemExit('The inputs-dictionary for this workflow must '
                         'contain the following keys:\n'
                         'material_1\nmaterial_2\ncomputational_params\n'
                         'interface_params')

    nav_mp = NavigatorMP()

    struct_1, mp_id_1 = nav_mp.get_low_energy_structure(
        chem_formula=mat_1.get('formula'), 
        mp_id=mat_1.get('mpid'))
    
    struct_2, mp_id_2 = nav_mp.get_low_energy_structure(
        chem_formula=mat_2.get('formula'),
        mp_id=mat_2.get('mpid'))

    functional = comp_params.get('functional', 'SCAN')

    initialize = check_inputs_fw(
        mat1_params=mat_1,
        mat2_params=mat_2,
        compparams=comp_params,
        interface_params=inter_params,
        FW_name='Check input parameters')
    #wf.append(initialize)
    
    converge_encut_m1 = Firework(
        FTStartEncutConvo(
            mp_id=mp_id_1,
            functional=functional),
            name='Start encut convergence for {}'.format(mat_1['formula']))
    #wf.append(converge_encut_m1)
    
    converge_encut_m2 = Firework(
        FTStartEncutConvo(
            mp_id=mp_id_2,
            functional=functional),
            name='Start encut convergence for {}'.format(mat_2['formula']))
    #wf.append(converge_encut_m2)
    
    converge_kpoints_m1 = Firework(
        FTStartKpointsConvo(
            mp_id=mp_id_1,
            functional=functional),
            name='Start kpoints convergence for {}'.format(mat_1['formula']))
    #wf.append(converge_kpoints_m1)
    
    converge_kpoints_m2 = Firework(
        FTStartKpointsConvo(
            mp_id=mp_id_2,
            functional = functional),
            name='Start kpoints convergence for {}'.format(mat_2['formula']))
    #wf.append(converge_kpoints_m2)
    
    final_params = Firework(
        FTUpdateCompParams(
            mp_id_1=mp_id_1,
            mp_id_2=mp_id_2,
            miller_1=mat_1.get('miller'),
            miller_2=mat_2.get('miller'),
            functional=functional),
            name = 'Consolidate computational paramters')
    #wf.append(final_params)
    
    make_slabs_m1 = Firework(
        FTStartSlabRelaxSWF(
            mp_id=mp_id_1,
            miller=mat_1.get('miller'),
            functional=functional),
            name='Make and relax slabs for {}'.format(mat_1['formula']))
    #wf.append(make_slabs_m1)
    
    make_slabs_m2 = Firework(
        FTStartSlabRelaxSWF(
            mp_id=mp_id_2,
            miller=mat_2.get('miller'),
            functional=functional),
            name='Make and relax slabs for {}'.format(mat_2['formula']))
    #wf.append(make_slabs_m2)
    
    make_interface = Firework(
        FTMakeHeteroStructure(
            mp_id_1=mp_id_1,
            mp_id_2=mp_id_2,
            miller_1=mat_1.get('miller'),
            miller_2=mat_2.get('miller'),
            functional=functional),
            name='Match the interface')
    #wf.append(make_interface)
    
    wf = [initialize, 
          converge_encut_m1, converge_encut_m2, 
          converge_kpoints_m1, converge_kpoints_m2, 
          final_params, 
          make_slabs_m1, make_slabs_m2,
          make_interface]
    
    #Define dependencies:
    dependencies = {initialize: [converge_encut_m1, converge_encut_m2],
                    converge_encut_m1: [converge_kpoints_m1],
                    converge_encut_m2: [converge_kpoints_m2],
                    converge_kpoints_m1: [final_params],
                    converge_kpoints_m2: [final_params],
                    final_params: [make_slabs_m1, make_slabs_m2],
                    make_slabs_m1: [make_interface],
                    make_slabs_m2: [make_interface]}

    wf_name = 'TribChem WF')

    wf = Workflow(wf, dependencies, name=wf_name)
    
    return 
