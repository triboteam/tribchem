#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 14:47:15 2021

Workflows for calculating and converging the surface energy of a slab.

The module contains:

    ** SurfEneWF **:
        General class to work on crystalline slabs, workflows are static method.
        It includes the following methods:
            - conv_slabthick_surfene
            - conv_slabthick_alat
            - _check_subwf_params
    
    Functions:
    - check_choice

    Author: Gabriele Losi (glosi000)
    Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 9th, 2021'


import numpy as np
from fireworks import Workflow, Firework

from tribchem.highput.utils.tasktools import create_tags, get_miller_str
from tribchem.highput.database.navigator import Navigator
from tribchem.highput.firetasks.structure import FT_RunVaspSimulation
from tribchem.highput.firetasks.dbtask import FT_MoveTagResults, FT_SaveVASPOutputs
from tribchem.highput.firetasks.slabs import FT_GenerateSlabs
from tribchem.highput.firetasks.surfene import FT_SurfaceEnergy
from tribchem.highput.workflows.wfcore import GeneralTribchemWF
from tribchem.highput.utils.errors import WorkflowError


class SurfEneWF(GeneralTribchemWF):
    """
    Collection of static methods to calculate the surface energy of a slab. 
    It also contains methods to converge the surface energy of a slab.
    
    """

    @staticmethod
    def conv_surface_energy(structure, mid, miller, collection, main_folder, 
                            formula=None, name=None, spec={}, db_file=None, 
                            database='tribchem', calc_type='slab_pos_relax', 
                            thick_min=4, thick_max=12, thick_incr=2, vacuum=10, 
                            in_unit_planes=True, ext_index=0, cluster_params={}, 
                            parallelization=None, recursion=0, override=False):
        """
        Description of the method...

        """

        # Define the first Firework
        # =====================================================================
        
        # Define the thicknesses to create and simulate the slabs
        if parallelization == 'low' or parallelization is None:
            if recursion:
                thickness = [thick_min]
            else:
                thickness = [0, thick_min, thick_max]

        elif parallelization == 'high':
            try:
                thickness = np.arange(thick_min, thick_max, thick_incr)
                thickness = thickness.tolist()

                # If the array length is zero, the thick values are not well set
                assert len(thickness) > 0

                thickness = [0] + thickness + [thick_max]

            except: WorkflowError("Thickness parameters are not defined correctly")
        
        else:
            raise WorkflowError("The value passed as parallelization is not known "
                              "Allowed value: None, 'low', 'high'")
        
        # Create the dictionary key where the unrelaxed slab will be saved
        formula = formula if formula is not None else structure.composition.reduced_formula
        miller_str = get_miller_str(miller)
        slab_entry = [['thickness', 'data_' + str(thk), 'input'] for thk in thickness]

        # Generate the slabs and store them in the low level database, under
        # the dictionary key given by `slab_entry`
        ft_gen_slabs = FT_GenerateSlabs(structure=structure,
                                        mid=mid,
                                        miller=miller,
                                        collection=collection,
                                        db_file=db_file,
                                        database=database,
                                        thickness=thickness,
                                        thick_bulk=thick_min,
                                        vacuum=vacuum,
                                        ext_index=ext_index,
                                        in_unit_planes=in_unit_planes,
                                        entry=slab_entry)

        fw_gen_slabs = Firework([ft_gen_slabs],
                                spec=spec,
                                name='Generate ' + str(len(thickness)) + ' slabs')

        # Define the second Firework
        # ==================================================

        # Create the tags to store the calculation of the slabs
        tag_prefix = [formula + '_slab_' + miller_str + '_' + str(t) for t in thickness]
        tags = create_tags(tag_prefix)

        # Create the Firetasks to relax the structures
        fw_relax = []
        fw_move = []
        fw_save = []
        for thk, n, t in zip(thickness, slab_entry, tags):
            
            # Define different minor options to set correctly the firetasks
            res = 'bulk_pos_relax' if thk == 0 else calc_type
            chkrel, chkmove = SurfEneWF.__check_choice(t, db_file, database, 
                                                       collection, mid, miller, 
                                                       thk, override=override)

            # Identify the position of the structure
            ft_1 = FT_RunVaspSimulation(mid=mid,
                                        formula=formula,
                                        name=name,
                                        collection=collection,
                                        entry=n,
                                        tag=t,
                                        db_file=db_file,
                                        database=database,
                                        calc_type=res,
                                        miller=miller,
                                        check_key=chkrel)
            fw_1 = Firework([ft_1],
                            spec=spec,
                            name='Relax structure: ' + formula + ' ' + 
                                 miller_str + ', thickness: ' + str(thk))
            
            # Define the source and destination for the simulation results
            tmp = SurfEneWF.transfer_data_to.copy()
            entry_to = [['thickness', 'data_'+str(thk), 'output', d[-1]] for d in tmp]
            entry_to += [['thickness', 'data_'+str(thk), 'output', 'structure']]
            entry_from = SurfEneWF.transfer_data_from.copy() + [['output', 'structure']]

            # Create the Firetask to move results
            ft_2 = FT_MoveTagResults(mid=mid,
                                     collection_from='tasks',
                                     collection_to=collection,
                                     tag=t,
                                     db_file=db_file,
                                     database_from=None,
                                     database_to=database,
                                     miller=miller,
                                     check_entry=chkmove,
                                     entry_to=entry_to,
                                     entry_from=entry_from,
                                     override=False,
                                     cluster_params=cluster_params)
            
            ft_3 = FT_SaveVASPOutputs(tag=t, folder=main_folder, 
                                      custom_name='/thickness_'+str(thk), 
                                      db_file=db_file)
                
            fw_1 = Firework([ft_1],
                            spec=spec,
                            name='Relax structure: ' + formula + ' ' + 
                                 miller_str + ', thickness: ' + str(thk))
            fw_2 = Firework([ft_2],
                            spec=spec,
                            name='Store structure in DB: ' + formula + ' ' +
                                 miller_str + ', thickness: ' + str(thk))
            fw_3 = Firework([ft_3],
                            spec=spec,
                            name='Save VASP output files: ' + formula + ' ' +
                                 miller_str + ', thickness: ' + str(thk))

            fw_relax.append(fw_1)
            fw_move.append(fw_2)
            fw_save.append(fw_3)

        # Define the third Firework(s)
        # ==================================================
        
        # Set the location of the energies in the high level DB
        entry_surfene = [['thickness', 'data_0', 'output']]
        thk_loop = thickness if recursion else thickness[1:]
        for thk in thk_loop:
            entry_surfene.append(['thickness', 'data_' + str(thk), 'output'])

        ft_surfene = FT_SurfaceEnergy(mid=mid,
                                      collection=collection,
                                      miller=miller,
                                      entry=entry_surfene,
                                      parallelization=parallelization,
                                      db_file=db_file,
                                      database=database)

        fw_surfene = Firework([ft_surfene],
                              spec=spec,
                              name='Calculate the Surface Energies')

        # Define and return the Workflow
        # ==================================================

        # Build the workflow list and the links between elements
        wf_list = [fw_gen_slabs]

        # Case of parallelization = None, make serial calculations
        if parallelization is None and not recursion:
            links = {fw_gen_slabs: fw_relax[0]}
 
            for n, fw in enumerate(zip(fw_relax, fw_move, fw_save)):
                fwr, fwm, fws = fw
                wf_list.append(fwr)
                wf_list.append(fwm)
                wf_list.append(fws)
                links.update({fwr: fwm})
                links.update({fwm: fws})
                if n < len(fw_relax) - 1:
                    links.update({fws: fw_relax[n+1]})
            
            links.update({fw_save[-1]: fw_surfene})
        
        # Other cases or when recursion is True (one element)
        else:
            links = {fw_gen_slabs: fw_relax}
            for fwr, fwm, fws in zip(fw_relax, fw_move, fw_save):
                wf_list.append(fwr)
                wf_list.append(fwm)
                wf_list.append(fws)
                links.update({fwr: fwm})
                links.update({fwm: fws})
                links.update({fws: fw_surfene})

        wf_list.append(fw_surfene)

        wf = Workflow(wf_list, links, name="Converge surface energy WF")

        return wf

    @staticmethod
    def __check_choice(tag, dbf, db, coll, mid, miller, thk, tol=1e-6, override=False):
        """
        Check if the results are already stored in the database. This is necessary
        at the moment to avoid that FT_MoveTagResults goes in conflicts with
        FT_RunVaspSimulation.

        """

        check_relax = None
        check_move = None

        if override:
            return check_relax, check_move
        
        # Start the navigators
        nav_low = Navigator(db_file=dbf, high_level=None)
        nav = Navigator(db_file=dbf, high_level=db)

        data = nav.find_data(coll, {'mid': mid, 'miller': miller})
        
        if data is not None:
            try:
                d = data['thickness']['data_' + str(thk)]['output']
                old_tag = d['task_label']
            except:
                d = None
                old_tag = None

            if d is not None:
                if 'energy' in d.keys() and 'energy_per_atom' in d.keys() and 'nsites' in d.keys():
                    if old_tag == tag:
                        check_relax = ['thickness', 'data_' + str(thk), 'output']
                        check_move = ['thickness', 'data_' + str(thk), 'output']
                    else:
                        calc = nav_low.find_data('tasks', {'task_label': old_tag})
                        if calc is not None:
                            if 'output' in calc.keys():
                                out = calc['output']
                                is_eq = abs(out['energy'] - d['energy']) < tol and\
                                        abs(out['energy_per_atom'] - d['energy_per_atom']) < tol and\
                                        d['nsites'] == calc['nsites']
                            else:
                                is_eq = False

                        else:
                            is_eq = False

                        if is_eq:
                            check_relax = ['thickness', 'data_' + str(thk), 'output']
                            check_move = ['thickness', 'data_' + str(thk), 'output']

            else:
                calc = nav_low.find_data('tasks', {'task_label': tag})
                if calc is not None:
                    if 'output' in calc.keys():
                        out = calc['output']
                        if 'energy' in out.keys() and 'energy_per_atom' in out.keys() and 'nsites' in calc.keys():
                            check_relax = ['thickness', 'data_' + str(thk), 'input']

        else:
            calc = nav_low.find_data('tasks', {'task_label': tag})
            if calc is not None:
                if 'output' in calc.keys():
                    out = calc['output']
                    if 'energy' in out.keys() and 'energy_per_atom' in out.keys() and 'nsites' in calc.keys():
                        check_relax = ['thickness', 'data_' + str(thk), 'input']

        return check_relax, check_move
