"""
Created on Mon June 8 16:54:55 2021

Collection of classes and methods to analyze and study the PPES

Author: Gabriele Losi (glosi000), Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi, Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'June 8th, 2021'


from uuid import uuid4
from fireworks import Firework, Workflow

from tribchem.highput.firetasks.slabs import FT_GenerateInterface, FT_GetHighSymmetryPts
from tribchem.highput.firetasks.pes import FT_PotentialEnergySurface
from tribchem.highput.firetasks.adhesion import FT_AdhesionEnergy
from tribchem.highput.workflows.wfcore import GeneralTribchemWF
from tribchem.highput.utils.manipulate_struct import generate_pes_inputs, generate_ppes_inputs
from tribchem.highput.firetasks.pes import FT_FitPPES
from tribchem.highput.workflows.wfcore import run_and_save
from tribchem.highput.utils.vasp_tools import VaspInputSet
from tribchem.highput.firetasks.dbtask import FT_SaveVASPOutputs


class InterfaceWF(GeneralTribchemWF):

    @staticmethod
    def match_interface(bot_slab, top_slab, mid, miller, collection,
                        inter_params, formula=None, name=None, db_file=None,
                        database='tribchem', inter_entry=['structure', 'init'],
                        hs_entry=['structure', 'init', 'highsym'], comp_params={}, vacuum=15,
                        update_op='$set', upsert=False, to_mongodb=True):
        """
        Create a workflow to match two slabs and build an interface.

        Parameters
        ----------
        bot_slab : pymatgen object
            Bottom slab constituting the interface (substrate material).

        top_slab : pymatgen object
            Upper slab constituting the interface (coating material).

        mid : str
            Material identifier of the interface. Combination of the mids of
            the two materials, e.g. 'mp-134_mp-134'.

        miller : list of lists of int
            Miller index identifying the interface. Combination of the two
            set of indexes of the slabs. Example: [[1, 0, 0], [1, 1, 1]]

        collection : str
            Collection of the Database with the interface.

        inter_params : dict
            Parameters with convergence thresholds to match the interface.

        formula : str or None, optional
            Formula of the interface, typically the combination of the formulas
            of the two single slabs, e.g. 'Al_Al. The default is None.

        name : str or None, optional
            Name of the interface. The default is None.

        db_file : str or None, optional
            Location of the Database file, if None the location will be selected
            automatically from env variables. The default is None.

        database : str or None, optional
            Select the right MongoDB database, if it is None the Fireworks 
            database is selected. The default is "tribchem".

        inter_entry : str or list of str, optional
            Location in the database where the initial structure should be
            saved. The default is ['structure', 'init'].

        hs_entry : str or list of str, optional
            Where to save the High Symmetry points of the interface.
            The default is ['highsym'].

        comp_params : dict, optional
            Desired computational parameters for the interface to be saved in 
            the Database. The default is {}.

        update_op : str, optional
            Update operation for the Database. The default is '$set'.

        upsert : bool, optional
            Set upsert option for MongoDB update. The default is False.

        to_mongodb : bool, optional
            Convert the dictionary to MongoDB format before updating, in order
            to avoid wrong overriding of Database data. The default is True.

        Returns
        -------
        wf : fireworks.Workflow
            Workflow to run the interface matching

        """

        slabs = [bot_slab, top_slab]

        # Generate and save the interface
        entry = inter_entry
        ft_1 = FT_GenerateInterface.from_locals(**locals())
        fw_1 = Firework(ft_1, name='Build and store a interface')

        # Calculate interface High Symmetry points
        entry = hs_entry
        ft_2 = FT_GetHighSymmetryPts.from_locals(**locals())
        fw_2 = Firework(ft_2, name='Calculate HS points of slabs, interface')

        wf = Workflow([fw_1, fw_2], {fw_1: [fw_2]},
                    name='Create solid interface and geometric properties SWF')
        
        return wf

    @staticmethod
    def calculate_pes(bot_slab, top_slab, shifts, mid, miller, collection, 
                      main_folder, formula=None, name=None, db_file=None, 
                      database='tribchem', calc_type='interface_z_relax', 
                      pes_type='energy', run_vasp=True, to_file=False, 
                      comp_params={}):
        """
        Create a workflow to compute the Potential Energy Surface of a
        solid-solid interface.

        Parameters
        ----------
        top_slab : pymatgen object
            Top slab of the interface.

        bottom_slab : pymatgen object
            Bottom slab of the interface.
        
        shifts : dict
            List of 

        interface_name : str, optional
            Unique name to find the interface in the database with.
            The default is None, which will lead to an automatic interface_name
            generation which will be printed on screen.

        comp_params : dict, optional
            Computational parameters to be passed to the vasp input file generation.
            The default is {}.
        
        main_folder : str
            Main folder where to save the VASP output files.

        Returns
        -------
        wf : fireworks.core.firework.Workflow
            A subworkflow intended to compute the PES of a certain interface.

        """

        # Define common tag prefix for all the simulations
        fname = mid.split('_')[0] + str(tuple(miller[0])) + '_' + \
                mid.split('_')[1] + str(tuple(miller[1]))

        # Define the first and second Firework : run vasp and save data
        # =====================================================================
    
        # Prepare the inputs for running all the simulation with VaspOnTheFly
        structure, vis, tags = generate_pes_inputs(bot_slab, top_slab, shifts,
                                                   comp_params, fname, calc_type)

        # Generate the list of entries
        entry_from, entry_to = InterfaceWF._select_entry('pes', shifts=shifts)
        
        # Change the name from 'pes' to 'pes_scf' if the PES has been calculated 
        # with a static calculation
        entry_to = InterfaceWF().pes_entry_name(calc_type=calc_type, 
                                                entry_to=entry_to)
        fw_1, fw_2 = run_and_save(mid, structure, vis, tags, calc_type, collection, 
                                  entry_from, entry_to, db_file, database, 
                                  formula, name, miller, 
                                  fw_names=['Run PES simulations for: ' + fname,
                                            'PES simulations in DB for: ' + fname])

        # Define the third Firework : compute PES and clean data
        # =====================================================================
        
        ft_3 = FT_PotentialEnergySurface(mid=mid, miller=miller, name=name,
                                         formula=formula, db_file=db_file,
                                         database=database, collection=collection,
                                         calc_type=calc_type, pes_type=pes_type, 
                                         to_file=to_file)
        fw_3 = Firework([ft_3], name='Calculate the {} PES for: {}'.format(pes_type, fname))
        
        # Save the VASP output files
        ft_4 = FT_SaveVASPOutputs(tag=tags, folder=main_folder, 
                                  custom_name=list(shifts.keys()), db_file=db_file)
        fw_4 = Firework([ft_4], name='Save the VASP output files for {}'.format(fname))

        # Prepare the workflow. It is possible to avoid to make new vasp 
        # simulations, and calculate the desired PES by setting run_vasp=false.
        if run_vasp:
            wf = Workflow([fw_1, fw_2, fw_3, fw_4], 
                          {fw_1: [fw_2], fw_2: [fw_3], fw_3: [fw_4]}, 
                          name='Run simulations and calculate {} PES wf'.format(pes_type))
        else:
            wf = Workflow([fw_3], name='Calculate {} PES wf'.format(pes_type))

        return wf

    @staticmethod
    def adhesion_energy(bot_slab, top_slab, interface, mid, miller, collection, 
                        main_folder, formula=None, name=None, db_file=None, 
                        database='tribchem', adhesion_type='regular',
                        calc_type='slab_pos_relax', run_vasp=True, comp_params={}):
        """
        Create a subworkflow to compute the adhesion energy for an interface.
         
        This workflow takes two matched slabs (their cells must be identical) 
        and a relaxed interface structure of those slabs and computes the 
        andhesion energy. The two matched slabs must be relaxed as well to get 
        correct results.
         
        Parameters
        ----------
        top_slab : pymatgen.core.surface.Slab
        Relaxed top slab of the interface.
        
        bottom_slab : pymatgen.core.surface.Slab
        Relaxed bottom slab of the interface.
         
        interface : list of pymatgen.core.surface.Slab
        Relaxed interface structure in minimum and maximum sites positions.
         
        interface_fltr : dict
        Filter for retriving the interface from the database.
         
        bottom_mpid : str, optional
        ID of the bulk material of the top slab in the MP database.
        The default is None.
         
        functional : str, optional
        Which functional to use; has to be 'PBE' or 'SCAN'. The default is 
        'PBE'.
         
        comp_params : dict, optional
        Computational parameters to be passed to the vasp input file 
        generation. The default is {}.
        
        main_folder : str
            Main folder where to save the VASP output files.
         
        Returns
        -------
        SWF : fireworks.core.firework.Workflow
        A subworkflow intended to compute the adhesion of a certain 
        interface.
        
        """

        # Define common tag prefix for all the simulations
        fname = mid.split('_')[0] + str(tuple(miller[0])) + '_' + \
                mid.split('_')[1] + str(tuple(miller[1]))
        labels = ['bot_slab', 'top_slab', 'interface_min', 'interface_max']


        # Define the first and second Firework : run vasp and save data
        # =====================================================================

        # Generate the inputs for running Vasp on the fly
        structure = [bot_slab, top_slab] + interface
        tags = [name + '_' + l + '_' + str(uuid4()) for l in labels]

        # Generate the list of entries
        entry_from, entry_to = InterfaceWF._select_entry('adhesion', labels=labels,
                                                         adhesion_type=adhesion_type)

        # Define different calc_type for the max site
        if calc_type not in ['slab_from_scratch', 'slab_pos_relax', 'interface_z_relax']:
            raise ValueError("For safety reasons calc_type can be only "
                             "'slab_from_scratch', 'slab_pos_relax', 'interface_z_relax'."
                             " Change the code at yor own risk")
        c_max = 'interface_z_relax' if calc_type == 'slab_pos_relax' else calc_type
        calcs = [calc_type]*3 + [c_max]

        fws_1, fws_2 = [], []
        for i in range(len(structure)):
            vis = VaspInputSet(structure[i], comp_params, calcs[i]).get_vasp_settings()
            fw_1, fw_2 = run_and_save(mid, structure[i], vis, tags[i], calcs[i],
                                      collection, [entry_from[i]], [entry_to[i]],
                                      db_file, database, formula, name, miller, 
                                      fw_names=['Run {}: {}'.format(labels[i], fname),
                                                'Data in DB for {}: {}'.format(labels[i], fname)])
            fws_1.append(fw_1)
            fws_2.append(fw_2)

        # Define the third Firework : calculate cohesion energy
        # =====================================================================            

        ft_3 = FT_AdhesionEnergy.from_locals(**locals())
        fw_3 = Firework([ft_3], name='Calculate Adhesion energy for: ' + fname)
        
        # Save the VASP output files
        ft_4 = FT_SaveVASPOutputs(tag=tags, folder=main_folder, 
                                  custom_name=labels, db_file=db_file)
        fw_4 = Firework([ft_4], name='Save the VASP output files for {}'.format(fname))
        
        # Run the simulation with VASP if required so
        if run_vasp:
            fws = fws_1 + fws_2 + [fw_3, fw_4]
            links = {fw_3: fw_4}
            for i in range(4):
                links.update({fws_1[i]: fws_2[i]})
                links.update({fws_2[i]: fw_3})
            wf = Workflow(fws, links, name='Calculate Adhesion wf')
        else:
            wf = Workflow([fw_3], name='Calculate Adhesion wf')

        return wf

    @staticmethod
    def calculate_ppes(interface, mid, miller, collection, main_folder, 
                       formula=None, name=None, db_file=None, database='tribchem', 
                       calc_type='slab_from_scratch', ppes_type='energy',
                       site='min_site', run_vasp=True, fit=None, comp_params={},
                       shifts=[-0.5, -0.25, 0.0, 0.25, 0.5, 2.5, 3.0, 4.0, 5.0, 7.5], check_entry=None):
        """
        Generate a subworkflow that calculates a PPES using static calculations.

        For a given interface in the high-level database this subworkflow performs
        static calculations for different distances of the two slabs modeling
        brittle cleavage under mode 1 loading using the rigid separation model.
        The results are saved in a energy vs distance array and saved i the high-
        level database alongside a fit to a UBER curve.

        Parameters
        ----------
        interface : dict
            Interface retrieved from the database.
         
        structure_name : str
            Name of the structure, which is made up by the materials project ids of 
            the slabs and the miller indexes.
            E.g.: mp-134_mp-30_001_001

        run_vasp : bool, default True
            If True the VASP calculation will be executed.

        comp_params : dict
            Dictionary of computational parameters for the VASP calculations.

        collection : str
            Name of the collection where to save the fitted value of PPES.

        distance_list : list of float, optional
            Modification of the equilibrium distance between the slabs.
            The default is [-0.5, -0.25, 0.0, 0.25, 0.5, 2.5, 3.0, 4.0, 5.0, 7.5].

        db_file : str
            Full path to the db.json file that should be used. Defaults to
            '>>db_file<<', to use env_chk.

        database : str, optional
            Database where to save the data.

        mid : list
            List containing all the materials project ids of the interface.
            E.g.: ['mp-134', 'mp-30']

        miller : list
            List containing all the miller indxes of the slabs of the interface.
            E.g.: [[1, 0, 0], [1, 0, 0]]

        spec : dict, optional
            fw_spec that can be passed to the SWF and will be passed on. The
            default is {}.

        Returns
        -------
        wf : fireworks.Workflow
            Subworkflow to calculate the PPES for a certain interface.

        """
        
        # Define common tag prefix for all the simulations
        fname = mid.split('_')[0] + str(tuple(miller[0])) + '_' + \
                mid.split('_')[1] + str(tuple(miller[1]))


        # Define the first and second Firework : run vasp and save data
        # =====================================================================
    
        # Generate the vasp inputs
        structure, vis, tags = generate_ppes_inputs(interface, shifts, 
                                                    comp_params, 'zero', 
                                                    fname, calc_type)

        # Generate the list of entries
        entry_from, entry_to = InterfaceWF._select_entry('ppes', shifts=shifts,
                                                         site=site, check_entry=check_entry)

        fw_1, fw_2 = run_and_save(mid, structure, vis, tags, calc_type, collection, 
                                  entry_from, entry_to, db_file, database, 
                                  formula, name, miller, 
                                  fw_names=['Run PPES simulations for: ' + fname,
                                            'PPES simulations in DB for: ' + fname])


        # Define the their Firework : fit the PPES
        # =====================================================================

        ft_3 = FT_FitPPES(mid=mid, miller=miller, formula=formula, name=name,
                          shifts=shifts, db_file=db_file, database=database,
                          collection=collection, ppes_type=ppes_type, site=site, 
                          fit=fit, check_entry=check_entry) 
        fw_3 = Firework([ft_3], name='PPES {} fit for '.format(fit) + fname)

        # Save the VASP output files
        shifts_str = ['shift_'+str(s) for s in shifts]
        ft_4 = FT_SaveVASPOutputs(tag=tags, folder=main_folder,
                                  custom_name=shifts_str, db_file=db_file)
        fw_4 = Firework([ft_4], name='Save the VASP output files for {}'.format(fname))

        # Prepare the workflow. It is possible to avoid to make new vasp 
        # simulations, and calculate the desired PES by setting run_vasp=false.
        if run_vasp:
            wf = Workflow([fw_1, fw_2, fw_3, fw_4], 
                          {fw_1: [fw_2], fw_2: [fw_3], fw_3: [fw_4]}, 
                          name='Run simulations and calculate {} PPES wf'.format(ppes_type))
        else:
            wf = Workflow([fw_3], name='Calculate {} PPES wf'.format(ppes_type))

        return wf

    def pes_entry_name(self, calc_type, entry_to):
        """
        Change the name of the entry for the PES from 'pes' to 'pes_scf' in case
        the PES is calculated using a static calculation.
        
        Parameters
        ----------
        calc_type : str
            Type of the calculation used to calculate the PES.
        
        entry_to : dict
            Dictionary containing the data to save in the database.
            
        Return
        ------
        entry_to : dict
            Modified version of the dictionary containing the data to be saved 
            in the database.

        """
        
        vis_static = VaspInputSet.static_types
        
        if calc_type in vis_static:
            for first_level in entry_to:
                for second_level in first_level:
                    second_level[0] = 'pes_scf'

        return entry_to
