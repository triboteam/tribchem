#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 11 11:01:31 2021

Workflow concerning the cutoff for the bulk, converge parameters.

@author: glosi000
"""

import os
from uuid import uuid4

import numpy as np
from fireworks import Workflow, Firework

from tribchem.highput.firetasks.encut import FT_EnergyCutoff
from tribchem.highput.database.navigator import Navigator, NavigatorMP, use_legacy_matproj
from tribchem.highput.utils.tasktools import read_default_params
from tribchem.highput.utils.vasp_tools import get_emin, VaspInputSet


currentdir = os.path.dirname(__file__) + '/../firetasks'


class EncutWF:

    @staticmethod
    def converge_encut_bulk(structure, flag, mid, collection, main_folder, 
                            comp_params={}, spec={}, formula=None, name=None, 
                            db_file=None, low_level=None, high_level='tribchem', 
                            entry='init', deformations=None, n_converge=3, 
                            kdens_default=10, bm_tol=0.01, volume_tol=0.01, 
                            encut_start=None, encut_incr=50, upsert=True, 
                            cluster_params={}):
        """
        Subworkflows that converges the Encut using a fit to an BM-EOS.
        
        Takes a given structure, computational parameters, and a optional list
        of deformations and uses these deformations to compute an
        Birch-Murgnahan equation of state for higher and higher energy cutoffs.
        Once bulk modulus and equilibrium volume do not change any longer,
        convergence is reached. Output is printed to the screen and saved in the
        high-level database where it can be queried using the mid
        of the material.

        Parameters
        ----------
        structure : pymatgen.core.structure.Structure
            The structure for which to converge the energy cutoff parameter.

        flag : str
            An identifier to find the results in the database. It is strongly
            suggested to use the proper Materials-ID from the MaterialsProject
            if it is known for the specific input structure. Otherwise use something
            unique which you can find again.

        comp_params : dict, optional
            Dictionary of computational parameters for the VASP calculations. The
            default is {}.
            
        main_folder : str
            Path to the main folder related to the upper firetask 
            FT_StartKpointsConvo. Within this folder all the folders related to 
            each VASP calculation will be created.

        spec : dict, optional
            Previous fw_spec that will be updated and/or passed on for child
            Fireworks. The default is {}.

        deformations: list of lists, optional
            List of deformation matrices for the fit to the EOS. Defaults to None,
            which results in 5 volumes from 90% to 110% of the initial volume.

        encut_start : float, optional
            Starting encut value for the first run. Defaults to the largest EMIN
            in the POTCAR.

        encut_incr : float, optional
            Increment for the encut during the convergence. Defaults to 25.

        n_converge : int, optional
            Number of calculations that have to be inside the convergence
            threshold for convergence to be reached. Defaults to 3.

        db_file : str, optional
            Full path to the db.json file that should be used. Defaults to
            None, in which case env_chk will be used in the FT. The default 
            value is None.

        cluster_params : dict, optional
            The default value is {}.

        Returns
        -------
        WF : fireworks.core.firework.Workflow
            A subworkflow intended to find the converged k_distance for a given
            structure.

        """
        
        # Set missing computational and cluster parameters to defaults
        dfl = currentdir + '/defaults.json'
        comp_params = read_default_params(default_file=dfl,
                                          default_key='comp_params', 
                                          dict_params=comp_params)
        cluster_params = read_default_params(default_file=dfl, 
                                             default_key='cluster_params', 
                                             dict_params=cluster_params)

        EncutWF._check_subwf_params(structure, flag, comp_params, cluster_params)

        # Get the largest EMIN value of the potcar and round up to the next whole 25
        if not encut_start:
            vis = VaspInputSet(structure, comp_params, 'bulk_from_scratch').get_vasp_settings()
            emin = get_emin(vis.potcar)
            encut_start = int(25 * np.ceil(emin/25))

        ft = FT_EnergyCutoff(structure=structure,
                             tag='BM group: {}'.format(str(uuid4())),
                             flag=flag,
                             mid=mid,
                             main_folder=main_folder,
                             formula=formula,
                             name=name,
                             db_file=db_file,
                             high_level=high_level,
                             collection=collection,
                             deformations=deformations,
                             encut_incr=encut_incr,
                             encut_start=encut_start, 
                             n_converge=n_converge,
                             kdens_default=kdens_default,
                             bm_tol=bm_tol,
                             volume_tol=volume_tol,
                             comp_params=comp_params,
                             upsert=upsert,
                             cluster_params=cluster_params)

        fw = Firework(ft, spec=spec, name='Encut Convergence')
        wf = Workflow([fw], name='Encut Convergence SWF of '+structure.composition.reduced_formula)
        
        return wf

    @staticmethod
    def _check_subwf_params(structure, flag, comp_params, cluster_params):
      
        # Check the consistency of the flag
        if flag.startswith('mp-') and flag[3:].isdigit():
            formula_from_struct = structure.composition.reduced_formula
            nav_mp = NavigatorMP()
            if use_legacy_matproj():
                formula_from_flag = nav_mp.get_property_from_mp(
                    mp_id=flag, 
                    properties=['pretty_formula'])
                formula_from_flag = formula_from_flag['pretty_formula']
            else:
                formula_from_flag = nav_mp.get_property_from_mp(
                    mp_id=flag, 
                    properties=['formula_pretty']).formula_pretty

            if not formula_from_flag == formula_from_struct:
                raise SystemExit('The chemical formula of your structure ({}) '
                                 'does not match the chemical formula of the flag '
                                 '(mp-id) you have chosen which corresponds '
                                 'to {}.\n'.format(formula_from_struct, formula_from_flag))

        if comp_params == {}:
            print('\nNo computational parameters have been defined!\n'
                  'Workflow will run with:\n'
                  '   ISPIN = 1\n'
                  '   ISMEAR = 0\n'
                  '   kpoint density kappa = 5000\n'
                  'We recommend to pass a comp_params dictionary'
                  ' of the form:\n'
                  '   {"use_vdw": <True/False>,\n'
                  '    "use_spin": <True/False>,\n'
                  '    "is_metal": <True/False>,\n'
                  '    "kdens": <int>}\n')

        if 'print_help' in cluster_params.keys():
            if cluster_params['print_help']:
                nav = Navigator()
                db_file = nav.path
                print('Once you workflow has finished you can access the '
                      'results from the database using this code:\n\n'
                      'import pprint\n'
                      'from tribchem.utils.database import GetBulkFromDB\n'
                      'results = GetBulkFromDB("{}", "{}", "{}")\n'
                      'pprint.pprint(results)\n'.format(flag, db_file, comp_params['functional']))
