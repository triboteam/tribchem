#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 18 11:43:35 2021

This workflow can be used to calculate the cohesion energy of a bulk

@author: glosi000
"""

from uuid import uuid4

from fireworks import Workflow, Firework

from tribchem.highput.workflows.wfcore import GeneralTribchemWF
from tribchem.highput.firetasks.structure import FT_RunVaspSimulation
from tribchem.highput.firetasks.dbtask import (
    FT_MoveTagResults, FT_MaterialInDB, FT_SaveVASPOutputs)
from tribchem.highput.firetasks.bulk import FT_CohesionEnergy
from tribchem.physics.base.solidstate import atom_in_a_box


class BulkWF(GeneralTribchemWF):

    @staticmethod
    def cohesion_energy(structure, mid, collection_bulk, collection_atoms, 
                        main_folder_bulk, main_folder_at, formula=None, 
                        name=None, db_file=None, spec={}, 
                        database_bulk='tribchem', database_atoms='tribchem',
                        entry_bulk=['structure', 'opt'], override=False, 
                        cluster_params={}, comp_params={}, 
                        check_bulk=['data', 'energy'], 
                        check_atoms=['data', 'energy'], 
                        half_kpts_first_relax=False):
        """
        Calculate the cohesion energy for a given bulk, made of a different
        number of atomic species. The input arguments: `mid`, `formula`, `name`,
        are used to identify the document containing the bulk in the Database
        and to retrieve its structure from `db_file.database.collection.entry`.
        The atomic species will be searched in the same db_file and database,
        in a collection named "atoms".

        """
        
        # Define the Fireworks for the bulk
        # ==================================================

        tag_bulk = structure.composition.reduced_formula + ' - ' + str(uuid4())

        ft_1 = FT_RunVaspSimulation(mid=mid,
                                    formula=formula,
                                    name=name,
                                    collection=collection_bulk,
                                    entry=entry_bulk,
                                    tag=tag_bulk,
                                    db_file=db_file,
                                    database=database_bulk,
                                    calc_type='bulk_from_scratch',
                                    override=override,
                                    check_key=check_bulk)

        ft_2 = FT_MoveTagResults(mid=mid,
                                 collection_from='tasks',
                                 collection_to=collection_bulk,
                                 tag=tag_bulk,
                                 db_file=db_file,
                                 database_from=None,
                                 database_to=database_bulk,
                                 entry_to=BulkWF.transfer_data_to + [['spacegroup']],
                                 entry_from=BulkWF.transfer_data_from + [['output', 'spacegroup']],
                                 check_entry=check_atoms,
                                 upsert=True,
                                 override=override,
                                 cluster_params=cluster_params)
        
        ft_3 = FT_SaveVASPOutputs(tag=tag_bulk, 
                                  folder=main_folder_bulk,
                                  custom_name='/../output')

        fw_1 = Firework([ft_1],
                        spec=spec,
                        name='Static simulation for bulk ' + structure.composition.reduced_formula)
        fw_2 = Firework([ft_2],
                        spec=spec,
                        name='Save results for bulk ' + structure.composition.reduced_formula)
        fw_3 = Firework([ft_3],
                        spec=spec,
                        name='Save VASP output files for bulk ' + structure.composition.reduced_formula)


        # Define the Fireworks for the atoms
        # ==================================================

        # Extract the elements of the structure
        at_list = []
        for at in structure.species:
            at_list.append(at.name)
        at_list = list(set(at_list))  # Find out the unique species
        
        # Create boxes for the unique species and define proper comp params
        species = atom_in_a_box(at_list, alat=15)
        atoms_params = {
            'functional': comp_params['functional'],
            'encut': comp_params['encut'],
            'is_metal': None,
            'use_spin': True,
            'vdw': None
        }
        
        fw_db_list = []
        fw_vasp_list = []
        fw_move_list = []
        fw_save_list = []
        tag_atoms = []

        for at, s in zip(at_list, species):

            # Define the tag for the given specie calculation
            tag = at + ' - ' + str(uuid4())
            tag_atoms.append(tag)

            ft_db = FT_MaterialInDB(structure=s,
                                    mid='element-'+str(at),
                                    formula=at,
                                    db_file=db_file,
                                    database=database_atoms,
                                    collection=collection_atoms,
                                    entry='structure',  
                                    comp_params=atoms_params,
                                    upsert=True,
                                    to_mongodb=True,
                                    fill_defaults=False)
            fw_db_list.append(Firework(ft_db, spec=spec,
                              name='Initialize ' + str(at) + ' atom in DB'))

            ft_vasp = FT_RunVaspSimulation(mid='element-'+str(at),
                                           formula=at,
                                           collection=collection_atoms,
                                           entry='structure',
                                           tag=tag,
                                           db_file=db_file,
                                           database=database_atoms,
                                           calc_type='atom',
                                           override=override,
                                           check_key=check_bulk)
            fw_vasp_list.append(Firework(ft_vasp, spec=spec,
                                name='Run ' + str(at) + ' atom simulation'))

            ft_move = FT_MoveTagResults(mid='element-'+str(at),
                                        collection_from='tasks',
                                        collection_to=collection_atoms,
                                        tag=tag,
                                        db_file=db_file,
                                        database_from=None,
                                        database_to=database_atoms,
                                        entry_to=BulkWF.transfer_data_to,
                                        entry_from=BulkWF.transfer_data_from,
                                        check_entry=check_atoms,
                                        override=override,
                                        upsert=True,
                                        cluster_params=cluster_params)
            fw_move_list.append(Firework(ft_move, spec=spec,
                                name='Save ' + str(at) + ' atom data'))
            
            ft_save_vasp = FT_SaveVASPOutputs(tag=tag,
                                              folder=main_folder_at,
                                              custom_name='/../output')
            fw_save_list.append(Firework(ft_save_vasp, spec=spec,
                                name='Save VASP output files for ' + str(at) + ' atom data'))
        
        # Define the Firetask to calculate the cohesion energy
        # ===================================================

        ft_cohesion = FT_CohesionEnergy(mid=mid, db_file=db_file, formula=formula, 
                                        name=name, collection_bulk=collection_bulk,
                                        collection_atoms=collection_atoms,
                                        database_bulk=database_bulk,
                                        database_atoms=database_atoms)

        fw_cohesion = Firework(ft_cohesion, spec=spec,
                               name='Calculate cohesion energy for bulk ' +\
                               structure.composition.reduced_formula)

        # Define the links between the Fireworks
        links = {fw_1: fw_2, fw_2: fw_3, fw_3: fw_cohesion}
        for db, vasp, move, save in zip(fw_db_list, fw_vasp_list, fw_move_list, fw_save_list):
            links.update({db: vasp, vasp: move, move: save, save: fw_cohesion})

        # Generate and return the Workflows
        wf = Workflow([fw_1, fw_2, fw_3, fw_cohesion] + fw_db_list + fw_vasp_list + fw_move_list + fw_save_list,
                      links, name='Calculate cohesion energy for bulk ' +\
                      structure.composition.reduced_formula + ' WF')

        return wf
