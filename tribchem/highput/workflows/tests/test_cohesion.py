#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 20 12:15:06 2021

Test the cohesion workflows.

@author: glosi000
"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'May 20th, 2021'


from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire
from pymatgen.core.structure import Structure

from tribchem.highput.firetasks.run import FT_StartCohesionBulk
from tribchem.highput.database.navigator import NavigatorMP
from tribchem.highput.workflows.cohesion_wfs import BulkWF


# Get the bulk from the online Database: Materials Project
formula = 'Mg'
mid = 'mp-110'
nav_mp = NavigatorMP()
structure, mid = nav_mp.get_low_energy_structure(
   chem_formula=formula, 
   mp_id=mid)
comp_params= {
    'functional': 'PBE',
    'use_vdw': False,
    'use_spin': True,
    'encut': 475,
    'kdens': 5.3
    }

# Cohesion workflow from run firetask
fw = Firework(FT_StartCohesionBulk(mid=mid, formula=formula,
                                   collection_bulk='PBE.bulk_elements',
                                   collection_atoms='PBE.elements',
                                   check_entry=None), 
                                   name='Start cohesion SWF - run')
wf = Workflow([fw], name='test cohesion energy - WF')

# Cohesion workflow
# wf = BulkWF.cohesion_energy(structure, mid, collection_bulk='PBE.bulk_elements',
#                             collection_atoms='PBE.elements',
#                             comp_params=comp_params, override=True)

# Launch the calculation
lpad = LaunchPad.auto_load()
lpad.add_wf(wf)
rapidfire(lpad)
