#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  4 15:51:22 2021

Test the generation of a interface and the calculation of a PES

@author: glosi000
"""

from pymatgen.core.surface import Structure, Slab
from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.workflows.interface import InterfaceWF
from tribchem.highput.firetasks.run import (
    FT_StartInterfaceMatch,
    FT_StartPES,
    FT_StartAdhesion,
    FT_StartPPES
    )


database = 'test'
mid = 'mp-110_mp-110'
mid_slab = 'mp-110'
miller = [[0, 0, 1], [0, 0, 1]]
collection = 'PBE.interface_elements'
collection_inter = collection
collection_slab = 'PBE.slab_elements'
inter_params = {
         "max_area" : 150,
         "interface_distance" : 2.5,
         "max_angle_tol" : 0.001,
         "max_length_tol" : 0.01,
         "max_area_ratio_tol" : 0.05
        }
database = 'test'


# Test 1 - Match interface
# fw = Firework(FT_StartInterfaceMatch(mid=[mid_slab, mid_slab], miller=miller,
#                                      database=database,
#                                      collection_slab=collection_slab,
#                                      collection_inter=collection_inter),
#               name='Start interface matching')
# wf = Workflow([fw], name='Match interface and calculate Hs points')
# nav = Navigator(high_level='test')
# field = nav.find_data(collection='PBE.interface_elements', fltr={'mid': 'mp-110_mp-110'})
# bot = Slab.from_dict(field['structure']['init']['bot_slab'])
# interface = Structure.from_dict(field['structure']['init']['interface'])
# top = Slab.from_dict(field['structure']['init']['top_slab'])
# bot.to('poscar', 'bot_POSCAR')
# top.to('poscar', 'top_POSCAR')
# interface.to('poscar', 'interface_POSCAR')


# Test 2 - Calculate the PES
# fw = Firework(FT_StartPES(mid=[mid_slab, mid_slab], miller=miller, 
#                               collection=collection_inter, database=database,
#                               run_vasp=False))
# wf = Workflow([fw], name='Calculate the PES')


# Test 3 - Adhesion
# fw = Firework(FT_StartAdhesion(mid=[mid_slab, mid_slab], miller=miller, 
#                                 run_vasp=False, collection=collection, 
#                                 database=database))
# wf = Workflow([fw], name='Calculate adhesion energy')


# Test 4 - PPES
fw = Firework(FT_StartPPES(mid=[mid_slab, mid_slab], miller=miller,
                           run_vasp=False, collection=collection, site='min_site',
                           database=database, fit='uber'))
wf = Workflow([fw], name='Calculate PPES')


# Launch the calculation
lpad = LaunchPad.auto_load()
lpad.add_wf(wf)
rapidfire(lpad)
