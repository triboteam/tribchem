#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 19 20:30:01 2021

Test the encut convergence workflow.

@author: glosi000
"""

from pymatgen.core.structure import Structure
from fireworks import LaunchPad
from fireworks.core.rocket_launcher import rapidfire
from fireworks import Workflow, Firework

from tribchem.highput.database.navigator import NavigatorMP
from tribchem.highput.workflows.encut_wfs import EncutWF


lpad = LaunchPad.auto_load()

# Get the bulk from the online Database: Materials Project
formula = 'Mg'
functional = 'PBE'
collection = 'PBE.bulk'
mid = 'mp-110'
nav_mp = NavigatorMP()
structure, mid = nav_mp.get_low_energy_structure(
   chem_formula='formula',
   mp_id=mid)

# SECOND TEST, RUN THE CONVERGENCE WORKFLOWS ALONE
wf_2 = EncutWF.converge_encut_bulk(structure=structure,
                                   flag=mid,
                                   mid=mid,
                                   collection=collection)

lpad.add_wf(wf_2)
# rapidfire(lpad)
