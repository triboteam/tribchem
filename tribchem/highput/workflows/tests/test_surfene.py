#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 4 11:36:15 2021

Tests for surface energy calculation.
The following tests are about the slabs generation and the following surface 
energy calculation for an aluminum system.

Author: Omar Chehaimi
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'March 4th, 2021'


from pymatgen.core.structure import Structure
from fireworks import LaunchPad
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.database.navigator import NavigatorMP
from tribchem.highput.workflows.surfene_wfs import SurfEneWF


# Get the bulk from the online Database: Materials Project
formula = 'Mg'
mid = 'mp-110'
nav_mp = NavigatorMP()
structure, mid = nav_mp.get_low_energy_structure(
   chem_formula=formula, 
   mp_id=mid)

# Get the bulk from a local simple Poscar
# structure = Structure.from_file('POSCAR')
# mid = 'custom-1'

# Surface generation tests
wf = SurfEneWF.conv_surface_energy(
    structure=structure,
    mid=mid,
    miller=[0, 0, 1],
    thick_min=2, 
    thick_max=3,
    thick_incr=1,
    parallelization=None)

# Launch the calculation
lpad = LaunchPad.auto_load()
lpad.add_wf(wf)
# rapidfire(lpad)
