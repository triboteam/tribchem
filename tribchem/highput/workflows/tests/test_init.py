#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  8 12:07:01 2021

Test the initialization of the database, by storing data in the tribchem db.

@author: glosi000
"""


import os

from fireworks import LaunchPad
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.workflows.initialize import InitWF
from tribchem.highput.utils.io_files import read_poscars


mid = ['mp-110']
db_file = None
database = 'tribchem'
collection = 'test'
currentdir = os.path.dirname(__file__)

lpad = LaunchPad.auto_load()

# First test, download data from MaterialsProject Database
wf_1 = InitWF.initialize_from_mp(mid=mid, collection=collection, entry='structure_MP',
                                 db_file=db_file, database=database, comp_params={},
                                 update_op='$set', upsert=True, to_mongodb=True)
lpad.add_wf(wf_1)
rapidfire(lpad)


# # Second test, read data from local Poscars files
# poscars = read_poscars(folder=currentdir + '/../../../structures/examples/')
# mid = ['custom-'+str(n) for n in range(len(poscars))]
# wf_2 = InitWF.initialize_from_local(poscars=poscars, mid=mid, collection=collection,
#                                     entry='structure_custom', db_file=db_file,
#                                     database=database, material_params={},
#                                     comp_params={}, update_op='$set', upsert=True)
# lpad.add_wf(wf_2)
# rapidfire(lpad)
