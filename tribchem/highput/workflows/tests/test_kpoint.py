#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 22 14:20:00 2021

Test the workflow to converge the k-points of a bulk.

Author: Omar Chehaimi (@omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'April 22nd, 2021'

from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.database.navigator import NavigatorMP
from tribchem.highput.workflows.kpoint_wfs import KpointWF
from tribchem.highput.firetasks.run import FT_StartKpointsConvo


lpad = LaunchPad.auto_load()

# Get the structure from MP database or from the psocar contained in this folder
mid = 'mp-135'
formula = 'Li'
nav_mp = NavigatorMP()
structure, mid = nav_mp.get_low_energy_structure(chem_formula=formula, 
                                                 mp_id=mid)

# Mid, functional and collection (mandatory values) and other ones are above
flag = mid
check_encut = ['data', 'encut_info']
encut_start = None
encut_incr = 50
kdens_start = 3
kdens_incr = 0.1
n_converge = 3

# Set the Database variables to upload everything
functional = 'PBE'
db_file = None
database = 'tribchem'
collection = functional + '.bulk_elements'
entry_kpts = ['structure', 'init']
miller = None  # Only useful for slabs
update_op = '$set'
upsert = False  # Create a new instance in the DB every time a document differs
override = False  # If set to true will converge everything despite it might be already done


# Initialize the WorkFlow
# wf = KpointWF.converge_kpoints_swf(structure=structure, flag=mid)


kpts = Firework(
    FT_StartKpointsConvo(mid=mid, flag=flag, db_file=db_file, 
                         collection=collection, low_level=None, formula=formula,
                         high_level=database, kdens_start=kdens_start,
                         kdens_incr=kdens_incr, n_converge=n_converge,
                         override=True, entry=entry_kpts),
    name = 'Test to converge k-points for {}'.format(mid))

wf = Workflow([kpts], name='Converge cutoff energy and k-points for {}'.format(mid))

lpad.add_wf(wf)
#rapidfire(lpad)
