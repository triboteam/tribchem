#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon June 9 11:49:00 2021

Author: Gabriele Losi (glosi000), Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi, Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'June 9th, 2021'

from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.firetasks.run import FT_StartPPES

mid = ['mp-110', 'mp-110']
miller = [[0, 0, 1], [0, 0, 1]]
collection = 'interface_elements'
functional = 'PBE'
distance_list = [-0.5, -0.25, 0.0, 0.25, 0.5, 2.5, 3.0, 4.0, 5.0, 7.5]
fw_ppes = Firework(FT_StartPPES(mid=mid, miller=miller, collection=collection, 
                                functional=functional, 
                                distance_list=distance_list), 
                   name='Start PPES test')
wf_ppes = Workflow([fw_ppes], name='PPES test')

# Launch the calculation
lpad = LaunchPad.auto_load()
lpad.add_wf(wf_ppes)
#rapidfire(lpad)