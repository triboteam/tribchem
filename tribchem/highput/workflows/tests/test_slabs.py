#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 16:41:17 2021

Test the worflow to calculate the optimal slab thickness by surface energy.

    Author: Gabriele Losi (glosi000)
    Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'April 6th, 2021'


from pymatgen.core.structure import Structure
from fireworks import LaunchPad
from fireworks.core.rocket_launcher import rapidfire
from fireworks import Workflow, Firework

from tribchem.highput.database.navigator import NavigatorMP
from tribchem.highput.workflows.slabs_wfs import SlabWF
from tribchem.highput.firetasks.run import FT_SlabOptThick


lpad = LaunchPad.auto_load()

# Get the bulk from the online Database: Materials Project
formula = 'Mg'
functional = 'PBE'
miller=[0, 0, 1]
mid = 'mp-110'
nav_mp = NavigatorMP()
structure, mid = nav_mp.get_low_energy_structure(
   chem_formula='formula',
   mp_id=mid)

# Get the bulk from a local simple Poscar
# structure = Structure.from_file('POSCAR')
# mid = 'custom-1'


# FIRST TEST, RUN THE SLAB OPT THICKNESS AS A SUBWORKFLOW
# ft = FT_SlabOptThick(mpid=mid,
#                      miller=miller, 
#                      functional=functional,
#                      thick_min=3, 
#                      thick_max=6,
#                      thick_incr=1,
#                      db_file=None,
#                      low_level=None,
#                      high_level='tribchem',
#                      vacuum=10,
#                      in_unit_planes=True,
#                      ext_index=0,
#                      conv_thr=0.01,
#                      parallelization='low',
#                      cluster_params={},
#                      override=True)
# fw = Firework(ft, name = 'Start a subworkflow with a single FT')
# wf_1 = Workflow([fw], name = 'Start a subworkflow to converge slab thickness')
# lpad.add_wf(wf_1)
# rapidfire(lpad)


# SECOND TEST, RUN THE OPTIMIZATION WORKFLOWS ALONE
wf_2 = SlabWF.conv_slabthick_surfene(structure=structure,
                                     mid=mid,
                                     miller=miller,
                                     thick_min=2,
                                     thick_max=5,
                                     thick_incr=1,
                                     db_file=None,
                                     low_level=None,
                                     high_level='tribchem',
                                     vacuum=10,
                                     in_unit_planes=True,
                                     ext_index=0,
                                     conv_thr=0.025,
                                     parallelization=None,
                                     recursion=0,
                                     cluster_params={},
                                     override=False)
lpad.add_wf(wf_2)
# rapidfire(lpad)
