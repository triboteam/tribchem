#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 15 11:30:39 2021

Test the charge displacement

@author: glosi000
"""

from pymatgen.core.surface import Slab
from fireworks import LaunchPad, Firework, Workflow
from fireworks.core.rocket_launcher import rapidfire

from tribchem.highput.database.navigator import Navigator
from tribchem.highput.firetasks.run import FT_StartChargeDisplacement


database = 'test'
mid = 'mp-110_mp-110'
mid_slab = 'mp-110'
miller = [[0, 0, 1], [0, 0, 1]]
collection = 'PBE.interface_elements'
database = 'test'


fw = Firework(FT_StartChargeDisplacement(
    mid=[mid_slab, mid_slab], miller=miller, collection=collection, database=database,
    run_vasp=False, calc_type='slab_from_scratch', operations=['-', '-'], 
    inter_entry=['structure', 'init', 'interface']), name='Test Charge Displacement')
wf = Workflow([fw], name='Calculate Charge Displacement')

nav = Navigator(high_level='tribchem')
field = nav.find_data(collection='PBE.slab_elements', fltr={'mid': 'mp-110'})
slab = Slab.from_dict(field['structure']['init'])

# Launch the calculation
lpad = LaunchPad.auto_load()
lpad.add_wf(wf)
rapidfire(lpad)
