#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thur Apr 20 11:53:31 2021

Workflow concerning the kpoint convergence.

@author: omarchehaimi
"""

import os
from uuid import uuid4

from fireworks import Workflow, Firework

from tribchem.highput.firetasks.kpoint import FT_KpointsConvo
from tribchem.highput.database.navigator import NavigatorMP, use_legacy_matproj
from tribchem.highput.utils.tasktools import read_default_params
from tribchem.highput.utils.errors import WorkflowError


currentdir = os.path.dirname(__file__) + '/../firetasks'


class KpointWF:
    """
    Collection of static methods about k-points manipulation.

    """

    @staticmethod
    def converge_kpoints(structure, mid, collection, flag, main_folder, 
                         formula=None, name=None, spec={}, db_file=None, 
                         low_level=None, high_level='tribchem', kdens_start=1, 
                         kdens_incr=0.1, n_converge=3, upsert=True, 
                         comp_params={}, cluster_params={}):
        
        """
        Subworkflows that converges the the k-mesh density via total energy.
        
        Takes a given structure, computational parameters (which includes the
        convergence criterion in eV/atom) and runs static vasp calculations with
        a denser and denser mesh (larger kdens parameter) until convergence in 
        the total energy is achieved. Output is printed to the screen and saved 
        in the high-level tribchem database where it can be queried using 
        the flag set.

        Parameters
        ----------
        structure : pymatgen.core.structure.Structure
            The structure for which to converge the K-pint grids.
        
        flag : str
            An identifier to find the results in the database. It is strongly
            suggested to use the proper Materials-ID from the MaterialsProject
            if it is known for the specific input structure. Otherwise use 
            something unique which you can find again.
        
        comp_params : dict
            Dictionary of computational parameters for the VASP calculations.
            Convergence criterion in eV/atom can be given here under the key:
            'energy_tol' and defaults to 0.001 (1meV/atom).
        
        spec : dict
            Previous fw_spec that will be updated and/or passed on for child
            Fireworks.
        
        functional : str
            Functional used in the simulation.
        
        main_folder : str
            Path to the main folder related to the upper firetask 
            FT_StartKpointsConvo. Within this folder all the folders related to each
            VASP calculation will be created.
        
        high_level : str
            High level database.
        
        kdens_start : int, optional
            Starting density value for the first run. Defaults to 500.
        
        kdens_incr : int, optional
            Increment for the k-mesh density during the convergence. Defaults to
            50. The increment might actually be larger if otherwise no new mesh
            would be formed!
        
        n_converge : int, optional
            Number of calculations that have to be inside the convergence
            threshold for convergence to be reached. Defaults to 3.
        
        db_file : str
            Full path to the db.json file that should be used. Defaults to
            None, in which case env_chk will be used in the FT.

        Returns
        -------
        WF : fireworks.core.firework.Workflow
            A subworkflow intended to find the converged k_distance for a given
            structure.

        """

        # Set missing computational and cluster parameters to defaults
        dfl = currentdir + '/defaults.json'
        cluster_params = read_default_params(default_file=dfl, 
                                             default_key='cluster_params', 
                                             dict_params=cluster_params)
        KpointWF._check_subwf_params(structure, flag, comp_params, cluster_params)

        # Define the tag
        pretty_formula = structure.composition.reduced_formula
        tag = "Kpoints group for {} - {}".format(pretty_formula, str(uuid4()))

        # Define the Firetask to run the kpoints convergence
        ft = FT_KpointsConvo(
            structure=structure,
            mid=mid,
            formula=formula,
            name=name,
            main_folder=main_folder,
            comp_params=comp_params,
            tag=tag,
            flag=flag,
            collection=collection,
            db_file=db_file,
            high_level=high_level,
            kdens_start=kdens_start,
            kdens_incr=kdens_incr,
            n_converge=n_converge,
            upsert=upsert,
            cluster_params=cluster_params)

        fw = Firework(ft, spec=spec, name='Kpoint Convergence')
        wf = Workflow([fw], 
                      name='Kpoint Convergence SWF of ' + pretty_formula)

        return wf

    @staticmethod
    def _check_subwf_params(structure, flag, comp_params, cluster_params):

        if flag.startswith('mp-') and flag[3:].isdigit():
            formula_from_struct = structure.composition.reduced_formula
            nav_mp = NavigatorMP()
            if use_legacy_matproj(): 
                formula_from_flag = nav_mp.get_property_from_mp(
                    mp_id=flag,
                    properties=['pretty_formula'])
                formula_from_flag = formula_from_flag['pretty_formula']
            else:
                formula_from_flag = nav_mp.get_property_from_mp(
                    mp_id=flag,
                    properties=['formula_pretty'])
                formula_from_flag = formula_from_flag.formula_pretty

            if not formula_from_flag == formula_from_struct:
                raise WorkflowError(
                    'The chemical formula of your structure ({}) '
                    'does not match the chemical formula of the flag '
                    '(mp-id) you have chosen which corresponds '
                    'to {}.\n'.format(formula_from_struct, formula_from_flag))

        if comp_params == {}:
            print('\n No computational parameters have been defined!\n'
                  'Workflow will run with:\n'
                  '   ISPIN = 1\n'
                  '   ISMEAR = 0\n'
                  '   ENCUT = 520\n'
                  'We recommend to pass a comp_params dictionary'
                  ' of the form:\n'
                  '   {"use_vdw": <True/False>,\n'
                  '    "use_spin": <True/False>,\n'
                  '    "is_metal": <True/False>,\n'
                  '    "encut": <int>}\n')

        if cluster_params['print_help']:
            # nav = Navigator()
            # db_file = nav.path
            print('Once you workflow has finished you can access the '
                  'results from the database using this code:\n\n'
                  'import pprint\n'
                  'from tribchem.utils.database import StructureNavigator\n'
                  'nav = StructureNavigator(db_file="localhost", high_level="tribchem")\n'
                  'results = nav.get_bulk_from_db("{}", "{}")\n'
                  'pprint.pprint(results)\n'.format(flag, comp_params['functional']))
