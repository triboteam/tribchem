from pprint import pprint, pformat
from pymatgen.io.vasp.inputs import Poscar
from fireworks import (
    Firework,
    Workflow,
    FileWriteTask
)

from tribchem.highput.utils.tasktools import (
    read_default_params, 
    select_struct_func
)
from tribchem.highput.utils.io_files import copy_output_files


def user_output(self, vasp_calc, p, dfl):

    # Get cluster params and set missing values
    cluster_params = p.get('cluster_params', None)
    cluster_params = read_default_params(default_file=dfl, 
                                            default_key="cluster_params", 
                                            dict_params=cluster_params)
    
    # Handle structure file output:
    # BE CAREFUL: it works only with structures elements
    if cluster_params['file_output']:
        
        # Recover the structure from the dictionary
        func = select_struct_func(p['struct_kind'])
        structure = func.from_dict(vasp_calc['output']['structure'])
        
        # Output to screen
        print('')
        print('Relaxed output structure as pymatgen.surface.Slab dictionary:')
        pprint(structure.as_dict())
        print('')
        
        # Define POSCAR and dictionary ouput names
        prefix = p['mp_id'] + p['struct_kind']
        if p['miller'] is not None:
            prefix = prefix + p['miller']
        poscar_name = prefix + '_POSCAR.vasp'
        structure_name = prefix + '_dict.txt'

        # Define the subworkflow to write and copy the structure
        poscar_str = Poscar(structure).get_string()
        write_ft = FileWriteTask(files_to_write=
                                    [{'filename': poscar_name,
                                    'contents': poscar_str},
                                    {'filename': structure_name,
                                    'contents': pformat(structure.as_dict())}])
        copy_ft = copy_output_files(file_list=[poscar_name, structure_name],
                                    output_dir=p['output_dir'],
                                    remote_copy=p['remote_copy'],
                                    server=p['server'],
                                    user=['server'],
                                    port=['port'])
        fw = Firework([write_ft, copy_ft],
                        name='Copy the results of structure result')
        wf = Workflow.from_Firework(fw, name='Copy structure to file')

    else:
        wf = None
    
    return wf
