#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 7 11:06:00 2021

Collection of Workflow for the adsorption of atoms and molecules on surfaces.

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'July 7th, 2021'


from uuid import uuid4

from fireworks import Workflow, Firework

from tribchem.highput.workflows.wfcore import GeneralTribchemWF
from tribchem.highput.firetasks.chemabs import (
    FT_AdsorbateSurface,
    FT_RunCleanSlab,
    FT_RunSlabAdatom,
    FT_ChemisorptionEnergy
)
from tribchem.highput.firetasks.dbtask import FT_MaterialInDB, FT_SaveVASPOutputs
from tribchem.utils.utils import FunctionTools
from tribchem.physics.base.solidstate import atom_in_a_box
from tribchem.highput.workflows.wfcore import run_vasp_simulation
from tribchem.highput.database.query import query_field_entry


call_fong = FunctionTools.call_fong


class ChemAdsWF(GeneralTribchemWF):
    """
    Collection of static methods for dealing with the adsorption of atoms or
    simple molecuels on surfaces.

    """

    @staticmethod
    def adsorbate_atom(mid, miller, adsorbate, collection, collection_slab,
                       collection_atom,  main_folder_atom, main_folder_slab,
                       main_folder_slab_atom, formula=None, name=None, 
                       db_file=None, database='tribchem', check_entry=True, 
                       both_surfaces=False, run_vasp=True, override=False, 
                       pymatgen_opt={}, comp_params={}, coverage=1, sites='all',
                       calc_type='interface_z_relax'):        
        """
        Given a Slab object, find all of its high symmetry points and chemisorbs
        of a given atomic specie on one or more site.

        """

        miller_str = ''.join([str(i) for i in miller])

        # Generate the adsorption sites and the structures of the interface
        # ========================================================
        ft_ads = FT_AdsorbateSurface.from_locals(**locals())
        fw_ads = Firework([ft_ads], name='Build surface with adsorbate: {} + {}'
                          .format(formula, adsorbate))


        # Calculation of the atom adsorbate
        # ========================================================
        cvr = str(float(coverage)).replace('.', ',')
        fws_atom, links_atom = ChemAdsWF.simulate_atom(adsorbate, comp_params,
                                                       collection_atom,
                                                       db_file, database,
                                                       folder=main_folder_atom,
                                                       override=override,
                                                       to_wf=False)

        # Calculation for the clean slab
        # ========================================================
        fw_slab = Firework(FT_RunCleanSlab.from_locals(**locals()),
                           name='Run the clean slab')


        # Calculation for the slab with the adsorbate on top of it
        # =======================================================
        fw_adslab = Firework(FT_RunSlabAdatom.from_locals(**locals()),
                             name='Run the slab+adatom')


        # Calculate the Chemisorption Energy
        # ========================================================
        fw_chemabs = Firework(
            FT_ChemisorptionEnergy.from_locals(**locals()),
            name = 'Calculate Chemisorption Energy')

        # Prepare the workflow. It is possible to avoid to make new vasp 
        # simulations, and calculate chemisorption energy with run_vasp=False        
        if run_vasp:
            # Initialize the first step
            init_list = [fw[0] for fw in [fws_atom] if fw is not None]
            init_list.extend([fw_adslab, fw_slab])
            links = {fw_ads: init_list, fw_adslab: fw_chemabs, fw_slab: fw_chemabs}
            
            # Prepare Fireworks and finish last steps for links
            fws = [fw_ads, fw_slab, fw_adslab, fw_chemabs]
            if links_atom is not None:
                links.update(links_atom)
                links.update({fws_atom[-1]: fw_chemabs})
            if fws_atom is not None:
                fws.extend(fws_atom)

            wf = Workflow(fws, links)

        else:
            wf = Workflow([fw_chemabs])

        return wf

    @staticmethod
    def simulate_atom(element, comp_params, collection, db_file=None, 
                      database='tribchem', check_entry=['data', 'energy'], 
                      alat=15, folder=None, override=False, to_wf=True):
        """
        Eventually add an atom in the database and simulate it.

        Parameters
        ----------
        adsorbate : TYPE
            DESCRIPTION.
        comp_params : TYPE
            DESCRIPTION.
        collection : TYPE
            DESCRIPTION.
        alat : TYPE, optional
            DESCRIPTION. The default is 15.
        db_file : TYPE, optional
            DESCRIPTION. The default is None.
        database : TYPE, optional
            DESCRIPTION. The default is 'tribchem'.
        check_entry : TYPE, optional
            DESCRIPTION. The default is 'data'.
        override : TYPE, optional
            DESCRIPTION. The default is False.
        to_wf : TYPE, optional
            DESCRIPTION. The default is False.

        Returns
        -------
        None.

        """

        # Check if data is already present in the database
        mid = 'element-'+element
        fltr = {'mid': mid, 'formula': element}
        field, entry = query_field_entry(fltr, collection, check_entry, db_file, database)
        if check_entry is not None:
            is_done = True if entry is not None else False
        else:
            is_done = False

        # Execute the insertion and/or run the simulation
        if not is_done or override:
            tag = element+' - '+str(uuid4())
            calc_type = 'atom'
            fws = []
            
            # Insert data in Database only if missing or override=True
            if field is None or override:
                # Create box for the adatom and define proper comp params
                atom = atom_in_a_box([element], alat=alat)[0]
                atoms_params = {
                    'functional': comp_params['functional'],
                    'encut': comp_params['encut'],
                    'is_metal': None,
                    'use_spin': True,
                    'vdw': None
                }

                # Define the insert Firetask to place the atom in the DB
                ft_1 = FT_MaterialInDB(structure=atom, mid=mid, formula=element,
                                       db_file=db_file, database=database,
                                       collection=collection, entry='structure',  
                                       comp_params=atoms_params)
                fws.append(Firework(
                    ft_1, name='Initialize ' + str(element) + ' atom in DB'))

            # Run the VASP simulation
            fw_2, fw_3 = run_vasp_simulation(
                mid=mid, formula=element,
                adsorbate=element, db_file=db_file, database=database, 
                collection=collection, entry_to=ChemAdsWF.transfer_data_to, 
                entry_from=ChemAdsWF.transfer_data_from, entry_vasp='structure',
                calc_type=calc_type, tag=tag)
            fws.extend([fw_2, fw_3])

            # Save the adatom results
            if folder is not None:
                ft_4 = FT_SaveVASPOutputs(
                    tag=tag, folder=folder,
                    custom_name='/output',
                    db_file=db_file)
                fws.append(Firework(
                    [ft_4], 
                    name='Save VASP output files for the adatom {}'.format(element)))

        else:
            return None, None

        # Build the links for the workflow
        links = {fws[-2]: fws[-1]}
        for i in range(len(fws)-1):
            links.update({fws[i]: fws[i+1]})

        if to_wf:
            return Workflow(fws, links, name='Run atom {}'.format(element))
        else:
            return fws, links
