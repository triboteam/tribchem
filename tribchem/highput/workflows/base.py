#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  4 20:51:33 2021

Generate a custom subworkflow to run vasp on the fly, useful to perform 
simulations on different materials at the same time, directly in a script.

@author: glosi000

"""

from copy import deepcopy

from fireworks import Workflow
from atomate.vasp.fireworks.core import ScanOptimizeFW, OptimizeFW, StaticFW
from atomate.vasp.powerups import add_modify_incar


def run_vasp_wf(data, calc_type, half_kpts_first_relax=False, wf_name=None):
    """
    Generate a workflow to perform a static (PBE) or a dynamic simulation 
    (PBE, SCAN) with VASP.

    Parameters
    ----------
    data : list of lists or list
        Contains all the data that is necessary to run a VASP simulation with
        Atomate Fireworks, i.e. the pymatgen structure/slab object, the vasp
        input set, and a tag. Different simulations can be run simultaneously 
        by providing a list of lists, of this kind:        
            [[structure_1, vasp_input_set_1, tag_1],
             [structure_2, vasp_input_set_2, tag_2],
             ....
             [structure_n, vasp_input_set_n, tag_n]]
    
    calc_type : str
        The type of the calculation to be run, it can be either a static or
        a dynamic simulation. The latter can be run also with SCAN.
        The possible calculation types are listed in VaspInputSet.

    half_kpts_first_relax : bool, optional
        Use half the kpoints for the first relaxation. The default is False

    wf_name : str, optional
        Name assigned to the returned workflow. The default is None.

    Returns
    -------
    WF : fireworks.core.firework.Workflow
        Workflow to relax a structure or many structures.

    """
    
    # Check types
    if not all([isinstance(x, list) for x in data]):
        data = [[d] for d in data]
    
    fw_list = []
    fw_links = {}

    for i, calc in enumerate(data):
        structure, vis, tag = calc
        
        # If the calculation is made with SCAN, only relaxations can be done
        if vis.incar.get('METAGGA') in ['Scan', 'R2scan']:
            if 'relax' in calc_type:
                
                # Set the vis parameters to be used in the SCAN optimization
                gga_vis = deepcopy(vis)
                gga_vis.user_incar_settings['ALGO'] = 'Normal'
                vis_params = {'user_incar_settings': vis.user_incar_settings,
                              'user_kpoints_settings': vis.user_kpoints_settings,
                              'user_potcar_functional': vis.potcar_functional,
                              'vdw': vis.vdw}
                
                # Call two times ScanOptimize (necessary for a bug in Atomate)
                fw_1 = ScanOptimizeFW(structure=structure, 
                                      vasp_input_set=gga_vis,
                                      vasp_input_set_params={'vdw': vis.vdw},
                                      name=tag + ' - PBEsolPreCalc')
                fw_2 = ScanOptimizeFW(vasp_input_set_params=vis_params,
                                      parents=fw_1,
                                      prev_calc_loc=True,
                                      name=tag)
            else:
                raise ValueError('SCAN simulations can only be relax!')

            # Update the workflow list and links
            fw_list.extend([fw_1, fw_2])
            fw_links[fw_1] = fw_2

        # Execute a simple PBE simulation
        else:
            if 'relax' in calc_type:
                fw = OptimizeFW(structure, name=tag, vasp_input_set=vis,
                                half_kpts_first_relax=half_kpts_first_relax)
            else:
                fw = StaticFW(structure, name=tag, vasp_input_set=vis)

            fw_list.append(fw)

    # Set the final workflow
    wf = Workflow(fw_list, fw_links, name=wf_name)

    return add_modify_incar(wf)
