#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  4 12:27:22 2021

General core workflows that can be used anywhere

@author: glosi000
"""

from fireworks import Workflow, Firework

from tribchem.highput.firetasks.structure import FT_RunVaspSimulation
from tribchem.highput.utils.errors import DFTSimulationError

class DFTSimulation:

    @staticmethod
    def run_and_save(mid, entry_to, entry_from, formula=None, name=None, miller=None, functional='PBE',
                     spec={}, db_file=None, low_level=None, high_level='tribchem',
                     relax_type='slab_pos_relax', comp_params={}, cluster_params={}, 
                     parallelization=True, override=True):

        # Check the types of the parameters and normalize them to lists
        DFTSimulationError.check_types(mid, formula, name, miller, comp_params)

        if not isinstance(mid, list):
            mid = [mid]
        n = len(mid)

        if not isinstance(formula, list):
            formula = [formula] * n
        if not isinstance(name, list):
            name = [name] * n

        for thk, n, t in zip(thickness, slab_entry, tags):

            ft_1 = FT_RunVaspSimulation(mid=mid,
                                     functional=functional,
                                     collection=functional+'.slab_data',
                                     entry=n,
                                     tag=t,
                                     db_file=db_file,
                                     database=low_level,
                                     relax_type=res,
                                     comp_params=comp_params,
                                     miller=miller,
                                     struct_kind=struct_kind,
                                     check_key=chkrel)

            ft_2 = FT_MoveTagResults(mid=mid,
                                     collection_from='tasks',
                                     collection_to=functional+'.slab_data',
                                     tag=t,
                                     db_file=db_file,
                                     database_from=low_level,
                                     database_to=low_level,
                                     miller=miller,
                                     check_entry=chkmove,
                                     entry_to=
                                        [['thickness', 'data_' + str(thk), 
                                          'output', 'structure'],
                                         ['thickness', 'data_' + str(thk), 
                                          'output', 'nsites'],
                                         ['thickness', 'data_' + str(thk), 
                                          'output', 'density'],
                                         ['thickness', 'data_' + str(thk), 
                                          'output', 'energy'],
                                         ['thickness', 'data_' + str(thk), 
                                          'output', 'energy_per_atom'],
                                         ['thickness', 'data_' + str(thk), 
                                          'output', 'bandgap'],
                                         ['thickness', 'data_' + str(thk), 
                                          'output', 'forces'],
                                         ['thickness', 'data_' + str(thk), 
                                          'output', 'stress'],
                                         ['thickness', 'data_' + str(thk), 
                                          'output', 'task_label']],
                                        entry_from=[
                                            ['output', 'structure'],
                                            ['nsites'],
                                            ['output', 'density'],
                                            ['output', 'energy'],
                                            ['output', 'energy_per_atom' ],
                                            ['output', 'bandgap'],
                                            ['output', 'forces'],
                                            ['output', 'stress'],
                                            ['task_label']
                                            ],
                                        override=False,
                                        cluster_params=cluster_params)

            fw_1 = Firework([ft_1],
                            spec=spec,
                            name='Relax structure: ' + formula + ' ' + 
                                    miller_str + ', thickness: ' + str(thk))
            fw_2 = Firework([ft_2],
                            spec=spec,
                            name='Store structure in DB: ' + formula + ' ' +
                                    miller_str + ', thickness: ' + str(thk))

            fw_relax.append(fw_1)
            fw_move.append(fw_2)
