#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Tue May 11 15:31:31 2021

Workflows dealing with the calculation of electronics properties of materials.
Currently we implemented only Charge Displacement for an interface.

"""


__author__ = 'Omar Chehaimi, Jacopo Mascitelli, Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'May 11th, 2021'


import os

from fireworks import Workflow, Firework

from tribchem.highput.workflows.wfcore import GeneralTribchemWF
from tribchem.highput.firetasks.charge import FT_ChargeDisplacement, FT_AverageCharge
from tribchem.highput.utils.manipulate_struct import  generate_chargedisp_inputs
from tribchem.highput.firetasks.dbtask import FT_SaveVASPOutputs

from tribchem.highput.workflows.wfcore import run_and_save
from tribchem.highput.utils.manipulate_struct import ManipulateStruct


currentdir = os.path.dirname(__file__) + '/../firetasks'


class ElectronicsWF(GeneralTribchemWF):
    """
    Workflows to study the electronics properties of materials.

    """
    
    @staticmethod
    def charge_displacement(interface, mid, miller, collection, operations, 
                            main_folder, db_file=None, database='tribchem', 
                            formula=None, name=None, integrate_in=None, axis=2,
                            rescale=10000, inter_type='zero', comp_params={},
                            calc_type='slab_from_scratch', site='min_site',
                            run_vasp=True, to_plot=False, check_entry=None):
        """
        Workflow to calculate the charge displacement of an interface.
        The workflow will execute three static calculations for the interface
        and its slab constituents.

        Parameters
        ----------	
        operations: list of str
            Allowed elements are '+' and '-'. The n-th element of this list is
            the operation that will be performed between the n-th and the
            (n+1)-th element of the structures that are expected.
    
        mid1 : str
            Material ID of the interface.
        
        miller : list of int, or str
            Miller indexes (h, k, l) to select the top slab orientation.
        	
        axis: int
		    Specifies along which axis to find the average value (0:x, 1:y, 2:z).

	    plot: bool, optional
		    If True, a plot of the obtained curve is shown.
         
        """
        # Define common tag prefix for all the simulations
        fname = mid.split('_')[0] + str(tuple(miller[0])) + '_' + \
                mid.split('_')[1] + str(tuple(miller[1]))
        labels = ['interface', 'bot_slab', 'top_slab']
        
        # Find the interface if the integration range is not given
        if integrate_in is None:
            inter_fixed = ManipulateStruct(interface).fix_interface(interface)
            inter_obj = ManipulateStruct(inter_fixed)
            integrate_in = inter_obj.estimate_interface(inter_type, axis)
            integrate_in = [i + interface.lattice.c/2 for i in integrate_in]


        # Define the first and second Firework : run vasp and save data
        # =====================================================================
        
        # Generate the inputs necessary to run the VASP simulations.
        # The slabs are identified and are shifted in the middle of the cell,
        # as well as the interface, in order to achieve nice plots
        structure, vis, tags = generate_chargedisp_inputs(inter_fixed, comp_params,
                                                          fname, inter_type, calc_type)

        # Generate the list of entries
        entry_from, entry_to = ElectronicsWF._select_entry('chgdisp', site=site, labels=labels, check_entry=check_entry)

        fw_1, fw_2 = run_and_save(mid, structure, vis, tags, calc_type, collection, 
                                  entry_from, entry_to, db_file, database, 
                                  formula, name, miller, 
                                  fw_names=['Run static simulations for ' + fname,
                                            'Static simulations in DB for ' + fname])


        # Define the third Firework : calculate average charge and displacement
        # =====================================================================

        ft_3 = FT_AverageCharge(mid=mid, miller=miller, collection=collection,
                                formula=formula, name=name, db_file=db_file,
                                database=database, site=site, axis='all', check_entry=check_entry)
        fw_3 = Firework(ft_3, name='Calculate average charges for ' + fname)

        ft_4 = FT_ChargeDisplacement.from_locals(**locals())     
        fw_4 = Firework(ft_4, name='Calculate charge displacement for ' + fname)

        ft_5 = FT_SaveVASPOutputs(tag=tags, folder=main_folder, 
                                  custom_name=labels, db_file=db_file)
        fw_5 = Firework(ft_5, name='Move the VASP output files for ' + fname)

        if run_vasp:
            wf = Workflow([fw_1, fw_2, fw_3, fw_4, fw_5], 
                          {fw_1: [fw_2], fw_2: fw_3, fw_3: fw_4, fw_4: fw_5},
                          name='Run simulations and calculate Charge Displacement wf')
        else:
            wf = Workflow([fw_3, fw_4], name='Charge Displacement wf')

        return wf
