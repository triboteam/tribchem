from pymatgen.core.structure import Structure
from tribchem.physics.tribology.interface_matcher import InterfaceMatcher
from tribchem.highput.utils.manipulate_struct import ManipulateStruct
#
# TG: broken create_interface, depends on MPInterface
#


poscar_names = ['Ti2C(OH)2_POSCAR', 'Ti2CF2_POSCAR', 'Ti2CO2_POSCAR']

fe = Structure.from_file('Fe_POSCAR')
fe.miller_index = [1, 1, 0]
m1 = Structure.from_file('Ti2C(OH)2_POSCAR')
m2 = Structure.from_file('Ti2CF2_POSCAR')
m3 = Structure.from_file('Ti2CO2_POSCAR')


inter_params={
      "max_area":300,
      "interface_distance":2,
      "max_angle_tol":0.1,
      "max_length_tol":0.03,
      "max_area_ratio_tol":0.05,
      "best_match":"area"
   }


interfaces = []
for i, d in enumerate(zip([m1, m2, m3], poscar_names)):
    m, pn = d
    m.miller_index = [0, 0, 1]
    
    bot_aligned, top_aligned, interface = ManipulateStruct.create_interface(
        fe.as_dict(), m.as_dict(), inter_params=inter_params)
    
    top_aligned.to('poscar', str(i)+'_'+pn)
    bot_aligned.to('poscar', str(i)+'_Fe_POSCAR')
    interface.to('poscar', str(i)+'_Fe_'+pn)
