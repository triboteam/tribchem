#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 16:45:09 2021

@author: gl
"""

import numpy as np
import matplotlib.pyplot as plt
import math
from PIL import Image

from tribchem.highput.utils.tasktools import read_json
from tribchem.physics.tribology.pes import PES
from tribchem.highput.database.query import query_field
from tribchem.highput.database.navigator import Navigator
from tribchem.highput.database.dbtools import convert_bytes_to_image

def make_pes(elements, to_fig, json, pes_type, plot_name):
    """
    Make the PES for a set of homogeneous interfaces.
    
    Parameters
    ----------
    elements : dict
        Elements of which calculate the PES.
        E.g.: elements = {'Fe': ['110', 'bcc']}
    
    to_fig : str
        Path where to save the plots.
    
    json : str
        Path where the homogeneous data are saved.
    
    pes_type : str
        Type of the PES (pes or pes_scf)
        
    plot_name : str
        Name of the plot.

    """

    for element, structure in elements.items():
        alat = json[element]['latcon [A]']
        data = np.array(json[element][structure[0]]['PES_data'])
        #cell = np.array([[0.5, -math.sqrt(2.)/2., 0.], [0.5, math.sqrt(2.)/2., 0.0], [0, 0, math.sqrt(2.)]]) * alat
        #cell = np.array([[1.0, 0., 0.], [- 0.5, math.sqrt(3) / 2., 0], [0, 0, math.sqrt(6.)]]) * alat * (1./math.sqrt(2.))
        # fcc 111
        if structure[0] == '111' and structure[1] == 'fcc':
            alat_cell = alat * (1./math.sqrt(2.))
            cell_unit = np.array([[1.0, 0., 0.], 
                                  [- 0.5, math.sqrt(3)/2., 0], 
                                  [0, 0, math.sqrt(6.)]])*alat_cell 
        # bcc 110
        if structure[0] == '110' and structure[1] == 'bcc':
            alat_cell = alat
            cell_unit = np.array([[0.5, -math.sqrt(2.)/2., 0.], 
                                  [0.5, math.sqrt(2.)/2., 0.0],
                                  [0, 0, math.sqrt(2.)]])
        # diamond 111
        if structure[0] == '111' and structure[1] == 'dia':
            alat_cell = alat * (1./math.sqrt(2.))
            cell_unit = np.array([[1.0, 0., 0.], 
                                  [- 0.5, math.sqrt(3)/2., 0], 
                                  [0, 0, math.sqrt(6.)]]) * alat_cell
        # hcp 0001
        if structure[0] == '0001' and structure[1] == 'hcp':
            alat_cell = alat
            cell_unit = np.array([[1.0, 0., 0.], 
                                  [-0.5, math.sqrt(3)/2., 0.], 
                                  [0., 0., 2*math.sqrt(2./3.)]]) * alat_cell

        pes = PES(data, cell_unit)
        pes.make_pes(replicate_of=(6, 6))
        pes.plot(extent=(2, 2), title=element+structure[0], 
                 to_fig=to_fig+element+'_regular.png')

        field = query_field(fltr={'formula': element+'_'+element}, 
                            collection='PBE.interface_elements', 
                            database='tribchem')
        data_new = np.array(field[pes_type]['energy']['pes_jm2'])
        cell_new = np.array(field['structure']['init']['short']['interface']['lattice']['matrix'])

        pes_new = PES(data_new, cell_new)
        pes_new.make_pes(replicate_of=(6, 6), orthorombize=True, theta=True)
        pes_new.plot(extent=(2, 2), title=element+structure[0], to_fig=to_fig+element+'_'+plot_name+'.png')

def plot_pes(save, pes_type, elements=None, db_file='localhost', 
             database='tribchem'):
    """
    Plot the pes saved in the database.
    
    Parameters
    ----------
    save : str
        The path where to save the plots.
    
    pes_type : str
        The pes type. It can be 'pes' or 'pes_scf'.
    
    elements : list, optional
        The list of interfaces to plot the pes.
        E.g.: ['W110_Al111', 'W110_Mo110']
        
    db_file : str, optional
        File containing the location of the database.
    
    database : str, optional
        Name of the database.

    """
    
    if not elements and isinstance(elements, list):
        raise ValueError("{} is not a list!".format(elements))
    
    nav = Navigator(db_file=db_file, high_level=database)
    
    if elements:
        inter = []
        for el in elements:
            int = nav.find_data(collection='PBE.interface_elements', 
                                fltr={'name': el})
            inter.append(int)
    else:
        inter = nav.find_many_data(collection='PBE.interface_elements', fltr={})
    
    for el in inter:
        if 'pes_scf' not in el.keys():
            continue
        
        img = convert_bytes_to_image(el[pes_type]['energy']['image'])
        img.save(save+el['name']+'.png')

# json = read_json('/home/omar/Lavoro/Unibo/TribChemProject/TribChem/tribchem/'
#                   'data/homogeneous/homogeneous_interface.json')
# to_fig = '/home/omar/Lavoro/Unibo/TribChemProject/interfaces/pes_all/'
# elements = {'Fe': ['110', 'bcc'], 'Cu': ['111', 'fcc'], 'Mg': ['0001', 'hcp'], 
#             'Ni': ['111', 'fcc'], 'Mo': ['110', 'bcc'], 
#             'Ag': ['111', 'fcc'], 'W': ['110', 'bcc'], 'Ir': ['111', 'fcc'], 
#             'Pt': ['111', 'fcc'], 'Au': ['111', 'fcc']}
# #elements = {'W': ['110', 'bcc']}
# make_pes(elements=elements, to_fig=to_fig, json=json, pes_type='pes_scf', 
#          plot_name='short')

to_fig = '/home/omar/Lavoro/Unibo/TribChemProject/interfaces/pes_all/'
plot_pes(save=to_fig, pes_type='pes_scf')