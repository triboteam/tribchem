#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 16:45:09 2021

@author: gl
"""

from tribchem.highput.utils.tasktools import read_json
from tribchem.physics.ml.homo_tools import get_ppes
import numpy as np
import matplotlib.pyplot as plt


json = read_json('homogeneous_interface.json')
data = get_ppes(json, cut=False, to_ev=False)
elements_100, elements_110, elements_111, elements_0001 = data

for e, hkl in zip(data, ['100', '110', '111', '0001']):
    folder = 'ppes_data/min/' + hkl
    for k in e:
        np.savetxt(folder + '/' + k + '.txt', e[k])


########################################################################

# Examples with a single material

for el in json: 
    for hkl in ['100', '110', '111', '0001']:
        if not hkl in json[el].keys():
            continue
        else:
            path = 'ppes_data/max/' + hkl + '/' + el + '.txt'
            
            j = json[el][hkl]
            
            vmin = np.array(j['PPES_data'])
            vmax = np.array(j['PPES_max_data'])

            # Shift to the correct position along z
            # vmin[:, 0] += j['x0_min [A]']
            vmax[:, 0] += j['x0_max [A]']

            pes = np.array(j['PES_data'])
            corrugation = max(pes[:, 2]) - min(pes[:, 2])
            
            # Set correctly the adhesion energies
            delta_e = min(vmin[:, 1]) + j['work_of_separation [J/m^2]']
            vmax[:, 1] -= (delta_e - corrugation)
            # vmin[:, 1] -= delta_e

            np.savetxt(path, vmax)


# TEST
v = np.loadtxt('ppes_data/min/111/Cu.txt')
V = np.loadtxt('ppes_data/max/111/Cu.txt')
plt.plot(v[:, 0], v[:, 1], 'o-', label='min')
plt.plot(V[:, 0], V[:, 1], 'o-', label='max')
plt.legend()
plt.xlabel('Distance $\AA$')
plt.ylabel('Energy ($J/m^2$)')
plt.show()
            