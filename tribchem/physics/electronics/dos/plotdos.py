import numpy as np
import matplotlib.pyplot as plt

shift = -1.43775  # Fermi energy, to shift the plot to 0
data = np.genfromtxt('DOS.pdos_tot')  # Read the DOS file
xdos = data[:,0] - shift
ydos = data[:,1]

# p states
for i in range(1,9):
    data1 = np.genfromtxt('DOS.pdos_atm#'+str(i)+'(P)_wfc#1(s)')
    if i == 1:
        xpdos1 = data1[:,0] - shift
        ypdos1 = data1[:,1]
    else:
        ypdos1 += data1[:,1]
        
# p states
for i in range(1,9):
    data2 = np.genfromtxt('DOS.pdos_atm#'+str(i)+'(P)_wfc#2(p)')
    if i == 1:
        xpdos2 = data2[:,0] - shift
        ypdos2 = data2[:,2:5]
    else:
        ypdos2 += data2[:,2:5]

# d states
for i in range(1,9):
    data3 = np.genfromtxt('DOS.pdos_atm#'+str(i)+'(P)_wfc#3(d)')
    if i == 1:
        xpdos3 = data3[:,0] - shift
        ypdos3 = data3[:,2:7]
    else:
        ypdos3 += data3[:,2:7]

# PLOT

# Name of the output file
name = 'AB'

# Parameters to make the plot
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelweight"] = "bold"
plt.figure(figsize=(3, 1.5), dpi=300)

# Make the plots

plt.plot(xdos,  ydos, lw=1.5, color='k', label='DOS')

plt.plot(xpdos1, ypdos1, lw=1.5, color='red', label='3s')

plt.plot(xpdos2, ypdos2[:,0], lw=1.5, label='3p$_z$')
plt.plot(xpdos2, ypdos2[:,1], lw=1.5, label='3p$_x$')
plt.plot(xpdos2, ypdos2[:,2], lw=1.5, label='3p$_y$')

plt.plot(xpdos3, ypdos3[:,0], lw=1.5, label='3d$_{z2}$')
plt.plot(xpdos3, ypdos3[:,1], lw=1.5, label='3d$_{zx}$')
plt.plot(xpdos3, ypdos3[:,2], lw=1.5, label='3d$_{zy}$')
plt.plot(xpdos3, ypdos3[:,3], lw=1.5, label='3d$_{x2-y2}$')
plt.plot(xpdos3, ypdos3[:,4], lw=1.5, label='3d$_{xy}$')

plt.plot([0, 0], [min(ydos), 15], '--', color='red', linewidth=1.5)
#plt.legend(loc='upper center', framealpha=1)  # To make the legend
plt.ylim(0, 15)
plt.xlim(-3,3)
plt.ylabel('Arbitrary units')
plt.xlabel('Energy (eV)')
plt.savefig('DOS_'+str(name)+'.pdf', dpi=300)


# Sum everything to see if you took the correct columns from pdos files
#plt.plot(xdos, ydos); plt.plot(xdos, ypdos2[:,0]+ypdos2[:,1]+ypdos2[:,2]+ypdos1)
#plt.show()