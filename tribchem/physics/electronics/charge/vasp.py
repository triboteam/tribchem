import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate as intgrt

from pymatgen.io.vasp.outputs import *
from pymatgen.core.structure import Structure
from pymatgen.io.vasp.inputs import Poscar

from atomate.utils.utils import env_chk

from fireworks import FWAction, FiretaskBase, Workflow
from fireworks.utilities.fw_utilities import explicit_serialize

"""
Version 1.0
Omar Chehaimi, 22.10.2020
omar.chehaimi2@unibo.it

This is the subworkflow for calculating the charge displacement based on the script 
developed by Gabriele Losi.
    Version 1.0
    Gabriele Losi, 5.05.2020
    gabriele.losi@unimore.it
    -------------------------------------------------------------------------------
    This script reads CHGCAR files of VASP and calculate the charge displacement.
    Delta_rho = rho_tot - rho_up - rho_down
    Files should be named: CHGCAR, CHGCAR-up, CHGCAR-down
    -------------------------------------------------------------------------------

TODO: add Functions to analyze the charges from cube files

"""


def extract_chgcar_data(chgcar_input_file):
    """
    Loads CHGCAR file in the correct format using VolumetricData.parse_file from
    pymatgen library (pymatgen.io.vasp.outputs).

    Parameters
    ----------
    chgcar_input_file: str
        This is location of the CHGCAR file.

    Returns
    -------
    chgcar: VolumetricData
        This is the CHGCAR parsed data.
    poscar: Poscar
        This is a Poscar object containing structure.
    data: 
        Actual data.
    data_aug:
        Augmentation charge data.
    """

    poscar, data, data_aug = VolumetricData.parse_file(chgcar_input_file)
    chgcar = VolumetricData(poscar, data, data_aug)
    return {'chgcar': chgcar, 'poscar': poscar, 
            'data': data, 'data_aug': data_aug}


def charge_displacement(chgcar_input_file): 
    """
    This function calculates and plots the charge displacement.

    Parameters
    ----------
    chgcar_input_file: str
        This is te location of the CHGCAR files.

    Returns
    ---------

    """

    # Extract data from the CHGCAR files
    charge_data_up = extract_chgcar_data(chgcar_input_file+'CHGCAR-up')
    charge_data_down = extract_chgcar_data(chgcar_input_file+'CHGCAR-down')
    charge = extract_chgcar_data(chgcar_input_file+'CHGCAR')

    nat = charge['poscar'].natoms[0]
    charge_structure = charge['poscar'].structure
    width = charge_structure.cart_coords[int(nat/2)][2] - charge_structure.cart_coords[int(nat/2-1)][2]
    print('Interface width: ', width)

    # Obtain the charge displacement and average along xy
    chgcar_diff = charge['chgcar'].linear_add(charge_data_down['chgcar'], - 1)
    chgcar_diff = chgcar_diff.linear_add(charge_data_up['chgcar'], - 1)
    data_orig = chgcar_diff.get_average_along_axis(2)

    chgcar_diff.data['total'] = abs(chgcar_diff.data['total'])
    data = chgcar_diff.get_average_along_axis(2)

    # Center the peak to zero
    l = len(data)
    c = charge['poscar'].structure.lattice.c
    z = np.arange(0, c, c/l)

    rho_0 = max(data[ : int(l*3/5)])
    coord_rho_0 = np.argwhere(data == rho_0)[0]

    z -= z[coord_rho_0]

    # Calculate rho_redistribution and the total_rho of the bulk
    #chgcar.data['total'] = abs(chgcar.data['total']) --> To solve this comment!
    data_tot = charge['chgcar'].get_average_along_axis(2)

    rho_tot = intgrt.simps(data_tot, z, even='avg')/c
    rho_red_all = intgrt.simps(data, z, even='avg')/c*.5

    index = np.argwhere(np.logical_and(z<=width/2., z>=-width/2.))

    if len(index) %2 != 0:
        index = np.append(index, index[-1]+1)
    rho_red = intgrt.simps(data[index], z[index], even='avg')/width

    # Plot and print the data
    volume = charge['poscar'].structure.volume 

    print('')
    print('Number of e- in bulk    = %.8f'%(rho_tot))
    print('rho_tot       [e-/A^3]  = %.8f'%(rho_tot/volume))
    print('rho_disp(z=0) [e-/A^3]  = %.8f ***'%(rho_0/volume))
    print('rho_red       [e-/A^3]  = %.8f ***'%(rho_red/volume))
    print('rho_red_all   [e-/A^3]  = %.8f'%(rho_red_all/volume))
    print('')
    
    with open('Charge_displacemet_data.txt','w') as file:
        file.write('Number of e- in bulk    = %.8f\n'%(rho_tot))
        file.write('rho_tot       [e-/A^3]  = %.8f\n'%(rho_tot/volume))
        file.write('rho_disp(z=0) [e-/A^3]  = %.8f\n'%(rho_0/volume))
        file.write('rho_red       [e-/A^3]  = %.8f\n'%(rho_red/volume))

    plt.plot(z, data_tot/volume)
    plt.title('Total Charge')
    plt.xlabel('z [A]')
    plt.savefig('Charge_tot.pdf')
    plt.show()

    plt.plot(z, data_orig/volume)
    plt.axvline(-width/2.,color='k',ls='--')
    plt.axvline(+width/2.,color='k',ls='--')
    #plt.axhline(0,color='k')
    plt.title('Charge displacement')
    plt.xlabel('z [A]')
    plt.savefig('Charge_displacement.pdf')
    plt.show()

    plt.plot(z, data/volume)
    plt.axvline(-width/2.,color='k',ls='--')
    plt.axvline(+width/2.,color='k',ls='--')
    #plt.axhline(0,color='k')
    plt.title('Charge displacement in absolute value')
    plt.xlabel('z [A]')
    plt.savefig('Charge_displacement_abs.pdf')
    plt.show()

    np.savetxt('Charge_displacement.txt', np.stack([z, data_orig], axis=1))
    np.savetxt('Charge_tot.txt', np.stack([z, data_tot], axis=1))
