#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  4 18:02:25 2021

Here some tools to extract, plot and elaborate charge data that are provided by
QEChargeAnalyzer/QEChargeProcessor.

@author: glosi000
"""

import os
from copy import deepcopy 

import numpy as np
import scipy.integrate as integrate
import matplotlib.pyplot as plt


class ElaborateCharge:
    """
    Basic class to upload data files saved by the save_charge method of the
    QEChargeAnalyzer or QEChargeProcessor classes.
    
    Attributes
    ----------
    array : np.ndarray
        Monodimensional charge data that has been averaged along a planar
        direction, e.g. xy average leads to rho(z).
    array_abs : np.ndarray
        Same as array, but the ordinate axis is in absolute value.

    Methods
    -------
    integrate(edges='all', absolute=False, rescale=1000)
        To integrate the charge between two specific points.
    plot(absolute=False, rescale=1000)
        To plot the array or the array_abs charge data.
    from_file(filename=None)
        Call the init method and initialize the object.
    
    """

    def __init__(self, filename='rho_tot.dat', rescale=1000):
        """
        Read a file containing a charge array and instantiate the class. The 
        data is remapped with an interpolation on the ordinatea, in order to 
        have a number of equally spaced points for the abscissa.

        Parameters
        ----------
        filename : str, optional
            Charge file containing the data. The default is 'rho_tot.dat'.
            
        rescale : int, optional
            Rescaling parameters. The default is 1000.

        """

        # Upload the array for charge data
        array = np.loadtxt(filename)
        
        # Rescale the array on other values
        x = np.linspace(min(array[:,0]), max(array[:,0]), num=rescale)
        y = np.interp(x, array[:,0], array[:,1])
        
        # Save the charge array attributes
        self.array = np.stack((x, y), axis=1)
        self.array_abs = np.stack((x, np.abs(y)), axis=1)
        
    
    def integrate(self, edges='all', absolute=False, rescale=1000):
        """
        Make an integration of the charge array between two points.

        Parameters
        ----------
        edges : str or tuple, optional
            Contains the points. If set to 'all' then the entire array is
            integrated, otherwise you can pass a tuple-like variable containing
            the initial and final point for the integration. The default is 'all'.
            
        absolute : bool, optional
            Decide whether to make an integration on the array in absolute 
            value or not. The default is False.

        """
        
        # Assign local data array
        array = self.array_abs.copy() if absolute else self.array.copy()
        
        # Automatically assign edges and detect data range for integration
        if edges == 'all':
            x = array[:,0]
            y = array[:,1]
            edges = (min(x), max(x))
            
        else:
            x = np.linspace(edges[0], edges[1], num=rescale)
            y = np.interp(x, array[:,0], array[:,1])
        
        # Integrate in the range
        integration = integrate.simps(y, x, even='avg')
        print('Integration in [{:.4f}, {:.4f}]: {:.6f}'
              .format(edges[0], edges[1], integration))
        
        return integration
    
    def plot(self, absolute=False):
        """
        Plotting method to plot either the charge array or its absoulte value.

        Parameters
        ----------
        absolute : bool, optional
            Decide whether to make an integration on the array in absolute 
            value or not. The default is False.

        """
        
        # Assign local data array
        array = self.array_abs.copy() if absolute else self.array.copy()
        
        # Make the plot
        plt.xlabel('z (\u212B)')
        plt.ylabel('\u03C1 (e-/\u212B)')
        plt.plot(array[:,0], array[:,1])
        plt.show()
        
    @classmethod
    def from_file(cls, filename: str):
        """
        Read a file and call the class passing it as input.

        Parameters
        ----------
        filename : str
            Charge file to be read

        Returns
        -------
        ElaborateCharge
            Instance of the class.

        """

        return cls(filename)
    
    def __add__(self, other):
        """
        Define the sum between two instances.

        """
        
        # Make the effective addiction
        res = deepcopy(self)
        res.charge_grid += other.charge_grid
        res.array_abs += other.array_abs
        
        return res

    def __sub__(self, other):
        """
        Define the substraction between two instances.

        """
        
        # Make the effective addiction
        res = deepcopy(self)
        res.array -= other.array
        res.array_abs -= other.array_abs
        
        return res
    

# =============================================================================
# FUNCTIONS
# ============================================================================= 

def grep_chargefiles(name='rho'):
    """
    Search for files named as: name_tot and name_#, in the current working
    directory.

    Parameters
    ----------
    name : str, optional
        Archetype filename to be searched. The default is 'rho'.

    Returns
    -------
    charges : list of str
        The list of files found in the current directory.

    """
    
    # Read the files from folder and isolate the correct ones
    p = os.getcwd()
    files = [f for f in os.listdir(p) if os.path.isfile(os.path.join(p, f))]
    charges = files.copy()
    for f in files:
        if not f.startswith(name+'_'):
            charges.remove(f)
        elif f != name+'_tot.dat' and f != name+'_disp.dat':
            residual = (f.split('_')[1]).split('.')[0]
            try:
                residual = int(residual)
            except:
                charges.remove(f)
    
    return charges


# =============================================================================
# MAIN
# ============================================================================= 

if __name__ == '__main__':
    
    ##########################################################################
    
    # USE THIS TO SET THE INITIAL AND FINAL POINT FOR THE INTEGRATION
    edges='all' # Integrate the whole curve. Default choice
    #x_min = -5
    #x_max = +5
    #edges = (x_min, x_max) # Integration points
    
    
    # CONTROL PANEL
    filename = 'rho_disp.dat' # To work with a single file.
    absolute = False # To integrate the charge array or its absolute value.
    name = 'rho' # Prefix to read multiple charge arrays from local folder.
    rescale = 1000 # Rescale data and integration range of this parameter.
    
    ##########################################################################
    
    # UNCOMMENT THIS IF YOU WANT TO WORK JUST WITH A SINGLE FILE, IN SIMPLE WAY 
    #chg = ElaborateCharge(filename=filename)
    #chg.integrate(edges=edges, absolute=absolute, rescale=rescale)
    #chg.plot(absolute=absolute)
    
    # EXTRACT ALL THE COMPATIBLE ELEMENTS FROM THE LOCAL FOLDER  
    paths = grep_chargefiles(name=name)
    paths.sort(reverse=True)
    integrals = []
    objs = []
    
    # Upload all the elements within paths
    for f in paths:
        # Create an istance, integrate and plot it
        element = ElaborateCharge(filename=f)
        res = element.integrate(edges=edges, absolute=absolute, rescale=rescale)
        objs.append(element) # Store the instance in a list
        integrals.append(res) # Store the value of the integrals
    
    # MAKE A SINGLE NICE PLOT WITH ALL THE DATA   
    for element in objs:
        plt.plot(element.array[:,0], element.array[:,1])

    index_disp = int(np.argwhere(np.array(paths) == name+'_disp.dat'))
    ogg = objs.copy()
    ogg.pop(index_disp)
    y = np.zeros(len(objs[1].array[:, 1]))
    for n, element in enumerate(ogg[1:]):
        y += element.array[:, 1]
    plt.plot(objs[1].array[:,0], y, label='Sum of non-interacting atoms')

    plt.xlabel('z (\u212B)')
    plt.ylabel('\u03C1 (e-/\u212B)')
    plt.show()

    # PLOT THE TOTAL CHARGE ALONE AND THE SUM OF SINGLE CHARGES
    plt.plot(objs[0].array[:,0], objs[0].array[:,1], label='Bulk')
    plt.plot(objs[1].array[:,0], y, label='Sum of non-interacting atoms')
    plt.xlabel('z (\u212B)')
    plt.ylabel('\u03C1 (e-/\u212B)')
    plt.show()
    
    # PLOT THE CHARGE DISPLACEMENT ALONE
    index_disp = int(np.argwhere(np.array(paths) == name+'_disp.dat'))
    element_disp = objs[index_disp] 
    plt.plot(element_disp.array[:,0], element_disp.array[:,1])
    plt.title('Charge Displacement')
    plt.xlabel('z (\u212B)')
    plt.ylabel('\u03C1 (e-/\u212B)')
    plt.show()
