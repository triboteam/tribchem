#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 11:18:21 2021

@author: glosi000
"""

import os
import contextlib
from pathlib import Path

import numpy as np
from tribchem.physics.electronics.charge.analyzer import (
    QEChargeAnalyzer, 
    QEChargeProcessor
    )
import matplotlib.pyplot as plt

############################## DEFINE PARAMETERS ##############################

get_inter = True
valence = True
geometry_norm = True

folders = ['fe-mos2-fe/chg', 'fe-mos2-mos2-fe/chg', 'fe-p-fe/chg', 'fe-p-p-fe/chg', 'fe-p-p-fe-sandwich/chg']
files_prefix = ['tot', 'up', 'dw']    

###############################################################################


# Run all the simulations
for fldr in folders:
    data = []
    for i in files_prefix:
        data.append(fldr + '/charge_' + str(i))

    a = QEChargeAnalyzer(chargefile=data[0], unpack=False)
    a.average_charge(get_inter=get_inter, geometry_norm=geometry_norm, valence=valence)
    aa = np.array(a.charge['axis']['z']['data'])
    plt.plot(aa[:, 0], aa[:, 1])
    plt.savefig(fldr+'/_charge_tot.pdf')
    plt.show()

    b = QEChargeProcessor(args=data)
    b.displace_charge(get_inter=get_inter, geometry_norm=geometry_norm, valence=valence)
    bb = np.array(b.charge_arrays[-1])
    plt.plot(bb[:, 0], bb[:, 1])
    plt.savefig(fldr+'/_charge_disp.pdf')
    plt.show()

    # Save data
    for n, i in enumerate(files_prefix + ['disp']):
        np.savetxt('rho_'+i+'.dat', b.charge_arrays[n])

    # Redirect stdout to files
    with open('rho_info.txt', 'w') as f:
        with contextlib.redirect_stdout(f):
            a.get_info()
            b.get_info()

    # Save pkl and json files, and rho data in fldr
    files_ls = os.listdir()
    for fls in files_ls:
        if fls.endswith('.json') or fls.endswith('.pkl') or fls.startswith('rho'):
            Path(fls).rename(fldr + "/" + fls)
    
# chg, obj = gui_chgdisp()

