#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 23 16:58:55 2020

@author: glosi000
"""

from pymatgen.io.vasp.outputs import Chgcar
import matplotlib.pyplot as plt

file='tests/Charges/CHGCAR'
chgcar = Chgcar.from_file(file)

array = chgcar.get_average_along_axis(2)
volume = chgcar.poscar.structure.volume
array /= volume

# p1 and p2 are two generic points in the lattice cell (fractional units)
p1 = [0, 0, 0.1]
p2 = [0, 0, 0.4]
line = chgcar.linear_slice(p1, p2, n=100)

plt.plot(array)
plt.show()

plt.plot(line)
plt.show()

# Implement the possibility to calculate the volume within a certain set of
# points or within a range in x,y,z (box). Based on linear slice -> value_at
# of pymatgen