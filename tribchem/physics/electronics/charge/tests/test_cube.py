#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Mon Nov 23 16:25:50 2020

Test cube modules from pymatgen.

@author: glosi000
"""

import pymatgen.io.cube as cube

# =============================================================================
# INPUT
# =============================================================================

# Change this setup to select the desired .cube files
CHG_TOT  = 'Charges/charge_tot.cube'
CHG_UP   = 'Charges/charge_up.cube'
CHG_DOWN = 'Charges/charge_down.cube'
CHG_DIFF = 'Charges/charge_diff.cube'


# =============================================================================
# Run TEST
# =============================================================================

charge_tot  = cube.Cube(CHG_TOT)
charge_up   = cube.Cube(CHG_UP)
charge_down = cube.Cube(CHG_DOWN)
charge_diff = cube.Cube(CHG_DIFF)





