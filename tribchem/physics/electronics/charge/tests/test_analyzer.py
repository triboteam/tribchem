#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 17:13:58 2020

Test the Charge Analyzers classes

@author: glosi000
"""

from tribchem.physics.electronics.charge.analyzer import (
    QEChargeAnalyzer,
    QEChargeProcessor
)
import numpy as np
import matplotlib.pyplot as plt
import time
import os


# =============================================================================
# CONTROL PANEL
# =============================================================================

path = 'Charges/'

# First and second test
chargefile_filplot = path + 'charge_tot'
phosphorene_filplot = path + 'charge_phosphorene'
chargefile_fileout = path + 'charge_tot.dat'
phosphorene_fileout = path + 'charge_phosphorene.dat'

# Third test
charge_tot = path + 'charge_tot'
charge_up = path + 'charge_up'
charge_down = path + 'charge_down'

CLEAN = True

if CLEAN:
    for f in os.listdir(path):
        if not f.endswith(".pkl") and not f.endswith(".json"):
            continue
        os.remove(os.path.join(path, f))


# =============================================================================
# Speed TEST
# =============================================================================


def time_test(chargefile):
    
    t = []
    n = 10
    
    # Test the speed when initializing by reading input charge files
    t1 = time.time()
    for i in range(n):
        a = QEChargeAnalyzer(chargefile, unpack=False)
    t2 = time.time()
    t.append(t2-t1)
    
    # Test the speed when initializing by unpacking files
    a.to_file()
    t1 = time.time()
    for i in range(n):
        a = QEChargeAnalyzer(chargefile, unpack=True)
    t2 = time.time()
    t.append(t2-t1)
    
    # Test the speed when saving data to files
    t1 = time.time()
    for i in range(n):
        a.to_file(chargefile)
    t2 = time.time()
    t.append(t2-t1)
    
    print('Speed test performing {} runs for chargefile: "{}".'
          .format(n, chargefile))
    print('')
    print('\t Reading charge file: {} s'.format(t[0]))
    print('\t Unpack data from file: {} s'.format(t[1]))
    print('\t Saving to file: {} s'.format(t[2]))
    print('')
    
# RUN TEST
time_test(chargefile_filplot)
time_test(chargefile_fileout)

# RESULTS
# Speed test performing 10 runs for chargefile: "Charges/charge_tot".

# 	 Reading charge file: 5.079824686050415 s
# 	 Unpack data from file: 0.014630556106567383 s
# 	 Saving to file: 0.5185031890869141 s

# Speed test performing 10 runs for chargefile: "Charges/charge_tot.dat".

# 	 Reading charge file: 4.962052822113037 s
# 	 Unpack data from file: 0.014965534210205078 s
# 	 Saving to file: 0.5126261711120605 s


# =============================================================================
# Integration TEST
# =============================================================================

def integration_test(chargefile, average='xy'):

    a = QEChargeAnalyzer(chargefile=chargefile, unpack=False)
    a.average_charge(average, shift=0, get_inter=True)
    a.detour_average(shift=7.258913263)
    a.get_info()
    
    data = np.array( a.charge['axis']['z']['data'] )
    plt.plot(data[:,0], data[:,1])
    plt.show()
    
    return a


def integration_diff_test(f1, f2, f3, average='xy'):
    
    c1 = QEChargeAnalyzer(chargefile=f1)
    c2 = QEChargeAnalyzer(chargefile=f2)
    c3 = QEChargeAnalyzer(chargefile=f3)
    
    c = c1 - c2 - c3
    c.average_charge(average, shift=7.258913263)
    
    data = np.array( c.charge['axis']['z']['data'] )
    plt.plot(data[:,0], data[:,1])
    plt.show()
    
    return c

# RUN TEST
a = integration_test(chargefile_filplot)
b = integration_test(chargefile_fileout)
c = integration_diff_test(charge_tot, charge_up, charge_down)

d = integration_test(phosphorene_filplot)
e = integration_test(phosphorene_fileout)


# =============================================================================
# ChargeProcessor TEST
# =============================================================================


def charge_processor_test(charges, average='xy'):
    
    c = QEChargeProcessor(charges, unpack=True)
    c.average_charge(axis='xy', get_inter=True) # shift=7.258913263
    c.displace_charge(axis='xy', save_charge=True)
    
    arrays = c.charge_arrays
    
    # Plot all the single data
    for n in range(len(arrays)-1):
        plt.plot(arrays[n][:,0], arrays[n][:,1])
    plt.show()
    
    # Plot the charge displacement
    plt.plot(arrays[-1][:,0], arrays[-1][:,1])
    plt.show()
    
    return c

# RUN TEST
f = charge_processor_test([charge_tot, charge_up, charge_down])
f.get_info()
