#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 11:32:52 2021

Base module to read the input and output file of Quantum Espresso.

@author: glosi000

"""

import os
import itertools

import numpy as np
import pandas as pd
from pymatgen.io.vasp import Poscar

from tribchem.physics.base.solidstate import pbc_coordinates, get_cell_qe, replicate
from tribchem.physics.base.units import PhysConstants


# =============================================================================
# Reading QE input
# =============================================================================

class ReadQEinput:
    """
    Read a Quantum Espresso Input File for a pw or a cp simulation.
    
    """

    flags = ['&CONTROL', '&SYSTEM', '&ELECTRONS', '&IONS', '&CELL']
    cards = ['ATOMIC_SPECIES', 'ATOMIC_POSITIONS', 'K_POINTS', 'CELL_PARAMETERS',
             'OCCUPATIONS', 'CONSTRAINTS', 'ATOMIC_FORCES', 'ATOMIC_VELOCITIES']

    def __init__(self, inputfile):
        
        # Set the path to the file
        self.fileqei = inputfile
        
        # Set the qei file as a string
        with open(inputfile, 'r') as f:
            self.rawqei = f.read()

        self.qei = self.read_inputfile()
        self._get_attributes()

    def find_line(self, value, split=None):
        return self._find_line(value, self.rawqei, split)

    def _find_line(self, value, rawfile, split=None):

        values = []
        
        if not isinstance(rawfile, list):
            rawfile = rawfile.split('\n')

        for line in rawfile: 
            if split is not None:
                line = line.split(split)
                if len(line) > 2:
                    raise IOError('Something strange happening in your raw file')
                else:
                    line = line[0]
            if value in line:
                values.append(line)

        return values
    
    def read_inputfile(self):
        return self._read_inputfile(self.fileqei)

    def _read_inputfile(self, inputfile):
        
        # Define a local function, to be used just here
        def define_unit_type(line, key):
            for typ in ['alat', 'angstrom', 'bohr', 'crystal']:
                if typ in line:
                    return {key: typ}
            return None    
        
        # Read the entire file and store it in a list
        with open(inputfile, 'r') as f:
            raw = f.read().split('\n')

        # Set the output dictionary
        qei = {}

        # Read any flags parameters and store them in a dictionary
        for f in ReadQEinput.flags:
            is_flag = False
            for line in raw:
                if not is_flag and (f in line or f.lower() in line):
                    is_flag = True
                    flag_dict = {}
                elif is_flag:
                    if not line.strip() == '/':
                        flag_line = line.strip().split('=')
                        key = flag_line[0].strip()
                        data = flag_line[1].strip().replace(',', '')\
                                                   .replace('"', '')\
                                                   .replace("'", '')
                        flag_dict.update({key: data})
                    else:
                        is_flag = False
            qei.update({f: flag_dict})
        
        # Read the cards
        for index, c in enumerate(ReadQEinput.cards):    
            
            is_card = False            

            cards_todo = ReadQEinput.cards.copy()
            for j in range(index):
                cards_todo.remove(ReadQEinput.cards[j])
            cards_todo.remove(c)
            card_dict = []
            
            for line in raw:

                array = []
                check = any([cb in line for cb in cards_todo])

                if check:
                    break

                elif c in line:
                    is_card = True
                    
                elif is_card:
                    # Clean the string and place it in a list
                    cline = []
                    cl = line.strip().split('!')
                    cl = cl[0].strip().split(' ')
                    for el in cl:
                        if el != '':
                            cline.append(el)

                    if len(cline) > 0:
                        
                        if c == 'ATOMIC_SPECIES':
                            for el, func in zip(cline, [str, float, str]):
                                array.append(func(el))
                        elif c == 'ATOMIC_POSITIONS':
                            data = [cline[0]]
                            for d in cline[1:4]:
                                data.append(self._string_algebra(d))

                            if len(cline) > 4:
                                for d in cline[4:]:
                                    data.append(int(d))
                            else:
                                for d in range(3):
                                    data.append(1)
                            array = data
                        elif c == 'K_POINTS':
                            array = [int(d) for d in cline]
                        elif c == 'CELL_PARAMETERS':
                            if len(cline) == 3:
                                for d in cline:
                                    array.append(float(d))
                        elif c in ['ATOMIC_FORCES', 'ATOMIC_VELOCITIES']:
                            for n, el in enumerate(cline):
                                func = str if n == 0 else float
                                array.append(func(el))
                        else:
                            raise ValueError('Card not implemented yet: {}'.format(c))
                    
                    if len(array) > 0:
                        card_dict.append(array)
            
            if len(card_dict) > 0:
                if c == 'K_POINTS':
                    card_dict = card_dict[0]
                qei.update({c: card_dict})
                
        # Get the type of the atomic coordinates and cell parameters
        pos_dict = define_unit_type(self._find_line('ATOMIC_POSITIONS', raw,
                                                    split='!')[0], 'pos_type')
        if pos_dict is not None:
            qei.update(pos_dict)
        try:
            cell_dict = define_unit_type(self._find_line('CELL_PARAMETERS',
                                                         raw, split='!')[0], 'cell_type')
            if cell_dict is not None:
                qei.update(cell_dict)
        except:
            pass

        return qei

    def _get_attributes(self):
        
        if not hasattr(self, 'qei'):
            raise ValueError('The classes does not have the qei attribute')
        
        # Set ibrav
        self.ibrav = int(self.qei['&SYSTEM']['ibrav'])
        
        # Set celldm
        celldm = []     
        celldm_keys = ['celldm({})'.format(n) for n in range(1, 7)]
        qei_keys = self.qei['&SYSTEM'].keys()
        for k in celldm_keys:
            if k in qei_keys:
                celldm.append(float(self.qei['&SYSTEM'][k].split('!')[0]))
            else:
                celldm.append(0)
        self.celldm = celldm
        
        # Set the type of coordinates and lattice cell
        if 'pos_type' in self.qei.keys():
            self.pos_type = self.qei['pos_type']
        else:
            self.pos_type = 'bohr'
        if 'cell_type' in self.qei.keys():
            self.cell_type = self.qei['cell_type']
        else:
            self.cell_type = 'alat'
        
        # Set the lattice cell
        if self.ibrav == 0:

            if self.cell_type == 'alat':
                factor = celldm[0] * PhysConstants.bohr_to_A
            elif self.cell_type == 'angstrom':
                factor = 1
            elif self.cell_type == 'bohr':
                factor = PhysConstants.bohr_to_A

            self.cell = np.array(self.qei['CELL_PARAMETERS']) * factor

        else:
            self.cell, _, _ = get_cell_qe(self.ibrav, celldm)
        
        # Set the number of atoms and types
        self.nat = int(self.qei['&SYSTEM']['nat'])
        self.ntyp = int(self.qei['&SYSTEM']['ntyp'])
        
        # Set the cutoffs
        self.ecutwfc = int(float(self.qei['&SYSTEM']['ecutwfc']))
        self.ecutrho = int(float(self.qei['&SYSTEM']['ecutrho']))

    def _string_algebra(self, data):
        """
        Read a string containing arithmetic operations and make them acting on
        the numbers contained there. Useful when reading pw input where the 
        atomic coordinates are given as operations.

        """
        # If the string can not be converted directly try to see if there are 
        # some operations within the string
        try:
            sm = float(data)
        except:
            sum_data = data.split('+')
            prod_data = [s.split('*') for s in sum_data]
            for i, s in enumerate(prod_data):
                prod = 1
                for p in s:
                    prod *= float(p)
                sum_data[i] = prod
            sm = 0
            for s in sum_data:
                sm += float(s)
        return sm

    def __repr__(self):
        return self.rawqei


# =============================================================================
# Reading QE output
# =============================================================================

class ReadQEpwo(ReadQEinput):
    """
    Read a pw output file.

    """

    def __init__(self, prefix):

        # Read information from the input file
        super().__init__(prefix + '.pwi')

        # Read now information from the output
        self.filepwo = prefix + '.pwo'
        self.position, self.energy, self.force, data = self.read_outputfile()

        if data is not None:
            for k, v in data.items():
                setattr(self, k, v)

    def find_line(self, value, read_from='out', split=None):

        # Set the qei file as a string
        with open(self.filepwo, 'r') as f:
            rawpwo = f.read()
        
        # Select the file to be read
        if read_from == 'out':
            readfile = rawpwo
        elif read_from == 'in':
            readfile = self.rawqei
        else:
            raise ValueError("Wrong input argument: read_from. It can be "
                             "either 'out' or 'in'")

        return self._find_line(value, readfile, split)

    def read_outputfile(self):
        return self._read_outputfile(self.filepwo, self.nat)

    def _read_outputfile(self, inputfile, nat):

        # Read the inputfile and get a list where each row is a line of the file
        with open(inputfile, 'r') as f:
            rawfile = f.read()
        rawfile = rawfile.split('\n')

        # Read the atomic positions
        positions = ReadQEpwo._read_atomic_property(rawfile, nat,
                                                    index_list=[0, 1, 2, 3],
                                                    func_list=[str, float, float, float],
                                                    fltr='ATOMIC_POSITIONS')

        # Read the forces acting on atoms
        forces = ReadQEpwo._read_atomic_property(rawfile, nat, 
                                                 index_list=[3, 6, 7, 8],
                                                 func_list=[int, float, float, float],
                                                 fltr='Forces acting on atoms')

        # Collect extra information, such as spin polarization, stresses, etc.
        data = {}   
        try:
            # Read stress
            data['stress'] = ReadQEpwo._read_atomic_property(rawfile, 3,
                                                             index_list=[3, 4, 5],
                                                             func_list=[float]*3,
                                                             fltr='total   stress  (Ry/bohr**3)')
        except:
            data = None

        # Read the energy
        energies = []        
        for line in rawfile:
            if '!' in line:
                energies.append(float(line.split(' ')[-2]))
        energies = np.array(energies) 

        return positions, energies, forces, data
    
    @staticmethod
    def _read_atomic_property(rawfile, nat, index_list=[0, 1, 2],
                              func_list = [str, float, float, float],
                              fltr='ATOMIC_POSITIONS'):
        """
        Read a list of properties related to atoms, such as atomic coordinates
        or forces. You need to provide a fltr that anticipate nat lines
        containing the property of interest.
        By setting nat, as the number of lines to be read after a line match,
        and func_list as the list of number.

        """

        properties = []
        j = 0
        to_take = False

        for line in rawfile:    
            if fltr in line:
                to_take = True
                j = 0
                tmp_prop = []

            elif to_take and j < nat:
                cline = []
                cl = line.strip().split(' ')
                for el in cl:
                    if el != '':
                        cline.append(el)

                if len(cline) > 0:
                    data = []
                    for i, func in zip(index_list, func_list):
                        data.append(func(cline[i]))

                    tmp_prop.append(data)             
                    j += 1

            elif j >= nat:
                j = 0
                to_take = False
                properties.append(np.array(tmp_prop, dtype=object))

        return properties

class ReadQEcpo(ReadQEinput):
    """
    Read the output files generated by a Car-Parrinello simulation.

    """

    def __init__(self, prefix, inputfile, check_shape=False):
        """
        Create an object reading all the files needed from a prefix.
        Differently from `ReadQEpwo`, in this case you need to provide the files
        named prefix.* contained in the `outdir` folder specified in the input.
        These files should contain: coordinates, velocities, forces, energies,
        and cell through the entire simulation.

        Required files:
            - prefix.pos
            - prefix.vel
            - prefix.for
            - prefix.evp
            - prefix.cel
 
        The code will search for these files and initialize the ones that are
        found within the prefix location. Some information, such as the number
        of atoms and species is collected from one of the cp input file.

        Parameters
        ----------
        prefix : str
            Prefix for the output of the cp simulation.

        inputfile : str
            Read one of the input file used for the cp dynamics.
        
        check_shape : bool, optional
            Decide whether to impose the number of steps to be the same for
            all the output files that are read.

        """

        # Read information from the input file if provided by the user
        self.filecpi = inputfile
        super().__init__(inputfile)

        # Read now information from the output files
        self.prefix = prefix
        #self.position, self.velocity, self.energy, self.force, self.cell = 
        self.read_cpo(check_shape)

    def read_cpo(self, check_shape=False):
        """
        Read the cpo file, by combining the prefix method to the most useful
        postfix from a cp simulation. Read the positions, velocities, forces,
        energies, and cell during the whole dynamic simulations.
        Be aware of the shape of the files, different number of steps 

        """
        
        data = self._read_cpo(self.prefix, self.nat)
        
        # Assign the data read the cp files to a dictionary
        position, velocity, force, evp, cell_dyn = data
        self.data_table = {
            'position': position,
            'velocity': velocity,
            'force': force,
            'evp': evp,
            'cell_dyn': cell_dyn}

        # Assign the time step and the saving step interval
        dt = float(self.qei['&CONTROL'].get('dt', 1).split('d')[0])
        self.dt = dt * QEVariables.dt['bare_cp']
        self.iprint = self.qei['&CONTROL'].get('iprint', 10)

        # Check the shape of the different matrices and raise error if set so
        nst = [int(d.shape[0] / v) for d, v in zip(data, [self.nat]*3 + [1, 3])]
        if check_shape:
            if not nst.count(nst[0]) == len(nst):
                raise ValueError('Wrong input: cp output files contain a ' 
                                 'different number of steps')
        self.nsteps = nst
        
        # Convert the pandas tables to lists of arrays
        n = self.nat
        self.position = [position[i:i+n].to_numpy() for i in range(0, position.shape[0], n)]
        self.velocity = [velocity[i:i+n].to_numpy() for i in range(0, velocity.shape[0], n)]
        self.force = [force[i:i+n].to_numpy() for i in range(0, force.shape[0], n)]
        
        # Extract the energies
        self.evp = evp.to_numpy()
        self.ekinc = evp['ekinc'][evp['ekinc'] != '********'].to_numpy()
        self.etot = evp['etot'].to_numpy()

        n = 3
        self.cell_dyn = [cell_dyn[i:i+n].to_numpy() for i in range(0, cell_dyn.shape[0], n)]

    def _read_cpo(self, prefix, nat):

        # Generate the name of the files to be read
        postfix = ['.pos', '.vel', '.for', '.evp', '.cel']
        outfiles = [self.prefix + p for p in postfix]
        
        # Generate the names of the coloumns to be created in pandas dataframe
        names = [['x', 'y', 'z'], ['vx', 'vy', 'vz'], ['fx', 'fy', 'fz'],
                 ['nfi', 'ekinc', 'temph', 'tempp', 'etot', 'enthal', 'econs',
                  'econt', 'vnhh', 't1', 't2'], ['x', 'y', 'z']]
        
        # Define the skip rows
        skiprows = [1, 1, 1, 0, 1]
        
        # Read the different files
        data = []
        for out, n, s in zip(outfiles, names, skiprows):
            if os.path.exists(out):
                if os.path.isfile(out):

                    f = pd.read_table(out, delim_whitespace=True, skiprows=s, names=n)
                    f.dropna(axis=0, inplace=True)
                    data.append(f)

        return data

class VaspInputToQE:
    """
    This class will read input file from VASP and try to convert them to QE
    inputs. At the moment it works only with POSCAR, in order to create a 
    temporary pwi to be read by XCRysden.
    
    """
    
    def __init__(self, path=None):
        """
        Load the VASP input files as pymatgen objects, to further work on them.

        Parameters
        ----------
        path : str or None, optional
            Path to the folder containing the VASP inputs. If nothing is provided
            it will try to read the files from CWD. The default is None.

        """
        
        if path is not None:
            if not path.endswith('/'): path += '/'
        else:
            path = './'
        self.path = path
        self.poscar = Poscar.from_file(path + 'POSCAR')

    def to_xcrysden(self):
        
        # Set cell and atomic coordinates
        cell = self.poscar.structure.lattice.matrix
        coordinates = [s.coords for s in self.poscar.structure.sites]
        
        # Set atomic species
        t = [[self.poscar.site_symbols[i]]*n for i, n in enumerate(self.poscar.natoms)]
        species = list(itertools.chain.from_iterable(t))
        
        # str(v.poscar.structure.composition).replace(' ', '')
        with open(self.path + 'qe.pwi', 'w') as f:
            f.writelines(' &CONTROL\n /\n\n')
            f.writelines(' &SYSTEM\n'
                         '    ibrav = 0\n'
                         '    nat = {}\n'
                         '    ntyp = {}\n /\n\n'.format(len(species), len(self.poscar.natoms)))
            f.writelines(' &ELECTRONS\n /\n\n')
            
            f.writelines(' CELL_PARAMETERS {angstrom}\n')
            for c in cell:
                f.writelines('    {0:.6f} {1:.6f} {2:.6f}\n'.format(c[0], c[1], c[2]))
                
            f.writelines('\n ATOMIC_SPECIES\n')
            for s in self.poscar.site_symbols:
                f.writelines('    {}\n'.format(s))

            f.writelines('\n ATOMIC_POSITIONS {angstrom}\n')
            for s, c in zip(species, coordinates):
                f.writelines('    {0}  {1:.6f} {2:.6f} {3:.6f}\n'.format(s, c[0], c[1], c[2]))
            f.writelines('\n')
            

# =============================================================================
# QE tools and utilities
# =============================================================================

def qepwo_to_xyz(prefix, replicate_of=(1, 1, 1), apply_pbc=True, nframe=1,
                 round_digit=5, to_file='default'):
    """
    Convert the coordinates of pw.x (suite Quantum Espresso) to a xyz.
    This file can be either a scf, a relax, a vc-relax, or a dynamics.

    Parameters
    ----------
    objpwo : tribchem.physics.dft.qe.ReadQEpwo
        Data of a Quantum Espresso output file (pwo), calculated with pw.x.
    
    replicate_of : tuple, optional
        Provide a tuple containing the number of replica you want to have at
        each frame along the x, y, z directions. The default is (1, 1, 1).
    
    apply_pbc : bool, optional
        Decide whether to apply the pbc at each frame. The default is True.

    nframe : int, optional
        Frame-step to be used to create a xyz file. If it is 1, all the frames
        of the simulation are collected, otherwise you export only every
        n-step of simulation. The default is 1.
    
    round_digit : int, optional
        Round the output coordinates to a given max numbers of digit. The 
        default is 5.
    
    to_file : str, optional
        Set the name of the output xyz file. As default you obtain a file named
        prefix.xyz. The default is 'default'

    Returns
    -------
    None.

    """
    
    # Read the QE file and prepare the data: coordinates, species, cell, nat
    objpwo = ReadQEpwo(prefix)
    coordinates = objpwo.position
    species = np.array(coordinates[0][:, 0].tolist() * np.prod(replicate_of))
    nat = int(objpwo.nat * np.prod(replicate_of))
    cell = objpwo.cell
    
    # Read the coordinate type and prepare the conversion factor to Angstrom
    pos_type = objpwo.pos_type
    if pos_type == 'alat':
        factor = objpwo.celldm[0] * PhysConstants.bohr_to_A
    elif pos_type == 'angstrom':
        factor = 1
    elif pos_type == 'bohr':
        factor = PhysConstants.bohr_to_A
    else:
        raise ValueError('Case not implemented yet: crystal')
    
    # Set the default file output name
    if to_file == 'default':
        to_file = prefix + '.xyz'
        
    # Start to generate the xyz file
    with open(to_file, 'w') as f:
    
        # Loop over the coordinates 
        for i, j in enumerate(range(0, len(coordinates), int(nframe))):
            
            # Select the frame coordinates and convert to Angstrom
            frame = coordinates[j]
            frame = frame[:, 1:] * factor

            # Apply periodic boundary conditions to each frame, and replicate
            if apply_pbc:
                frame = pbc_coordinates(frame, cell)
            frame, _ = replicate(frame, cell, replicate_of)
            frame = frame.round(round_digit)  # Round the digit
            
            # Print the frame header TO FILE
            f.writelines(str(nat) + '\n' + str(j + 1) + '\n')
            
            # Print the coordinates array and the species TO FILE
            data = np.column_stack((species, frame))
            for row in data:
                d = str(row).strip('[').strip(']').replace("'", '')
                f.writelines(d + '\n')


class QEVariables:
    """
    Class containing a set of default dt for Quantum Espresso simulations.

    """

    dt = {
        'bare_cp': 2.4189e-17,
        'bare_pw': 4.8378e-17,
        'cp': 10*5*2.4189e-17,
        'pw': 20*4.8378e-17,
        'femto': 1e-15,
        'pico': 1e-12,
        'nano': 1e-9,
        'micro': 1e-6,
        'milli': 1e-3
         }
