#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 15:40:32 2021

Module to help handling input and output from lammps.

@author: glosi000
"""

import numpy as np

from tribchem.physics.base.solidstate import pbc_coordinates
from tribchem.physics.dft.qe import ReadQEinput
from tribchem.physics.base.units import PhysConstants
from tribchem.highput.utils.errors import ReadParamsError


def qei_to_lammps(inputfile, lammps_data='input.data'):
    """
    Convert a Quantum Espresso input (pw or cp) to a LAMMPS data file.

    """
    
    obj = ReadQEinput(inputfile)
    cell = obj.cell
    
    element, coords = extract_coordinates(obj, cell)
    species, masses, dict_sp = extract_species(obj)
    
    with open(lammps_data, 'w') as f:
        
        # Write header and atomic numbers and types
        f.write('# Input data\n\n')
        f.write(str(obj.nat) + ' atoms\n')
        f.write(str(obj.ntyp) + ' atom types\n\n')
        
        # Write the cell
        f.write('0 ' + str(cell[0, 0]) +  ' xlo xhi\n')
        f.write('0 ' + str(cell[1, 1]) +  ' ylo yhi\n')
        f.write('0 ' + str(cell[2, 2]) +  ' zlo zhi\n\n')
        
        # Write atomic masses
        f.write('Masses\n\n')
        for sp, ms in zip(species, masses):
            f.write(str(sp) + ' ' + str(ms) +'\n')
        f.write('\n')
        
        # Write coordinates
        f.write('Atoms\n\n')
        vac_dict = {1:' ' * 5, 2: ' ' * 4, 3: ' ' * 3, 4: ' ' * 2, 5: ' '}
        for i, d in enumerate(zip(element, coords)):
            
            pre_vac = vac_dict[len(str(i))]
            c = '%.6f %.6f %.6f'%(d[1][0], d[1][1], d[1][2])
            
            f.write(pre_vac + str(i + 1) + ' ' + str(dict_sp[d[0]]) + ' ' + 
                    str(c) + '\n')

def extract_coordinates(obj, cell):
    
    # Read the coordinate type and prepare the conversion factor
    pos_type = obj.pos_type
    if pos_type == 'alat':
        factor = obj.celldm[0] * PhysConstants.bohr_to_A
    elif pos_type == 'angstrom':
        factor = 1
    elif pos_type == 'bohr':
        factor = PhysConstants.bohr_to_A
    else:
        raise ReadParamsError('Case not implemented yet: crystal')
    
    data = np.array(obj.qei['ATOMIC_POSITIONS'])
    atoms = data[:, 0]
    coordinates = pbc_coordinates(np.array(data[:, 1:4], dtype=float) * factor,
                                  cell)
    
    return atoms, coordinates

def extract_species(obj):
    
    species = []
    masses = []
    dict_sp = {}
    
    for i, element in enumerate(obj.qei['ATOMIC_SPECIES']):
        dict_sp.update({element[0]: i+1})
        species.append(i+1)
        masses.append(element[1])
    
    return species, masses, dict_sp

qei_to_lammps('tests/IO_files/md_ph_iron.pwi')
