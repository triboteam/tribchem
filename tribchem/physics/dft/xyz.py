#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 24 14:19:03 2021

Tools to read, modify, and manipulate xyz files.

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'April 24th, 2021'

import sys
import pandas as pd


class XYZReader:
    """
    Class to read xyz files and store their data for further elaboration.

    """

    def __init__(self, inputfile: str, skiprows=2, names=['atoms', 'x', 'y', 'z']):
        """
        Initialize an XYZReader object by reading the information contained in
        formatted xyz files passed as input.

        Parameters
        ----------
        inputfile : str
            XYZ input file to be read.

        """

        # Read an extarnal xyz file
        self.natoms, self.steps, self.xyz = self.read_xyz(inputfile, skiprows, names)

        # Save the unique species of the system and their numbers
        self.species = {}
        species = pd.unique(self.xyz.iloc[:, 0]).tolist()
        for el in species:
            data = self.xyz.iloc[:, 0][self.xyz.iloc[:, 0] == el]
            self.species[el] = int(data.count() / self.steps)

    @staticmethod
    def read_xyz(inputfile: str, skiprows=2, names=['atoms', 'x', 'y', 'z']):
        """
        Read an xyz file, returning the number of atoms and steps, and a pandas
        DataFrame containig the coordinates. By tuning `skiprows` and `names` it
        is possible to read whatever type of xyz file, as well as other generic
        text files containing a list of matrices of data.

        Parameters
        ----------
        inputfile : str
            XYZ input file to be read.
            
        skiprows : int or None, optional
            Number of rows that should be skipped at the beginning of inputfile.

        names : list or None, optional
            Provide the number of columns to be read from inputfile and the 
            corresponding names for the columns of the pandas DataFrame.
            The default is ['atoms', 'x', 'y', 'z'].
            Ex. By providing five elements, the function will read matrices
                formed by 5 columns within the text inputfile.

        Returns
        -------
        natoms : int
            Number of atoms in the xyz file, i.e. row numbers in each matrix.
        
        steps : int
            The total number of steps that is found, i.e. the number of times
            that the coordinates matrix is repeated within inputfile.
        
        xyz : pd.DataFrame
            Pandas dataframe containing as a table the entire set of coordinates.

        Raises
        ------
        sys.exit
            If some data is missing, the number of steps cannot be calculated
            correctly, and a sys.exit is raised.

        """

        # Read atomic coordinates from input file
        xyz = pd.read_table(inputfile, skiprows=skiprows, delim_whitespace=True,
                            names=names)
        xyz.dropna(axis=0, inplace=True)

        # Calculate the number of atoms
        natoms = XYZReader.get_natoms(inputfile)

        # Calculate the number of steps
        try:
            steps = int(xyz.shape[0] / natoms)
        except ValueError('Inputfile is corrupt, some data is missing'):
            raise sys.exit()

        return natoms, steps, xyz

    @staticmethod
    def get_natoms(inputfile: str) -> int:
        """
        Read the number of atoms of your system

        Parameters
        ----------
        inputfile : str
            Input file, allowed format: xyz.

        Returns
        -------
        int
            Number of atoms
        """

        with open(inputfile) as f:
            natoms = f.readline()
        
        print(natoms)

        return int(natoms)

    def set_dt(self, dt: float):
        """
        Set the timestep attribute. It reflects the time separation between
        two steps of the xyz file, therefore it must take into account both
        the stepframe used to save the file and the effective adopted dt.

        Parameters
        ----------
        dt : float
            Time step of the simulation.

        Examples
        --------
        Typically when doing a CP simulation with Quantum Espresso, the
        coordinates are saved any 10 steps, while the bare dt is taken as a
        multiple (usually 5 or more) of the unit: 2.4189e-17 s.
        In this case you must pass dt=10*5*2.4189e-17 as input parameter.

        """

        self.dt = dt
