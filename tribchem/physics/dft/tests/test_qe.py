#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 13:18:53 2021

Test the Quantum Espresso utilities

@author: glosi000

"""

from tribchem.physics.dft.qe import (
    ReadQEinput,
    ReadQEpwo,
    ReadQEcpo,
    VaspInputToQE,
    qepwo_to_xyz
)


# Read the pw files
folder = 'IO_files/'

prefix = folder + 'pes_5_5'
pwi = prefix + '.pwi'

objpwi = ReadQEinput(pwi)
objpwo = ReadQEpwo(prefix)


# Try to create a xyz file out of a pwo file
qepwo_to_xyz(prefix, replicate_of=(5, 5, 1), apply_pbc=True, nframe=1,
             round_digit=5, to_file='default')


# Read the cp files
cp_prefix = folder + 'CP-files/CMS2-600'
inputfile = folder + 'CP-files/cp.in'
objcpi = ReadQEinput(inputfile)
objcpo = ReadQEcpo(cp_prefix, inputfile)


# Convert a VASP POSCAR to QE pwi
v = VaspInputToQE('IO_files')
v.to_xcrysden()
