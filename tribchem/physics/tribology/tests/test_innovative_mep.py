#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 13:13:16 2021

Test the experimental GA calculation of the mep

@author: glosi000
"""

from tribchem.physics.tribology.innovative_mep import curve, function, run_ga
from tribchem.physics.tribology.simple_pes import pes, plot_pes
import matplotlib.pyplot as plt
import numpy as np

#x, y, E, xnew, ynew, Enew, rbf = pes('Structures/array_for_2dpes.dat')

limit = 5
x = np.linspace(-limit, limit, 1000)
curves = np.array([])

number = 20
sinusoids = 5
order = 2

for n in range(order):
    for k in range(sinusoids):
        
        csins = (np.random.randint(limit), np.random.randint(-limit*4, limit*4))
        for j in range(number):
        
    
            cpoly = np.zeros(order)
            if n == 0:
                cpoly[:n+1] = np.random.randint(-limit, limit)
            else:
                cpoly[:n+1] = np.random.rand(n+1) * np.random.choice([1,-1], size=n+1)

            y = curve(x, cpoly, csins)
            plt.plot(x, y)
            curves = np.append(curves, y)


plt.xlim(-5, 5)
plt.ylim(-5, 5)
plt.show()


# var = np.array([[0, limit]]*12)
# run_ga(function, 12, var)