#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 15:51:47 2021

Test the analytic PES.

@author: glosi000
"""

import numpy as np
import matplotlib.pyplot as plt

from tribchem.highput.utils.tasktools import read_json
from tribchem.physics.tribology.pes import AnalyticPES, PESModel
from tribchem.physics.tribology.ppes import PPES
from tribchem.physics.base.solidstate import reciprocal_cell_2d, reciprocal_cell


vmin = np.genfromtxt('phosphorene_ppes_ab.txt')
vmax = np.genfromtxt('phosphorene_ppes_w.txt')
vmax2 = np.genfromtxt('phosphorene_ppes_aa.txt')

# Define the g vectors of interest
cell = np.array([[3.31420142, 0], [0, 4.449501367]])
k_cell = reciprocal_cell_2d(cell)

g = []
kx = k_cell[0, :]
ky = k_cell[1, :]

grange = 1

for i in range(-grange, grange + 1):
    for j in range(-grange, grange + 1):
        if i==0 and j==0:
            pass
        else:
            print(i, j)
            g.append(i*kx + j*ky)

#g = [k_cell[0, :], k_cell[1, :], k_cell[0, :] + k_cell[1, :], k_cell[0, :] - k_cell[1, :]]
# cell_test = np.array([[3.31420142, 0, 0], [0, 4.449501367, 0], [0, 0, 1]])
# k_cell_test = reciprocal_cell(cell_test)

obj = AnalyticPES(vmin, vmax2, g)
obj.make_pes()
obj.plot()

# RUN MODEL FOR GRAPHENE

########################

grid = np.genfromtxt('../../ml/tests/graphene/energy.txt')
# grid[:, 2] = grid[:, 2] - min(grid[:, 2])
alat = 4.669095596 * .529177
# cell = np.array([ [np.sqrt(3)/2, 1/2], [np.sqrt(3)/2, -1/2]]) * alat
# k_cell = reciprocal_cell_2d(cell)

cell_3d = np.array([[np.sqrt(3)/2, 1/2, 0], [np.sqrt(3)/2, -1/2, 0], [0, 0, 1/alat]]) * alat
k_cell = reciprocal_cell(cell_3d)
print(k_cell)

kx = k_cell[0, :2]
ky = k_cell[1, :2]
g = [kx, ky, kx+ky]

pes = PESModel(cmin=PESModel.gr_min, cmax=PESModel.gr_max, g=g, n=4)
# x = np.linspace(-5, 5, 10000)
# y = np.linspace(-5, 5, 10000)
# z = 3.15 # CAMBIARE CON d eq DEL GRAFENE
#e1, e2 = pes.v(x, y, z, n=4)

a = 4
b = 4
mptx = 200j
mpty = 200j
x, y = np.mgrid[- a : a : mptx, - b : b : mpty]
z = y.copy()
for i, el in enumerate(z):
    for j, val in enumerate(el):
        z[i][j] = 3.2

x = x.flatten()
y = y.flatten()
z = z.flatten()

e1, e2 = pes.v(x, y, z, n=4)
e = e1 + e2

x = x.reshape((200, 200))
y = y.reshape((200, 200))
e = e.reshape((200, 200))

# Create the plot
fig = plt.figure(figsize=(7, 7), dpi=100)
ax = fig.add_subplot(111)
ax.set_aspect('equal')
anglerot='vertical'
zt=plt.contourf(x, y, e, 43, cmap=plt.cm.RdYlBu_r)
cbar=plt.colorbar(zt, ax=ax, orientation=anglerot, shrink=1)
cbar.set_label(r'$E_{adh} (meV/atom)$', rotation=270, labelpad=20,
                fontsize=15, family='serif')
plt.show()

########################


vmin = np.genfromtxt('Ti3C2O2_ppes_min.txt')
vmax = np.genfromtxt('Ti3C2O2_ppes_max.txt')

# Define the g vectors of interest
a = 2.974964365
cell = np.array([[1, 0], [-0.5, np.sqrt(3)/2]]) * a
#cell_3d = np.array([[np.sqrt(3)/2, 1/2, 0], [np.sqrt(3)/2, -1/2, 0], [0, 0, 1/alat]]) * a
k_cell = reciprocal_cell(cell_3d)
print(k_cell)

g = []
kx = k_cell[0, :2]
ky = k_cell[1, :2]
print(kx, ky)

grange = 1

for i in range(-grange, grange + 1):
    for j in range(-grange, grange + 1):
        if i==0 and j==0:
            pass
        else:
            print(i, j)
            g.append(i*kx + j*ky)

g = [kx, ky, kx+ky]

#g = [k_cell[0, :], k_cell[1, :], k_cell[0, :] + k_cell[1, :], k_cell[0, :] - k_cell[1, :]]
# cell_test = np.array([[3.31420142, 0, 0], [0, 4.449501367, 0], [0, 0, 1]])
# k_cell_test = reciprocal_cell(cell_test)

obj = AnalyticPES(vmin, vmax2, g)
obj.make_pes()
obj.plot()



# 2ND TEST ON Co111

# path = '/home/gl/Work/WORKFLOW/tribchem/tribchem/data/homogeneous_interface.json'
# j = read_json(path)['Fe']['110']
# vmin = np.array(j['PPES_data'])
# vmax = np.array(j["PPES_max_data"])

# # Shift to the correct position along z
# vmin[:, 0] += j['x0_min [A]']
# vmax[:, 0] += j['x0_max [A]']

# # Set correctly the adhesion energies
# delta_e = min(vmin[:, 1]) + j['work_of_separation [J/m^2]']
# vmin[:, 1] -= delta_e
# vmax[:, 1] -= delta_e

# # import matplotlib.pyplot as plt
# # plt.plot(vmin[:, 0], vmin[:, 1]) ; plt.plot(vmax[:, 0], vmax[:, 1])
# # print(min(vmax[:, 1]), min(vmin[:, 1]))

# # Make cell
# cell = np.array([[1.0, 0., 0.],
#                   [-0.5, np.sqrt(3) / 2., 0.],
#                   [0., 0., 1.61954187503]]) * 2.49249778643
# k_cell = reciprocal_cell(cell)[0:2, 0:2]
# g = [k_cell[0, :], k_cell[1, :], k_cell[0, :] + k_cell[1, :]]

# obj = AnalyticPES(vmin, vmax, g, p0=(1e15, 1, 10), maxfev=100000)
# obj.make_pes()
# obj.plot()

# a = PPES(vmin)
# a.fit(method='n4', p0=(1e15, 1, 10))
