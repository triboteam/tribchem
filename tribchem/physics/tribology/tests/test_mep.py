#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 11:00:20 2021

@author: glosi000

"""

import numpy as np
import matplotlib.pyplot as plt

from tribchem.physics.tribology.pes import PES
from tribchem.physics.tribology.mep import MEP, initialize_string, get_bs_line


rbf = lambda x, y: np.sin(x) + np.cos(y)
cell = 5 * np.eye(3)

grid = np.genfromtxt('grid.txt')[:, 1:]
cell = np.array([3.31420142, 4.449501367])
a = PES(grid, cell)
a.make_pes(replicate_of=(2, 2), orthorombize=False, theta=False, density=10, tol=1e-4)

# x, y, data = initialize_string(cell, a.rbf, optimization='bs_line')
# plt.plot(x, y)
# a.plot()

mep = MEP(a.rbf, cell)
mep.get_mep(nstepmax=1000, tol=1e-6)
mep.get_shear_strenght()

# Test the mep module (https://pypi.org/project/mep/)
# from mep.optimize import ScipyOptimizer
# from mep.path import Path
# from mep.neb import NEB
# from mep.models import LEPS

# leps = LEPS() # Test model 
# op = ScipyOptimizer(leps) # local optimizer for finding local minima
# x0 = op.minimize([1, 4], bounds=[[0, 4], [-2, 4]]).x # minima one
# x1 = op.minimize([3, 1], bounds=[[0, 4], [-2, 4]]).x # minima two

# path = Path.from_linear_end_points(x0, x1, 101, 1)  # set 101 images, and k=1
# neb =NEB(leps, path) # initialize NEB
# history = neb.run(verbose=True) # run