#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 15:55:13 2021

Test the generation of a PES

@author: glosi000

"""

import numpy as np
from tribchem.physics.tribology.pes import PES
from tribchem.physics.base.solidstate import get_cell_info

import sys

n = int(sys.argv[1])

# grid = np.genfromtxt('../../ml/tests/graphene/energy.txt')

# a = 4.669095596 * .529177
# cell = np.array([[1, 0, 0], [-1/2, np.sqrt(3)/2, 0], [1, 0, 0]]) * a
# # TO j/m^2
# area, _ = get_cell_info(cell)
# grid[:, 2] *= (1/1000 * 16.02 / area)


# ob = PES(grid, cell) 
# ob.make_pes(replicate_of=(5, 5), orthorombize=False, theta=False, density=10, tol=1e-4)
# ob.plot(extent=(3, 3), mpts=(200j, 200j), to_fig='Phosphorene')

grid = np.genfromtxt('Ti3C2O2_pes.txt')
cell = np.array([[1, 0], [-0.5, np.sqrt(3)/2]]) * 2.974964365

import time
start_time = time.time()

a = PES(grid, cell)

PES_time = time.time()

a.make_pes(replicate_of=(n, n), orthorombize=False, theta=False, density=20, tol=1e-4)

make_pes_time = time.time()

a.plot(extent=(2, 2), mpts=(200j, 200j), symm=True, to_fig='Ti3C2O2')

end_time = time.time()

init_time = PES_time - start_time
pes_time = make_pes_time - PES_time 
plot_time = end_time - make_pes_time 

print(f"{n}x{n} replicas : PES time [s] = {init_time}")
print(f"{n}x{n} replicas : make_pes time [s] = {pes_time}")
print(f"{n}x{n} replicas : PES.plot time [s] = {plot_time}")

# grid = np.genfromtxt('grid.txt')[:, 1:]
# cell = np.array([3.31420142, 4.449501367])
# a = PES(grid, cell)
# a.make_pes(replicate_of=(2, 2), orthorombize=False, theta=False, density=10, tol=1e-4)
# a.plot(extent=(1, 1), mpts=(200j, 200j), symm=True, to_fig='Phosphorene')

# grid = np.genfromtxt('ptse2.txt')
# cell = np.array([[3.7100000381, 0.000000000, 0.000000000],
#                   [-1.8550000191, 3.2129542811, 0.000000000],
#                   [0.00000000000, 0.000000000, 30.000000000]])
# b = PES(grid, cell)
# b.make_pes(replicate_of=(3, 3), orthorombize=True, theta=True, density=20, tol=1e-4)
# b.plot(extent=(2, 2), mpts=(350j, 350j), to_fig='PtSe2')
