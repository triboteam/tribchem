#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 11:02:15 2021

Simple functions for the PES

@author: glosi000
"""

__author__ = 'Gabriele Losi'
__credits__ = 'This module is based on the previous workflow, MIT'
__copyright__ = 'Copyright 2021, Prof. M. Clelia Righi, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'January 20th, 2021'

import numpy as np
from scipy.interpolate import RBFInterpolator
import matplotlib.pyplot as plt


def pes(grid_file, alat=None, scale=0., element='interface', lattice='', miller=''):
    """
    Calculation of the PES
    array_accumulo            : high symmetry points with corresponding energy
    structure                : aiida object with lattice cell information
    element, lattice, miller : information about the cell
    scale                    : rescaling factor of lattice due to pressure
    """
    
    array_accumulo = np.genfromtxt(grid_file)
    
    if alat is None:
        alat_x = 3.331843818
        alat_y = 4.444098655
    
    expansion = 1.0 + scale
    x = np.array(array_accumulo[:,1])*expansion
    y = np.array(array_accumulo[:,2])*expansion
    E = np.array(array_accumulo[:,3])
	grid = np.vstack((x, y)).T
    
    fact = 1
    xnew, ynew = np.mgrid[-fact*alat_x :fact*alat_x:200j,-fact*alat_y:fact*alat_y:346j]
	gridnew = np.vstack((xnew, ynew)).T

    rbf  = RBFInterpolator(grid, E, kernel='cubic')
    Enew = rbf(gridnew)

    return x, y, E, xnew, ynew, Enew, rbf


def plot_pes(xnew, ynew, Enew, alat=None, path='./', element='interface', miller=''):

    if alat is None:
        alat_x = 3.331843818
        alat_y = 4.444098655    

    #alat_x = alat[0]
    #alat_y = alat[1]
    fact=1.
    level= 43
    fig = plt.figure(figsize=(7, 7), dpi=100)
    ax = fig.add_subplot(111)
    ax.set_aspect('equal')
    anglerot='vertical'
    shrin=1.
    zt1=plt.contourf(xnew, ynew, Enew, level, extent=(-fact*alat_x,fact*alat_x,-fact*alat_y,fact*alat_y), cmap=plt.cm.RdYlBu_r)
    cbar1=plt.colorbar(zt1,ax=ax,orientation=anglerot,shrink=shrin)
    cbar1.set_label(r'$E_{adh} (J/m^2)$', rotation=270, labelpad=20,fontsize=15,family='serif')
    plt.title("PES for " + element + miller, fontsize=18,family='serif')
    ax.quiver(0. , 0., 1., 0.,scale=1.,scale_units='inches',width=0.01,color='white')
    ax.quiver(0. , 0., 0., 1.,scale=1.,scale_units='inches',width=0.01,color='white')
    ax.plot(0.,0.,'w.',ms=7)
    ax.text(0.5,-0.5,'[1 0 1]',rotation='horizontal',color='white', fontsize=14)
    ax.text(-0.5,1.,'[1 2 1]',rotation='vertical',color='white', fontsize=14)
    ax.axis([-fact*alat_x,fact*alat_x,-fact*alat_y,fact*alat_y])
    plt.xlabel(r"distance ($\AA$)",fontsize=12,family='serif')
    plt.ylabel(r"distance ($\AA$)",fontsize=12,family='serif')

    for zt1 in zt1.collections:
       zt1.set_edgecolor("face")
       zt1.set_linewidth(0.000000000001)

    plt.savefig(path + "PES" + str(element) + str(miller) + ".pdf")
