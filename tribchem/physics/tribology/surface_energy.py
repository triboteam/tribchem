#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 11:00:54 2021

Functions to calculate the surface energy of a slab.

The module contains the following functions:
    - calculate_surface_energy

    Author: Gabriele Losi (glosi000)
    Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 22nd, 2021'


import numpy as np
from scipy import stats
import matplotlib.pyplot as plt


def calculate_surface_energy(output_list, sym_surface=True):
    """
    Calculate the surface energy by passing a list containing all the dictionary
    with the output data. The first element is treated to be the bulk oriented
    along a specific miller index direction.

    Parameters
    ----------
    output_list : list of dict
        List of dictionaries where each one contains the output of a VASP
        simulation. Basically output_list can be whatever dictionary, fundamental
        keys that should be present are: structure, energy, energy_per_atom.
    
    sym_surface : bool, optional
        If the surfaces have to be considered symmetric or not.
    
    Returns
    -------
    surfene : list of floats
        List containing the surface energies calculated from output_list.
    """

    # Take out the energy per atom of the bulk    
    energy_per_atom = output_list[0]['energy_per_atom']

    # Loop over the slab elements of output_list and calculate surface energy
    surfene = np.array([])
    for d in output_list[1:]:
        energy = d['energy']
        nsites = d['nsites']
        surfene = np.append(surfene, energy - energy_per_atom * nsites)

    # Divide the surface energies by two if the surfaces are symmetric
    if sym_surface:
        surfene /= 2.

    return np.array(surfene)

def fit_surface_energy(field, to_plot=False):
    """
    Fit the list of energies and corresponding number of layers to:    
        E_slab = sigma + N * E_bulk
    The sigma and E_bulk parameters will be used as fitting values,
    sigma is returned.
    

    Parameters
    ----------
    energies : list of float
        List of energies to be fitted
    n : list of int
        List with the number of layers, corresponding to the energies.

    Returns
    -------
    sigma
        Estimation of the surface energy of the material.

    """
    
    # Extract the number of layers and the energies
    n = []
    energy = []
    
    # Identify the list with the number of layers
    n = [int(k.split('_')[1]) for k in field['thickness'].keys() if k.startswith('data')]
    n.remove(0)
    n.sort()
    
    # Collect the list of the total energies
    energy = []
    for k in n:
        energy.append(field['thickness']['data_'+str(k)]['output']['energy'])

    # Make the fit    
    slope, intercept, _, _, _ = stats.linregress(x=n, y=energy)
    ebulk, sigma = slope, intercept/2.
    
    if to_plot:
        plt.plot(n, energy, 'o')
        x = np.linspace(min(n), max(n))
        plt.plot(x, x*slope + intercept)
        plt.show()

    return sigma, ebulk, np.column_stack((n, energy))
    