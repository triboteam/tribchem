#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 12 11:58:29 2021

Module containing classes and tools to manipulate structures, such as molecules,
bulks, slabs, interfaces.

@author: glosi000

"""

import os

import numpy as np
from ase.build import molecule
from pymatgen.core.surface import Structure, Slab
from pymatgen.io.ase import AseAtomsAdaptor
from pymatgen.core.sites import PeriodicSite
#from mpinterfaces.transformations import get_aligned_lattices

from tribchem.physics.base.solidstate import get_cell_info
from tribchem.highput.utils.tasktools import read_default_params



class ManipulateStructure:
    """
    Class containing different base methods and tools to modify and manipulate
    pymatgen dictionary and objects, i.e. structures and slabs.

    """

    def __init__(self, structure):
        """
        Read a pymatgen object related to an atomic structure within a lattice
        cell and store relevant information. You can pass either the pymatgen
        object itself or the corresponding dictionary (generated with .as_dict
        method). 

        Parameters
        ----------
        structure : Structure or Slab objects (pymatgen), or dicts
            Atomic structure to be read.

        """

        # Check the types of structure and slab
        if isinstance(structure, (Structure, Slab)):
            structure = structure.as_dict()
        elif not isinstance(structure, dict):
            raise ValueError('Wrong type for input argument: structure')
        
        self.structure = structure
        self.__converter = Slab.from_dict if structure['@class']=='Slab' else Structure.from_dict

    @property
    def structure_object(self):
        
        return self.__converter(self.structure)
    
    def as_slab(self, slab, as_dict=False, transfer_keys=['miller_index', 'shift',
                'oriented_unit_cell', 'energy', 'reconstruction', 'scale_factor']):
        """
        Convert a structure object to a corresponding slab, only works for a
        pymatgen Structure.

        """
        
        # Check the type
        if self.structure['@class'] == 'Slab':
            print('The method as_slab cannot be called on a Slab')
        else:
            s = structure_to_slab(self.structure, slab, as_dict, transfer_keys)
            return s
        

class ManipulateSurface(ManipulateStructure):
    """
    Class containing specific methods and tools to work on surfaces.
    
    """
    
    def __init__(self, structure):
        super().__init__(structure)


    def estimate_interface(self, inter_type='zero', axis=2):
        """
        Estimate the edges of the interface of the structure along axis.
        The kind of infert can be specified with inter_type.
        
        Options:
            - "zero": The interface must be at z = 0, the closest atoms above
                      and below are considered to delimit the interface.
            - "even": The interface is considered to be between the n/2 and 
                      n/2+1 atoms. Only works for an even number of atoms.

        Parameters
        ----------
        inter_type : str, optional
            Type of the interface. The default is 'zero'.
        
        axis : int, optional
            Axis orthogonal to the interface, a common choice is to choose a
            vertically oriented structure, i.e. interface in the xy plane.
            Integers values refer to the matrix columns (x: 0, y: 1, z: 2).
            The default value is 2.
        
        Returns
        -------
        list :
            List of two elements containing the lower and upper delimiter
            of the interface, along axis.

        """
        
        # Extract the coordinates along axis from the dictionary
        sites = np.array([s['xyz'] for s in self.structure['sites']])
        coords = np.sort(sites[:, axis])
        
        # Identify the elements of the structure
        if inter_type == 'zero':  # Default case, interface at z=0
            low = max(coords[coords < 0])
            up  = min(coords[coords > 0])

        elif inter_type == 'even':  # Interface is between n/2 and n/2+1 atom
            n = len(coords)

            if n % 2 == 0:
                low = coords[int(n/2)]
                up  = coords[int(n/2) + 1]
            else:
                raise ValueError('Your structure as an odd number of atoms, '
                                 'inter_type="even" does not make sense for it')

        return [low, up]

    def estimate_vacuum(self, kind='default', axis=2):
        """
        Estimate the vacuum for the cell along axis. Allowed estimation kinds:
            - 'default': The structure is fully inside to the cell and the
                         coordinates are continuos (no negative, no outside cell)
            - 'zero': The interface is centered on the zero for axis, so the 
                      two constituent slabs are at the cell extremities.
        
        WARNING: This works only with "squared" lattices, such as cubic and
        orthorombic-like structures.

        Parameters
        ----------
        kind : str, optional
            Kind of estimation. The default is 'default'.

        """
        
        # Group the coordinates of the atoms along axis
        c_axis = []
        sites = self.structure['sites']
        for s in sites:
            c_axis.append(s['xyz'][axis])
        c_axis = np.array(c_axis)

        # Estimate the vacuum for the various cases
        edge = self.structure['lattice']['matrix'][axis][axis]
        if kind == 'default':
            vacuum = edge - abs(max(c_axis) - min(c_axis))

        elif kind == 'zero':
            vacuum = edge - abs(max(c_axis[c_axis > 0]) - min(c_axis[c_axis < 0]))
        
        return vacuum

    def decompose_structure(self, inter_type='zero', as_dict=False):
        """
        Extract the constituents of an interface object, based on the interface 
        type that is passed. 
        
        The options provided for `inter_type` are:
            - "zero" : The interface is set on the bottom xy plane of the cell.
                       Top slab: atoms with z > 0. Bot slab: atoms with z < 0.
                       It is the default choice, returns [bot_slab, top_slab].
    
        Parameters
        ----------
        inter_type : str, optional
            Type of the interface. The default is 'zero'.
        
        as_dict : bool, optional
            Dedice whether to return dictionaries or pymatgen objects.
        
        Returns
        -------
        result : list
            Variable list, containing the elements composing the interface.

        """

        # Extract the sites from the dictionary
        sites = self.structure['sites']

        # Identify the elements of the structure
        if inter_type == 'zero':  # Default case, interface at z=0
            bot_sites = []
            top_sites = []
            for i, site in enumerate(sites):
                if site['xyz'][2] > 0:
                    top_sites.append(site)
                elif site['xyz'][2] < 0:
                    bot_sites.append(site)
                else:
                    raise ValueError('One atom found with z=0, perhaps '
                                     'inter_type=0 is not the right choice')

            # Decompose the structure
            bot_slab = self.structure.copy()
            top_slab = self.structure.copy()
            bot_slab['sites'] = bot_sites
            top_slab['sites'] = top_sites
            result = [bot_slab, top_slab]

        if not as_dict:
            result = [self.converter(r) for r in result]
        
        return result

    def make_vacuum_around(self, vacuum, vac_ext='mid', vac_kind='default', 
                           axis=2, inplace=False, as_dict=False):
        """
        Set the vacuum of a cell along axis. It increases or reduces the cell size
        in order to have a certain level of vacuum.
        
        WARNING: It works only for orthorombic-like and cubic cells.
    
        Parameters
        ----------
        structure : pymatgen structure or dict
            Structure to be modified.
            
        vac_ext : str, optional
            How to extend the vacuum around axis, allowed values are: 
                'up': Increase or decrease the vacuum in the direction of positive and
                      higher values for the atomic coordinates along axis.
                'dw': The same, but increases the cell from the side of lower
                      coordinate values.
                'mid': Try to modify the vacuum equally from both the sides of
                       the lattice cell, along the axis direction. It is defalt
        
        vac_kind : float or int, optional
            Kind of vacuum, and so of atomic structure, that is provided. It is
            used to estimate the initial existing vacuum of the cell.
            
        inplace : bool, optional
            If set to True, the instance object is modified, otherwise a new
            modified structure is returned.
        
        as_dict : bool, optional
            Decide to return a pymatgen object or a dict. It only makes sense
            in the case inplace=False. The default is False.
    
        """

        # Estimate and evaluate vacuum of the cell along axis
        vacuum_old = self.estimate_vacuum(vac_kind, axis)
        cell_edge = np.array(self.structure.copy()['lattice']['matrix'])[axis, axis]
        slab_thick = cell_edge - vacuum_old
        
        # Apply the vacuum to the cell structure
        structure = self.structure.copy()
        lattice = structure['lattice']
        # Change the axis length
        k = 'a' if axis == 0 else 'b' if axis == 1 else 'c'
        lattice[k] = slab_thick + vacuum
        # Change the lattice matrix
        new_cell_edge = slab_thick + vacuum
        lattice['matrix'][axis][axis] = new_cell_edge
        # Change the volume
        lattice['volume'], _ = get_cell_info(lattice['matrix'])
        structure['lattice'] = lattice

        # Rescale xyz coordinates, to stay coherent with the new lattice edge
        sites = structure['sites'].copy()
        for i, s in enumerate(sites):
            v = s['abc'][axis] * cell_edge / new_cell_edge
            sites[i]['abc'][axis] = v
        structure['sites'] = sites

        if inplace:
            self.structure = structure
        else:
            if not as_dict:
                structure = self.converter(structure)
            return structure

    def _rescale_sites(self, structure, cell_edge, new_cell_edge, axis=2):
        sites = structure['sites'].copy()
        for i, s in enumerate(sites):
            sites[i]['abc'][axis] = s['abc'][axis] * cell_edge / new_cell_edge
        structure['sites'] = sites
        return structure
    
    @staticmethod
    def create_interface(bot_slab, top_slab, vacuum=15, inter_params={}):
        """
        TG: remove mpinterface dependency (this method is currently not used).

        Match an interface out of two slabs passed as input.

        Parameters
        ----------
        bot_slab : TYPE
            DESCRIPTION.
        top_slab : TYPE
            DESCRIPTION.
        vacuum : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        
        # Define the variables to be returned
        bot_aligned = None
        top_aligned = None
        interface = None

        # Set missing interface_params
        dr = os.path.dirname(__file__) + '/../../highput/firestasks/firetasks'
        dfl = dr + '/defaults.json'
        inter_params = read_default_params(dfl, 'inter_params', inter_params)
        
        # Add safe vacuum to the slabs and try to align them
        d = inter_params.copy()
        _ = d.pop('interface_distance')
        bot_slab = ManipulateStructure(bot_slab).make_vacuum_around(vacuum=50)
        top_slab = ManipulateStructure(top_slab).make_vacuum_around(vacuum=50)
        bot_aligned, top_aligned = get_aligned_lattices(bot_slab, top_slab, **d)

        # If the structures do exist, then the interface can be matched
        if bot_aligned and top_aligned:     

            # Center bot and top slab around 0 and form an interface
            top_aligned, bot_aligned = recenter_aligned_slabs(
                top_aligned, bot_aligned, d=inter_params['interface_distance'])
            interface = stack_aligned_slabs(bot_aligned, top_aligned)
            interface = clean_up_site_properties(interface)

            # Assign the desired vacuum to the interface and rescale slabs
            obj = ManipulateStructure(interface)
            obj.make_vacuum_around(vacuum=vacuum, vac_kind='zero', inplace=True)
            bot_aligned, top_aligned = obj.decompose_structure(inter_type='zero')
            interface = obj.converter(obj.structure)

            # Convert the obtained structure back to slabs objects
            bot_aligned = structure_to_slab(bot_aligned, bot_slab)
            top_aligned = structure_to_slab(top_aligned, top_slab)

        return bot_aligned, top_aligned, interface


class ManipulateMolecule:
    
    def __ini__(self, molecule):
        self.molecule = molecule

    def rotate_molecule(self, angle, vector, center=(0, 0, 0), rotate_cell=False):
        """
        Create an adsorbate and rotate it.
        
        Parameters
        ----------        
        angle : float
            Rotation angle to rotate the atom around the vector 'v'.
            If 'a' is a vector, it is rotate into 'v'.

        v : str
            Vector to rotate the atoms around. You can also provide vectors
            as strings, such as 'x', '-y', 'z', etc.

        center : tuple, optional
            Center is kept fixed under the rotation. Use 'COM' to fix
            the center of mass, 'COP' to fix the center of positions or
            'COU' to fix the center of cell. The default is (0, 0, 0)
            (from ase.Atom import rotate)

        rotate_cell : bool, optional
            The cell is also rotated together with the molecule if set to 
            True. The default is False.
        
        Return
        ------
        adsorbate : pymatgen.core.structure.Molecule
            Rotated molecule.

        """
        self.molecule.rotate(a=angle, v=vector, center=center, 
                             rotate_cell=rotate_cell)
        
        # ads = molecule(name=name, vacuum=vacuum)
        # ads.rotate(a=a, v=v, center=center, rotate_cell=rotate_cell)
        # return AseAtomsAdaptor.get_molecule(ads)

    @classmethod
    def from_atom(cls, name, vacuum=None):
        """
        Create an adsorbate.
        
        Parameters
        ----------
        name : str
            Name of the atom or molecule to be adsorbed.
        
        vacuum : float, optional
            Additional vacuum to be used to surround the molecule.
        
        Return
        ------
        ManipulateMolecule object
    
        """
        ads = molecule(name=name, vacuum=vacuum)
        return cls(AseAtomsAdaptor.get_molecule(ads))

def structure_to_slab(structure, slab, as_dict=False, transfer_keys=['miller_index', 
                      'oriented_unit_cell', 'energy', 'reconstruction', 
                      'scale_factor', 'shift']):
        """
        Convert a structure object to a slab object by working directly on their 
        dictionaries. This is useful when using Atomate to run VASP simulations.
        In that case, despite you start with a slab, you end up having a structure
        in the `tasks` collection. With this function you can integrate the final
        structure object with missing dictionary keys from its reference slab.
        The `structure` and `slab` can be either pymatgen objects or their dicts.
    
        Parameters
        ----------
        structure : dict or pymatgen.core.structure.Structure
            Structure to be converted to a Slab.
    
        slab : dict or pymatgen.core.surface.Slab
            Reference slab to be used to integrate the structure keys.
        
        transfer_magmoms : bool or str, optional
            Decide whether to transfer the magnetic moments properties from the
            reference slab to the structure. If set to 'all', it will transfer all
            the properties that might be present in the slabs, such as: 'magmom',
            'bulk_equivalent', 'bulk_wyckoff'. The default is False.
        
        as_dict : bool, optional
            Return a dictionary or a pymatgen Slab object. The default is False.
        
        transfer_keys: dict of str, optional
            List of attributes that should be transferred as is from the reference
            slab to the structure. 
            The default is: ['miller_index', 'oriented_unit_cell', 'energy', 
                             'reconstruction', 'scale_factor', 'shift']
    
        Returns
        -------
        out_slab : pymatgen.core.surface.Slab
            The converted slab already as pymatgen object.
    
        """
    
        # Check the types of structure and slab
        if isinstance(structure, Structure):
            structure = structure.as_dict()
        elif not isinstance(structure, dict):
            raise ValueError('Wrong type for input argument: structure')
        if isinstance(slab, Slab):
            slab = slab.as_dict()
        elif not isinstance(slab, dict):
            raise ValueError('Wrong type for input argument: slab')
    
        # Update the structure and convert it back to a slab
        out_slab = structure.copy()
        out_slab['@class'] = 'Slab'
        out_slab['@module'] = 'pymatgen.core.surface'
        for k in transfer_keys:
            out_slab[k] = slab[k]
    
        if not as_dict:
            out_slab = Slab.from_dict(out_slab)
    
        return out_slab

def clean_up_site_properties(structure):
    """
    Cleans up site_properties of structures that contain NoneTypes.
    
    If an interface is created from two different structures, it is possible
    that some site properties like magmom are not set for both structures.
    This can lead later to problems since they are replaced by None.
    This function replaces NoneTypes with 0.0 for magmom and deletes all other
    site_properties if None entries are found in it.


    Parameters
    ----------
    structure : pymatgen.core.structure.Structure
        Input structure

    Returns
    -------
    struct : pymatgen.core.structure.Structure
        Output structure

    """
    struct = structure.copy()
    for key in struct.site_properties.keys():
        if key == 'magmom':
            new_magmom = []
            for m in struct.site_properties[key]:
                if m == None:
                    new_magmom.append(0.0)
                else:
                    new_magmom.append(m)
            struct.add_site_property('magmom', new_magmom)
        else:
            struct.remove_site_property(key)
    return struct

def stack_aligned_slabs(bottom_slab, top_slab, top_shift=[0,0,0]):
    """
    Combine slabs that are centered around 0 into a single structure.
    
    Optionally shift the top slab by a vector of cartesian coordinates.

    Parameters
    ----------
    bottom_slab : pymatgen.core.structure.Structure or pymatgen.core.surface.Slab
        Bottom slab.
    top_slab : pymatgen.core.structure.Structure or pymatgen.core.surface.Slab
        Top slab.
    top_shift : list of 3 floats, optional
        Vector of caresian coordinates with which to shift the top slab.
        The default is [0,0,0].

    Returns
    -------
    interface : pymatgen.core.structure.Structure or pymatgen.core.surface.Slab
                depending on type of bottom_slab
        An interface structure of two slabs with an optional shift of the top
        slab.

    """
    interface = bottom_slab.copy()
    t_copy = top_slab.copy()
    
    t_copy.translate_sites(indices=range(len(t_copy.sites)),
                           vector=top_shift,
                           frac_coords=False, to_unit_cell=False)
    
    for s in t_copy.sites:
        new_site = PeriodicSite(lattice=interface.lattice,
                                coords=s.frac_coords,
                                coords_are_cartesian=False,
                                species=s.species,
                                properties=s.properties)
        interface.sites.append(new_site)
    
    return interface

def recenter_aligned_slabs(top_slab, bottom_slab, d=2.5):
    """
    Center two slabs around z=0 and give them the distance d.

    Parameters
    ----------
    top_slab : pymatgen.core.structure.Structure
        The slab that should be on top.
    bottom_slab : pymatgen.core.structure.Structure
        The slab that should be on the bottom.
    d : float, optional
        The desired distance between the slabs. The default is 2.5.

    Returns
    -------
    t_copy : pymatgen.core.structure.Structure
        Top slab that is shifted so that the lowest atom is at +d/2
    b_copy : pymatgen.core.structure.Structure
        Bottom slab that is shifted so that the topmost atom is at -d/2

    """
    t_copy = top_slab.copy()
    b_copy = bottom_slab.copy()
    top_zs=[]
    bot_zs=[]
    for s in t_copy.sites:
        top_zs.append(s.coords[-1])
    top_shift = -min(top_zs) + d/2
    
    for s in b_copy.sites:
        bot_zs.append(s.coords[-1])
    bot_shift = -max(bot_zs) - d/2

    t_copy.translate_sites(indices=range(len(t_copy.sites)),
                           vector=[0, 0, top_shift],
                           frac_coords=False, to_unit_cell=False)
    b_copy.translate_sites(indices=range(len(b_copy.sites)),
                           vector=[0, 0, bot_shift],
                           frac_coords=False, to_unit_cell=False)
    return t_copy, b_copy

def estimate_interface(inter, inter_type='zero', axis=2):
        """
        Estimate the edges of the interface of the structure along axis.
        The kind of infert can be specified with inter_type.
        
        Options:
            - "zero": The interface must be at z = 0, the closest atoms above
                      and below are considered to delimit the interface.
            - "even": The interface is considered to be between the n/2 and 
                      n/2+1 atoms. Only works for an even number of atoms.

        Parameters
        ----------
        inter_type : str, optional
            Type of the interface. The default is 'zero'.
        
        axis : int, optional
            Axis orthogonal to the interface, a common choice is to choose a
            vertically oriented structure, i.e. interface in the xy plane.
            Integers values refer to the matrix columns (x: 0, y: 1, z: 2).
            The default value is 2.
        
        Returns
        -------
        list :
            List of two elements containing the lower and upper delimiter
            of the interface, along axis.

        """
        
        # Extract the coordinates along axis from the dictionary
        sites = np.array([s['xyz'] for s in inter.structure['sites']])
        coords = np.sort(sites[:, axis])
        
        # Identify the elements of the structure
        if inter_type == 'zero':  # Default case, interface at z=0
            low = max(coords[coords < 0])
            up  = min(coords[coords > 0])

        elif inter_type == 'even':  # Interface is between n/2 and n/2+1 atom
            n = len(coords)

            if n % 2 == 0:
                low = coords[int(n/2)]
                up  = coords[int(n/2) + 1]
            else:
                raise ValueError('Your structure as an odd number of atoms, '
                                 'inter_type="even" does not make sense for it')

        return [low, up]
