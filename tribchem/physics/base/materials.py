#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 11:57:57 2021

Categories for the different materials that can be used.

@author: glosi000
"""

class PeriodicTable:

    alkali_metals = ['Li', 'Na', 'K', 'Rb', 'Cs', 'Fr']
    alkaline_earth_metals = ['Be', 'Mg', 'Ca', 'Sr', 'Ba', 'Ra']
    s_metals = alkali_metals + alkaline_earth_metals

    transition_metals = ['Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 
                         'Zn', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 
                         'Ag', 'Cd', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 
                         'Au']
    d_metals = transition_metals

    poor_metals = ['Al', 'Ga', 'In', 'Sn', 'Tl', 'Pb', 'Bi', 'Po']
    p_metals = poor_metals

    lanthanoids = ['La', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er',
                   'Tm', 'Yb', 'Lu']
    actinoid = ['Ac', 'Th', 'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es',
                'Fm', 'Md', 'No', 'Lr']
    f_metals = lanthanoids + actinoid

    semimetals = ['B', 'Si', 'Ge', 'As', 'Sb', 'Te', 'At']
    semiconductors = semimetals

    non_metals = ['H', 'C', 'N', 'O', 'P', 'S', 'Se']
    halogens = ['F', 'Cl', 'Br', 'I', 'At', 'Ts']
    noble_gases = ['He', 'Ne', 'Ar', 'Kr', 'Xe', 'Rn', 'Og']

    metals = s_metals + p_metals + d_metals + f_metals

class AtomicRadius:

    clementi = {
            "H": 0.53,
            "He": 0.31,
            "Li": 1.67,
            "Be": 1.12
            }
    def atomic_radius(self):
        return
