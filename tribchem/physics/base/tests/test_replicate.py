import numpy as np
from tribchem.physics.base.solidstate import replicate, orthorombize_cell, rotate

#### Replicate the cell

fe = np.genfromtxt('iron')
o  = np.genfromtxt('oxygen')
cell = np.genfromtxt('cell')

a, c = replicate(fe, cell, (2, 2, 1))
b, _ = replicate(o, cell, (2, 2, 1))

np.savetxt('fe_new', a)
np.savetxt('o_new', b)
np.savetxt('cell_new', c)


#### Try to orthorombize

cell = rotate(cell, theta=30)
fe = rotate(fe, theta=30)
o = rotate(o, theta=30)

orthorombic_fe, cell_2d = orthorombize_cell(fe, cell)
orthorombic_o, _ = orthorombize_cell(o, cell)

np.savetxt('fe_orthorombic', orthorombic_fe)
np.savetxt('o_orthorombic', orthorombic_o)
