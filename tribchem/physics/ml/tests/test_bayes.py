import os
import pandas as pd

from tribchem.physics.ml.bayes import BayesRegression

# data_path = '/home/omar/Lavoro/Unibo/TriboFlowProject/machine_learning/meb/dataset.csv'

# data = BayesRegression.data_to_pd(data_path)

# print(data)

# train_in, transf, train_out = BayesRegression.trans_data(data=data, 
#                                                          target='MEB')

# print(train_in)

# print(train_out)

# print(transf)

# by = BayesRegression(n_des=15, n_samp=5)
# mu, sigma = by.fit(train_in, transf, train_out)

# print(mu)

# Test on graphene
local_path = os.path.dirname(__file__)
energy_path = local_path + '/graphene/energy.txt'
min_path = local_path + '/graphene/grafpes_min.pwi'
max_path = local_path + '/graphene/grafpes_max.pwi'
saddle_path = local_path + '/graphene/grafpes_saddle.pwi'

dataset = BayesRegression.generate_dataset(min_path, max_path, 
                                           saddle_path, energy_path)
train_in, tran, train_out = BayesRegression.trans_data(dataset, 'Energy')

by = BayesRegression(n_des=4, n_samp=3)
norm_in, norm_tran, norm_out, min_out, max_out = by.znorm(
    input_data=train_in, 
    tran=tran,
    out=train_out)
norm_tot_in = pd.merge(norm_in, norm_tran, how='outer')

mu_total = []
for i in range(norm_tot_in.shape[0]-4):
    mu, sigma = by.fit(norm_tot_in[i:i+4], train_out)
    # Shift by one the train_out using the last estimate of the energy
    train_out.pop(0)
    train_out = train_out.append(pd.Series(mu[-1]))
    
    mu_total.append(mu[-1])

print(mu_total)