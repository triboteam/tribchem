import os
import numpy as np
import numpy.linalg as linalg
import matplotlib.pyplot as plt

from tribchem.physics.ml.bayes import BayesRegression
from tribchem.physics.tribology.pes import PES
from tribchem.physics.base.qeio import ReadQEpwi
from tribchem.physics.base.solidstate import generate_uniform_grid, pbc_coordinates


def get_relat_pos(mat1, mat2):
    """
    Return a list containing the relative lateral positions between the coordinates
    of material 1 with respect to material 2

    """

    relat_pos = []
    for m1 in mat1:
        local = []

        for m2 in mat2:
            local.append(linalg.norm(m2 - m1))
        relat_pos.append(local)

    return np.array(relat_pos)

def plot_points(relat_pos):
    relat_pos = np.array(relat_pos)
    plt.plot(relat_pos[:, 0], relat_pos[:, 1], 'o')

local_path = os.path.dirname(__file__)
energy_path = local_path + '/graphene/energy.txt'
min_path = local_path + '/graphene/grafpes_min.pwi'
max_path = local_path + '/graphene/grafpes_max.pwi'
saddle_path = local_path + '/graphene/grafpes_saddle.pwi'

pes_data = np.genfromtxt(energy_path)
a = 4.669095596 * .529177
cell_2d = np.array([[1, 0], [-1/2, np.sqrt(3)/2]]) * a
pes = PES(pes_data, cell_2d)
pes.make_pes(replicate_of=(3, 3), orthorombize=False, theta=False, density=10, tol=1e-4)
#pes.plot(extent=(2, 2), mpts=(200j, 200j), to_fig='Graphene')


obj1 = ReadQEpwi(min_path)
obj2 = ReadQEpwi(max_path)
obj3 = ReadQEpwi(saddle_path)

relat_pos_min = get_relat_pos(obj1.coords[:2, :], obj1.coords[2:, :])
relat_pos_max = get_relat_pos(obj2.coords[:2, :], obj2.coords[2:, :])
relat_pos_sad = get_relat_pos(obj3.coords[:2, :], obj3.coords[2:, :])   

cell = np.array([[1, 0, 0], [-1/2, np.sqrt(3)/2, 0], [0, 0, 10]]) * a
grid = generate_uniform_grid(cell, pts_a=7, to_plot=False)

# Find out equivalent shifts and remove them
p = np.column_stack((pes_data[:, :2], np.zeros(pes_data.shape[0])))
shifts = np.vstack((grid.copy(), p))
shifts = np.unique(shifts, axis=0)
plot_points(shifts)

relat_pos_list = []
upper_layer = obj1.coords[:2, :]

for s in grid:
    
    # Check if the point does exist already
    struct = obj1.coords.copy()
    shifted_layer = upper_layer - s
    struct[:2, :] = shifted_layer

    struct = pbc_coordinates(struct, cell)
    pos = get_relat_pos(struct[:2, :], struct[2:, :])
    
    c1 = all((pos == relat_pos_min).flatten())
    c2 = all((pos == relat_pos_max).flatten())
    c3 = all((pos == relat_pos_sad).flatten())
    #print(c1, c2, c3)
    
    if not c1 and not c2 and not c3:
        pos = pos.flatten()
        relat_pos_list.append(pos)

dataset = BayesRegression.generate_dataset(min_path, max_path, 
                                           saddle_path, energy_path)
print(dataset)
