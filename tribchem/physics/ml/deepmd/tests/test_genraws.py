#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 13:36:39 2021

Generate the raw files

@author: glosi000
"""

from tribchem.physics.ml.deepmd.genraws import genraws_pw, genraws_cp


genraws_cp('../../../dft/tests/IO_files/CP-files/CMS2-600', 
            '../../../dft/tests/IO_files/CP-files/cp.in')
#genraws_pw('../../../dft/tests/IO_files/md_ph_iron')
