#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 13:18:54 2021

Extract information from a BOMD simulation made with Quantum Espresso and 
prepare the raw data to feed deepmd.

@author: glosi000

"""

import numpy as np

from tribchem.physics.dft.qe import ReadQEpwo, ReadQEcpo
from tribchem.physics.base.solidstate import pbc_coordinates
from tribchem.physics.base.units import PhysConstants
from tribchem.highput.utils.errors import ReadParamsError

def genraws_pw(prefix):
    """
    Create the raw files from a Quantum Espresso pw simulation, i.e. BO dynamics,
    to feed the DeepMD package and generate a ML FF.

    """

    obj = ReadQEpwo(prefix)
    nstep = len(obj.position)
    lattice_cell = obj.cell

    # Generate the box.raw file
    with open('box.raw', 'w') as f:
        cell = str(obj.cell.flatten()).replace('[', '').replace(']', '').replace('\n', '').strip()
        for i in range(nstep):
            f.write(cell + '\n')

    # Generate the type.raw file
    with open('type.raw', 'w') as f:
        for t in obj.force[0][:, 0]-1:
            f.write(str(int(t)) + ' ')

    # Generate the energy.raw file
    np.savetxt('energy.raw', obj.energy * PhysConstants.ry_to_eV)

    # Read the coordinate type and prepare the conversion factor
    pos_type = obj.pos_type
    if pos_type == 'alat':
        factor = obj.celldm[0] * PhysConstants.bohr_to_A
    elif pos_type == 'angstrom':
        factor = 1
    elif pos_type == 'bohr':
        factor = PhysConstants.bohr_to_A
    else:
        raise ReadParamsError('Case not implemented yet: crystal')
    
    # Generate the coords.raw file
    lines= []
    with open('coord.raw', 'w') as f:
        for coords in obj.position:
            c = coords[:, 1:] * factor
            c = pbc_coordinates(c, lattice_cell).flatten()
            c = str(c).replace('[', '').replace(']', '').replace('\n', '').strip()
            lines.append(c + '\n')
        f.writelines(lines)

    # Generate the force.raw file
    lines = []
    with open('force.raw', 'w') as f:
        for coords in obj.force:
            c = coords[:, 1:].flatten() * PhysConstants.rybohr_to_eVA
            c = str(c).replace('[', '').replace(']', '').replace('\n', '').strip()
            lines.append(c + '\n')
        f.writelines(lines)

    # Generate the virial.raw file
    with open('virial.raw', 'w') as f:
        if not hasattr(obj, 'stress'):
            virial = '0 0 0 0 0 0 0 0 0'  
            for i in range(nstep):
                f.write(virial + '\n')
        else:
            lines = []
            for stress in obj.stress:
                s = stress.flatten() * 1000  # Convert from kbar to bar
                s = str(s).replace('[', '').replace(']', '').replace('\n', '').strip()
                lines.append(s + '\n')
            f.writelines(lines)

def genraws_cp(prefix, inputfile):
    """
    Create the raw files from a Quantum Espresso cp simulation to feed the
    DeepMD package and generate a ML FF.

    """

    obj = ReadQEcpo(prefix, inputfile, check_shape=True)

    # Generate the box.raw file
    with open('box.raw', 'w') as f:
        for c in obj.cell_dyn:
            cell = str(c.flatten() * PhysConstants.bohr_to_A).replace('[', '').replace(']', '').replace('\n', '').strip()
            f.write(cell + '\n')

    # Generate the unique types of the atomic coordinates
    dict_type = {}
    unique_types = []
    for t in np.unique(np.array(obj.qei['ATOMIC_POSITIONS'])[:, 0]):
        ut = ''.join([i for i in t if not i.isdigit()])
        unique_types.append(ut)
    for i, t in enumerate(np.unique(unique_types)):
        dict_type[t] = i
    # Generate the type.raw file
    keys = dict_type.keys()
    with open('type.raw', 'w') as f:
        for el in np.array(obj.qei['ATOMIC_POSITIONS'])[:, 0]:
            for k in keys:
                if el.startswith(k):
                    f.write(str(dict_type[k]) + ' ')
    
    # Generate the energy.raw file
    np.savetxt('energy.raw', obj.etot * PhysConstants.ha_to_eV)

    # Generate the coords.raw file
    lines= []
    with open('coord.raw', 'w') as f:
        for i, coords in enumerate(obj.position):
            c = coords * PhysConstants.bohr_to_A
            c = pbc_coordinates(c, obj.cell_dyn[i] * PhysConstants.bohr_to_A).flatten()
            c = str(c).replace('[', '').replace(']', '').replace('\n', '').strip()
            lines.append(c + '\n')
        f.writelines(lines)

    # Generate the force.raw file
    lines = []
    with open('force.raw', 'w') as f:
        for coords in obj.force:
            c = coords.flatten() * PhysConstants.habohr_to_eVA
            c = str(c).replace('[', '').replace(']', '').replace('\n', '').strip()
            lines.append(c + '\n')
        f.writelines(lines)

    # Generate the virial.raw file
    virial = '0 0 0 0 0 0 0 0 0'
    with open('virial.raw', 'w') as f:
        for i in range(obj.nsteps[0]):
            f.write(virial + '\n')

def set_qefiles():
    return input("File prefix ($prefix.pwi: input, $prefix.pwo: output): ")
