#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 2 09:13:00 2021

In this script there are all the functions necessary to generate the DeepMD 
(https://github.com/deepmodeling/deepmd-kit) inputs/outputs files by using the
dpdata pachake (https://github.com/deepmodeling/dpdata).

Author: Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'September 2nd, 2021'

import os
import argparse

import dpdata
from glob import glob

# Get the parameters from the command line
description = "A Python script to export the output files of VASP to DeepMD format."
parser = argparse.ArgumentParser(description=description)

help_source = "Path where the output files are saved. \
               Use the following notation to get all the folders which contain \
               a file named like OUTCAR: /../PBE.interface_elements/*/pes_scf/*/OUTCAR.gz."
parser.add_argument('-source', type=str, help=help_source)

help_destination = "Path where to save the DeepMD raw files."
parser.add_argument('-destination', type=str, help=help_destination)

help_vasp_output_file = "VASP output file (e.g.: OUTCAR, POSCAR...). \
                         By default is set to OUTCAR. \
                         \nThis value must be the same as the name of the file \
                         in source."
parser.add_argument('-vasp_file', type=str, help=help_vasp_output_file, 
                    default='OUTCAR')

help_gz = "Set to True if the file is compressed."
parser.add_argument('-is_compressed', type=str, help=help_gz, default='True')

args = parser.parse_args()

source = args.source
destination = args.destination
vasp_file = args.vasp_file
is_compressed = args.is_compressed

# Decompress the files
if is_compressed=='True':
    fs_gz = glob(source)

    if not fs_gz:
        raise ValueError("The folder indicated in source has something wrong."
                         " Provide a valid path or set of path with the" 
                         "notation /*/name_of_the_subfolder/*/vasp_file.")

    for f in fs_gz:
        command = "gunzip " + f
        os.system(command)
    source = source.strip('.gz')
    fs = glob(source)
else:

    fs = glob(source)

    if any([f.endswith('.gz') for f in fs]):
        raise ValueError("There is a file which is compressed. "
                         "Check that all the files are not compressed, "
                         "otherwise set 'is_compressed=True'.")
    if not fs:
        raise ValueError("The folder indicated in source has something wrong."
                         " Provide a valid path or set of path with the" 
                         "notation /*/name_of_the_subfolder/*/vasp_file.")

ms = dpdata.MultiSystems()
for f in fs:
    try:
        ls = dpdata.LabeledSystem(f)
    except:
        print(f)
    if len(ls)>0:
        ms.append(ls)
print("Saving the system...")
print(ms)
try:
    ms.to_deepmd_raw(destination)
except:
    ValueError("Somthing wrong happend while saving the DeepMD files. \
                Check if the destination path is correct.")

# Compress the files again
if is_compressed:
    for f in fs:
        command = "gzip " + f
        os.system(command)