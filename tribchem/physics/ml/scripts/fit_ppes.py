#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 15:46:42 2021

Script to fit PPES data of homogeneous interfaces

@author: glosi000

"""

import pandas as pd

from tribchem.highput.utils.tasktools import read_json
from tribchem.physics.ml.homo_tools import (
    load_homo_interfaces,
    get_ppes,
    make_fits
)
from tribchem.physics.tribology.ppes import PPESModel
from tribchem.physics.base.materials import PeriodicTable


# Read PPES data
cut = False
to_ev = False
json = load_homo_interfaces('../../../data/homogeneous_interface.json')
e100, e110, e111, e0001 = get_ppes(json, cut, to_ev)

# Set the folders where fits should be stored, the functions to be used and labels
folder = 'n4'
labels = ['n4', 'uber']
at = 'Fe'
test = {at: e111[at]}

ppes_n, ppes_n4, uber = PPESModel.potential_n, PPESModel.potential_n4, PPESModel.uber
func = [ppes_n, uber]

popts_100 = make_fits(json, e100, '100', func, labels, cut, to_ev, folder)
popts_110 = make_fits(json, e110, '110', func, labels, cut, to_ev, folder)
popts_111 = make_fits(json, e111, '111', func, labels, cut, to_ev, folder)
popts_0001 = make_fits(json, e0001, '0001', func, labels, cut, to_ev, folder)

# Recover the exponent for given materials
data = {}
for folder in ['n', 'n_well', 'n_well_short']:
    for at in PeriodicTable.lanthanoids:
        for hkl in ['100', '110', '111']:        
            file = folder + '/fit' + hkl + '.json'
            j = read_json(file)
            
            if at in j.keys():
                try:
                    data[hkl].update({at: j[at]['n']['popt'][-1]})
                except:
                    data.update({hkl: {at: j[at]['n']['popt'][-1]}})

    frame = pd.DataFrame.from_dict(data)
    #print(frame)