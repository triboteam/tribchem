#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 15 14:37:05 2021

Tools to analyze the homogeneous interface data.

@author: glosi000

"""


import json as js

import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

from tribchem.highput.utils.tasktools import read_json
from tribchem.physics.tribology.ppes import PPESModel
from tribchem.physics.base.units import PhysConstants


def load_homo_interfaces(file):
    return read_json(file)

def get_ppes(json, cut=False, to_ev=False):
    
    elements_100 = {}
    elements_110 = {}
    elements_111 = {}
    elements_0001 = {}
    
    for at in json.keys():
        if json[at]['lattice'] != 'hcp':            
            elements_100.update({at: _get_ppes(json[at]['100'], cut, to_ev)})
            elements_110.update({at: _get_ppes(json[at]['110'], cut, to_ev)})
            elements_111.update({at: _get_ppes(json[at]['111'], cut, to_ev)})
        else:
            elements_0001.update({at: _get_ppes(json[at]['0001'], cut, to_ev)})
    
    return elements_100, elements_110, elements_111, elements_0001

def _get_ppes(data_dict, cut=False, to_ev=False):
    
    ppes = np.array(data_dict['PPES_data'])
    dist_eq = data_dict['x0_min [A]']
    adhesion = data_dict['work_of_separation [J/m^2]']
    
    if cut:
        index = ppes[:, 0] <= 1.25
        ppes = ppes[index]

    ppes[:, 0] = ppes[:, 0] + dist_eq
    ppes[:, 1] -= adhesion
    
    if to_ev:
        ppes[:, 1] *= (data_dict['area [A^2]'] * PhysConstants.J_to_eV)
    
    return ppes

def call_uber(x, json, at, miller):
    g = json[at][miller]['G [GPa]']
    l = json[at][miller]['l [A]']
    x0 = json[at][miller]['x0_min [A]']
    y = PPESModel.uber(x, g, l, x0) - json[at][miller]['work_of_separation [J/m^2]']
    return y

def make_fits(json, data_dict, miller, func, labels, cut=False, to_ev=False,
              to_file=None):
    
    if not isinstance(func, list):
        func = [func]
    if not isinstance(labels, list):
        labels = [labels]
    if len(func) != len(labels):
        raise ValueError('func and labels should have the same length')
    
    popts = {}

    for at, v in data_dict.items():
        
        print(at, miller)
        
        if at in ['Cr', 'Fe', 'Mo', 'Nb', 'V', 'W'] and miller == '111':
            pass
        else:

            d_orig = _get_ppes(json[at][miller], False, to_ev)
    
            # Prepare x data for curve plotting, prepare the plot        
            #x = np.linspace(min(v[:, 0]), max(v[:, 0]), 1000)
            x = np.linspace(min(d_orig[:, 0]), max(d_orig[:, 0]), 1000)
            plt.plot(d_orig[:, 0], d_orig[:, 1], 'o')
    
            for f, l in zip(func, labels):
    
                # Make UBER fit
                if l == 'uber':
                    y = call_uber(x, json, at, miller)
                    plt.plot(x, y, label=l)
    
                else:
                    if l in ['n', 'n4', 'n6', 'npoly']:
                        
                        # Generate the starting guess values and fit data
                        p0 = p0_nmodels(at, miller, v, l, cut).tolist()
                        popt, pcov = curve_fit(f, v[:, 0], v[:, 1], 
                                               maxfev=100000, bounds=(1., np.inf))
                        plt.plot(x, f(x, *popt), label=l)
                        
                        # Print the coefficient of the fit and the errors
                        print(popt)
                        print(np.sqrt(np.diag(pcov)).tolist())
                        
                        # Store the results of the fit
                        popts.update(
                            {at: 
                                 {
                                        l: {
                                            'popt': list(popt), 
                                            'pcov': np.sqrt(np.diag(pcov)).tolist()
                                           }
                                 }
                            })
    
            # Make the plot and eventually save it to a folder
            plt.legend()
            plt.title(str(at) + ' ' + str(miller))
            if to_file is not None:
                plt.savefig(to_file + '/pdf/' + str(at) + str(miller) + '.pdf')
            plt.show()

    if to_file:
        with open(to_file + '/fit' + str(miller) + '.json', 'w') as f:
            js.dump(popts, f, indent=4, sort_keys=True)

    return popts

def p0_nmodels(at, miller, value, model='n', cut=False):

    # Make FIT with graphene model (n=4 for the attractive part)
    c0 = 1e15
    c1 = 1 / min(value[:, 1])
    c2 = 1e19

    if at == 'Zn' and miller == '0001':
        c2 = 1e14
    elif at == 'Cd' and miller == '0001':
        c0, c1, c2 = (1, 1, 1)

    if at in ['Na', 'Li', 'Ca', 'Sr', 'K', 'Pb', 'Rb', 'Cs', 'Sn', 'Eu', 'Ba',
              'Yb', 'Ge', 'Be', 'Hf', 'Ti', 'Zn', 'Co', 'Os', 'Mg', 'Cd']:
        c1 = 1

    c = np.array([c0, c1, c2])

    if model == 'n':
        c3 = 4

        if at in ['Os', 'Ag']:
            c0, c1, c2, c3 = (10, 1, -5, 5)

        if at in ['Cd', 'Cr', 'Al', 'Ag']:
            c0, c1, c2, c3 = (1e14, 1, -5, 3.5)

        if at in ['Rh', 'Pt', 'Pd', 'Ni', 'Fe', 'Mo', 'Si', 'W']:
            c0, c3 = (1e14, 1)

        if miller == '111' and at in ['Ta', 'Cr', 'Li']:
            c0, c1, c2, c3 = (-1, 1, -5, -1)
        
        if cut:
            if at in ['Cd']:
                c0, c1, c2, c3 = (-1, 1, -5, -1)
            elif at in ['Li', 'Ru', 'Cu', 'V']:
                c0, c1, c2, c3 = (1e10, -1, 0.1, 1)
            elif miller == '110' and at in ['Ag', 'Au', 'Ir', 'Ge']:
                c0, c1, c2, c3 = (1e10, -1, 0.1, 1)
            elif miller == '110' and at in ['Rb', 'Na', 'Ta']:
                c0, c1, c2, c3 = (0.0001, 1, 0.0001, 1)
        
        if not cut:
            if at in ['V', 'Al', 'Cu', 'Au', 'Ir']:
                c0, c1, c2, c3 = (-0.1, 1, 1, 1.5)
            
            if miller == '111':
                if at in ['Ce']:
                    c1, c2, c3 = (1, -20, 5)
                elif at in ['Cu', 'Li']:
                    c0, c1, c2, c3 = (1e20, -0.5, 0.1, 5)
                
                elif at in ['V', 'Fe', 'Nb', 'Cr', 'W', 'Mo']:
                    c0, c1, c2, c3 = (1, 1, 1, 1)
    
        c = np.append(c, c3)

    return c
