#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  14 16:13:19 2021

Bayes method implementation. At first this module contains the implementation
of the Bayes method as shown here https://link.springer.com/article/10.1007/s11249-020-01294-w.

Author: Omar Chehaimi (@omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Prof. M.C. Righi, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'April 14th, 2021'

import pandas as pd
import numpy as np
from scipy import linalg

from tribchem.physics.base.qeio import ReadQEpwi
from tribchem.physics.base.solidstate import generate_uniform_grid, pbc_coordinates


class BayesRegression:
    """
    In this class are implemented all the methods required for performing
    a regression by using the Bayes method as presented in 
    https://link.springer.com/article/10.1007/s11249-020-01294-w.

    Attributes
    ----------
    n_des : int
        Number of descriptors (features) of the model.

    n_samp : int
            Number of output samples.

    Methods
    -------
    data_to_pd(data, sep, header)
        Load a csv file into a pandas dataframe.
    
    trans_data(data)
        Extract transfer data from a pandas dataframe.

    znorm(input_data, tran, out)
        Normalize the input (descriptors) and output data using the 
        Z-normalization.
    
    __norm_const(data, axis)
        Extract the minimum and maximum values for the Z-normalization, and 
        perform a check of the size of the dataframes.

    fit(input_data, tran, out)
        Perform the training of the Bayes model.

    predict(des)
        Predict the MEB given the descriptors.

    """

    def __init__(self, n_des, n_samp):
        """
        Parameters
        ----------
        n_des : int
            Number of descriptors (features) of the model.
    
         n_samp : int
            Number of output samples.

        """

        self.n_des = n_des
        self.n_samp = n_samp

    @staticmethod
    def generate_dataset(omin, omax, osaddle, energy):
        """
        Generate a dataset given the input of Quantum Espresso.
        
        Parameters
        ----------
        mat : lst

        energy : str

        """
        
        # Get relative position
        obj1 = ReadQEpwi(omin)
        obj2 = ReadQEpwi(omax)
        obj3 = ReadQEpwi(osaddle)

        relat_pos_min = BayesRegression.get_relat_pos(obj1.coords[:2, :], obj1.coords[2:, :])
        relat_pos_max = BayesRegression.get_relat_pos(obj2.coords[:2, :], obj2.coords[2:, :])
        relat_pos_sad = BayesRegression.get_relat_pos(obj3.coords[:2, :], obj3.coords[2:, :])  
        
        # Get the energies and the shifts
        energy_val = BayesRegression.get_energy(energy)[:, 2]
        shifts = BayesRegression.get_energy(energy)[:, :2]
      
        min_all = np.append(relat_pos_min.flatten(), [energy_val[0], 0])
        max_all = np.append(relat_pos_max.flatten(), [energy_val[1], 0])
        sad_all = np.append(relat_pos_sad.flatten(), [energy_val[2], 0])
        
        data = np.array([min_all, max_all, sad_all])

        # Generating the transfer dataset
        a = 4.669095596 * .529177 
        cell = np.array([[1, 0, 0], [-1/2, np.sqrt(3)/2, 0], [0, 0, 10]]) * a
        grid = generate_uniform_grid(cell, pts_a=7, to_plot=False)
        
        upper_layer = obj1.coords[:2, :]
        
        for s in grid:
    
            # Check if the point does exist already
            struct = obj1.coords.copy()
            shifted_layer = upper_layer - s
            struct[:2, :] = shifted_layer

            struct = pbc_coordinates(struct, cell)
            pos = BayesRegression.get_relat_pos(struct[:2, :], struct[2:, :])
            
            c1 = all((pos == relat_pos_min).flatten())
            c2 = all((pos == relat_pos_max).flatten())
            c3 = all((pos == relat_pos_sad).flatten())
            
            if not c1 and not c2 and not c3:
                pos = pos.flatten()
                #new_line = np.append(pos, [np.nan, 1])
                data = np.vstack((data, np.append(pos, [np.nan, 1])))

        header = ['p1', 'p2', 'p3', 'p4', 'Energy', 'Transfer data']

        df = pd.DataFrame(data, columns=header)
        
        return df 
        
    @staticmethod
    def get_energy(energy):
        return np.genfromtxt(energy)
    
    @staticmethod
    def get_relat_pos(mat1, mat2):
        """
        Return a list containing the relative lateral positions between the coordinates
        of material 1 with respect to material 2

        """
        
        relat_pos = []
        for m1 in mat1:
            local = []

            for m2 in mat2:
                local.append(np.linalg.norm(m2 - m1))
            
            relat_pos.append(local)
        
        return np.array(relat_pos)
    
    @staticmethod
    def data_to_pd(data, sep=',', header='infer', ):
        """
        Load a csv file into a pandas dataframe, and perform some quality checks
        on the data.

        Parameters
        ----------
        data : str
            Location of the csv file.
        
        sep : str, default ','
            Separator of the csv file.
        
        header : int, list of int, default 'infer'
            Number of rows to use as column name.

        Return
        ------
        dataframe : pandas dataframe
            The pandas dataframe associated to the csv file passed in input.

        """

        try:
            dataframe = pd.read_csv(filepath_or_buffer=data, 
                                    sep=sep, header=header, index_col=0)
        except:
            raise ImportError("Something went wrong during the loading of {}."
                              .format(data))

        return dataframe

    @staticmethod
    def trans_data(data, target):
        """
        Extract the training and transfer data from a pandas dataframe. 
        It is required that in the dataframe it is present a column named 
        'Transfer data' reporting whether the sample is a transfer data or not.

        Parameters
        ----------
        data : pandas dataframe
            Pandas dataframe from which extracts the transfer data.
        
        target : str
            Value to be predicted.

        Returns
        -------
        train_in : pandas dataframe
            Input training data.
        
        tran : pandas dataframe
            Transfer data.

        train_out : pandas dataframe
            Output training data.

        """

        columns = data.columns
        if (not 'Transfer data' in columns) or (not target in columns):
            raise ValueError("In the provided dataframe there are no columns "
                             "named 'Transfer data' or '{}'.".format(target))
        
        # Extract the input, output and transfer samples
        train_out = data[data['Transfer data'] == 0][target]
        train_in = data[data['Transfer data'] == 0].drop('Transfer data', 1).drop(target, 1)
        tran = data[data['Transfer data'] == 1].drop('Transfer data', 1).drop(target, 1)

        return train_in, tran, train_out

    def znorm(self, input_data, tran, out):
        """
        Normalize the input (descriptors) and output data using the 
        Z-normalization.

        Parameters
        ----------
        input_data : pandas dataframe
            Descriptors.
        
        tran : pandas dataframe
            Transfer data.

        out : pandas dataframe
            Desired output.

        Returns
        -------
        norm_in : pandas dataframe
            Normalized input.
        
        norm_tran : pandas dataframe
            Normalized transfer dataset.

        norm_out : pandas dataframe
            Normalized output.

        """
        
        min_in, max_in = self.__norm_const(input_data, axis=0)
        min_tran, max_tran = self.__norm_const(tran, axis=0)
        min_out, max_out = out.min(), out.max()
        
        # Normalize input
        norm_in = (input_data - min_in)/(max_in - min_in)

        # Normalize transfer
        norm_tran = (tran - min_tran)/(max_tran - min_tran)

        # Normalize output
        norm_out = (out - min_out)/(max_out - min_out)

        return norm_in, norm_tran, norm_out, min_out, max_out

    def __norm_const(self, data, axis):
        """
        Extract the minimum and maximum values for the Z-normalization, and 
        perform a check of the size of the dataframes.

        Parameters
        ----------
        data : pandas dataframe
            Data from which extract the values.

        axis : int
            Extract the max and the min over rows or columns.
            0 for row, 1 for column.
  
        """

        try:
            min_d = data.min(axis=axis)
            max_d = data.max(axis=axis)
            min_shape = min_d.shape
            max_shape = max_d.shape
        except:
            raise ValueError("'{}' dataframe has some problems. "
                             "Check if the passed dataframe is correct.".format(data))

        try:
            assert data.shape[1] == min_shape[0]
            assert data.shape[1] == max_shape[0]
        except:
            raise ValueError("The number of columns of data and of min/max "
                             "dataframes are not the same.")

        return min_d, max_d

    def fit(self, input_data, out):
        """
        Train the Bayes model.

        Parameters
        ----------
        input_data : pandas dataframe
            Descriptors.
        
        tran : pandas dataframe
            Transfer data.

        out : pandas dataframe
            Desired output.
        
        Returns
        -------
        mu : float
            Mean of the Gaussian.

        sigma : float
            Covariance matrix of the Gaussian.

        """

        # Normalize data
        tran=pd.DataFrame()
        norm_in, norm_tran, norm_out, min_out, max_out = self.znorm(
            input_data=input_data, 
            tran=tran, out=out)

        # # Merge input and transfer for calculating the total covariance matrix
        # norm_tot_in = pd.merge(norm_in, norm_tran, how='outer')
        norm_tot_in = input_data
        norm_out = out
        
        mu = norm_out.mean()*np.ones(norm_tot_in.shape[0])
        sigma = norm_tot_in.cov().values
        noise_variance = np.std(norm_out)

        # Perform bayesian update assuming normal prior and noise, after 
        # observing outcome y for alternative labeled sample i
        for i in range(0, self.n_samp):
            gamma = 1 / (0.0170 + sigma[i, i])
            mu = mu + (norm_out.values[i] - mu[i]) * gamma * sigma[:, i]
            sigma = sigma - gamma * (sigma[:, i]*sigma[i, :])

        mu = mu * (max_out - min_out) + min_out
        return mu, sigma

    def predict(self, des):
        """
        Predict the MEB given the descriptors.

        Parameters
        ----------
        des : pandas dataframe
            Descriptors of the structure of interest.

        Returns
        -------
        out : float
            Output of the model.

        """
        pass
