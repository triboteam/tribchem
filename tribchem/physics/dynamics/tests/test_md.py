#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 17:27:53 2021

Test the MDAnalyzer class and print results.

@author: glosi000
"""

from tribchem.physics.dynamics.md import MDAnalyzer
import matplotlib.pyplot as plt
import numpy as np


folder = 'XYZTest/'
inputfile = folder + 'long.xyz'
output_file = folder + 'test.dat'
output_plot = folder + 'test.pdf'

# Initialize the object
cp = MDAnalyzer(inputfile)

# Calculate its center of mass
cp.calc_centermass()

# extract the interface distance between two elements, with a given modality
layer1 = 'S1'
layer2 = 'S3'
axis = 'z'
dist_cm = cp.interdist(layer1, layer2, 'cm', axis=axis, dt_kind='cp',
                       to_file=False)
dist_2p = cp.interdist(layer1, layer2, '2planes', axis=axis, dt_kind='cp',
                       to_file=output_file)

# Print number of steps, total atoms, and atoms per specie
print('Number of steps: {}'.format(cp.steps))
print('\nSystem of {} total atoms:'.format(cp.natoms))
for k, n in cp.species.items():
    print('{}: {} atoms'.format(k, n))

# Print the average CM throughout the entire simulation
if hasattr(cp, 'cm'):
    print('')
    i = {'x': 0, 'y': 1, 'z': 2}.get(axis, 2)
    for k, n in cp.cm.items():
        print('Average CM of {}: {:.4f} \u212B'.format(k, np.mean(n[:, i])))

# Print the average distance throughout the entire simulation
print('\nAverage cm distance {}-{}: {:.4f} \u212B'.format(layer1, layer2,
                                                          np.mean(dist_cm)))
print('Average 2p distance {}-{}: {:.4f} \u212B'.format(layer1, layer2,
                                                        np.mean(dist_2p)))

# Make a plot of dist_2p and save the figure
plt.plot(dist_2p[:, 0], dist_2p[:, 1])
plt.savefig(output_plot)
plt.show()
