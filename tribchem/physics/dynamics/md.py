#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains useful classes to analyze the output of an xyz file
obtained from a molecular dynamics simulation.

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

Version 1.2.2
-------------------------------------------------------------------------------
- Bug fix: correct the attributes name to avoid infinite recursive loop
- Bug fix: timesteps method was not called correctly

Version 1.2.1
-------------------------------------------------------------------------------
- Normalize units to SI, now units of pos, vel, acc are meter and seconds
- Bug fix: data are always converted to float when extracting coordinates

Version 1.2.0
-------------------------------------------------------------------------------
- Add two hidden methods to make the derivative of a dictionary attribute
- Add the calculation of velocity and acceleration for each single atomic 
- Now also the velocity and acceleration of the center of mass is calculated
- Add a general analyze_data method which make all the analysis
- Add new options for dt in QEVariables
- Created a script to run easily the md module
- Minor refactoring in methods names

Version 1.1.1
-------------------------------------------------------------------------------
- Now you can obtain the coordinate position of any atoms of each specie

Version 1.1.0
-------------------------------------------------------------------------------
- Created a new class containing global variables for standard dt values of QE
- Add a dt attribute for the simulation
- Add a a method to create a timesteps array, to be the abscissa of plots
- Add a dt_kind argument to generate automatically a timesteps array when
  calculating the distance between two atomic species
- Add a method to save arrays obtained from get_interdist to file
- Minor improvements in style to respect PEP8

Version 1.0.0
-------------------------------------------------------------------------------
- First release of the module, contains class: MDAnalyzer

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

TODO:
    - Add methods to calculate the velocity of CM of species (careful to pbc)
    - Implement new kind options for get_interdist, e.g. for 1-plane 2D layer,
      mixed-atomic distances, follow a single atom, etc...
    - Add the possibility to apply pbc if xyz files are made without.
    - Remove the possible pbc already present in xyz from parsed files.
    - Add the possibility to save new elaborated xyz files.
    - Add a new parameter to get_coords in order to automatically separate the
      atoms when you have a 2planes-type structure.

"""

__author__ = 'Gabriele Losi'
__version__ = "1.2.2"
__copyright__ = 'Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'January 8th, 2021'

import numpy as np

from tribchem.physics.dft.xyz import XYZReader
from tribchem.physics.dft.qe import QEVariables


class MDAnalyzer(XYZReader):
    """
    Analyze the result of MD simulations and elaborate data, such as finding
    the CM, calculate interfacial distance between two species.

    """

    def __init__(self, inputfile: str):
        """
        Initialize the MDAnalyzer class by calling the parent constructor.

        Parameters
        ----------
        inputfile : str
            Path to the input file.

        dt : float or None
            Single time step of the simulation. The default is None.

        """
        
        # Read the xyz input file and store variables
        super().__init__(inputfile)
    
    def analyze_data(self, dt_factor=1, dt_kind=None):
        
        # Set the time step of the simulation
        dt = dt_factor * QEVariables.dt.get(dt_kind, getattr(self, 'dt', 1))
        self.set_dt(dt)
        
        # Get position, velocity, acceleration for each single atom
        self.calc_positions()
        self.calc_velocities(dt_factor, dt_kind)
        self.calc_accelerations(dt_factor, dt_kind)
        
        # Calculate position, velocity, acceleration of cofm of atomic species
        self.calc_centermass()
    
    def calc_centermass(self, dt_factor=1, dt_kind=None):
        """
        Calculate the center of mass of each atomic specie present in the
        system at any step, during the entire simulation. Velocity and 
        acceleration of the center of masses are calculated as well.
        Three dictionary attributes are created: cm_pos, cm_vel, cm_acc.
        Keys: atomic species; Values: numpy arrays of shape (nstep, 3).

        Parameters
        ----------
        dt_factor : int or float, optional
            Multiplicative factor for dt. The default is 1.
            
        dt_kind : TYPE, optional
            Identify the dt to be used to create the timesteps array. 
            The default is None.
            
        """

        cm = {}

        # Calculate the mc for each atomic specie at sny step
        for el, nat in self.species.items():
            # Extract the coordinates for the atomic specie el
            data = self.get_coords(species=el)

            # Calculate the average x, y, z
            coords = self._average_coords(data, nat) * 1e-10  # Normalize to SI
            cm.update({el: coords})

        # Assign positioon and derive it twice to get velocity and acceleration
        self.cm_pos = cm 
        self.cm_vel = self.__make_derivative(self.cm_pos, dt_factor, dt_kind)
        self.cm_acc = self.__make_derivative(self.cm_vel, dt_factor, dt_kind)

    def interdist(self, specie1: str, specie2: str, kind='cm', axis=None,
                  dt_factor=1, dt_kind=None, to_file=False):
        """
        Calculate the relative inter-species distance between two different
        atomic types, specie1 and specie2, by subracting the coordinates of
        the second from the first one. The parameters kind and axis specify
        what kind of calculation to do and along any, or a given, axis.
        The relative distance is calculated for each step of the simulation.

        Parameters
        ----------
        specie1 : str
            First atomic specie to consider.

        specie2 : str
            Second atomic specie to consider.

        kind : str, optional
            The type of calculation to be performed. The default is 'cm'.
            It allows different parameters:

                - 'cm' :
                    A simple difference in the coordinates of the center of
                    mass of specie1 and specie2. It returns an array of shape
                    ( nstep, 3 (1 if axis is not None) ).
                    The center of mass of any specie is automatically
                    calculated when selecting this option.

                - '2planes' :
                    Meaningful for layered materials made, at least, of two
                    equal atomic planes, e.g. phosphorene or TMDs (MX2) for
                    the X atoms. It also works for structures where the atoms
                    are well clustered along two "rows" within each slab and
                    the two atomic species don't mix together.
                    The average coordinate along an axis is found fot the two
                    species and used to identify the upper and lower atomic
                    planes within the layers. The innermost distance is then
                    calculated. Here it is necessary to specify an axis.
                    It returns an array of shape (nstep, 1).
                
                - 'slab' :
                    Calculate the innermost smaller distance between two groups
                    of atoms. It makes sense to be used when all the atoms of
                    a slab (specie1) are always above the atoms of another slab
                    (specie2) during the entire simulation. Axis is required.
                    At each frame specie1.min(axis) and specie2.max(axis) are
                    calculated. The delta array of shape (nstep, 1) is returned.

        axis : str or None, optional
            Cartesian axis along which the average distance is calculated.
            The default is None.

        dt_kind : float or None, optional
            Select the kind of dt to call the get_timesteps method and create
            a proper temporal abscissa for the returned distance array.
            The default is None.

        to_file : bool or str, optional
            Export the distance array to a file. A string with the name of the
            file should be passed. The default is False.

        Returns
        -------
        interlayer_distance : np.ndarray
            Distance array between the coordinates of specie1 and specie2.
            The first column is the number of steps (or the simulation time).
            Negative values means that, on average, the two layers exchanged
            position at some point during the simulation.

        """

        # Check and set the required parameters
        assert(specie1 in self.species.keys())
        assert(specie2 in self.species.keys())
        i = {'x': 0, 'y': 1, 'z': 2}.get(axis, None)

        # Check if the center of mass has been already calculated
        if (kind == 'cm' or kind == 'defaut') and hasattr(self, 'cm_pos'):
            distance = self.cm_pos[specie1] - self.cm_pos[specie2]

            if i is not None:
                distance = distance[:, i]

        else:
            # Extract data of the two species
            data1 = self.get_coords(specie1)
            data2 = self.get_coords(specie2)

            # Number of atoms of the two species in the system
            nat1 = self.species[specie1]
            nat2 = self.species[specie2]

            # Average the coordinates # MAYBE USEFUL FOR NEW KIND
            # coords1 = self._average_coords(data1, nat1)
            # coords2 = self._average_coords(data2, nat2)

            # Default case, calculate the CM
            if kind == 'cm' or kind == 'default':
                self.calc_centermass()
                self.interdist(specie1, specie2, kind, axis)

            # Case of two slabs made of two planes of the given specie
            elif kind == '2planes':
                distance = self._average_2planes(data1, data2, nat1, nat2, i)
            
            elif kind == 'slab':
                distance = self._average_slab(data1, data2, nat1, nat2, i)

        # Get the timesteps array
        timesteps = self.timesteps(dt_factor, dt_kind)
        distance = np.column_stack([timesteps, distance])
        
        # Save the data to file
        if isinstance(to_file, str):
            np.savetxt(to_file, distance)

        return distance

    def _average_2planes(self, data1, data2, nat1: int, nat2: int, index=None):
        """
        Make the average of kind='2planes', as described in get_interdist
        docstring, for two sets of coordinates for two species of atoms.

        """

        if index is None:
            raise ValueError('For a "2planes" interdistance evaluation, '
                             'axis must be specified')

        j1, j2 = 0, 0
        k1, k2 = nat1, nat2
        distance = np.array([])

        for frame in range(self.steps):
            # Get the coordinates and their mean in the corresponding frame.
            # This is necessassry because the number of elements above or below
            # the correspective mean value may differ, i.e. len(up) != len(low)
            d1 = data1[j1:k1][:, index]
            d2 = data2[j2:k2][:, index]
            mean1 = np.mean(d1)
            mean2 = np.mean(d2)

            # Extract upper and lower values and calculate average distance.
            up = np.mean(d1[d1 < mean1])
            low = np.mean(d2[d2 > mean2])

            # Save the value
            distance = np.append(distance, [up - low])

            # Update parameters
            j1 += nat1
            j2 += nat2
            k1 += nat1
            k2 += nat2

        return np.array(distance)
    
    def _average_slab(self, data1, data2, nat1: int, nat2: int, index=None):
        """
        Make the average of kind='slab', as described in get_interdist
        docstring, for two sets of coordinates for two species of atoms.

        """

        if index is None:
            raise ValueError('For a "slab" interdistance evaluation, '
                             'axis must be specified')

        j1, j2 = 0, 0
        k1, k2 = nat1, nat2
        distance = np.array([])

        for frame in range(self.steps):
            # Get the coordinates and their mean in the corresponding frame.
            # This is necessassry because the number of elements above or below
            # the correspective mean value may differ, i.e. len(up) != len(low)
            d1 = data1[j1:k1][:, index]
            d2 = data2[j2:k2][:, index]

            up = np.min(d1)
            down = np.max(d2)

            # Save the value
            distance = np.append(distance, [up - down])

            # Update parameters
            j1 += nat1
            j2 += nat2
            k1 += nat1
            k2 += nat2

        return np.array(distance)

    def get_coords(self, species, frame=None, axis=None):
        """
        Extract the coordinates for a particular specie or a list of species,
        evaluated through the entire simulation. It is possible to extract a
        particular set of frames or just a given coordinate axis.

        Parameters
        ----------
        species : str or list (of str)
            List containing the species to be processed.

        frame : int (float) or list (np.ndarray), optional
            Frames of the simulation to be extracted. If a list is passed, the
            data will be sliced of [frame[0]:frame[1]]. The default is None.

        axis : str or None, optional
            The coordinates will be sliced along this Cartesian direction.
            Allowed values: 'x', 'y', 'z', None. If None (x, y, z) coordinates
            are returned. The default is None.

        Returns
        -------
        np.ndarray or list of np.ndarray
            Atomic coordinates for the selected species.
            If frame and axis are None, the array has shape (nstep*nat, 3).

        """

        species = [species] if isinstance(species, str) else species
        coordinates = []

        # Loop over the species to extract
        for s in species:
            # Get the whole set of data for specie s
            data = (self.xyz[self.xyz.iloc[:, 0] == s].iloc[:, 1:])
            data = data.to_numpy(dtype=np.float64)

            # Select the frame or frames of interest
            if frame is not None and isinstance(frame, (int, float)):
                data = data[int(frame), :]

            elif isinstance(frame, (list, np.ndarray)) and len(frame) == 2:
                data = data[int(frame[0]):int(frame[1]), :]
                data = (self.xyz[self.xyz.iloc[:, 0] == s].iloc[:, 1:]
                        ).to_numpy(dtype=np.float64)

            # Isolate the coordinates for a specific axis
            i = {'x': 0, 'y': 1, 'z': 2}.get(axis, None)
            if i is not None:
                data = data[:, i]

            # Save the coordinates for the specie s
            coordinates.append(data)

        return (coordinates[0] if len(coordinates) == 1 else coordinates)

    def _average_coords(self, data: np.ndarray, nat: int):
        """
        Sum a set of nat atoms along the entire simulation. Useful to calculate
        the average x, y, z coordinates for a given atomic specie.

        """

        x = np.add.reduceat(data[:, 0], np.arange(0, len(data[:, 0]), nat))
        y = np.add.reduceat(data[:, 1], np.arange(0, len(data[:, 1]), nat))
        z = np.add.reduceat(data[:, 2], np.arange(0, len(data[:, 2]), nat))
        
        coords = np.column_stack([x, y, z]) / nat
        
        return coords
    
    def timesteps(self, dt_factor=1, dt_kind=None):
        """
        Get the array of the time steps during the entire simulation.

        Parameters
        ----------
        nsteps : int
            Number of steps
            
        dt_factor : int or float, optional
            Multiplicative factor for dt. The default is 1.

        dt_kind : str or None, optional
            Identify the dt to be used to create the timesteps array.
            It can assume the following values:
                - 'bare_cp': 2.4189e-17
                - 'bare_pw': 4.8378e-17
                - 'cp': 10*5*2.4189e-17
                - 'pw': 20*4.8378e-17
                - 'femto': 1e-15
                - 'pico': 1e-12
            If None is passed, it uses the already existing dt attribute and
            if dt is not found it simply returns an array with steps number.

        Returns
        -------
        np.ndarray
            The timesteps array

        """
        
        # Get the default dt and calculate the timestep array
        dt = QEVariables.dt.get(dt_kind, getattr(self, 'dt', 1))

        timesteps = dt * dt_factor * (np.arange(0, self.steps) + dt)

        return timesteps
    
    def calc_positions(self):
        
        self.pos = {}
        for key in self.species.keys():
            self.pos[key] = []
        
        for index in range(self.natoms):
            at_table = self.xyz.iloc[range(index, len(self.xyz), self.natoms)]
            specie = at_table['atoms'].to_list()[0]
            matrix = at_table[['x', 'y', 'z']].to_numpy(dtype=np.float64)
            
            self.pos[specie].append(matrix * 1e-10)  # Normalize to SI
        
    def calc_velocities(self, dt_factor=1, dt_kind=None):
        
        if not hasattr(self, 'pos'):
             self.calc_positions()
        
        self.vel = self.__make_derivative(self.pos, dt_factor, dt_kind)
    
    def calc_accelerations(self, dt_factor=1, dt_kind=None):
        
        if not hasattr(self, 'vel'):
             self.calc_velocities()

        self.acc = self.__make_derivative(self.vel, dt_factor, dt_kind)
    
    def __make_derivative(self, attribute, dt_factor=1, dt_kind=None):
        """ 
        Make the derivative of an attribute dictionary.
        Elements of the dictionary can be either matrix or a list of matrix.
        
        """
        
        # Calculate the time step to derive
        timesteps = self.timesteps(dt_factor, dt_kind)
        dt = timesteps[1] - timesteps[0]
        
        # Calculate velocities and reassign value to corresponding key
        derivative = attribute.copy()
        for key in derivative.keys(): 
            element = derivative[key]
            
            # Deal with the case of lists of elements
            if isinstance(element, list):
                tmp = []
                for el in element:
                    differential = self.__diff_xyz(el, dt)
                    tmp.append(differential)
            else:                
                tmp = self.__diff_xyz(element, dt)
            
            derivative[key] = tmp

        return derivative
    
    def __diff_xyz(self, matrix, dt):
        """ Calculate differentials for x, y, z columns of a matrix
        """
        
        dx = np.diff(matrix[:, 0]) / dt
        dy = np.diff(matrix[:, 1]) / dt
        dz = np.diff(matrix[:, 2]) / dt
        
        return np.column_stack((dx, dy, dz))
