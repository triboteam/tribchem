#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 09:42:02 2021

Basic script to use the `md` Module.

@author: glosi000
"""

__author__ = 'Gabriele Losi'
__copyright__ = 'Prof. M.C. Righi, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'January 8th, 2021'

from tribchem.physics.dynamics.md import MDAnalyzer, QEVariables
import matplotlib.pyplot as plt

# ============================================================================
# CONTROL PANEL
# ============================================================================

# XYZ Input file
inputfile = '../tests/XYZTest/md.xyz'

# Define the time step of the simulation as  dt_factor*QEVariables.dt[dt_kind]
dt_factor = 20  # Factor you choose to multiply the dt of 1 a.u. (CONTROL card) 
dt_kind = 'bare_pw'  # Key to extract default dt values from QEVariables.dt


# ============================================================================
# Run the Program
# ============================================================================

md = MDAnalyzer(inputfile)  # Initialize the object

md.set_dt(dt_factor * QEVariables.dt[dt_kind])  # Set the time step

md.analyze_data()  # Get pos, vel, acc for atoms and cofmass : SI units

timesteps = md.timesteps()  # Get the timesteps array

plt.plot(timesteps, md.cm_pos['Si1'][:, 2])  # Plot example: z-pos S1 cm atoms
plt.show()
plt.plot(timesteps[:-1], md.cm_vel['Si1'][:, 2])
plt.show()
plt.plot(timesteps[:-2], md.cm_acc['Si1'][:, 2])
plt.show()

# plt.plot(timesteps, md.pos['Si1'][0][:, 2])  # Plot example: pos of 1st S1 atom
