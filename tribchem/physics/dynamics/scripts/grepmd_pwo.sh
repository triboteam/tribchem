#!/bin/bash

###################################################################################
#Script Name	: grepmd_pwo.sh
#Description	: Retrieve info from a md simulation made with pw.x and make plots
#Args           : Output file of type *.pwo 
#Copyright      : Prof. Maria Clelia Righi, University of Bologna
#Author       	: Michele Cutini                                                
#Email         	: michele.cutini@unibo.it                                           
###################################################################################

# Retrieve all the info that we need from a .pwo file
# Temperature, Kinetic Energy, Potential Energy, Total Energy, time step, ps
# Put these info inside a temporary file (.tmp), using also support files as .tmp2 and .tmp3
# The argument of the script is $1
grep 'temperature           ='  $1  | awk '{print $3}' > .tmp
grep 'kinetic energy (Ekin) =' $1 | awk '{print $5}' > .tmp2  && paste .tmp .tmp2 > .tmp3 && mv .tmp3 .tmp
grep '!    total energy              =' $1 | awk '{print $5}' > .tmp2  && paste .tmp .tmp2 > .tmp3 && mv .tmp3 .tmp
grep 'Ekin + Etot (const)   = ' $1 | awk '{print $6}' > .tmp2  && paste .tmp .tmp2 > .tmp3 && mv .tmp3 .tmp
grep 'Entering Dynamics:    iteration =' $1 | awk '{print $5}' > .tmp2  && paste .tmp .tmp2 > .tmp3 && mv .tmp3 .tmp
grep ' time      = ' $1 | awk '{print $3}' > .tmp2  && paste .tmp .tmp2 > .tmp3 && mv .tmp3 .tmp
rm .tmp2

# Convert the .pwo file to a xyz file with coordinates
num=$(grep -m 1 number\ of\ atoms $1 | awk '{print $5}')
grep -A $a ATOMIC_POSITIONS $1 >  $1.xyz
sed -i "s/--/$num/g" $1.xyz
sed -i "1s/^/$num \n/" $1.xyz
sed -i 's/0   0   0//g' $1.xyz

# Let's do a nice plot for T, ekin, epot, etot vs simulation time (ps)
# A file input.p to be given to gnuplot is vuilt
# Set the output to a png file
# echo > input.p
echo "set terminal png               " > input.p
echo "set output 'data.png'          " >> input.p
echo "set multiplot layout 2,2 title   '$1'   " >> input.p
echo "set xtics rotate               " >> input.p
echo "unset key                      " >> input.p
echo "                               " >> input.p
echo "set title 'temperature'        " >> input.p
echo "plot '.tmp' using 6:1 w l      " >> input.p
echo "set title 'kinetic energy'     " >> input.p
echo "plot '.tmp' using 6:2 w l      " >> input.p
echo "set  title 'potential energy'  " >> input.p
echo "plot '.tmp' using 6:3 w l      " >> input.p
echo "set title 'total energy'       " >> input.p
echo "plot '.tmp' using 6:4 w l      " >> input.p

gnuplot input.p
display data.png &

mv .tmp data_md
wait
