#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 1st 15:25:00 2021

Tests for the ChemAds class.

Author: Gabriele Losi (glosi000), Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi, Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'July 1st, 2021'


from pymatgen.core.surface import Slab

from tribchem.highput.database.navigator import Navigator
from tribchem.physics.chemabs.adsorption import ChemAds

database = 'tribchem'

def get_slab(element, miller):
    """
    Helper function for getting the slab on which to execute the test from the 
    database.
    
    Parameters
    ----------
    element : str
        Formula of the element.
    
    miller : list
        Miller indices of the slab.

    Return
    ------
    slab : pymatgen structure
        Slab.

    """

    if not isinstance(element, str):
        raise ValueError("The element is not a string!")

    if not isinstance(miller, list):
        raise ValueError("The Miller indices are not in a list!")
    
    nav = Navigator(high_level=database)
    slab = nav.find_data(collection='PBE.slab_elements', 
                        fltr={'formula': element, 'miller': miller})
    
    slab = Slab.from_dict(slab['structure']['opt'])
    
    return slab

def test_get_sites():
    """
    Test the methods of the ChemAds class.
    """

    slab = get_slab(element='Cu', miller=[1, 1, 1])

    ads = ChemAds()

    sites = ads.get_sites(slab=slab, element='Cu', plot=True)
    print(sites.find_adsorption_sites())

    poscar = '/home/omar/Lavoro/Unibo/TribChemProject/test_adsorption/POSCAR'
    sites = ads.sites_from_file(file=poscar, element='Fe', plot=True)

def test_add_adsorbate():
    """
    Test if an adsorbate is added to a slab in the adsorption sites.
    """
    
    element = 'Fe'
    miller = [0, 0, 1]
    
    nav = Navigator(high_level=database)
    slab = nav.find_data(collection='PBE.slab_elements', 
                        fltr={'formula': element, 'miller': miller})
    slab = Slab.from_dict(slab['structure']['opt'])
    
    ads = ChemAds(slab)
    
    # Cu
    # slab = get_slab(element='Cu', miller=[0, 0, 1])
    # adsorbate = 'O'
    # sites = ads.get_sites(slab=slab, element='Cu', plot=True)
    # sites_ads = ads.add_adsorbate(adsorbate=adsorbate, element='Cu',
    #                               sites=sites, plot_adsorbeds=True)
    # print(sites_ads)

    # Al
    slab = get_slab(element=element, miller=miller)
    adsorbate = 'O'
    sites = ads.get_sites(to_plot=True)

    sites_ads = ads.add_adsorbate(adsorbate, to_plot=True)

    # sites_ads_both = ads.add_adsorbate_both_surface(adsorbate=adsorbate, 
    #                                                 element=element, 
    #                                                 sites=sites, 
    #                                                 plot_adsorbeds=True)

# test_get_sites()
test_add_adsorbate()
