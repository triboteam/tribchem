#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 1st 11:15:00 2021

Author: Gabriele Losi (glosi000), Omar Chehaimi (omarchehaimi)
Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna

"""

__author__ = 'Gabriele Losi, Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'July 1st, 2021'


import os

import numpy as np
import matplotlib.pyplot as plt
# from ase.build import molecule
# from pymatgen.io.ase import AseAtomsAdaptor
from pymatgen.core.structure import Molecule
from pymatgen.analysis.adsorption import AdsorbateSiteFinder, plot_slab
from pymatgen.core.surface import Slab

from tribchem.physics.tribology.highsym import get_slab_hs


class ChemAbs:
    """
    Class containing all the necessary methods for dealing with adsorbates 
    object on slabs.
    
    Methods
    -------
    get_sites()
        Get the adsorption sites of a pymatgen structure.
    
    get_sites_from_file()
        Get the adsorption sites of a structure from a file.
    
    plot_adsorption()
        Plot the adsorption sites of a slab.
    
    get_adsorbate()
        Create the adsorbate.
        
    get_rotated_adsorbate()
        Create an adsorbate and rotate it.
    
    add_adsorbate()
        Add atoms or moleculas in the adsorption sites of a slab.

    """

    def __init__(self, structure, name=None):
        
        # Check for input consistency
        if not type(structure) == Slab:
            raise ValueError("The slab is not a 'Structure' object!")
        
        # Assign attributes
        self.structure = structure
        self.name = name

    def get_sites(self, height=0.9, mi_vec=None, selective_dynamics=False, 
                  to_plot=False):
        """
        Get the adsorption sites of a pymatgen structure.
        
        Parameters
        ----------
        slab : Structure
            The slab of which to find the adsorption points.
        
        element : str
            Name of the element.
        
        selective_dynamics : bool
            Flag for whether to assign non-surface sites as fixed for selective 
            dynamics.
            (from pymatgen.analysis.adsorption import AdsorbateSiteFinder)
        
        height : float 
            Height criteria for selection of surface sites.
            (from pymatgen.analysis.adsorption import AdsorbateSiteFinder)
        
        mi_vec : 3-D array-like 
            Vector corresponding to the vector concurrent with the miller index, 
            this enables use with slabs that have been reoriented, but the 
            miller vector must be supplied manually.
            (from pymatgen.analysis.adsorption import AdsorbateSiteFinder)
        
        plot : bool, optional
            Set to True to plot the adsorption sites.
        
        Return
        ------
        sites_adf : AdsorbateSiteFinder
            Object containing all the information about the adsoption sites.
        
        sites : dict
            Dictionary containing the location of the adsorption sites in 
            cartesian coordinates for all the high symmetry points.

            E.g.:
            {'ontop': [array([ 1.23743687,  1.23743687, 16.875])], 
            'bridge': [array([1.23743687e+00, 4.27067681e-16, 1.68750000e+01]), 
                       array([9.76600041e-16, 4.27067681e-16, 1.68750000e+01])], 
            'hollow': [], 
            'all': [array([ 1.23743687,  1.23743687, 16.875]), 
                    array([1.23743687e+00, 4.27067681e-16, 1.68750000e+01]), 
                    array([9.76600041e-16, 4.27067681e-16, 1.68750000e+01])]}

        """

        # Create the AdsorbateFinderObject
        sitefinder = AdsorbateSiteFinder(self.structure, height=height, mi_vec=mi_vec,
                                         selective_dynamics=selective_dynamics)
        
        # Assign the object and the sites
        self.sitefinder = sitefinder
        self.sites, self.all_sites = get_slab_hs(self.structure)

        if to_plot:
            self.plot_sites()

    def add_adsorbate(self, adsorbate, coverage=1, both_surfaces=False, hdist=1.5,
                      to_plot=False, min_lw=5, translate=True, reorient=True, 
                      find_args=None, a=None, v=None, center=(0, 0, 0), 
                      rotate_cell=False, height=0.9, mi_vec=None, 
                      selective_dynamics=False):
        """
        Add atoms or moleculas in the adsorption sites of a slab.

        Parameters
        ----------
        adsorbate : str
            Name of the atom or molecule to add to a slab.

        element : str
            Name of the element.
        
        sites : AdsorbateSiteFinder
            Adsorption sites object.
        
        plot_adsorbeds : bool, optional
            Set to True to plot the adsorbates.
        
        hdist : int or float, optional
            Vertical distance in Angstrom of the adsorbates from the surface.
            The default is 1.

        min_lw : float
            Minimum length and width of the slab, only used if repeat is None.
            (from pymatgen.analysis.adsorption import AdsorbateSiteFinder)
        
        repeat : 3-tuple or list 
            Repeat argument for supercell generation.
            (from pymatgen.analysis.adsorption import AdsorbateSiteFinder)
        
        translate : bool 
            Flag on whether to translate the molecule so that its CoM is at the 
            origin prior to adding it to the surface.
            (from pymatgen.analysis.adsorption import AdsorbateSiteFinder)
        
        reorient : bool 
            Flag on whether or not to reorient adsorbate along the miller index.
            (from pymatgen.analysis.adsorption import AdsorbateSiteFinder)
        
        find_args : dict 
            Dictionary of arguments to be passed to the call to 
            self.find_adsorption_sites, e.g. {"distance":2.0}.
            (from pymatgen.analysis.adsorption import AdsorbateSiteFinder)

        a : float
            Angle that the atoms is rotated around the vector 'v'. 'a'
            can also be a vector and then 'a' is rotated
            into 'v'.
            (from ase.Atom import rotate)

        v : str
            Vector to rotate the atoms around. Vectors can be given as
            strings: 'x', '-x', 'y', ... .
            (from ase.Atom import rotate)

        center : tuple, (0, 0, 0)
            The center is kept fixed under the rotation. Use 'COM' to fix
            the center of mass, 'COP' to fix the center of positions or
            'COU' to fix the center of cell.
            (from ase.Atom import rotate)

        rotate_cell : False
            If true the cell is also rotated.
            (from ase.Atom import rotate)

        Return
        ------
        adsorbent : struct
            Slab with the adsorbate. 
        
        """

        if not hasattr(self, 'sites') or not hasattr(self, 'sitefinder'):
            # Check for the presence of HS sites
            self.get_sites(height, mi_vec, selective_dynamics, to_plot)
        
        # If the angle of rotation is indicated get a rotated adsorbate,
        # otherwise get a simple adsorbate.
        if a:
            ads = self.get_rotated_adsorbate(name=adsorbate, a=a, v=v, 
                                             center=center,
                                             rotate_cell=rotate_cell)
        else:
            ads = self.get_adsorbate(name=adsorbate)

        # Find repeat argument for supercell generation
        ads_on_slab, repeat = self._add_adsorbate(ads, coverage, hdist,
                                                  both_surfaces=both_surfaces,
                                                  translate=translate, reorient=reorient)    

        # Assign the results
        self.ads_structure = ads_on_slab
        self.repeat = repeat
        self.adsorbate = adsorbate

        if to_plot:
            self.plot_adsorbate()

    def _add_adsorbate(self, molecule, coverage, hdist=1.5, thr=0.01, max_replica=10, 
                       both_surfaces=False, translate=True, reorient=True):
        """
        Find the sites and calculate the repetition to replicate the cell and
        obtain a certain level of coverage. Then put the adsorbate on the sites.
        You can also select the percentage threshold of error in selecting the
        coverage, i.e. thr=0.01 means that your coverage of 0.5, can be obtained
        between 0.49 and 0.51. The maximum number of lateral replica along
        axis direction can be set.

        """
        
        # Check input arguments
        if not isinstance(coverage, (int, float)):
            raise ValueError('Wrong coverage, should be a number')
        elif coverage <= 0 or coverage > 1:
            raise ValueError('Wrong coverage, should be 0 < coverage <= 1')
        
        # Define the vertical shift, for placing correctly the adsorbate along z
        shift = np.array([0, 0, hdist])

        # Treat the replica case
        repeat = {}
        surfaces = {}
        for k in self.sites.keys():
            repeat_tmp = []
            for i in range(1, max_replica+1):
                for j in range(1, max_replica+1):
                    count_replica = i * j
                    if abs(1/count_replica - coverage) < thr:
                        repeat_tmp.append((i, j, 1))
            # Find out the best candidate
            v = [rt[0] + rt[1] for rt in repeat_tmp]
            repeat[k] = repeat_tmp[np.argwhere(v==np.min(v))[0][0]]

            # Select the method to be used for the adsorption operation
            if not both_surfaces:
                surfaces[k] = self.sitefinder.add_adsorbate(
                    molecule,
                    self.sites[k][0] + shift,
                    repeat=repeat[k],
                    translate=translate,
                    reorient=reorient
                    )

        # for coords in self.sitefinder.find_adsorption_sites()["all"]:

        # Select the sites
        # adding_sites = {}
        # repeat = {}
        # surfaces = {}
        # to_replicate = False
        # for k in self.sites.keys():
        #     # Count how many points of the same kind are within the cell
        #     count = len(self.all_sites[k])
        #     adding_sites[k] = None
            
        #     if count == 1:
        #         # If I have one point and ask for 100%, keep that point
        #         if coverage == 1:
        #             adding_sites[k] = self.sites[k]
        #             repeat[k] = (1, 1, 1)

        #         # Default case, I have one point and less than 100%
        #         # If we are above threshold, then we have to replicate
        #         else:
        #             if abs(1/count - coverage) < thr:
        #                 adding_sites[k] = self.sites[k]
        #                 repeat[k] = (1, 1, 1)
        #             else:
        #                 to_replicate = True
            
            # else:
            #     # If I have more points and ask for 100%, keep all of them
            #     if count > 1 and coverage == 1:
            #         adding_sites[k] = self.all_sites[k]
            #         repeat[k] = (1, 1, 1)
    
            #     # If I have more points and ask for something below 100%
            #     # Before replicating the cell check first in the unit one
            #     elif count > 1 and coverage < 1:
            #         n = [abs(i/count - coverage) < thr for i in range(1, count+1)]
            #         try:
            #             required_n = next(i+1 for i, v in enumerate(n) if v)
            #             adding_sites[k] = self.all_sites[:required_n]
            #             repeat[k] = (1, 1, 1)
            #         except: 
            #             to_replicate = True
                            
            # if to_replicate:
            #     found = False
            #     for i in range(1, max_replica+1):
            #         for j in range(1, max_replica+1):
                        
            #             # if (i, j) == (1, 1): continue

            #             # Search for the closest value satisfying condition
            #             count_replica = count * i * j
            #             r = range(1, count_replica+1)
            #             n = [abs(i/count_replica - coverage) < thr for i in r]

            #             try:
            #                 required_n = (np.array(n) == True)[0]
            #                 slab = self.structure.copy()
            #                 slab.make_supercell(repeat=(i, j, 1))
            #                 _, hs = get_slab_hs(slab)
            #                 adding_sites[k] = hs[k][:required_n]
            #                 found = True
            #             except:
            #                 count_tmp = count_replica
                        
            #             if found:
            #                 break
            #         if found:
            #             break

            # # Treat the replica case
            # if to_replicate:
            #     found = False
            #     for i in range(1, max_replica+1):
            #         for j in range(1, max_replica+1):
            #             count_replica = i * j
            #             if abs(1/count_replica - coverage) < thr:
            #                 adding_sites[k] = hs[k][:required_n]
            #                 found = True

            # # Select the method to be used for the adsorption operation
            # if not both_surfaces:
            #     structs = []
            #     for coords in adding_sites[k]:
            #         structs.append(
            #             self.sitefinder.add_adsorbate(
            #                 molecule,
            #                 coords,
            #                 repeat=repeat,
            #                 translate=translate,
            #                 reorient=reorient,
            #             )
            #         )
            #     surfaces[k] = structs

        return surfaces, repeat

    def get_adsorbate(self, name):
        """
        Create an adsorbate.
        
        Parameters
        ----------
        name : str
            Name of the atom or molecule to be adsorbed.
        
        Return
        ------
        pymatgen.core.structure.Molecule
            Atom or molecule as a pymatgen molecule object.
        
        """
        return Molecule(name, [[0, 0, 0]])

    @classmethod
    def from_file(cls, file):
        """
        Load a structure from file.
        
        Parameters
        ----------
        file : str
            Location where the structure is saved.
        
        Return
        ------
        tribchem.physics.chemabs.adsorption.ChemAds object

        """
        
        # Check if the file exists
        if not os.path.isfile(file):
            raise ValueError("The file {} does not exist!".format(file))
            
        # Read the structure from file
        structure = Slab.from_file(file)
  
        return cls(structure)

    def plot_sites(self):
        """
        Plot the adsorption sites of a slab.
        
        Parameters
        ----------
        sites : AdsorbateSiteFinder
            Adsorbate sites.
        
        element : str
            Name of the element.

        """
        
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plot_slab(self.sitefinder.slab, ax, adsorption_sites=True)
        plt.title("Adsorption sites of {}".format(str(self.name)))
        plt.show()

    def plot_adsorbate(self):
        """
        Plot the adsorbeds on the slab.

        Parameters
        ----------
        adsorbent : struct
            Slab with the adsorbate.
        
         element : str
            Name of the element.
        
        adsorbate_name : str
            Name of the adsorbate.

        """

        fig = plt.figure()
        axes = [fig.add_subplot(1, len(self.ads_structure)+1, i) 
                for i in range(1, len(self.ads_structure)+1)]

        for i in range(len(self.ads_structure)):
            # Plot all adsorption configurations
            plot_slab(self.ads_structure[i], axes[i], adsorption_sites=False)
        plt.title("{} adsorbates on a slab of {}".format(str(self.adsorbate),
                                                         str(self.name)),
                  loc='center')
        plt.show()
