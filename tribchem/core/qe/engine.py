#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 12:24:00 2021

This class implement a simple engine able to launch and submit a Quantum
ESPRESSO calculation in a cluster system.

Author: Omar Chehaimi (omarchehaimi), Gabriele Losi (glosi000)
Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna

"""

__author__ = 'Omar Chehaimi, Gabriele Losi'
__copyright__ = 'Prof. M.C. Righi, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'October 21st, 2021'

import os
import json
from pathlib import Path

from tribchem.core.logging import LoggingBase
from tribchem.highput.database.navigator import Navigator

class Engine:
    """
    Base class which implements the main methods to submit a series of Quantum
    ESPRESSO calculations.
    
    Attributes
    ----------
    job_name : str
        Name of the job to be launched.
        
    w_dir : str
        Working direcotry where the jobscripts are saved. Inside this folder
        are created the folder of each job, named as job_name.
        
    modules : str or list
        If modules='cineca' the modules used are the default for using QE in 
        Cineca. If instead a list of strings is passed, each of them contains 
        the module to be loaded.
    
    software : str, optional
        Name of the software to be used. It could be vasp or qe.
        E.g.: qe-gpu/6.8 or vasp/6.1.2
        
    kwargs : dict
        Slurm options.

    """
    
    def __init__(self, job_name, w_dir, software, modules='cineca', 
                 **kwargs):
        
        self.job_name = job_name
        self.w_dir = w_dir
        self.modules = modules
        self.software = software
        self.kwargs = kwargs
        
        self.job_dir = self.w_dir + "/" + self.job_name
        self.job_file_name = self.job_dir + "/" + self.job_name + ".script"
        
        # Logger initialization
        self.log = self.__logger_initializer()
    
    def __logger_initializer(self):
        """
        Initialize the logger.
        
        Return
        ------
        log : LoggingBase
            Object of the LoggingBase class for logging.

        """
        
        configurations = LoggingBase.get_config()
        log_path = LoggingBase.logging_path()

        # General log for failures caused by the computer
        log = LoggingBase(
            name='qe_log',
            console_level=configurations['logging']['console_logging_level'], 
            path=log_path+'/qe_log.log',
            file_level=configurations['logging']['file_logging_level'])
        
        return log
    
    def generate_jobscript(self):
        """
        Generate the jobscript and save it.
        """
        job_script = ""
        job_script += self.__genereate_header()
        job_script += "\n"
        job_script += self.__load_modules()
        job_script += "\n"
        job_script += self.__run_command()
        
        with open(self.job_file_name, "w") as f:
            f.write(job_script)
        
        self.log.info("Jobscript saved in {}".format(self.job_dir))
    
    def __genereate_header(self):
        """
        Generate the header of the jobscript.
        """
        
        header = "#!/bin/bash -l\n"
        settings = self.__get_default_settings()
        for name, setting in settings.items():
            # If a Slurm setting is passed use it
            if name in self.kwargs:
                setting = self.kwargs[name]

            total = name + "=" + setting
            header += self.__generate_line(total)

        return header
    
    def __get_default_settings(self):
        """
        Get the default settings for the header file. These settings are
        optimized fot Cineca Marconi100.
        """
        
        project_folder = os.path.dirname(__file__)
        settings_path = project_folder + '/settings.json' 
        with open(settings_path, 'r') as settings:
            settings = json.load(settings)
        
        # Naming the job-name, the output, and the error
        settings['job-name'] = self.job_name
        settings['output'] = self.job_name + "-%j.out"
        settings['error'] = self.job_name + "-%j.error"
        
        return settings
    
    def __generate_line(self, setting):
        """
        Generate a line of the header.
        
        Parameters
        ----------
        setting : str
            Name of the setting with its value. 
        """
        line = "#SBATCH --" + setting + "\n"
        return line
    
    def __load_modules(self):
        """
        Generate the loading modules section and change the directory where the
        input file is saved.
        """
        
        modules = ""        
        if self.modules == "cineca":
            modules += "module purge\n"
            modules += "module load profile/chem-phys\n"
            modules += "module load autoload " + self.software + "\n"
        elif isinstance(self.modules, list):  
            for module in self.modules:
                modules += module + "\n"
        else:
            raise ValueError("The modules attribute is neither a string nor "
                             "a list!")

        self.__check_dir()
        modules += "cd " + self.job_dir + "\n"

        return modules

    def __check_dir(self):
        """
        Check if the directory for the job exists.
        """
        
        job_path = Path(self.job_dir)
        
        if not job_path.is_dir():
            os.mkdir(self.job_dir)
            job_path = Path(self.job_dir)
            if not job_path.is_dir():
                raise ValueError("The creation of tje path has failed!")
        
    def __run_command(self):
        """
        Write the command for running VASP or QE.
        """
        
        software = self.software
        software = software.split("/")[0]
        if software == "vasp":
            return "mpirun -np 4 --rank-by core vasp_std_acc\n"
        elif software == "qe-gpu":
            return "srun --cpu-bind=core pw.x -input input.in > output.out\n"
        else:
            raise ValueError("The name of the software is neither VASP nor QE!")

    def submit(self):
        """
        Submit a job.
        """
        
        try:
            cmd = "sbatch " + self.job_name
            os.system(cmd)
            self.log.info("Job {} sumbitted".format(self.job_name))
        except:
            self.log.critical("The submission of the job failed!")
            raise ValueError("The submission of the job failed!")
    
    def _get_from_db(self, collection, fltr, db_file='localhost', 
                     database='tribchem'):
        """
        Get the data from the database.
        
        Parameters
        ----------
        collection : str
            Name of the collection.
            
        fltr : dict
            Dictionary containing the filter of the search.
        
        db_file : str, optional
            Path where the database file is saved.
            
        database : str, optional
            Name of the database.

        """
        
        nav = Navigator(db_file=db_file, high_level=database)
        
        try:
            data = nav.find_data(collection, fltr)
            
            if not data:
                self.log.warning("No data were found with the filter {} in "
                                 "the collection {}".format(fltr, collection))
        except:
            raise ValueError("An error occurred while getting data from the " 
                             "database")

class QE(Engine):
    """
    In this class are implemented the methods which deal the Quantum ESPRESSO
    inputs/outputs with the database.

    Attributes
    ----------
    job_names : list
        List of string containing the name of the jobs to be executed.
    
    """
    
    def __init__(self, job_names, w_dir, software, modules='cineca', **kwargs):
        self.job_names = job_names
        self.w_dir = w_dir
        self.software = software
        self.modules = modules
        self.kwargs = kwargs
    
    def generate_all_jobscripts(self):
        """
        Generate all the jobscripts.
        """
        
        for job_name in self.job_names:
            en = Engine(job_name=job_name, w_dir=self.w_dir, 
                        software=self.software, modules=self.modules, 
                        kwargs=self.kwargs)
            en.generate_jobscript()
        