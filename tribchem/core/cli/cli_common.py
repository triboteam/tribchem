#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 14:44:00 2021

Auxiliary script for the CLI containing commong methods.

@author: omarchehaimi

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'November 16th, 2021'

from numpy import true_divide


class CliCommon:
    """
    Class containing some common methods for the cli.

    """
    
    @staticmethod
    def get_par(p):
        """
        Get the compulsory parameter.
        """
        try:
            p = p.split('=')[1]
        except:
            raise ValueError("Specify the parameter name. E.g.: \'pes_type=pes\'.")
        
        return p

    @staticmethod
    def get_list(l):
        """
        Get a list of string for a compulsory parameter of the cli.
        """
        l = l.split('=')[1]
        l = l.replace('[', '')
        l = l.replace(']', '')
        l = l.replace(' ', '')
        l = l.split(',')

        return l

    @staticmethod
    def get_par_list_opt(p):
        """
        Get an optional list parameter.
        """
        p = p.replace('[', '').replace(']', '').replace(',', '').replace(' ', '')

        return [int(i) for i in p]

    @staticmethod
    def get_list_opt(l, t=None):
        """
        Get a list for an optional parameter of the cli.
        """
        l = l.replace('[', '')
        l = l.replace(']', '')
        l = l.replace(' ', '')
        l = l.split(',')
        
        if t == 'int':
            l = [int(i) for i in l]
        elif t == 'float':
            l = [float(i) for i in l]
        elif t == 'string':
            l = [str(i) for i in l]
        elif (t != 'int' or t != 'float' or t != 'string') and t:
            raise ValueError("Type can only be either 'int', 'float' or 'string'.")
            
        return l
    
    @staticmethod
    def get_dict(d):
        """
        Get a dictionary out of an optional parameter of the cli.
        """
        res = {}
        d = d.split(',')
        
        for kv in d:
            kv = kv.replace('{', '').replace('}', '')
            k, v = kv.split(':')
            k = k.replace(' ', '').replace('\'', '')
            v = v.replace(' ', '')
            res[k] = float(v)
        
        return res
    
    @staticmethod
    def get_none(p):
        """
        Get parameters that might be None by default.
        """
        if p:
            return p
        else:
            return None
    
    @staticmethod
    def get_miller(miller):
        """
        Get the miller indices from the cli.
        """
        miller = miller.split('=')[1]
        miller = miller.split('],')
        
        miller = [i.replace('[', '') for i in miller]
        miller = [i.replace(']', '') for i in miller]
        miller = [i.replace(' ', '') for i in miller]
        
        for i in range(0, len(miller)):
            miller[i] = miller[i].split(',')
            miller[i] = [int(j) for j in miller[i]]
        
        return miller

    @staticmethod
    def get_int(v):
        """
        Convert a string of the format 'value=10' to int of 10.
        """
        v = v.split('=')[1]

        return int(v)
    
    @staticmethod
    def check_mids_formulas(mids, formulas):
        """
        Check if the mids and formulas have the same size.
        """
        if len(mids) != len(formulas): 
            raise ValueError('The number of mids is different from the number '
                             'of formulas.')
    
    @staticmethod
    def check_mids_formulas_miller(mids, formulas, miller):
        """
        Check if the mids and formulas have the same size.
        """
        if (len(mids) != len(formulas)) or (len(mids) != len(miller)) or (len(formulas) != len(miller)): 
            raise ValueError('The number of mids is different from the number '
                             'of formulas and miller.')
            
    @staticmethod
    def set_run_vasp(rv):
        """
        Set the run_vasp variable to True or False.
        """
        if rv == 'y':
            return True
        elif rv == 'n':
            return False
        else:
            raise ValueError("The input for run_vasp (rv) must be 'y' or 'n'.")
