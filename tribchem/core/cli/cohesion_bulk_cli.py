#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 14:44:00 2021

Auxiliary script for the CLI for cohesion_bulk.py.

@author: omarchehaimi

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'November 16th, 2021'

from tribchem.core.cli.cli_common import CliCommon

class CliCohesionBulk(CliCommon):
    """
    Class containing all the methods necessary to deal with the command line
    interface for the interface wf.

    """
    
    @staticmethod
    def print_messsage(formulas):
        """
        Print a summary message.
        """
        message = 'Calculating the cohesion energy for {}'.format(formulas)
        print(message)