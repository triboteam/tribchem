#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 15 11:00:00 2021

Auxiliary script for the CLI for calc_interface.py. Here are set and generated
the inputs for the various cases that might happen for launching the 
calc_interface.py workflows.

@author: omarchehaimi

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'November 15th, 2021'

from os import stat
from fireworks import Firework

from tribchem.core.cli.cli_common import CliCommon

class CliInterface(CliCommon):
    """
    Class containing all the methods necessary to deal with the command line
    interface for the interface wf.

    """
    
    @staticmethod
    def get_wfs(wf):
        """
        Get the workflow arguments from the cli.        
        """
        wf = wf.split('=')[1]
        wf = wf.replace('[', '')
        wf = wf.replace(']', '')
        wf = wf.split(',')
        # Remove white spaces
        wf = [i.replace(' ', '') for i in wf]
        
        return wf
        
    @staticmethod
    def check_workflows(wf):
        """
        Check if the workflows have the proper order of execution.
        
        Parameters
        ----------
        wf : list of string
            List with the workflows. The available workflows are:
                - FT_StartInterfaceMatch
                - FT_StartPES
                - FT_StartAdhesion
                - FT_StartPPES
                - FT_StartChargeDisplacement
            The order can not be changed.
        
        Return
        ------
        wf : list
            List of the wf to be runned.
        
        """
        
        available_wf = ['interface', 'pes', 'adhesion', 'ppes', 'charge']
        
        if not set(wf).issubset(set(available_wf)):
            message = "The available workflows are: interface, pes, adhesion, ppes, "\
                      "charge."
            raise ValueError("One or more fireworks are incorrect."+message)
        
        # Check the order of the wf
        n_wf = len(wf)
        
        if n_wf == 2:
            if wf[0] == 'interface' and wf[1] != 'pes':
                raise ValueError("After 'interface' wf there must be the 'pes' wf.")

            if wf[0] == 'pes' and wf[1] != 'adhesion':
                raise ValueError("After 'pes' wf there must be the 'adhesion' wf.")

            if wf[0] == 'adhesion' and wf[1] != 'ppes':
                raise ValueError("After 'adhesion' wf there must be the 'ppes' wf.")

            if wf[0] == 'ppes' and wf[1] != 'charge':
                raise ValueError("After 'ppes' wf there must be the 'charge' wf.")
        
        if n_wf == 3:
            if wf[0] == 'interface' and wf[1] != 'pes':
                raise ValueError("After 'interface' wf there must be the 'pes' wf.")
            elif wf[1] == 'pes' and wf[2] != 'adhesion':
                raise ValueError("After 'pes' wf there must be the 'adhesion' wf.")

            if wf[0] == 'pes' and wf[1] != 'adhesion':
                raise ValueError("After 'pes' wf there must be the 'adhesion' wf.")
            elif wf[1] == 'adhesion' and wf[2] != 'ppes':
                raise ValueError("After 'adhesion' wf there must be the 'ppes' wf.")
            
            if wf[0] == 'adhesion' and wf[1] != 'ppes':
                raise ValueError("After 'adhesion' wf there must be the 'ppes' wf.")
            elif wf[1] == 'ppes' and wf[2] != 'charge':
                raise ValueError("After 'ppes' wf there must be the 'charge' wf.")
        
        if n_wf == 4:
            if wf[0] == 'interface' and wf[1] != 'pes':
                raise ValueError("After 'interface' wf there must be the 'pes' wf.")
            elif wf[1] == 'pes' and wf[2] != 'adhesion':
                raise ValueError("After 'pes' wf there must be the 'adhesion' wf.")
            elif wf[2] == 'adhesion' and wf[3] != 'ppes':
                raise ValueError("After 'adhesion' wf there must be the 'ppes' wf.")
            
            if wf[0] == 'pes' and wf[1] != 'adhesion':
                raise ValueError("After 'pes' wf there must be the 'adhesion' wf.")
            elif wf[1] == 'adhesion' and wf[2] != 'ppes':
                raise ValueError("After 'adhesion' wf there must be the 'ppes' wf.")
            elif wf[2] == 'ppes' and wf[3] != 'charge':
                raise ValueError("After 'ppes' wf there must be the 'charge' wf.")
        
        if n_wf == 5:
            if wf[0] == 'interface' and wf[1] != 'pes':
                raise ValueError("After 'interface' wf there must be the 'pes' wf.")
            elif wf[1] == 'pes' and wf[2] != 'adhesion':
                raise ValueError("After 'pes' wf there must be the 'adhesion' wf.")
            elif wf[2] == 'adhesion' and wf[3] != 'ppes':
                raise ValueError("After 'adhesion' wf there must be the 'ppes' wf.")
            elif wf[3] == 'ppes' and wf[4] != 'charge':
                raise ValueError("After 'ppes' wf there must be the 'charge' wf.")
            
        return wf 
        
    @staticmethod
    def set_interface(use_spin, **kwargs):
        """
        Set the parameters for the interface.
        
        Parameters
        ----------
        use_spin : bool
            Set True for magnetic materials.

        Return
        ------
        inter_settings : list
            Settings for interface.

        """
        
        inter_params = {
            "max_area":160,
            "interface_distance":2,
            "max_angle_tol":0.01,
            "max_length_tol":0.02,
            "max_area_ratio_tol":0.05,
            "best_match":"area"
        }
        
        if kwargs:
            for p in kwargs:
                if p not in inter_params.keys():
                    raise ValueError("The parameter {} is not admitted.".format(p))
                
                # All the values except best match are float
                if p != 'best_match':
                    inter_params[p] = float(kwargs[p])
        
        return use_spin, inter_params

    @staticmethod
    def set_pes(pes_type):
        """
        Set the values for executing pes or pes scf.
        
        Parameters
        ----------
        pes_type : str
            It is the pes type, and it can be 'pes' or 'pes_scf'
        
        Return
        ------
        pes_settings : list
            List containing all the pes settings.

        """
        
        if pes_type not in ['pes', 'pes_scf']:
            raise ValueError("The pes must be 'pes' or 'pes_scf'.")
        
        if pes_type == 'pes':
            #calc_type_pes = 'slab_pos_relax'
            calc_type_pes = 'interface_z_relax'
            check_entry_pes = 'pes'
        elif pes_type == 'pes_scf':
            calc_type_pes = 'slab_from_scratch'
            check_entry_pes = 'pes_scf'
        
        return calc_type_pes, check_entry_pes

    @staticmethod
    def set_adh(adh_type):
        """
        Set the values for executing adhesion regular or adhesion short.
        
        Parameters
        ----------
        adh_type : str
            It is the pes type, and it can be 'pes' or 'pes_scf'
        
        Return
        ------
        adhesion_settings : list
            List containing all the adhesion settings.

        """
        
        if adh_type not in ['regular', 'short']:
            raise ValueError("The adhesion must be 'regular' or 'short'.")
        
        if adh_type == 'regular':
            calc_type_adhesion = 'slab_pos_relax'
            adhesion_type = 'regular'
            pes_type = 'pes'
        elif adh_type == 'short':
            calc_type_adhesion = 'slab_pos_relax'
            adhesion_type = 'short'
            pes_type = 'pes_scf'
        
        return calc_type_adhesion, adhesion_type, pes_type
    
    @staticmethod
    def set_ppes(pes_type):
        """
        Set the ppes check entry.
        """
        
        if pes_type not in ['regular', 'short']:
            raise ValueError("The pes must be 'regular' or 'short'.")
        
        if pes_type == 'regular':
            calc_type_ppes = 'slab_from_scratch'
            check_entry_ppes = 'regular'
        elif pes_type == 'short':
            calc_type_ppes = 'slab_from_scratch'
            check_entry_ppes = 'short'
        
        return calc_type_ppes, check_entry_ppes 
    
    @staticmethod
    def print_messsage(wf, formulas, mids, miller):
        """
        Print a summary message about which workflows are being executed, the
        elements, and the miller indices.
        """
        
        wf_str = str(wf).replace('\'', '')
    
        miller1 = str(miller[0]).replace('[', '').replace(']', '').replace(',', '').replace(' ', '')
        miller2 = str(miller[1]).replace('[', '').replace(']', '').replace(',', '').replace(' ', '')

        message = "Execution of the " + wf_str + " workflows for the "
        message += "interface " + formulas[0] + "(" + miller1 + ")-"
        message += formulas[1] + "(" + miller2 + ") (" + mids[0] + "_" + mids[1] + ")."
        print(message)
        
    @staticmethod
    def get_wf_variables(wf, **kwargs):
        """
        Get the list of wf and the dictionary with the order of execution.
        E.g.: [fw_pes, fw_adh], {fw_pes: fw_adh}.
        """
        
        variables_name = {'interface': 'fw_interface', 'pes': 'fw_pes', 
                          'adhesion': 'fw_adh', 'ppes': 'fw_ppes', 
                          'charge': 'fw_charge'}

        wf_list = []
        wf_dict = {}
        
        for p in kwargs:
            if p not in variables_name.values():
                raise ValueError("The variable name {} is not available.".format(p))

        for w in wf:
            if w not in variables_name.keys():
                raise ValueError("The workflow name {} is not available.".format(w))
            var_name = variables_name[w]
            wf_list.append(kwargs[var_name])
        
        if len(wf_list) == 1:
            wf_dict = None
        else:
            for i in range(0, len(wf_list)):
                if i+1 == len(wf_list):
                    break
                wf_dict[wf_list[i]] = wf_list[i+1]
        
        return wf_list, wf_dict
