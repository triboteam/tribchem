#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 17 11:41:00 2021

Auxiliary script for the CLI for converge_bulk.py.

@author: omarchehaimi

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'November 17th, 2021'

from tribchem.core.cli.cli_common import CliCommon

class CliConvergeSlab(CliCommon):
    """
    Class containing all the methods necessary to deal with the command line
    interface for the interface wf.

    """
    
    @staticmethod
    def print_messsage(formulas, miller):
        """
        Print a summary message.
        """
        fm = zip(formulas, miller)
        fm = [f+'('+str(m)+')' for f, m in fm]
        fm = [i.replace('[', '').replace(']', '') for i in fm]
        fm_s = ''
        for i in range(0, len(fm)):
            if i+1 == len(fm):
                fm_s += fm[i]
                continue
            fm_s += fm[i] + ', '
        
        message = 'Converging the slab of {}.'.format(fm_s)
        print(message)
        
    @staticmethod
    def get_atom_comp_par(comp, n_el):
        """
        Get the comp params.
        """
        
        if not isinstance(n_el, int):
            raise ValueError("The number of element {} must be an interger.".format(n_el))
        
        if comp == 'None':
            return [None]*n_el
        else:
            return CliCommon.get_list_opt(comp)
