#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 19 09:42:00 2021

Auxiliary script for the CLI for calc_adsorbate.py.

@author: omarchehaimi

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Copyright 2021, Prof. M.C. Righi, TribChem, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'November 19th, 2021'

from tribchem.core.cli.cli_common import CliCommon

class CliCalcAdsorbate(CliCommon):
    """
    Class containing all the methods necessary to deal with the command line
    interface for the interface wf.

    """
    
    @staticmethod
    def print_messsage(adsorbate, formula, mids):
        """
        Print a summary message.
        """

        message = 'Calculating the adsorption of {} on {} ({}).'.format(adsorbate, formula, mids)
        print(message)
    
    @staticmethod
    def get_fa(fa):
        """
        Get the find_arguments pymatgen parameter.
        """
        if fa:
            find_args = CliCalcAdsorbate.get_dict(fa)
        else:
            find_args = None
        
        return find_args
    
    @staticmethod
    def get_mv(mv):
        """
        Get the mi_vec pymatgen parameter.
        """
        if mv:
            mv = CliCalcAdsorbate.get_par_list_opt(mv)
        else:
            mv = None
        
        return mv
        