#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 12:24:00 2021

This class implement a simple engine able to launch and submit a Quantum
ESPRESSO calculation in a cluster system.

Author: Omar Chehaimi (omarchehaimi), Gabriele Losi (glosi000)
Copyright 2021, Prof. M.C. Righi, TribChem, University of Bologna

"""

__author__ = 'Omar Chehaimi, Gabriele Losi'
__copyright__ = 'Prof. M.C. Righi, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'October 21st, 2021'

from tribchem.core.qe.engine import Engine, QE

w_dir = "/home/omar/Lavoro/Unibo/TribChemProject/test_run_qe"
en = Engine(job_name="inter_fe_ni", w_dir=w_dir, software="qe-gpu/6.8")
en.generate_jobscript()

job_names = ['fe_w', 'fe_ni', 'fe_mo', 'fe_pt', 'fe_cu']
qe = QE(job_names=job_names, w_dir=w_dir, software="qe-gpu/6.8")
qe.generate_all_jobscripts()