#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  7 16:46:17 2021

Tools to parse metadata from README and init files.

@author: glosi000
"""

import os
import re
import io

##############################################################################
# TEST TAKEN BY https://github.com/textbook/atmdb

PKG_NAME = 'tribchem'

HERE = os.path.abspath(os.path.dirname(__file__))

PATTERN = r'^{target}\s*=\s*([\'"])(.+)\1$'

AUTHOR = re.compile(PATTERN.format(target='__author__'), re.M)
#DOCSTRING = re.compile(r'^([\'"])\1\1(.+)\1\1\1$', re.M)
CREDITS = re.compile(PATTERN.format(target='__credits__'), re.M)
COPYRIGHT = re.compile(PATTERN.format(target='__copyright__'), re.M)
LICENSE = re.compile(PATTERN.format(target='__license__'), re.M)
VERSION = re.compile(PATTERN.format(target='__version__'), re.M)
MAINTAINER = re.compile(PATTERN.format(target='__maintainer__'), re.M)
EMAIL = re.compile(PATTERN.format(target='__email__'), re.M)
STATUS = re.compile(PATTERN.format(target='__status__'), re.M)
WEB = re.compile(PATTERN.format(target='__website__'), re.M)

METADATA = [AUTHOR, COPYRIGHT, LICENSE, VERSION, MAINTAINER, EMAIL, STATUS, WEB]

# Open the __init__ file containing metadata
def open_init():
    """
    Open the init file containing the metadata and return a string.

    """
    with open(os.path.join(HERE, '../../README.md')) as f:
        init = f.read()
        
    return init

def parse_init(init, metadata):
    """
    Parse the the init string and search for a given metadata.

    Parameters
    ----------
    init : str
        String object, obtained by reading the init file in tribchem.
        
    metadata : re.Pattern
        Regex containing the corresponding value.Admitted values are:
            AUTHOR
            COPYRIGHT
            LICENSE
            VERSION
            MAINTAINER
            EMAIL
            STATUS
            WEBSITE

    Returns
    -------
    str
        The metadata of interest

    """
    
    return metadata.search(init).group(2)

def get_tribchem_metadata():
    """
    Return all the relevant metadata for the tribchem package.

    """
    
    init_file = open_init()
    
    keys = ['author', 'copyright', 'license', 'version', 'maintainer', 
            'email', 'status', 'web']
    metadata = {}
    
    for i, regex in enumerate(METADATA):
        
        match = parse_init(init_file, regex)
        metadata[keys[i]] = match
        
    return metadata
    

##############################################################################

def read(*filenames, **kwargs):
    """
    TODO: Rewrite it

    """
    encoding = kwargs.get('encoding', 'utf-8')
    sep = kwargs.get('sep', '\n')
    buf = []
    for filename in filenames:
        with io.open(filename, encoding=encoding) as f:
            buf.append(f.read())
    return sep.join(buf)
