#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  7 14:50:55 2021

General utilities and tools that that can be used eventually by any other
functions or classes within the TribChem package. Any 

@author: glosi000

"""


import inspect


class FunctionTools:
    """
    Initialize a high level function object to call it with very specific
    options, or returning relevant properties.
    
    """
    
    def __init__(self, func):
        """
        Initialize the object for a given function.

        Parameters
        ----------
        func : function
            Function object to work with.

        """

        self.func = func

    def run_func(self, **kwargs):
        """
        Run the function object by passing a **kwargs dictionary as input
        arguments. This is a high level call of the _run_func static method.

        """
        return FunctionTools._run_func(self.func, **kwargs)
    
    @staticmethod
    def _run_func(func, **kwargs):
        """
        Run a function passed by input, employing a kwargs dictionary.
        In practice, the keys of kwargs MUST correspond to the names of the 
        input flags of func, while the values are the inputs to be passed.

        """

        # Extract the arguments of the function
        args = inspect.getfullargspec(func).args
    
        # Prepare an argument dictionary out of **kwargs, to call the function
        call_args = {}
        for k, it in dict(kwargs).items():
            if k in args:
                call_args.update({k: it})

        return func(**call_args)

    @staticmethod
    def input_argval(func, loc):
        """
        Return a dictionary containing the name of the input arguments as keys
        and the values that have been passed by the user as values.
        This makes sense to be used only in the function scope when calling it.
        
        """

        # Read all the local variables and the args passed by the user
        args = inspect.getfullargspec(func).args

        # Isolate the function input arguments from any local waste
        input_argval = {}
        for arg in args:
            input_argval[arg] = loc[arg]

        return input_argval

    @staticmethod
    def call_fong(f, g, loc):
        """
        Call the function g based on the input arguments of the function f.
        This is useful as a shortcut to call a function g inside another function
        f, when both of them have many input arguments with the same name.

        The verbose call of g inside f (as shown below) is changed by calling call_fong:
            f(a, b, c, ...):
                ...
                g(a=a, b=b, c=c, ...)
                ...

        Be careful: if many of the input arguments of g are custom or modified
        and assigned within f, then it might become too complicated to call
        this method.

        Parameters
        ----------
        f : function
            Function to be called.

        g : function
            Reference used.

        loc : list
            List of locals variables, i.e. input arguments, in the scope of f.

        """

        # Create a kwargs dictionary with input arguments and values of f
        f_argval = FunctionTools.input_argval(f, loc)

        # Extract the arguments of g
        g_args = inspect.getfullargspec(g).args

        # Select the input arguments of g from those of f
        f_filtered = {}
        for arg in g_args:
            f_filtered[arg] = f_argval[arg]

        return g(**f_filtered)

class ClassTools:
    """
    Extract relevant information from a class.
    
    """
    
    def __init__(self, cl):
        """
        Initialize a class object

        Parameters
        ----------
        cl : class
            Custom class to work on.

        """

        self.cl = cl

    def get_attr(self, attr):
        """
        Return the value for a a class attribute. Interface of _get_attr.

        """
        return ClassTools._get_attr(self.cl, attr)    
    
    @staticmethod
    def _get_attr(cl, attr):
        """
        Read a class attribute

        Parameters
        ----------
        cl : class
            Custom class.

        attr : str
            Name of the attribute of the class to be returned.

        Returns
        -------
        The attribute with name attr from the class cl.

        """
        return inspect.getattr_static(cl, attr)

    @staticmethod
    def call_conf(cls, c_args=None, **kwargs):
        """
        Create an instance of cl based on the input arguments of a function f.
        This is useful as a shortcut to initialize a class inside a function,
        when both of them have many input arguments with the same name.

        This can be used with functions having the following structures:
            f(a, b, c, ...):
                ...
                obj = c(a=a, b=b, c=c, ...)
                ...

        Parameters
        ----------
        cl : class
            Class to be initialized.

        f : function
            Read the locals variables of this function.

        c_args : list or None, optional
            List of input arguments of the class to be initialized, if nothing
            is passed, the argval of the constructor __init__ are taken.
            The default is None.

        **kwargs
            Dictionary containing the keys and the values of the input argument
            of a certaing class f, that should be filtered by the c_args values
            in order to call the class c. You typycaly might want to pass
            **locals() when you call this method inside a function f.

        """

        # Read the input argument to instantiate the class
        if c_args is None:
            c_args = inspect.getfullargspec(cls.__init__).args
            c_args.remove('self')

        # Select the input arguments of g from those of f
        f_filtered = {}
        for arg in c_args:
            if arg in kwargs:
                f_filtered[arg] = kwargs[arg]

        return cls(**f_filtered)

if __name__ == '__main__':
    
    class cl:
        def __init__(self, a, b):
            self.a = a
            self.b = b

    def f(a, b, c):
        return ClassTools.call_conf(cl, **locals())
    
    o = f(1, 2, 3)
