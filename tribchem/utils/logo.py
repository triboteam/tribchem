#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  7 16:59:43 2021

Logo and box for the Tribchem Package.

@author: glosi000
"""

from tribchem.utils.metadata import get_tribchem_metadata
from tabulate import tabulate


# Extract the project metadata
metadata = get_tribchem_metadata()


# =============================================================================
# LOGO AND BOX GENERATION
# =============================================================================

building_blocks = ['█', '╗', '╔', '╚', '╝', '═', '║']

# Tribchem - logo
logo = """

████████╗           ██╗     ███████╗██╗  ██╗███████╗██═╗ ██╗
╚══██╔══╝ ████  ███╗██║    ██╔═════╝██║  ██║██╔════╝███╚███║ 
   ██║   ██╔═██╗╚══╝█████╗ ██║      ███████║█████╗  ██╔█╔██║
   ██║   ██║ ╚═╝███╗██  ██╗██║      ██╔══██║██╔══╝  ██║╚╝██║
   ██║   ██║    ███║█████╔╝╚███████╗██║  ██║███████╗██║  ██║ 
   ╚═╝   ╚═╝    ╚══╝ ╚═══╝  ╚══════╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝ {0}
""".format('v.'+metadata['version'])

def tribchem_logo():
    """ Print the Tribchem logo
    """
    print(logo)

# Tribchem - box
def create_box(table_format='grid', shortname=True):
    """
    Create a box containing information about the TribChem package.

    Parameters
    ----------
    table_format : str, optional
        Table format to make the box, parameter passed to tabulate. Options:
        'plain', 'simple', 'grid', 'pipe', 'orgtbl', 'rst', 'mediawiki',
        'latex', 'latex_raw', and 'latex_booktabs'. The default is 'grid'.
    
    shortname : bool, optional
        Shorten the names of authors and maintainers. The default is True.
    
    Returns
    -------
    output : str
        Box containing TribChem metadata.

    """
    
    # Shorten the authors and maintainers names:
    if shortname:
        authors = shorten_name(metadata['author'])
        maintainers = shorten_name(metadata['maintainer'])
    else:
        authors = metadata['author']
        maintainers = metadata['maintainer']
    
    text = "TribChem - Materials Science Simulations\n" + \
    "{}\n".format(metadata['copyright']) + \
    "Version: {}\n".format(str(metadata['version'])) + \
    "Authors: {}\n".format(authors) + \
    "Maintainers: {}\n".format(maintainers) + \
    "Site: {}".format(str(metadata['web']))
    
    table = [[text]]
    output = tabulate(table, tablefmt=table_format)

    return output

def tribchem_box(table_format='grid', shortname=True):
    """ Plot the tribchem box with the project metadata 
    """
    out = create_box(table_format, shortname)
    print(out)
    

# =============================================================================
# CREATE AN HEADER FOR TRIBCHEM
# =============================================================================

def shorten_name(names: str):
    """
    Shorten a string containing many names separated by commas.

    Parameters
    ----------
    names : str
        Contains a list of names separated by commas. 
        Ex. name1 surname1, name2 surname2, ...
        The names are shortened with their first letter followed by a dot.

    Returns
    -------
    authors : str
        List of authors with shortened names

    """
    
    # Create a list out of a string name
    author_list = str(names).split(',')
    tot_len = len(author_list)
    authors = ''
    
    # Start the loop
    for i, person in enumerate(author_list):
         # Isolate name and surname
         p = person.split(' ')
         
         if p[0] == '':
             name = p[1:-1]
         else:
             name = p[:-1]
         surname = p[-1]
         
         short_name = ''
         for n in name:
             short_name = short_name  + n[0] + '. '
         
         # Append the purged name
         string = short_name + surname + ', '
         if i == tot_len -1:
             string = string[:-2]
            
         authors = authors + string
    
    return authors

def tribchem_header(table_format='grid', shortname=True):
    """
    Print TribChem header: logo and metadata box.

    Parameters
    ----------
    width : int, optional
        Length of the string with padded characters. The default is 50.

    table_format : str, optional
        Format of the box. The default is 'grid'.

    shortname : bool, optional
        Shorten the names of authors and maintainers. The default is True.

    Returns
    -------
    None.

    """
    
    box = create_box(table_format, shortname)
    
    # Plot the header
    print(logo)
    print(box)


if __name__ == '__main__':
    
    tribchem_header(table_format='grid', shortname=True)
