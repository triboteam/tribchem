#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 1 13:25:00 2022

Main function for the CLI.

@author: omarchehaimi

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Prof. M.C. Righi, SolidMat, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 1st, 2022'

from .parser import Parser

def main():
    """
    TribChem entry point.
    """
    
    Parser().run()