#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 1 12:48:00 2022

CLI inputs definition.

@author: omarchehaimi

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Prof. M.C. Righi, SolidMat, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 1st, 2022'

import argparse
from typing import List, Optional

from tribchem.entrypoints.parsers.utils import GenParser
from tribchem.entrypoints.parsers.functions import Functions
from tribchem.highput.utils.scripts.save_surfene_adh import save_surfene_adh

class Parser:
    """
    Class containing the call for the execution of the commands get from the
    CLI.
    """

    def parse_args(self):
        """
        Parser for the TribChem package.
        """
        
        action_parser = argparse.ArgumentParser(
            description='TribChem is a package which allows to generate and '
                        'execute high throughput workflows for tribological '
                        'applications.'
        )

        subparsers = action_parser.add_subparsers(
            help='Choose wether execute a workflow or perform an analysis of the '
                'data stored in the database.'
        )
        
        # workflows
        sp_wf = subparsers.add_parser(
            'workflow', help='The workflow to be executed.'
        )
        
        wf_subparsers = sp_wf.add_subparsers(
            help='Selection of the workflow.',
            dest='wf_name'
        )
        
        init_bulk_parser = GenParser.init_bulk_parser(wf_subparsers)
        converge_bulk_parser = GenParser.conv_bulk_parser(wf_subparsers)
        cohesive_bulk_parser = GenParser.cohes_bulk_parser(wf_subparsers)
        converge_slab_parser = GenParser.conv_slab_parser(wf_subparsers)
        calc_interface_parser = GenParser.cal_interface_parser(wf_subparsers)
        calc_adsorbate_parser = GenParser.cal_adsorbate_parser(wf_subparsers)
        
        # analysis
        sp_an = subparsers.add_parser(
            'analysis', help='The data analysis to be carried out.'
        )

        analysis_subparsers = sp_an.add_subparsers(
            help='Selection of the data analysis to carry out.',
            dest='analysis'
        )
        
        dump_db_parser = GenParser.dump_db_parser(analysis_subparsers)
        get_input_inter_parser = GenParser.get_input_inter(analysis_subparsers)
        get_slab_parser = GenParser.get_slab(analysis_subparsers)
        pes_img_parser = GenParser.pes_img(analysis_subparsers)
        plot_adh_parser = GenParser.plot_adh(analysis_subparsers)
        plot_charge_parser = GenParser.plot_charge(analysis_subparsers)
        plot_ppes_parser = GenParser.plot_ppes(analysis_subparsers)
        plot_eos_parser = GenParser.plot_eos(analysis_subparsers)
        save_inter_parser = GenParser.save_inter(analysis_subparsers)
        save_inter_params_parser = GenParser.save_inter_params(
            analysis_subparsers
        )
        save_results_parser = GenParser.save_results(analysis_subparsers)
        save_structure_parser = GenParser.save_structure(analysis_subparsers)
        save_surfene_adh_parser = GenParser.save_surfene_adh(
            analysis_subparsers
        )
        save_surfene_parser = GenParser.save_surfene(analysis_subparsers)
        work_function_parser = GenParser.work_function(analysis_subparsers)

        args = action_parser.parse_args()

        return args

    def __execute(self, f_name):
        """
        Private method to get a function given its name.
        
        Parameters
        ----------
        f_name : str
            Name of the function.
            
        Return
        ------
        The function we want from the Functions class.

        """
        
        return getattr(Functions, f_name)
        
    
    def run(self):
        """
        Execute the command based on the inputs passed from the CLI. 
        """

        args = self.parse_args()
        if 'wf_name' in vars(args):
            self.__execute(args.wf_name)(args)
        elif 'analysis' in vars(args):
            self.__execute(args.analysis)(args)