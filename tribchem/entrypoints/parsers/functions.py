#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 2 10:59:00 2022

Utils containing the call to the functions.

@author: omarchehaimi

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Prof. M.C. Righi, SolidMat, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 2nd, 2022'


from tribchem.highput.workflows.scripts.calc_adsorbate import calc_adsorbate
from tribchem.highput.workflows.scripts.initialize_bulk import initialize_bulk
from tribchem.highput.workflows.scripts.converge_bulk import converge_bulk
from tribchem.highput.workflows.scripts.cohesive_bulk import cohesive_bulk
from tribchem.highput.workflows.scripts.converge_slab import converge_slab
from tribchem.highput.workflows.scripts.calc_interface import calc_interface
from tribchem.highput.utils.scripts.dumpdb import dump_db
from tribchem.highput.utils.scripts.get_input_inter import get_input_inter
from tribchem.highput.utils.scripts.get_slab import get_slab
from tribchem.highput.utils.scripts.pes_img import pes_img
from tribchem.highput.utils.scripts.plot_adh import plot_adh
from tribchem.highput.utils.scripts.plot_charge import plot_charge
from tribchem.highput.utils.scripts.plot_ppes import plot_ppes
from tribchem.highput.utils.scripts.save_inter import save_inter
from tribchem.highput.utils.scripts.save_interparams import save_inter_params
from tribchem.highput.utils.scripts.save_results import save_results
from tribchem.highput.utils.scripts.save_structure import save_structure
from tribchem.highput.utils.scripts.save_surfene_adh import save_surfene_adh
from tribchem.highput.utils.scripts.save_surfene import save_surfene
from tribchem.highput.utils.scripts.plot_eos import plot_eos
from tribchem.highput.utils.scripts.work_function import work_function

class Functions:
    
    @classmethod
    def initialize_bulk(cls, args):
        initialize_bulk(args)
    
    @classmethod
    def converge_bulk(cls, args):
        converge_bulk(args)
    
    @classmethod
    def cohesive_bulk(cls, args):
        cohesive_bulk(args)
    
    @classmethod
    def converge_slab(cls, args):
        converge_slab(args)
    
    @classmethod
    def calc_interface(cls, args):
        calc_interface(args)
    
    @classmethod
    def calc_adsorbate(cls, args):
        calc_adsorbate(args)
        
    @classmethod
    def dump_db(cls, args):
        dump_db(args)
        
    @classmethod
    def get_input_inter(cls, args):
        get_input_inter(args)
        
    @classmethod
    def get_slab(cls, args):
        get_slab(args)
        
    @classmethod
    def pes_img(cls, args):
        pes_img(args)
        
    @classmethod
    def plot_adh(cls, args):
        plot_adh(args)
        
    @classmethod
    def plot_charge(cls, args):
        plot_charge(args)
        
    @classmethod
    def plot_ppes(cls, args):
        plot_ppes(args)
        
    @classmethod
    def save_inter(cls, args):
        save_inter(args)
        
    @classmethod
    def save_inter_params(cls, args):
        save_inter_params(args)
    
    @classmethod
    def save_results(cls, args):
        save_results(args)
        
    @classmethod
    def save_structure(cls, args):
        save_structure(args)
    
    @classmethod
    def save_surfene_adh(cls, args):
        save_surfene_adh(args)
        
    @classmethod
    def save_surfene(cls, args):
        save_surfene(args)
        
    @classmethod
    def plot_eos(cls, args):
        plot_eos(args)
        
    @classmethod
    def work_function(cls, args):
        work_function(args)