#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 2 10:59:00 2022

Utils for the parsers.

@author: omarchehaimi

"""

__author__ = 'Omar Chehaimi'
__copyright__ = 'Prof. M.C. Righi, SolidMat, ERC-SLIDE, University of Bologna'
__contact__ = 'clelia.righi@unibo.it'
__date__ = 'February 2nd, 2022'


from email.policy import default
from random import choice


class GenParser:
    """
    Class for generating the parsers.
    """
    
    @staticmethod
    def init_bulk_parser(subparser):
        """
        Initialize the cohesive_bulk_parser for the initialization of the bulk.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        init_bulk_parser : argpase
            Parser for the initialize_bulk workflow with all the parameters.

        """
        
        init_bulk_parser = subparser.add_parser(
            'initialize_bulk', 
            help='Workflows generation for the bulk initialization from the '
                 'Materias Project.'
                 'Example: mid="[mp-13, mp-90]" formulas="[Fe, Cr]".'
        )

        # Positional arguments
        init_bulk_parser.add_argument(
            'mids', metavar='mids', type=str,
            help='Mids of the elements. Set to \'all\' to converge all the '
                 'bulks. E.g. for Fe and Cr: [mp-13, mp-90].'
        )
        init_bulk_parser.add_argument(
            'formulas', metavar='formulas', type=str,
            help='Formulas of the elements. Set to \'all\' to converge all the '
                 'bulks. E.g.: [Fe, Cr].'
        )
        
        # Optional arguments
        init_bulk_parser.add_argument(
            '-d', metavar='debug', type=bool, default=False,
            help='Set to \'y\' the execution of the workflows will be done'
                 ' on the current machine. This setting is useful for'
                 ' debugging operations.'
        )
        init_bulk_parser.add_argument(
            '-dbf', metavar='db_file', type=str, 
            default=None, help='Location of the database file.'
        )
        init_bulk_parser.add_argument(
            '-db', metavar='database', type=str, 
            default='tribchem', help='Database for the bulks.'
        )

        init_bulk_parser.add_argument(
            '-fln', metavar='fltr_name', type=str, default=None, 
            help='Name of the filter. Available name are: \n'
                 '- crystal_system \n'
                 '- space_group \n'
                 '- point_group \n'
        )
        init_bulk_parser.add_argument(
            '-fl', metavar='fltr', type=str, default=None, 
            help='Filter.'
        )
        init_bulk_parser.add_argument(
            '-f', metavar='functional', type=str, default='PBE', 
            help='Functional.')
        init_bulk_parser.add_argument(
            '-ce', metavar='check_entry', type=str, default='[structure, init]', 
            help='Location where to save the bulk.'
        )
        init_bulk_parser.add_argument(
            '-uo', metavar='update_op', type=str, default='$set', 
            help='Update operation for the database.'
        )
        init_bulk_parser.add_argument(
            '-up', metavar='upsert', type=bool, 
            default=False, 
            help='Set to \'y\' to create a new document '
                 'in the DB, otherwise it update it '
                 '(IF PRESENT).'
        )
        init_bulk_parser.add_argument(
            '-tm', metavar='to_mongodb', type=bool, 
            default=False, 
            help='Set to \'y\' to convert the dict to a suitable form.'
        )
        
        return init_bulk_parser
    
    @staticmethod
    def conv_bulk_parser(subparser):
        """
        Initialize the cohesive_bulk_parser for the initialization of the bulk.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        converge_bulk_parser : argpase
            Parser for the converge_bulk_parser workflow with all the 
            parameters.

        """
        
        converge_bulk_parser = subparser.add_parser(
            'converge_bulk', 
            help='Workflows generation for the bulk convergence.'
                 'Example: mid="[mp-13, mp-90]" formulas="[Fe, Cr]".'
        )

        # Positional arguments
        converge_bulk_parser.add_argument(
            'mids', metavar='mids', type=str,
            help='Mids of the elements. Set to \'all\' to converge all the '
                 'bulks. E.g. for Fe and Cr: [mp-13, mp-90].'
        )
        converge_bulk_parser.add_argument(
            'formulas', metavar='formulas', type=str,
            help='Formulas of the elements. Set to \'all\' to converge all the '
                 'bulks. E.g.: [Fe, Cr].'
        )
        
        # Optional arguments
        converge_bulk_parser.add_argument(
            '-d', metavar='debug', type=bool, default=False,
            help='If set to \'y\' the execution of the workflows will be done'
                 ' on the current machine. This setting is useful for'
                 ' debugging operations.'
        )
        converge_bulk_parser.add_argument(
            '-o', metavar='override', type=bool, 
            default=False, help='Set to \'y\' to override a calculation.'
        )
        converge_bulk_parser.add_argument(
            '-dbf', metavar='db_file', type=str, 
            default=None, help='Location of the database file.'
        )
        converge_bulk_parser.add_argument(
            '-db', metavar='db', type=str, 
            default='tribchem', help='Database for the bulks.'
        )
        converge_bulk_parser.add_argument(
            '-fln', metavar='fltr_name', type=str, default=None, 
            help='Name of the filter. Available name are: \n'
                 '- crystal_system \n'
                 '- space_group \n'
                 '- point_group \n'
        )
        converge_bulk_parser.add_argument(
            '-fl', metavar='fltr', type=str, default=None, help='Filter.'
        )
        converge_bulk_parser.add_argument(
            '-ce', metavar='check_encut', type=str, 
            default='[data, encut_info]', 
            help='Location where the energy cutoff info are saved.'
        )
        converge_bulk_parser.add_argument(
            '-es', metavar='encut_start', type=str, default=None, 
            help='Energy cutoff starting value.'
        )
        converge_bulk_parser.add_argument(
            '-ei', metavar='encut_incr', type=str, default='25', 
            help='Energy cutoff increment.'
        )
        converge_bulk_parser.add_argument(
            '-ks', metavar='kdens_start', type=str, default='3', 
            help='Kpoints density starting value.'
        )
        converge_bulk_parser.add_argument(
            '-ki', metavar='kdens_incr', type=str, default='0.1', 
            help='Kpoints density increment value.'
        )
        converge_bulk_parser.add_argument(
            '-nc', metavar='n_converge', type=str, default='3', 
            help='n_converge.'
        )
        converge_bulk_parser.add_argument(
            '-f', metavar='functional', type=str, default='PBE', 
            help='Functional.'
        )
        converge_bulk_parser.add_argument(
            '-ee', metavar='entry_encut', type=str, default='[structure, init]', 
            help='Location where the energy cutoff is saved.'
        )
        converge_bulk_parser.add_argument(
            '-ek', metavar='entry_kpts', type=str, 
            default='[structure, opt]', 
            help='Location where the optimal K-points are saved.'
        )
        converge_bulk_parser.add_argument(
            '-uo', metavar='update_op', type=str, default='$set', 
            help='Location where the optimal K-points are saved.'
        )
        converge_bulk_parser.add_argument(
            '-up', metavar='upsert', type=str, default=True,
            help='Create a new instance in the DB every time a document '
                 'differs.'
        )
        
        return converge_bulk_parser
    
    @staticmethod
    def cohes_bulk_parser(subparser):
        """
        Initialize the cohesive_bulk_parser for the initialization of the bulk.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        cohesive_bulk_parser : argpase
            Parser for the cohesive_bulk_parser workflow with all the parameters.

        """
        
        cohesive_bulk_parser = subparser.add_parser(
            'cohesive_bulk',
            help='Workflows generation for the calculation of the cohesion '
                 'energy of bulks. Example: mid="[mp-13, mp-90]" '
                 'formulas="[Fe, Cr]".'
        )
        
        # Positional argument
        cohesive_bulk_parser.add_argument(
            'mids', metavar='mids', type=str,
            help='Mids of the elements. Set to \'all\' to converge all the '
                 'bulks. E.g. for Fe and Cr: [mp-13, mp-90].'
        )
        cohesive_bulk_parser.add_argument(
            'formulas', metavar='formulas', type=str,
            help='Formulas of the elements. Set to \'all\' to converge all the ' 
                 'bulks. E.g.: [Fe, Cr].'
        )
        
        # Optional argument
        cohesive_bulk_parser.add_argument(
            '-d', metavar='debug', type=bool, default=False,
            help='If set to \'y\' the execution of the workflows will be done'
                 ' on the current machine. This setting is useful for'
                 ' debugging operations.'
        )
        cohesive_bulk_parser.add_argument(
            '-fln', metavar='fltr_name', type=str, default=None, 
            help='Name of the filter. Available name are: \n'
                 '- crystal_system \n'
                 '- space_group \n'
                 '- point_group \n'
        )
        cohesive_bulk_parser.add_argument(
            '-fl', metavar='fltr', type=str, default=None, help='Filter.'
        )
        cohesive_bulk_parser.add_argument(
            '-o', metavar='override', type=bool, default=False, 
            help='Set to \'y\' to override a calculation.'
        )
        cohesive_bulk_parser.add_argument(
            '-hk', metavar='half_kpts_first_relax', type=bool, default=False, 
            help='Set True to perform a double scf, 1st with halved kpoints.'
        )
        cohesive_bulk_parser.add_argument(
            '-dbf', metavar='db_file', type=str, default=None, 
            help='Location of the database file.'
        )
        cohesive_bulk_parser.add_argument(
            '-dbb', metavar='db_bulk', type=str, default='tribchem',
            help='Database for the bulks.'
        )
        cohesive_bulk_parser.add_argument(
            '-dba', metavar='db_atoms', type=str, 
            default='tribchem', help='Database for the atoms.'
        )
        cohesive_bulk_parser.add_argument(
            '-cb', metavar='collection_bulk', type=str, 
            default='PBE.bulk_elements', 
            help='Collection for the bulks.'
        )
        cohesive_bulk_parser.add_argument(
            '-ca', metavar='collection_atoms', type=str, default='PBE.elements',
            help='Collection for the atoms.'
        )
        cohesive_bulk_parser.add_argument(
            '-ce', metavar='check_entry', type=str, 
            default='[data, cohesion_energy]', 
            help='Location where the cohesion energy is saved.'
        )
        cohesive_bulk_parser.add_argument(
            '-eb', metavar='entry_bulk', type=str, default='[structure, opt]', 
            help='Location where to save the optimal bulk structure.'
        )
        cohesive_bulk_parser.add_argument(
            '-chb', metavar='check_bulk', type=str, default='[data, energy]', 
            help='Location where to save the bulk energy.'
        )
        cohesive_bulk_parser.add_argument(
            '-cha', metavar='check_atoms', type=str, default='[data, energy]', 
            help='Location where to save the atom energy.'
        )
        
        return cohesive_bulk_parser
    
    @staticmethod
    def conv_slab_parser(subparser):
        """
        Initialize the cohesive_bulk_parser for the initialization of the bulk.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        converge_slab_parser : argpase
            Parser for the converge_slab_parser workflow with all the 
            parameters.

        """
        
        converge_slab_parser = subparser.add_parser(
            'converge_slab',
            help='Workflows generation for slab convergence.'
                 'Example: mids="[mp-13, mp-90]" formulas="[Fe, Cr]" '
                 'miller="[[1, 1, 1], [1, 1, 1]]".'
        )
        
        # Positional arguments
        converge_slab_parser.add_argument(
            'mids', metavar='mids', type=str,
            help='Mids of the two elements. E.g. for Fe and Cr: '
                 '[mp-13, mp-90].'
        )
        converge_slab_parser.add_argument(
            'formulas', metavar='formulas', type=str,
            help='Formulas of the two elements. E.g.: [Fe, Cr].'
        )
        converge_slab_parser.add_argument(
            'miller', metavar='miller', type=str,
            help='Miller indices of the slabs. E.g.: [[1, 1, 0], [1, 1, 0]].'
        )
        converge_slab_parser.add_argument(
            'tmin', metavar='t_min', type=str, 
            help='Minimum thickness of the slab. Set to 3 for 111, 4 for 110 '
                 'and 0001.'
        )
        converge_slab_parser.add_argument(
            'tmax', metavar='t_max', type=str, 
            help='Maximum thickness of the slab. 12 is always a good value.'
        )
        
        # Optional arguments
        converge_slab_parser.add_argument(
            '-d', metavar='debug', type=bool, default=False,
            help='If set to \'y\' the execution of the workflows will be done'
                 ' on the current machine. This setting is useful for'
                 ' debugging operations.'
        )
        converge_slab_parser.add_argument(
            '-o', metavar='override', type=bool, 
            default=False, help='Set to \'y\' to override a calculation.'
        )
        converge_slab_parser.add_argument(
            '-os', metavar='override_slab', type=bool, 
            default=False, 
            help='Set to \'y\' to override the calculation of a slab.'
        )
        converge_slab_parser.add_argument(
            '-dbf', metavar='db_file', type=str, default=None, 
            help='Location of the database file.'
        )
        converge_slab_parser.add_argument(
            '-db', metavar='database', type=str, default='tribchem', 
            help='Database for the bulks.'
        )
        converge_slab_parser.add_argument(
            '-cb', metavar='collection_bulk', type=str, 
            default='PBE.bulk_elements', help='Collection for the bulks.'
        )
        converge_slab_parser.add_argument(
            '-cs', metavar='collection_slab', type=str, 
            default='PBE.slab_elements', help='Collection for the slabs.'
        )
        converge_slab_parser.add_argument(
            '-ce', metavar='check_entry', type=str, 
            default='[data, opt_thickness]', 
            help='Location where the optimal thickness is saved.'
        )
        converge_slab_parser.add_argument(
            '-be', metavar='bulk_entry', type=str, default='[structure, opt]', 
            help='Location where the optimal bulk is saved.'
        )
        converge_slab_parser.add_argument(
            '-p', metavar='parallelization', type=str, default='high', 
            help='Parallelization level.')
        """
        EXTREMELY IMPORTANT: the next values are used to write the comp_params 
        of the slab either if an entry in the database is not already present 
        or if you set override_slab_db to True. Setting them you can change the 
        value that are read from the bulk. Ex. your bulk is not magnetic, but 
        your (111) surface is. In that case you want that slab to be run with 
        use_spin=True, while bulk doesn't matter. If these values are left to 
        None, the bulk values are used.
        """
        converge_slab_parser.add_argument(
            '-im', metavar='is_metal', type=str, default='None', 
            help='Computational parameter if it is a metal for each element. '
                'EXTREMELY IMPORTANT: setting them you can change the '
                'value that are read from the bulk.'
                'E.g.: [True, True, False].'
        )
        converge_slab_parser.add_argument(
            '-us', metavar='use_spin', type=str, default='None', 
            help='Computational parameter if use spin for each element. '
                'EXTREMELY IMPORTANT: setting them you can change the '
                'value that are read from the bulk.'
                'E.g.: [True, True, False].'
        )
        converge_slab_parser.add_argument(
            '-vdw', metavar='vdw', type=str, default='None', 
            help='Computational parameter if use vdw for each element. '
                'EXTREMELY IMPORTANT: setting them you can change the '
                'value that are read from the bulk.'
                'E.g.: [True, True, False].'
        )
        converge_slab_parser.add_argument(
            '-ct', metavar='conv_thr', type=float, default=0.01, 
            help='Convergence threshold for surface energy with respect' 
                 ' to max thick slab.'
        )
        converge_slab_parser.add_argument(
            '-ti', metavar='thick_incr', type=int, default=1, 
            help='Typically 1 is fine, but RUN only stuff with MONO '
                 'atomic basis for now.'
        )
        converge_slab_parser.add_argument(
            '-va', metavar='vacuum', type=float, default=15, 
            help='Vacuum over the surface.'
        )
        converge_slab_parser.add_argument(
            '-up', metavar='in_unit_planes', type=bool, default=True, 
            help='Build the slab based on n.layers or angstrom.'
        )
        converge_slab_parser.add_argument(
            '-cct', metavar='calc_type', type=str, default='slab_pos_relax', 
            help='Type of calculation for the slab.'
        )
        converge_slab_parser.add_argument(
            '-ei', metavar='ext_index', type=int, default=0, 
            help='What slab to consider from the list, if unsure leave '
                'it to zero.'
        )
        
        return converge_slab_parser
    
    @staticmethod
    def cal_interface_parser(subparser):
        """
        Initialize the cohesive_bulk_parser for the initialization of the bulk.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        calc_interface_parser : argpase
            Parser for the calc_interface_parser workflow with all the 
            parameters.

        """
        
        calc_interface_parser = subparser.add_parser(
            'calc_interface',
            help='Workflows generation for slab convergence.'
                 'Example: mids="[mp-13, mp-90]" formulas="[Fe, Cr]" '
                 'miller="[[1, 1, 1], [1, 1, 1]]".'
        )
        
        ### Positional arguments
        # List of the workflows
        calc_interface_parser.add_argument(
            'wf', metavar='wf', type=str,
            help='List of the workflows. The available workflows are:'
                ' interface, pes, adhesion, ppes, charge.'
                ' E.g.: [interface, pes, adhesion].'
        )

        # Common arguments to all workflows
        calc_interface_parser.add_argument(
            'mids', metavar='mids', type=str,
            help='Mids of the two elements. E.g. for Fe and Cr: '
                 '[mp-13, mp-90].'
        )
        calc_interface_parser.add_argument(
            'formulas', metavar='formulas', type=str, 
            help='Formulas of the two elements. E.g.: [Fe, Cr].'
        )
        calc_interface_parser.add_argument(
            'miller', metavar='miller', type=str,
            help='Miller indices of the slabs. E.g.: [[1, 1, 0], [1, 1, 0]].'
        )
        
        ### Optional arguments
        calc_interface_parser.add_argument(
            '-d', metavar='debug', type=bool, default=False,
            help='If set to \'y\' the execution of the workflows will be done'
                 ' on the current machine. This setting is useful for'
                 ' debugging operations.'
        )
        calc_interface_parser.add_argument(
            '-cs', metavar='collection_slab', type=str, required=False, 
            default='PBE.slab_elements',
            help='Collection where to save/retrive data about the slabs.'
        )
        calc_interface_parser.add_argument(
            '-ci', metavar='collection_inter', type=str, required=False, 
            default='PBE.interface_elements',
            help='Collection where to save/retrive data about the interfaces.'
        )
        calc_interface_parser.add_argument(
            '-o', metavar='override', type=bool, default=False, 
            help='Set to \'y\' to override a calculation.'
        )
        calc_interface_parser.add_argument(
            '-rv', metavar='run_vasp', type=str, default='y', 
            help='Set to \'n\' to not run the VASP calculation.'
        )

        # Arguments of FT_StartInterfaceMatch
        calc_interface_parser.add_argument(
            '-spin', metavar='use_spin', type=bool, default=True,
            help='Set \'y\' for the magnetic materials.'
        )
        calc_interface_parser.add_argument(
            '-marea', metavar='max_area', type=str, default='160', 
            help='Max area for the interface matching (Zur).'
        )
        calc_interface_parser.add_argument(
            '-idist', metavar='interface_distance', type=str, default='2', 
            help='Distance of the interface.'
        )
        calc_interface_parser.add_argument(
            '-max_angle_tol', metavar='max_angle_tol', type=str, default='0.01', 
            help='Maximum angle tolerance (Zur).'
        )
        calc_interface_parser.add_argument(
            '-max_length_tol', metavar='max_length_tol', type=str, default='0.02', 
            help='Maximum length tolerance (Zur).'
        )
        calc_interface_parser.add_argument(
            '-max_area_ratio_tol', metavar='max_area_ratio_tol', type=str, default='0.05', 
            help='Max area ratio tolerance (Zur).'
        )
        calc_interface_parser.add_argument(
            '-bm', metavar='best_match', type=str, default='area', 
            help='Best match.'
        )

        # Arguments of FT_StartPES
        calc_interface_parser.add_argument(
            '-pt', metavar='pes', type=str, default='pes', 
            help='Pes type for PES and PPES. It can only be \'pes\' or '
                 '\'pes_scf\'.'
        )

        # Arguments of FT_StartAdhesion
        calc_interface_parser.add_argument(
            '-ad', metavar='adhesion', type=str, default='regular',
            help='Adhesion type. It can only be \'regular\' or \'short\'.'
        )

        # Arguments of FT_StartPPES
        calc_interface_parser.add_argument(
            '-sh', metavar='shifts', type=str, 
            default='[-0.75, -0.50, -0.25, 0, 0.25, 0.50, 0.75, 1, 1.25, 1.50, '
                    '1.75, 2, 2.25, 2.75, 3]', help='Distance list.'
        )
        calc_interface_parser.add_argument(
            '-ppt', metavar='ppes type', type=str, default='regular', 
            help='Pes type for PES and PPES. It can only be \'regular\' or '
                 '\'short\'.'
        )
        # Arguments of FT_StartChargeDisplacement
        calc_interface_parser.add_argument(
            '-op', metavar='operations', type=str, default='[-, -]',
            help='Operation for the charge displacement.'
        )
        calc_interface_parser.add_argument(
            '-s', metavar='sites', type=str, default='min_site', 
            help='Site where to calculate the charge displacement.'
        )
        calc_interface_parser.add_argument(
            '-tp', metavar='to_plot', type=bool, default=False, 
            help='Set True to plot the charge displacement.'
        )
        calc_interface_parser.add_argument(
            '-chgtype', metavar='charge type', type=str, default='regular', 
            help='Charge type. It can only be \'short\' or '
                 '\'regular\'.'
        )
        
        return calc_interface_parser
    
    @staticmethod
    def cal_adsorbate_parser(subparser):
        """
        Initialize the cohesive_bulk_parser for the initialization of the bulk.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        calc_adsorbate_parser : argpase
            Parser for the calc_interface_parser workflow with all the 
            parameters.

        """
        
        calc_adsorbate_parser = subparser.add_parser(
            'calc_adsorbate',
            help='Workflows generation for adsorption.'
                 'Example: mid="mp-13" formula="Fe" miller="[1, 1, 1]" '
                 'adsorbate="H" sites="ontop_1".'
        )
        
        # Positional arguments
        calc_adsorbate_parser.add_argument(
            'mid', metavar='mid', type=str,
            help='Mid of the element. E.g. for Fe: mp-13.'
        )
        calc_adsorbate_parser.add_argument(
            'formula', metavar='formula', type=str,
            help='Formula of the element. E.g.: Fe.'
        )
        calc_adsorbate_parser.add_argument(
            'miller', metavar='miller', type=str,
            help='Miller indices of the slab. E.g.: [1, 1, 0].'
        )
        calc_adsorbate_parser.add_argument(
            'adsorbate', metavar='adsorbate', type=str,
            help='Adsorbate element. E.g.: H.'
        )
        calc_adsorbate_parser.add_argument(
            'sites', metavar='sites', type=str,
            help='Site where to calculate the adsorption. '
                 'Set to \'all\' for all sites. E.g.: ontop_1.'
        )
        
        # Optional arguments
        calc_adsorbate_parser.add_argument(
            '-d', metavar='debug', type=bool, default=False,
            help='If set to \'y\' the execution of the workflows will be done'
                    ' on the current machine. This setting is useful for'
                    ' debugging operations.'
        )
        calc_adsorbate_parser.add_argument(
            '-rv', metavar='run_vasp', type=str, default='y', 
            help='Set to \'n\' to not run the VASP calculation.'
        )
        calc_adsorbate_parser.add_argument(
            '-c', metavar='collection', type=str, required=False, 
            default='PBE.adsorbate_elements',
            help='Collection where to save/retrive data about the adsorbates.'
        )
        calc_adsorbate_parser.add_argument(
            '-cs', metavar='collection_slab', type=str, required=False, 
            default='PBE.slab_elements',
            help='Collection where to save/retrive data about the slabs.'
        )
        calc_adsorbate_parser.add_argument(
            '-ca', metavar='collection_atom', type=str, required=False, 
            default='PBE.elements',
            help='Collection where to save/retrive data about the atoms.'
        )
        calc_adsorbate_parser.add_argument(
            '-o', metavar='override', type=bool, default=False, 
            help='Set to \'y\' to override a calculation.'
        )
        calc_adsorbate_parser.add_argument(
            '-dbf', metavar='db_file', type=str, default=None, 
            help='Location of the database file.'
        )
        calc_adsorbate_parser.add_argument(
            '-db', metavar='database', type=str, default='tribchem', 
            help='Database for the bulks.'
        )
        calc_adsorbate_parser.add_argument(
            '-ck', metavar='check_key', type=str, default='[data, all_is_done]', 
            help='Location where the adsorption results are saved.'
        )
        calc_adsorbate_parser.add_argument(
            '-cct', metavar='calc_type', type=str, default='interface_z_relax', 
            help='Type of calculation for the slab'
        )
        calc_adsorbate_parser.add_argument(
            '-bs', metavar='both_surfaces', type=bool, default=False, 
            help='Set to \'y\' for having both surfaces.'
        )
        calc_adsorbate_parser.add_argument(
            '-co', metavar='coverage', type=float, default=0.5, 
            help='Coverage.')

        # Pymatgen optional
        calc_adsorbate_parser.add_argument(
            '-sd', metavar='selective_dynamics', type=bool, 
            default=False, help='Set to \'y\' to have selective dynamics.'
        )
        calc_adsorbate_parser.add_argument(
            '-he', metavar='height', type=float, default=0.9, 
            help='Threshold in angstroms of distance from topmost '
                 'site in slab along the slab c-vector to include '
                 'in surface site determination.'
        )
        calc_adsorbate_parser.add_argument(
            '-mv', metavar='mi_vec', type=str, default=None,
            help='Vector corresponding to the vector concurrent with '
                 'the miller index. E.g.: [1, 1, 1]'
        )
        calc_adsorbate_parser.add_argument(
            '-ml', metavar='min_lw', type=float, default=5,
            help='Minimum length and width of the slab, only used if repeat is'
                 ' None.'
        )
        calc_adsorbate_parser.add_argument(
            '-t', metavar='translate', type=float, default=5, help='Translate.'
        )
        calc_adsorbate_parser.add_argument(
            '-r', metavar='reorient', type=bool, default=False, 
            help='Set to \'y\' to reorient the adsorbate along the miller '
                 'index.'
        )
        calc_adsorbate_parser.add_argument(
            '-fa', metavar='find_args', type=str, default=None, 
            help='Dictionary. E.g.: {“distance”:2.0}'
        )
        calc_adsorbate_parser.add_argument(
            '-a', metavar='a', type=float, default=None, help='a'
        )
        calc_adsorbate_parser.add_argument(
            '-vv', metavar='vv', type=float, default=None, help='v'
        )
        calc_adsorbate_parser.add_argument(
            '-cen', metavar='center', type=str, default='[0, 0, 0]',
            help='Center slab. E.g.: [0, 0, 0]'
        )
        calc_adsorbate_parser.add_argument(
            '-rc', metavar='rotate_cell', type=bool, default=False, 
            help='Set to \'y\' to rotate the cell.'
        )

        return calc_adsorbate_parser
    
    @staticmethod
    def dump_db_parser(subparser):
        """
        Initialize the dump_db_parser for the dump of the database.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        dump_db_parser : argpase
            Parser.

        """
        
        dump_db_parser = subparser.add_parser(
            'dump_db',
            help='Utility to download and load data from and to the online '
                 'database.'
        )
        
        dump_db_parser.add_argument(
            '-action',
            help='\'to_local\' for dumping the database from the cloud to '
                 'local.\n \'to_cloud\' for loading the the database from '
                 'local to cloud.'
        )
        dump_db_parser.add_argument(
            '-dfolder',
            help='Folder where the dump is saved. The name of the dump '
                 'must be the same of the database in case of an upload.'
        )
        dump_db_parser.add_argument(
            '-database',
            help='Name of the database to dump.'
        )
        dump_db_parser.add_argument(
            '-username',
            help='Username of the database.'
        )
        dump_db_parser.add_argument(
            '-password',
            help='Password of the database.'
        )
        dump_db_parser.add_argument(
            '-port',
            help='Port of the connection.'
        )
        dump_db_parser.add_argument(
            '-address',
            help='Address of the server.'
        )
        dump_db_parser.add_argument(
            '-deamon',
            help='The path containing the local database.'
        )
        
        return dump_db_parser
    
    @staticmethod
    def get_input_inter(subparser):
        """
        Initialize get_input_inter parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        get_input_inter_parser : argpase
            Parser.

        """
        
        get_input_inter_parser = subparser.add_parser(
            'get_input_inter',
            help='Input generator for the PES and adhesion calculation of an '
                 'interface.'
        )
        
        get_input_inter_parser.add_argument(
            'mids', metavar='mids', type=str,
            help='Mids of the two elements. E.g. for Fe and Cr: '
                 '[mp-13, mp-90].'
        )
        get_input_inter_parser.add_argument(
            'formulas', metavar='formulas', type=str,
            help='Formulas of the two elements. E.g.: [Fe, Cr].'
        )
        get_input_inter_parser.add_argument(
            'miller', metavar='miller', type=str,
            help='Miller indices of the slabs. E.g.: [[1, 1, 0], [1, 1, 0]].'
        )
        get_input_inter_parser.add_argument(
            'save', metavar='save', type=str, 
            help='Folder where to save the results.'
        )
        get_input_inter_parser.add_argument(
            'type', metavar='it', type=str,
            help='Type of the input: pes or adhesion.'
        )
        get_input_inter_parser.add_argument(
            '-s', metavar='-sites', type=str, default=None,
            help='Minimum and maximum site. '
                 'E.g.: -s="[hollow_1-bridge_1, bridge_1-bridge_1]".'
        )
        get_input_inter_parser.add_argument(
            '-df', metavar='-database_file', type=str, default=None,
            help='Location of the database file.'
        )
        get_input_inter_parser.add_argument(
            '-db', metavar='-database', type=str, default='tribchem',
            help='Name of the database where the data are contained.'
        )
        
        return get_input_inter_parser
    
    @staticmethod
    def get_slab(subparser):
        """
        Initialize get_slab parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        get_slab_parser : argpase
            Parser.

        """
        
        get_slab_parser = subparser.add_parser(
            'get_slab',
            help='Get the slab structure for all the slabs in the database and '
                 'save the outputs in a given location.'
        )
        
        get_slab_parser.add_argument(
            '-df', metavar='-database_file', type=str, default=None,
            help='Location of the database file.'
        )
        get_slab_parser.add_argument(
            '-db', metavar='-database', type=str, default='tribchem',
            help='Name of the database where the data are contained.'
        )
        get_slab_parser.add_argument(
            'save', metavar='save', type=str, 
            help='Folder where to save the results.'
        )
        
        return get_slab_parser
    
    @staticmethod
    def pes_img(subparser):
        """
        Initialize pes_img parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        pes_img_parser : argpase
            Parser.

        """
        
        pes_img_parser = subparser.add_parser(
            'pes_img',
            help='Save to files the potential energy surfaces stored in the '
                 'database.'
        )
        
        pes_img_parser.add_argument(
            'p', metavar='path', type=str,
            help='Path where to save the results.'
        )
        pes_img_parser.add_argument(
            'pt', metavar='pes_type', type=str,
            help='Pes type. It can be only \'pes\' or \'pes_scf\'.'
        )
        pes_img_parser.add_argument(
            'i', metavar='interface_name', type=str,
            help='Name of the interface. E.g.: Co_Ag.'
        )
        pes_img_parser.add_argument(
            '-df', metavar='-database_file', type=str, default=None,
            help='Database containing the location where is the database.'
        )
        pes_img_parser.add_argument(
            '-d', metavar='-database', type=str, default='tribchem',
            help='Database containing the data.'
        )
        
        return pes_img_parser
    
    @staticmethod
    def plot_adh(subparser):
        """
        Initialize plot_adh parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        plot_adh_parser : argpase
            Parser.

        """
        
        pes_img_parser = subparser.add_parser(
            'plot_adh',
            help='Plot the adhesion energies and corrugations for all the '
                 'calculated interfaces or a given set of elements.'
        )
        
        pes_img_parser.add_argument(
            'p', metavar='path', type=str,
            help='Path where to save the results.'
        )

        pes_img_parser.add_argument(
            '-r', metavar='-round', type=int, default=2,
            help='Number of digits to keep.'
        )
        pes_img_parser.add_argument(
            '-e', metavar='-elements', type=str, default=None,
            help='Elements to add in the plot. E.g.: -e="[Al111, Fe110]"'
        )
        pes_img_parser.add_argument(
            '-a', metavar='-adh_type', type=str, default='regular',
            help='Type of the adhesion.'
        )
        pes_img_parser.add_argument(
            '-ls', metavar='-label_size', type=int, default=16,
            help='Label size in the plot.'
        )
        pes_img_parser.add_argument(
            '-ans', metavar='-annotation_size', type=int, default=18,
            help='Annotation size in the plot.'
        )
        pes_img_parser.add_argument(
            '-df', metavar='-database_file', type=str, default=None,
            help='Database containing the location where is the database.'
        )
        pes_img_parser.add_argument(
            '-d', metavar='-database', type=str, default='tribchem',
            help='Database containing the data.'
        )
        
        return pes_img_parser
    
    @staticmethod
    def plot_charge(subparser):
        """
        Initialize plot_charge parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        plot_charge_parser : argpase
            Parser.

        """
        
        plot_charge_parser = subparser.add_parser(
            'plot_charge',
            help='Plot the charge displacement for all the '
                 'calculated interfaces or for a given set of elements.'
        )
        
        plot_charge_parser.add_argument(
            'p', metavar='path', type=str,
            help='Path where to save the results.'
        )
        plot_charge_parser.add_argument(
            '-dir', metavar='-direction', type=str, default='z',
            help='Direction where to calculate the charge displacement.'
        )
        plot_charge_parser.add_argument(
            '-s', metavar='-sites', type=str, default='min_site',
            help='Sites where to calculate the charge displacement. '
                 'Available sites are: \'all\', \'min_site\', \'max_site\'.'
        )
        plot_charge_parser.add_argument(
            '-e', metavar='-elements', type=str, default=None,
            help='Elements to add in the plot. E.g.: -e="[Al111, Fe110]"'
        )
        plot_charge_parser.add_argument(
            '-df', metavar='-database_file', type=str, default=None,
            help='Database containing the location where is the database.'
        )
        plot_charge_parser.add_argument(
            '-d', metavar='-database', type=str, default='tribchem',
            help='Database containing the data.'
        )
        plot_charge_parser.add_argument(
            '-tp', metavar='-type-plot', default='matrix',
            help='Type of plot. It can be either \'matrix\' or \'all\'.'
        )
        
        return plot_charge_parser
    
    @staticmethod
    def plot_ppes(subparser):
        """
        Initialize plot_ppes parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        plot_ppes_parser : argpase
            Parser.

        """
        
        plot_ppes_parser = subparser.add_parser(
            'plot_ppes',
            help='Plot the Perpendicular Potential Energy Surface (PPES) along '
                 'a given direction.'
        )
        
        plot_ppes_parser.add_argument(
            'path', metavar='path', type=str,
            help='Path where to save the results.'
        )
        plot_ppes_parser.add_argument(
            '-pt', metavar='-ppes_type', type=str, default='regular',
            help='Type of PPES. It can be \'regular\' or \'short\'.'
        )
        plot_ppes_parser.add_argument(
            '-e', metavar='-elements', type=str, default=None,
            help='Elements to add in the plot. E.g.: -e="[Al111, Fe110]".'
        )
        plot_ppes_parser.add_argument(
            '-df', metavar='-database_file', type=str, default=None,
            help='Database containing the location where is the database.'
        )
        plot_ppes_parser.add_argument(
            '-d', metavar='-database', type=str, default='tribchem',
            help='Database containing the data.'
        )
        
        return plot_ppes_parser
    
    @staticmethod
    def plot_eos(subparser):
        """
        Initialize plot_eos parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        plot_eos_parser : argpase
            Parser.

        """
        
        plot_eos_parser = subparser.add_parser(
            'plot_eos',
            help='Plot the equation of state for all the elements in the '
                 'database or for a given set.'
        )
        
        plot_eos_parser.add_argument(
            'inputs',
            help='Path where are saved all the folders of each element.'
        )
        plot_eos_parser.add_argument(
            'saveplot', help='Path where to save the plots.'
        )
        plot_eos_parser.add_argument(
            '-elements', metavar='element', default=None,
            help='Element to plot the eos and the energy vs k-points.'
        )
        
        return plot_eos_parser
    
    @staticmethod
    def save_inter(subparser):
        """
        Initialize save_inter parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        save_inter_parser : argpase
            Parser.

        """
        
        save_inter_parser = subparser.add_parser(
            'save_inter',
            help='Save a summary about the PES, adhesion, PPES, and charge '
                 'displacement for all the interfaces.'
        )
        
        save_inter_parser.add_argument(
            'path', metavar='path', type=str,
            help='Path where to save the csv file with the results.'
        )
        save_inter_parser.add_argument(
            '-df', metavar='-db_file', type=str, default='localhost',
            help='Database file with the location of the database.'
        )
        save_inter_parser.add_argument(
            '-db', metavar='-database', type=str, default='tribchem',
            help='Name of the database.'
        )
        
        return save_inter_parser
    
    @staticmethod
    def save_inter_params(subparser):
        """
        Initialize save_inter_params parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        save_inter_params_parser : argpase
            Parser.

        """
        
        save_inter_params_parser = subparser.add_parser(
            'save_inter_params',
            help='Save the interface parameters matching.'
        )
        
        save_inter_params_parser.add_argument(
            's', metavar='source', type=str,
            help='Path where the std output are saved.'
        )
        save_inter_params_parser.add_argument(
            'f', metavar='files', type=str,
            help='Name of the files containing the standard output. '
                 'E.g.: f="[FW_job-46360.out]"'
        )
        save_inter_params_parser.add_argument(
            'p', metavar='path', type=str,
            help='Path where to save the results.'
        )
        
        return save_inter_params_parser
    
    @staticmethod
    def save_results(subparser):
        """
        Initialize save_results parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        save_results_parser : argpase
            Parser.

        """
        
        save_results_parser = subparser.add_parser(
            'save_results',
            help='Save the results of your bulk, slab, interface, whatever, '
                 'to a readable csv table, easily accessible by external '
                 'people.'
        )
        
        save_results_parser.add_argument(
            'p', metavar='path', type=str,
            help='Path where to save the results.'
        )
        save_results_parser.add_argument(
            '-df', metavar='--database_file', type=str, default=None,
            help='Database containing the location where is the database.'
        )
        
        return save_results_parser
    
    @staticmethod
    def save_structure(subparser):
        """
        Initialize save_structure parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        save_structure_parser : argpase
            Parser.

        """
        
        save_structure_parser = subparser.add_parser(
            'save_structure',
            help='Save the POSCAR files in a given location.'
        )
        
        save_structure_parser.add_argument(
            'p', metavar='path', type=str, 
            help='Path where to save the results.'
        )
        save_structure_parser.add_argument(
            '-df', metavar='-database_file', type=str, default=None,
            help='Database containing the location where is the database.'
        )
        save_structure_parser.add_argument(
            '-a', metavar='-adh_type', type=str, default='regular',
            help='Type of the adhesion.'
        )
        save_structure_parser.add_argument(
            '-s', metavar='-init_opt', type=str, default='init',
            help='Whether to save initial (init) or relaxed (opt) structures.'
        )
        
        return save_structure_parser
    
    @staticmethod
    def save_surfene_adh(subparser):
        """
        Initialize save_surfene_adh parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        save_surfene_adh_parser : argpase
            Parser.

        """
        
        save_surfene_adh_parser = subparser.add_parser(
            'save_surfene_adh',
            help='Save a summary for the surface and adhesion energy for all '
                 'the availabe interfaces.'
        )
        
        save_surfene_adh_parser.add_argument(
            'path', metavar='path', type=str,
            help='Path where to save the tex file with the results.'
        )
        save_surfene_adh_parser.add_argument(
            '-df', metavar='-db_file', type=str, default='localhost',
            help='Database file with the location of the database.'
        )
        save_surfene_adh_parser.add_argument(
            '-db', metavar='-database', type=str, default='tribchem',
            help='Name of the database.'
        )
        
        return save_surfene_adh_parser
    
    @staticmethod
    def save_surfene(subparser):
        """
        Initialize save_surfene parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        save_surfene_parser : argpase
            Parser.

        """
        
        save_surfene_parser = subparser.add_parser(
            'save_surfene',
            help='Save the results of the surface energy to a csv file.'
        )
        
        save_surfene_parser.add_argument(
            'path', metavar='path', type=str,
            help='Path where to save the results.'
        )
        save_surfene_parser.add_argument(
            '-df', metavar='-database_file', type=str, default=None,
            help='Database containing the location where is the database.'
        )
        
        return save_surfene_parser
    
    @staticmethod
    def work_function(subparser):
        """
        Initialize work_function parser.
        
        Parameters
        ----------
        subparser : argpase
            Subparser.
        
        Return
        ------
        work_function_parser : argpase
            Parser.
        """
        
        work_function_parser = subparser.add_parser(
            'work_function',
            help='Save the data for calculating the work function.'
        )
        
        work_function_parser.add_argument(
            'source', metavar='source', type=str,
            help='Path containing the vasp output files.'
        )
        
        work_function_parser.add_argument(
            'struct_fld', metavar='structure_folder', type=str,
            help='The structure of the folders. '
                 'E.g.: interface_name/output/pes_type/site.'
        )
        work_function_parser.add_argument(
            'save', metavar='save', type=str,
            help='Folder where to save the results.'
        )
        work_function_parser.add_argument(
            '-a', metavar='-axis', type=str, default='z', 
            choices=['x', 'y', 'z'],
            help='Axis along which calculate the potential energy.'
        )
        work_function_parser.add_argument(
            '-c', metavar='-compressed', type=str, default='y', 
            choices=['y', 'n'],
            help='If the folder containing the output files is compressed or '
                 'not.'
        )
        
        return work_function_parser
