The following workflow is able to generate an interface, to calculate the potential energy surface (PES), the adhesion energy, the perpendicular potential energy surface (PPES), and the charge displacement. The permitted order to execute more than one workflow is:

```mermaid
graph LR
    interface --> pes
    pes --> adhesion 
    adhesion --> ppes
    ppes --> charge
```

Check out with `lpad get_fws` or by using the FireWorks dashboard if the workflow has been correctly generated and saved in the database. If everything is fine, the workflows can be submitted to the calculation nodes by using `qlaunch singleshot`.

# CLI Parameters
Usage: `tribchem workflow calc_interface wf="[interface, pes, adhesion, ppes, charge]" mids="[mp-13, mp-90]" formulas="[Fe, Cr]" miller="[[1, 1, 1], [1, 1, 1]]"`.  

### Positional Arguments common to all wfs
- **wf**: list of the workflows. The available workflows are: interface, pes, adhesion, ppes, charge. E.g.: `[interface, pes, adhesion]`.
- **mids**: Mids of the two elements. E.g. for Fe and Cr: `[mp-13, mp-90]`.
- **formulas**: Formulas of the two elements. E.g.: `[Fe, Cr]`.
- **miller**: Miller indices of the slabs. E.g.: `[[1, 1, 0], [1, 1, 0]]`.

### Optional Parameters for wf="[interface]"
- **-h [help]**: show this help message and exit.
- **-d [debug]**: if set to 'y' the execution of the workflows will be done on the current machine. This setting is useful for debugging operations.
- **-cs [collection_slab]**: collection where to save/retrive data about the slabs. Default is 'PBE.slab_elements'.
- **-ci [collection_inter]**: collection where to save/retrive data about the interfaces. Default is 'PBE.interface_elements'.
- **-o [override]**: set to 'y' to override a calculation.
- **-spin [use_spin]**: set True for the magnetic materials.
- **-marea [max_area]**: max area for the interface matching.
- **-idist [interface_distance]**: distance of the interface.
- **-max_angle_tol [max_angle_tol]**: maximum angle tolerance.
- **-max_length_tol [max_length_tol]**: maximum length tolerance.
- **-max_area_ratio_tol [max_area_ratio_tol]**: max area ratio tolerance.
- **-bm [best_match]**: best match.


### Optional Parameters for wf="[pes]"
- **-h [help]**: show this help message and exit.
- **-d [debug]**: if set to 'y' the execution of the workflows will be done on the current machine. This setting is useful for debugging operations.
- **-pt [pes]**: pes type. It can only be 'pes'(default) or 'pes_scf'.
- **-o [override]**: set to 'y' to override a calculation.
- **-rv [run_vasp]**: set to 'n' to not run the VASP calculation.
- **-ci [collection_inter]**: collection where to save/retrive data about the interfaces.


### Optional Parameters for wf="[adhesion]"
- **-h [help]**: show this help message and exit.
- **-d [debug]**: if set to 'y' the execution of the workflows will be done on the current machine. This setting is useful for debugging operations.
- **-ad [adhesion]**: adhesion type. It can only be 'regular'(default) or 'short'.
- **-o [override]**: set to 'y' to override a calculation.
- **-rv [run_vasp]**: set to 'n' to not run the VASP calculation.

### Optional Parameters for wf="[ppes]"
- **-h [help]**: show this help message and exit.
- **-d [debug]**: if set to 'y' the execution of the workflows will be done on the current machine. This setting is useful for debugging operations.
- **-ppt [ppes]**: pes type for the PPES. It can only be 'regular' (default) or 'short'.
- **-o [override]**: set to 'y' to override a calculation.
- **-rv [run_vasp]**: set to 'n' to not run the VASP calculation.
- **-sh [shifts]**: distance list. Default is from -0.75 Å to 3 Å with a step of 0.25 Å.
- **-s [sites]**: site where to calculate the PPES curve. Default is 'min_site' (corresponds to the min of the PES).

### Optional Parameters for wf="[charge]"
- **-h [help]**: show this help message and exit.
- **-d [debug]**: if set to 'y' the execution of the workflows will be done on the current machine. This setting is useful for debugging operations.
- **-chgtype [charge type]**: charge type for the calculation. It can only be 'regular' (default) or 'short'.
- **-o [override]**: set to 'y' to override a calculation.
- **-rv [run_vasp]**: set to 'n' to not run the VASP calculation.
- **-op [operations]**: operation for the charge displacement. Default is `[-,-]`.
- **-s [sites]**: site where to calculate the charge displacement. Default is 'min_site' (corresponds to the min of the PES).
- **-tp [to_plot]**: set True to plot the charge displacement.

