The following workflow calculates the cutoff energy and the K-points of the bulk of a set of elements.

Check out with `lpad get_fws` or by using the FireWorks dashboard if the workflow has been correctly generated and saved in the database. If everything is fine, the workflows can be submitted to the calculation nodes by using `qlaunch singleshot`.

# CLI Parameters
Usage: `tribchem workflow converge_bulk mids="[mp-13, mp-90]" formulas="[Fe, Cr]"`.

### Positional Arguments
- **mids**: Mids of the elements. Set to 'all' to converge all the bulks. E.g. for Fe and Cr: [mp-13, mp-90].
- **formulas**: Formulas of the elements. Set to 'all' to converge all the bulks. E.g.: [Fe, Cr].

### Optional Parameters
- **-h [help]**: show this help message and exit.
- **-d [debug]**: if set to 'y' the execution of the workflows will be done on the current machine. This setting is useful for debugging operations.
- **-o [override]**: set to 'y' to override a calculation.
- **-dbf [db_file]**: location of the database file.
- **-db [db]**: database for the bulks.
- **-fln [fltr_name]**: name of the filter. Available name are: crystal_system, space_group, point_group.
- **-fl [fltr]**: filter.
- **-ce [check_encut]**: location where the energy cutoff info are saved.
- **-es [encut_start]**: energy cutoff starting value.
- **-ei [encut_incr]**: energy cutoff increment.
- **-ks [kdens_start]**: kpoints density starting value.
- **-ki [kdens_incr]**: kpoints density increment value.
- **-nc [n_converge]**: convergence is reached when n_converge number of calculations show the same energy as the last one, within a given threshold.
- **-f [functional]**: functional.
- **-ee [entry_encut]**: location where the energy cutoff is saved.
- **-ek [entry_kpts]**: location where the optimal K-points are saved.
- **-uo [update_op]**: location where the optimal K-points are saved.
- **-up [upsert]**: create a new instance in the DB every time a document differs.