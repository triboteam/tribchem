The following workflow saves the structure of the bulk of a set of elements from the [Materials Project](https://materialsproject.org/) database to the local one. Since the execution of the script is very fast, it is fine to execute it on the login node or even on a personal computer.

# CLI Parameters
Usage: `tribchem workflow initialize_bulk mids="[mp-13, mp-90]" formulas="[Fe, Cr]"`.

### Positional Arguments
- **mids**: Mids of the elements. Set to 'all' to converge all the bulks. E.g. for Fe and Cr: [mp-13, mp-90].
- **formulas**: Formulas of the elements. Set to 'all' to converge all the bulks. E.g.: [Fe, Cr].

### Optional Parameters
- **-h [help]**: show this help message and exit.
- **-d [debug]**: set to 'y' the execution of the workflows will be done on the current machine. This setting is useful for debugging operations.
- **-o [override]**: set to 'y' to override a calculation.
- **-dbf [db_file]**: location of the database file.
- **-db [db]**: database for the bulks.
- **-fln [fltr_name]**: name of the filter. Available name are: crystal_system, space_group, point_group.
- **-fl [fltr]**: filter.
- **-f [functional]**: functional.
- **-ce [check_entry]**: location where to save the bulk.
- **-uo [update_op]**: update operation for the database.
- **-up [upsert]**: set to 'y' to create a new document in the DB, otherwise it update it (IF PRESENT).
- **-tm [to_mongodb]**: set to 'y' to convert the dict to a suitable form.