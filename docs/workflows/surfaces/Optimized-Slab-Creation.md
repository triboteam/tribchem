The following workflow generates a set of slabs given a set of elements for a specific orientation of the atomic plane. The obtained slabs will have the optimal thickness based on the convergence over the surface energy.

The steps done are:
- Create the slab out of a bulk
- Relax its structure and collect the physical results
- Calculate the optimal number of atomic layers (thickness)
- Calculate the surface energy

WARNING: The generation of the slabs and the convergence of their thickness are
guaranteed to work properly only for bulks with monoatomic basis.

Check out with `lpad get_fws` or by using the FireWorks dashboard if the workflow has been correctly generated and saved in the database. If everything is fine, the workflows can be submitted to the calculation nodes by using `qlaunch singleshot`.

# CLI Parameters
Usage: `tribchem workflow converge_slab mids="[mp-13, mp-90]" formulas="[Fe, Cr]" miller="[[1, 1, 1], [1, 1, 1]]"`.

### Positional Arguments
- **mids**: Mids of the elements. Set to 'all' to converge all the bulks. E.g. for Fe and Cr: [mp-13, mp-90].
- **formulas**: Formulas of the elements. Set to 'all' to converge all the bulks. E.g.: [Fe, Cr].
- **miller**: miller indices of the slabs. E.g.: `"[[1, 1, 0], [1, 1, 0]]"`.
- **t_min**: minimum thickness of the slab. Set to 3 for 111, 4 for 110 and 0011.
- **t_max**: maximum thickness of the slab. 12 is always a good value.

### Optional Parameters
- **-h, [help]**: show this help message and exit.
- **-d [debug]**: if set to 'y' the execution of the workflows will be done on the current machine. This setting is useful for debugging operations.
- **-o [override]**: set to 'y' to override a calculation.
- **-os [override_slab]**: set to 'y' to override the calculation of a slab.
- **-dbf [db_file]**: location of the database file.
- **-db [db]**: database for the bulks.
- **-cb [collection_bulk]**: collection for the bulks.
- **-cs [collection_slab]**: collection for the slabs.
- **-ce [check_entry]**: location where the optimal thickness is saved.
- **-be [bulk_entry]**: location where the optimal bulk is saved.
- **-p [parallelization]**: parallelization level.
- **-im [is_metal]**: computational parameter if it is a metal for each element. EXTREMELY IMPORTANT: setting them you can change the value that are read from the bulk.E.g.: [True, True, False].
- **-us [use_spin]**: computational parameter if use spin for each element. EXTREMELY IMPORTANT: setting them you can change the value that are read from the bulk.E.g.: [True, True, False].
- **-vdw [vdw]**: computational parameter if use vdw for each element. EXTREMELY IMPORTANT: setting them you can change the value that are read from the bulk.E.g.: [True, True, False].
- **-ct [conv_thr]**: convergence threshold for surface energy with respect to max thick slab.
- **-ti [thick_incr]**: typically 1 is fine, but RUN only stuff with MONO atomic basis for now.
- **-va [vacuum]**: vacuum over the surface.
- **-up [in_unit_planes]**: build the slab based on n.layers or angstrom.
- **-cct [calc_type]**: type of calculation for the slab.
- **-ei [ext_index]**: what slab to consider from the list, if unsure leave it to zero.