The utility `tribchem analysis dump_db -h` dumps or uploads the database to local or to the cloud respectively.

# CLI Parameters
### Optional Parameters
- **-action [action]**: `to_local` for dumping the database from the cloud to local. `to_cloud` for loading the the database from local to cloud.
- **-dfolder [dfolder]**: folder where the dump is saved. The name of the dump must be the same of the database in case of an upload.
- **-database [database]**: name of the database to dump.
- **-username [username]**: username of the database.
- **-password [password]**: password of the database.
- **-port [port]**: port of the connection.
- **-address [address]**: address of the server.
- **-deamon [deamon]**: the path containing the local database.