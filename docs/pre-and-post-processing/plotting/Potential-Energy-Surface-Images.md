The utility `tribchem analysis pes_img -h` saves to files the potential energy surfaces stored in the database.

# CLI Parameters
Usage: `tribchem analysis pes_img path=saving_path pes_type=pes interface_name=Co_Ag`.

### Positional Arguments
- **path**: path where to save the results.
- **pes_type**: it can be only `pes` or `pes_scf`.
- **interface_name**: name of the interface. E.g.: Co_Ag.

### Optional Parameters
- **-df [database_file]**: database containing the location where is the database.
- **-db [database]**: name of the database. E.g.: tribchem.