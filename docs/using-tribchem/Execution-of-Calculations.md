In this section are discussed all the necessaries steps for launching high throughput calculations using TribChem. The official documentation of FireWorks can be found [here](https://materialsproject.github.io/fireworks/index.html). In particular, the sections about the creation of a job and its subsequent submission to a queue are very important, and can be found in [Defining Jobs using Firetasks](https://materialsproject.github.io/fireworks/firetask_tutorial.html), and in [Launch Rockets through a queue](https://materialsproject.github.io/fireworks/queue_tutorial.html) respectively.

### Preliminaries steps
1. Make sure that the parameters which define a job, contained in `fw_qadapter.yaml` (in particular `nodes` and `walltime`), and in `my_fworker.yaml` (in particular the number of processes in `mpirun -n ...`), are correctly set for the specific calculation to being executed.
2. Activate `tribchem` environment using: `conda activate tribchem` (the name of the environment could be different if during the installation the default value of `tribchem` was not used).

### Launch a calculation
- (**Optional step**) In case the database is on a different machine where the calculation will be run (e.g. when MongoDB Atlas is used), this step is not necessary. Launch the MongoDB daemon using: `mongo_start`. This command is an alias, and it is defined during the installation process ([Installation](TribChem/Installation)). Check there in case the shell is unable to recognize the command. Moreover, in case the daemon has already been launched this command is unnecessary. Check with `top` or `htop` if the daemon is already running. At the moment the local database is located in the login node, but the location could be different. In this case it is necessary to check if that the daemon is running in that location.
- Check the status of the previous calculations by using: `lpad get_fws`. The output should be a list of dictionaries (see below), in which all the FireWorks initialized with all their details are saved. In case some old FireWorks are still in the list, a good practice before starting a new calculation is to remove them in order to not execute some old ones that might not work properly. For doing so run the command: `lpad reset`.
```json
[
    {
        "fw_id": 10,
        "created_on": "2021-03-22T08:07:35.805694",
        "updated_on": "2021-03-22T08:07:35.805695",
        "name": "Calculate the Surface Energies",
        "state": "WAITING"
    },
    {
        "fw_id": 11,
        "created_on": "2021-03-22T08:07:35.805665",
        "updated_on": "2021-03-22T08:07:35.805666",
        "name": "Relax and store in DB, slab: Cu_slab_100_1",
        "state": "WAITING"
    },
    {
        "fw_id": 12,
        "created_on": "2021-03-22T08:07:35.805618",
        "updated_on": "2021-03-22T08:07:35.805619",
        "name": "Relax and store in DB, slab: Cu_slab_100_1",
        "state": "WAITING"
    },
    {
        "fw_id": 13,
        "created_on": "2021-03-22T08:07:35.805565",
        "updated_on": "2021-03-22T08:07:35.805568",
        "name": "Relax and store in DB, slab: Cu_slab_100_0",
        "state": "WAITING"
    },
    {
        "fw_id": 14,
        "created_on": "2021-03-22T08:07:35.805306",
        "updated_on": "2021-03-22T08:07:35.829632",
        "state": "READY",
        "name": "Generate 3 slabs"
    }
]

```
- Execute the python script using: `python script_name.py`, and check again, by using `lpad get_fws`, if all the FireWorks have been created correctly.
- In case all the FireWorks are correct, submit the job to the queue system by using: `qlaunch singleshot`. This command will submit only one FireWork, and it is enough for starting a calculation made up by several FireWorks or WorkFlows, as FireWork automatically submit the new FireWorks. In case we would like to run multiple independent FireWorks, there is an automatic way to submit them by using: [qlaunch rapidfire -m 3](https://materialsproject.github.io/fireworks/queue_tutorial.html#adding-more-power-using-rapid-fire-mode), where `-m 3` means to submit three jobs to the queue (more details on this command are discussed in the documentation). If the previous command is run without any arguments (`qlaunch rapidfire`) FireWorks will continuously submit jobs to the queue (which is a very dangerous thing as it is very easy to submit dozen of jobs to the queue in short amount of time). In this case FireWorks waits that the every FireWork finishes the execution (it will take all the time required for completing the calculation, then it could take a lot of time). Check out with `squeue` if the FireWork was submitted correctly to the queue system. The job file, the job log, the job error, and all the VASP files are generated in the folder where the submission command was launched. For this reason it is a good practice to create a folder within `calculation` (which is outside the root of `tribchem`) properly named for the calculation just run.
If we would like to run the first available FireTasks in the local machine, run `rlaunch singleshot` ([Defining Jobs using Firetasks](https://materialsproject.github.io/fireworks/firetask_tutorial.html)), and `rlaunch rapidfire` for launching all the available FireTasks.

### Fizzled Job
If for some reason an error occurs during the execution of a FireTask, the state of the job is set to `FIZZLED`, and its execution stops. The errors for debugging the problem are reported in the files generated within the directory and in the database. A fast way to check the error obtained for a `FIZZLED` FireTask is to launch `lpad get_fws -i id_of_the_firetaks -d more`. In the long output displayed, corresponding to `_exception` key, there is the error message obtained during the execution. Once solved the errors, the fizzled FireTask can be set to `WAITING` with `lpad rerun_fws -s FIZZLED`, and the calculation can be submitted to the queue again. For more details on this command check the documentation [rerun a firework](https://materialsproject.github.io/fireworks/rerun_tutorial.html).

### Webgui
FireWorks provides a nice web interface for checking and analyzing the state of the FireTasks with a powerful graphical view. For using the webgui run the following command: `lpad webgui -s` and open the address (typically `http://127.0.0.1:5000`) in a web browser. This command does not work on a cluster, and it is necessary to run it on you local machine. Make sure that the database location is the same of the cluster (make sure that in the `config` folder the `db.json` file has the proper host), otherwise the displayed data will not match or the command will not even work.

### VASP parameters
`KPAR` and `NCORE` of `my_fworker.yaml` must be consistent with `ntasks_per_node` of `my_qadapter.yaml`. If `ntasks_per_node` is not a multiple of `KPAR` VASP will crash. Then, it is very important to set these values correctly. The VASP manual suggests to set `NCORE=2`, and a consistent `KPAR` value, as it greatly speeds up the calculation.