In this tutorial we will show how to calculate the adhesion energy of an interface of aluminum Al(111) and magnesium Mg(0001).

Before starting this tutorial, make sure that the `TribChem` package and the database are fully installed and configured. For more details on the installation refer to [Installation](Installation), [Configuration Files](Configuration Files), and [Set up the MongoDB Atlas Cloud Database](Set up the MongoDB Atlas Cloud Database).

#### Preliminary steps
Activate the anaconda environment where the package `tribchem` is installed with: `conda activate tribchem`. By default the name of the environment is `tribchem`. If during the installation a different name was used the command becomes: `conda activate name_of_the_environment` (`conda env list` lists all the available environments). To check if everything is working correctly execute the command `lpad get_fws`. If everything works fine, the list of all the workflows loaded in the database should be displayed. In case no workflow is loaded the command will return an empty list.

#### The adhesion energy calculation from the bulks to the PES
```mermaid
flowchart LR
    A[1. Download Bulks] --> B[2. Converge Bulks]
    B --> C[3. Slabs Creation]
    C --> D[4. Interface Creation]
    D --> E[5. PES Calculation]
    E --> F[6. Adhesion Energy]
```

#### 1. Download the structure of the bulks from the Materials Project database

The first step is to download the desired bulk structures from the [Materials Project](https://materialsproject.org/) database to the local database called `tribchem`. For doing so we will use the workflow `tribchem workflow initialize_bulk -h`.

```mermaid
flowchart LR
    A[(Materials Project)] --> B[(Tribchem)] 
```

In this case we would like to download only the bulks of aluminium and magnesium. To do so it is enough to execute: `tribchem workflow initialize_bulk mids="[mp-134, mp-153]" formulas="[Al, Mg]"`. For the list of all the available mids of the elements of the periodic table, check out the file `tribchem/highput/database/mid/elements.json`, where are also reported some properties about the structure of the bulks. In case one would like to download all the elements of `elements.json`, we would set `mids=all formulas=all`. If instead we would like to download some elements based on the crystal structure, we would set `mids=all formulas=all` and (let us suppose we would like to download all the cubic systems) `-fln=crystal_system -fl=cubic`.

For the complete list of the available parameters check [Bulk Initialization](Bulk Initialization).

Check that the writing operation of the workflow in the database worked correctly with the command:`lpad get_fws` or from the FireWorks dashboard. Submit the calculations with: `qlaunch singleshot` and check the execution on the dashboard. Once completed, in the table `PBE.bulk_elements` of `tribchem` there will be the bulks just downloaded.

#### 2. Converge the cutoff energy and the K-points
To converge the cutoff energy and the K-points the workflow used is `tribchem workflow converge_bulk -h`. As in the previous step the command is: `tribchem workflow converge_bulk mids="[mp-134, mp-153]" formulas="[Al, Mg]"`. Again, to apply to all the available materials set `mids=all formulas=all`, and if we would like to base the selection on the crystal system set `-fln=crystal_system -fl=cubic` (for all the cubic systems).

For the complete list of the available parameters check [Bulk Creation](Bulk Creation). 

Again, check and submit the calculations with `lpad get_fws` and `qlaunch singleshot` respectively. Check on the FireWorks dashboard the progress, and the final result in the table `PBE.bulk_elements` once completed. The VASP output files will be saved at one level up with respect to the root folder of the project in `../calculations/PBE.bulk_elements`.

#### 3. Creation of the slabs
For the slab creation the script used is `tribchem workflow converge_slab -h`. In this case the execution of the script must be broken down in two parts. Although it is possible to execute more than one element at a time, in this case the surfaces we are interested in are different, and we can pass only one set of Miller indices at a time, since the variable `thick_min` varies with the Miller indices. In fact, `thick_min` is 3 when the Miller indices are 111, and 4 when they are 0001. This is because of the different packing structure of the two elements: fcc for Al, and hcp for Mg. The execution of the calculations of this workflow can be parallelized by setting the parameter `parallelization` to one of the following values: `None`, `Low`, and `High`. Besides `parallelization` there are many more parameters which can be set. The default values are almost always valid. 

For more information on all possible parameters check [Optimized Slab Creation](Optimized Slab Creation). 

Therefore, the first execution is: `tribchem workflow converge_slab mids="[mp-134]" formulas="[Al]" miller="[[1, 1, 1]]"`, and the second `tribchem workflow converge_slab mids="[mp-153]" formulas="[Mg]" miller="[[0, 0, 1]]"`.

Check and submit as in the previous step. 

The results will be saved in the table `PBE.slab_elements`, while the VASP output files one level up with respect to the root folder in `../calculations/PBE.slab_elements`. Troubleshooting: sometimes the execution of the script fails. Very often this happens because the bulk is not in the database or there are some errors in the definition of `mids` and/or `formulas`.

#### 4. Generation of the interface
For the generation of the interface we use `tribchem workflow calc_interface -h`. The command is then: `tribchem workflow calc_interface wf="[interface]" mids="[mp-134, mp-153]" formulas="[Al, Mg]" miller="[[1, 1, 1], [0, 0, 1]]" -spin=y`

For the complete list of all available parameters check [Interfaces](Interfaces).

The results will be written in the table `PBE.interface_elements`.

#### 5. PES calculation 
To calculate the PES we execute `tribchem workflow calc_interface wf="[pes]" mids="[mp-134, mp-153]" formulas="[Al, Mg]" miller="[[1, 1, 1], [0, 0, 1]]"`.

For the complete list of all the available parameters check [Interfaces](Interfaces).

Check and submit. The results will be saved in the table `PBE.slab_elements`, while the VASP output files one level up with respect to the root folder in `../calculations/PBE.slab_elements`.

#### 6. Adhesion energy calculation
For the adhesion energy calculation we execute `tribchem workflow calc_interface wf="[adhesion]" mids="[mp-134, mp-153]" formulas="[Al, Mg]" miller="[[1, 1, 1], [0, 0, 1]]"`. 

For more details on the parameters check [Interfaces](Interfaces).

As before, write in the database and submit. The results of the adhesion energy will be written in the database in the table `PBE.interface_elements`. For the VASP output files instead, they will be saved one level up with respect to the root folder of the project in `../calculations/PBE.interface_elements`. Troubleshooting: sometimes the execution of the script fails. Very often this happens because the interface is not in the database or there are some errors in the definition of `mid` or `formula`.

#### Merging the steps 4, 5 and 6
The previous two steps can be merged together in a single one, by generating the workflow of the interface, the PES, and the adhesion energy at the same time, by using: `tribchem workflow calc_interface wf="[interface, pes, adhesion]" mids="[mp-134, mp-153]" formulas="[Al, Mg]" miller="[[1, 1, 1], [0, 0, 1]]"`.
