### Installation
- [Installation Guide](installation/Installation-Guide.md)
- [Configuration Files](installation/Configuration-Files.md)
- [Set up the MongoDB Atlas Cloud Database](installation/Set-up-the-MongoDB-Atlas-Cloud-Database.md)
- [VASP Compilation](installation/VASP-Compilation.md)

### Using TribChem
- [Execution of Calculations](using-tribchem/Execution-of-Calculations.md)
- Tutorials
  - [Adhesion energy of an interface](using-tribchem/tutorials/Adhesion-energy-of-an-interface.md)

### Workflows
- Bulks
  - [Bulk Initialization](workflows/bulks/Bulk-Initialization.md)
  - [Bulk Creation](workflows/bulks/Bulk-Creation.md)
- Surfaces
  - [Optimized Slab Creation](workflows/surfaces/Optimized-Slab-Creation.md)
- Interfaces
  - [Interfaces](workflows/interfaces/Interfaces.md)

### Pre and Post processing
- Plotting
  - [Potential Energy Surface Images](pre-and-post-processing/plotting/Potential-Energy-Surface-Images.md)
- General
   - [Dump Database](pre-and-post-processing/general/Dump-Database.md)
