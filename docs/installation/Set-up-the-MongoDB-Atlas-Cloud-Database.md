For using the FireWorks package it is necessary to set up a MongoDB database which manages the workflow, and store some results of the calculations. There are several ways to set up the database (for detailed information read here [atomate installation](https://atomate.org/installation.html#mongodb)). In this guide the fastest and simplest solution is reported: the usage of MongoDB Atlas. MongoDB Atlas is a cloud MongoDB database, with up to 512 MB of free space in the free plan. 

In order to use `TribChem` with MongoDB Atlas it is necessary, first of all, to subscribe and create a user here [MongoDB Atlas](https://www.mongodb.com/cloud/atlas). In case a database has not been created yet, create a new one. 

To connect `TribChem` to the database it is necessary to write the correct host addresses in `db.json` file and in `my_launchpad.yaml`. The notation in which the host is written is different for the two files. This is a big source of errors and requires a lot of attention. To obtain the host we have to log in the MongoDB Atlas web page, select the cluster in which our database is located, and click on `CONNECT` option. A menu with three options should pop up. Select the option `collect with the mongo shell` for getting the host address to be used in `db.json` file (copy and paste only the first part of the address as the name of the database is set in the option `database`), and select `Connect your application` for getting the the host address to be used in `my_launchpad.yaml` (in this case the username, the password, and the name of the database must be written in the address). Follow the examples below.

`db.json` looks like:  
```json
{  
   "host": "mongodb+srv://cluster0.pj6wi.mongodb.net",
   "port": 27017,  
   "database": "FireWorks",  
   "collection": "tasks",  
   "admin_user": <adminUsername>,  
   "admin_password": <adminPassword>,  
   "readonly_user": <readOnlyUsername>,  
   "readonly_password": <readOnlyPassowrd>,  
   "aliases": {},  
   "authsource": "admin"
}
```

In the case of `my_launchpad.yaml` set all the settings to `null` except for: `authsource: admin`, `host: mongodb+srv://<username>:<password>@cluster...`, and `strm_lvl: INFO`.
`my_launchpad.yaml` will look like:
```yaml
authsource: admin
host: mongodb://<username>:<password>@cluster0-shard-00-00.pj6wi.mongodb.net:27017,cluster0-shard-00-01.pj6wi.mongodb.net:27017,cluster0-shard-00-02.pj6wi.mongodb.net:27017/FireWorks?ssl=true&replicaSet=atlas-ogvrgh-shard-0&authSource=admin&retryWrites=true&w=majority
logdir: null
mongoclient_kwargs: {}
name: FireWorks
password: root
port: 27017
ssl: true
ssl_ca_certs: null
ssl_certfile: null
ssl_keyfile: null
ssl_pem_passphrase: null
strm_lvl: INFO
uri_mode: false
user_indices: []
username: root
wf_user_indices: []
```