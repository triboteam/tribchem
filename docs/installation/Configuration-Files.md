In this section all the configuration files specifications are discussed, with particular attention to the settings about the VASP parameters. A more detailed explanation on the settings for the MongoDB Atlas can be found on this page [Set up the MongoDB Atlas Cloud Database](TribChem/High Throughput/Set up the MongoDB Atlas Cloud Database).

The following files are contained inside a folder called `config`, which usually is one level up the root folder of `tribchem`.

For a more detailed description of these files check out the [FireWorks](https://materialsproject.github.io/fireworks/index.html), and [Atomate](https://atomate.org/) documentation.

#### FW_config.yaml
Inside this file the variable `CONFIG_FILE_DIR` is defined, which locates the folder where all the configuration files are stored. Moreover, a message line is printed for debug purposes.
```yaml
CONFIG_FILE_DIR: <YourPath>/config
ECHO_TEST: Database at <YourPath>/config/FW_config.yaml is getting selected.
``` 

#### db.json
Inside this file some settings of the database management system are defined. For a detailed explanation of this file, check out here [Set up the MongoDB Atlas Cloud Database](TribChem/High Throughput/Set up the MongoDB Atlas Cloud Database).

#### my_fworker.yaml
Inside this file the settings for the VASP execution are defined.

```yaml
name: name_of_the_worker
category: ''
query: '{}'
env:
    db_file: <YourPath>/config/db.json
    vasp_cmd: srun --mpi=pmi2 --cpu-bind=core <PathToVASP>/vasp_std
    scratch_dir: <PathToScratch>
    vdw_kernel_dir: <PathToVdwKernel>/vdw_kernel
    incar_update:
        KPAR: 1
        NCORE: 1

```

#### my_launchpad.yaml
Inside this file some settings on the database management system are defined. For a detailed explanation of this file, check out here [Set up the MongoDB Atlas Cloud Database](TribChem/High Throughput/Set up the MongoDB Atlas Cloud Database).
  
#### my_qadapter.yaml
Inside this file the settings about submitting a job to the queue are defined. Moreover, all the possible settings for SLURM are reported at the end of the file.

```yaml
_fw_name: CommonAdapter
_fw_q_type: SLURM
ocket_launch: rlaunch -c <YourPath>/config rapidfire --timeout 172800
nodes: 1
#ntasks: 16
ntasks_per_node: 24
walltime: 20:00:00
queue: null
account: null
job_name: null

pre_rocket: source <YourModuleLocation> && source <YourCondaLocation>anaconda/miniconda3/etc/profile.d/conda.sh && conda activate <YourEnvironment>
post_rocket:
logdir: <YourPathToLog>/configlog

# Settings for SLURM
#SBATCH --nodes=$${nodes}
#SBATCH --ntasks=$${ntasks}
#SBATCH --ntasks-per-node=$${ntasks_per_node}
#SBATCH --ntasks-per-core=$${ntasks_per_core}
#SBATCH --core-spec=$${core_spec}
#SBATCH --exclude=$${exclude_nodes}
#SBATCH --cpus-per-task=$${cpus_per_task}
#SBATCH --gpus-per-task=$${gpus_per_task}
#SBATCH --gres=$${gres}
#SBATCH --qos=$${qos}
#SBATCH --time=$${walltime}
#SBATCH --time-min=$${time_min}
#SBATCH --partition=$${queue}
#SBATCH --account=$${account}
#SBATCH --job-name=$${job_name}
#SBATCH --license=$${license}
#SBATCH --output=$${job_name}-%j.out
#SBATCH --error=$${job_name}-%j.error
#SBATCH --constraint=$${constraint}
#SBATCH --signal=$${signal}
#SBATCH --mem=$${mem}
#SBATCH --mem-per-cpu=$${mem_per_cpu}
#SBATCH --mail-type=$${mail_type}
#SBATCH --mail-user=$${mail_user}

```