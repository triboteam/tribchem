Compile the VASP package might be a problematic process sometimes. In this small guide we report all the links to the official documentation, resources, and tips in order to compile VASP in the best and easiest way possible. In particular the installations for the parallel version with MPI using the GNU fortran compiler, with the Intel MPI, and the serial version for an Intel compiler are described.

The following link refers to the official VASP documentation: [VASP Manual](https://www.vasp.at/wiki/index.php/The_VASP_Manual).

The first step for compiling VASP is to extract the archive containing the source code. In the VASP root folder there is `arch/` which contains several `makefiles.include` for different systems (some of them can be found also here [VASP Makefile](https://www.vasp.at/wiki/index.php/Category:Installation)). 

### GNU Compiler Parallel Version with MPI
1. From `arch/` copy in the root folder of VASP `makefile.include.linux_gnu` and rename it to `makefile.include`. Edit this file accordingly to the specifications of the system in which VASP is being installed. In particular, change the location for `BLAS`, `SCALAPACK`, and `FFTW`. Moreover, check the proper optimization variables for the compiler accordingly. 
2. For the default compilation run the following command: `make -jN std gam ncl`, where N is the number of processes to use for the parallel compilation (e.g. 4 means that the compilation is parallelized on 4 processes). If all the paths to the libraries are properly set, the compilation process should finish correctly, and the executables should be generated in `bin/`.

### Intel Compiler Parallel Version with MPI
1. Load the Intel compiler using `source ../intel/bin/compilervars.sh intel64` or `module load name_of_the_module`.
2. From `arch/` copy in the root folder `makefile.include.linux_intel` and rename it to `makefile.include`. In this case `makefile.include` might not require any modifications for the location of the libraries, as `$MKLROOT` is usually already set to the root folder where the Intel compiler is located. To check if the location is correctly set, just type in the shell `$MKLROOT`, which should return something like: `../intel/compilers_and_libraries_2020.2.254/linux/mkl`. At this step change the optimization variables for the compiler accordingly to the CPU specifications. 
3. Compile the FFTW interface library. From the root folder of the Intel compiler copy  `interfaces/fftw3xf` in the root of VASP. Move inside `vasp_root/fftw3xf` and then compile the library by using: `make -jN libintel64`.
4. Compile VASP by using: `make -jN std gam ncl`. If the compilation finishes without any errors the executables should be in `bin/`.

### Intel Compiler Serial Version
1. Load the Intel compiler using `source ../intel/bin/compilervars.sh intel64` or `module load name_of_the_compiler`.
2. Copy from `arch/` to the root folder `makefile.include.linux_intel` and rename it to `makefile.include`. In this case `makefile.include` might not require any modifications for the location of the libraries, as `$MKLROOT` is usually already set to the root folder where the Intel compiler is located. To check if the location is correctly set, just type in the shell `$MKLROOT`, which should return something like: `../intel/compilers_and_libraries_2020.2.254/linux/mkl`. At this step change the optimization variables for the compiler accordingly to the CPU specifications.
3. Compile VASP by using: `make -jN std gam ncl`. If the compilation finishes without any errors the executables should be in `bin/`.