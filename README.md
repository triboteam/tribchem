# TribChem

TribChem is a collection of workflows designed to build crystalline interfaces within a wide range of materials, and to calculate their tribological figures of merit using Density Functional Theory (DFT), storing the results in databases.
TribChem uses the [FireWorks](https://materialsproject.github.io/fireworks/index.html) framework and relies heavily on [pymatgen](https://pymatgen.org/), [atomate](https://atomate.org/), and the [Materials Project](https://materialsproject.org/) in general. For the DFT calculations the Vienna Ab initio Simulation Package ([VASP](https://www.vasp.at/)) is used.

Since 2022, the TribChem development is coordinated by Dr. Margherita Marsili, a senior member of [Prof. M. Clelia Righi's group](http://www.tribchem.it/) at the University of Bologna, within the ”Advancing Solid Interface and Lubricants by First Principles Material Design" (SLIDE) project that has received funding from the European Research Council (ERC) under the European Union’s Horizon 2020 research and innovation program (Grant agreement No. 865633).

In case any issue arises during software installation or while running it, you can contact margherita.marsili@unibo.it or clelia.righi@unibo.it for reporting. 

When publishing any work performed using TribChem, we kindly ask citing this paper to properly acknowledge the developers contribution:

- G. Losi, O. Chehaimi, and M.C. Righi, "[TribChem: a Software for the First-principles, High-Throughput Study of Solid Interfaces and their Tribological properties](https://pubs.acs.org/doi/10.1021/acs.jctc.3c00459)", Journal of Chemical Theory and Computation 19, 5231 (2023).

## Documentation

To install TribChem, please refer to this [installation guide](docs/installation/Installation-Guide.md). 

The full documentation about the latest release can be found on the [TribChem website](https://triboteam.gitlab.io/tribchem/).


## METADATA
__author__ = "Gabriele Losi, Omar Chehaimi, Maria Clelia Righi"

__copyright__ = "Copyright 2021, M.C. Righi, TribChem, ERC-SLIDE, University of Bologna"

__credits__ = "G. Losi, O. Chehaimi, and M.C. Righi, "[TribChem: a Software for the First-principles, High-Throughput Study of Solid Interfaces and their Tribological properties](https://pubs.acs.org/doi/10.1021/acs.jctc.3c00459)" Journal of Chemical Theory and Computation 19, 5231 (2023)

__license__ = "Creative Commons BY-SA 4.0 (license [link](https://creativecommons.org/licenses/by-sa/4.0/))"

__version__ = "1.0.0"

__maintainer__ = "Margherita Marsili, Tommaso Gorni, Elisa Damiani"

__email__ = "clelia.righi@unibo.it"

__status__ = "Prototype"

__website__ = "[https://tribchem.it/](https://tribchem.it/)"
